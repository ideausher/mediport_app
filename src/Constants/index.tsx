import { Dimensions } from "react-native";
import ScaleSheet from 'react-native-scalesheet';

export const APP_HEADER_TITLE_COLOR = '#FFFFFF';
export const APP_HEADER_BACK_COLOR = '#2F679D';
export const APP_GREY_TEXT_COLOR = '#525050';
export const APP_GREY_BACK = '#FAFAFA';
export const APP_GREEN_COLOR = '#42CAA2';
export const APP_MEDIUM_GREY_COLOR = "#808080";
export const APP_LIGHT_GREY_COLOR = "#d9d9d9";
export const App_MIDIUM_GREY_COLOR = '#9B9B9B';
export const APP_BORDER_COLOR = '#C9D9E4';
export const App_LIGHT_BLUE_COLOR = '#E8F0F5';
export const APP_DARK_BLUE_COLOR = '#3D576F';
export const APP_DARK_GREY_COLOR = '#707070';
export const APP_THEME_LIGHT_COLOR = '#a9d3ef'; 
export const APP_GREY_PLACEHOLDER_COLOR = '#A6A6A6';
export const APP_SHADOW_COLOR = '#cecece';
export const APP_BLUE_COLOR = '#5DAAF6';
export const APP_WELCOME_DARK_GREY = '#3B566E';
export const APP_WELCOME_LIGHT_GREY = '#8199AF';

export const LOGIN_DATA = "loginInfo"

export const commonShadowStyle = {
    shadowColor: APP_SHADOW_COLOR,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    elevation: 3,
}

export const navHdrTxtStyle =  {
     textAlign: 'center',
     color: APP_HEADER_TITLE_COLOR,
     fontWeight: 'bold',
     fontSize: 18,
     width: Dimensions.get('screen').width - 120,
 }

 export const headerTitleStyle =  {
    alignSelf: 'center',
    textAlign: "center",
    justifyContent: 'center',
    flex: 1,
    fontWeight: 'bold',
    textAlignVertical: 'center'
}

export const headerStyle = ScaleSheet.create({
    style: {
        backgroundColor: APP_HEADER_BACK_COLOR,
        height: 110,
        elevation: 0,
        borderBottomWidth: 0,
    },
})