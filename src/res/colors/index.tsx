/**
 * APP THEME COLOR's
 */
export const APP_THEME_COLOR_RED = '#eb5055';
export const WHITE_COLOR = '#fff';
export const BLACK_COLOR = '#000';
export const APP_SHADOW_COLOR = '#cecece';
export const APP_THEME_BLUE = "#4153C5"
export const APP_THEME_GREEN = "green"


/**
 * APP TEXT COLOR's
 */
export const TEXT_COLOR_ORANGE = '#FF9D26';
export const TEXT_COLOR_BLUE = '#504DE5';
export const TEXT_COLOR_GRAY_MEDIUM = '#707070';
export const TEXT_COLOR_GRAY_LIGHT = '#A2A2A2';
export const TEXT_COLOR_GRAY_DARK = '#404142';
export const APP_TEXT_COLOR_WHITE = '#fff';

/**
 * APP BUTTON BACKGROUND COLOR's
 */
export const BUTTON_BACKGROUND_COLOR_SKIN_LIGHT = '#e0e0b1';
export const BUTTON_BACKGROUND_COLOR_SKIN = '#ffb15e';

export const FACEBOOK_BUTTON_BACKGROUND_COLOR = "#4460A0"
/**
 * APP INPUT_FIELD COLOR's
 */
export const INPUTFIELD_PLACEHOLDER_COLOR = "#c7c3c3";


/**
 * APP BORDER COLOR's
 */
export const  APP_BORDER_MEDIUM_GRAY = '#C9C9C9';
export const APP_BORDER_LIGHT_GRAY = '#E5E4E5';



