import {setGlobalLanguageAddCard} from "./addCardDetail"
import { setGlobalLanguageCardList } from "./cardList"
import { setGlobalLanguageCommon } from "./common"
import {setGlobalLanguageDestination} from './destination'
import { setGlobalLanguageEnterPhoneNumber } from "./enter_phone_number"
import { setGlobalLanguageFaq } from "./faq"
import { setGlobalLanguageHistory } from "./history"
import { setGlobalLanguageLocation } from "./location"
import { setGlobalLanguageLogin } from "./login"
import { setGlobalLanguageNotifications } from "./notifications"
import { setGlobalLanguagePayment } from "./payment"
import { setGlobalLanguagePrivacyPloicy } from "./privacy_policy"
import { setGlobalLanguagePromotion } from "./promotion"
import { setGlobalLanguageScanQR } from "./scan_qr"
import { setGlobalLanguageSetting } from "./settings"
import { setGlobalLanguageStopRide } from "./stop_ride"
import { setGlobalLanguageSubscription } from "./subscription"
import { setGlobalLanguageTC } from "./terms_conditions"
import { setGlobalLanguageUnlockVehicle } from "./unlock_vehicle"
import { setGlobalLanguageVerifyPhone } from "./verify_phone"
import { setGlobalLanguageWallet } from "./wallet"
import { setGlobalLanguageDrawer } from "./drawer"

export const setGlobalLanguage = (language) => {
    setGlobalLanguageAddCard(language)
    setGlobalLanguageCardList(language)
    setGlobalLanguageCommon(language)
    setGlobalLanguageDestination(language)
    setGlobalLanguageEnterPhoneNumber(language)
    setGlobalLanguageFaq(language)
    setGlobalLanguageHistory(language)
    setGlobalLanguageLocation(language)
    setGlobalLanguageLogin(language)
    setGlobalLanguageNotifications(language)
    setGlobalLanguagePayment(language)
    setGlobalLanguagePrivacyPloicy(language)
    setGlobalLanguagePromotion(language)
    setGlobalLanguageScanQR(language)
    setGlobalLanguageSetting(language)
    setGlobalLanguageStopRide(language)
    setGlobalLanguageSubscription(language)
    setGlobalLanguageTC(language)
    setGlobalLanguageUnlockVehicle(language)
    setGlobalLanguageVerifyPhone(language)
    setGlobalLanguageWallet(language)
    setGlobalLanguageDrawer(language)
}