import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        NOTIFICATIONS : "Notifications",
        NO_NOTIFICATION: 'NO NOTIFICATIONS YET!',
        FUTURE_NOTIFY_MSG : "You will see notifications related to your orders",

    },
    en: {
        NOTIFICATIONS : "Notifications",
        NO_NOTIFICATION: 'NO NOTIFICATIONS YET!',
        FUTURE_NOTIFY_MSG : "You will see notifications related to your orders",
    },
    pl: {
        NOTIFICATIONS : "Powiadomienia",
        NO_NOTIFICATION: 'NIE JESZCZE POWIADOMIENIA!',
        FUTURE_NOTIFY_MSG : "Zobaczysz powiadomienia związane z Twoimi zamówieniami",
    }
  });

  export const setGlobalLanguageNotifications = (language) => {
    localize.setLanguage(language)
  }