import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        PROMOTIONS : "Promotions",
        VOUCHER : "Voucher",
        USE_CODE : "Use Code",
        WARNING_MESSAGE : "No Promo Code!"
    },
    en: {
        PROMOTIONS : "Promotions",
        VOUCHER : "Voucher",
        USE_CODE : "Use Code",
        WARNING_MESSAGE : "No Promo Code!"
    },
    pl: {
        PROMOTIONS : "Promocje",
        VOUCHER : "Talon",
        USE_CODE : "Użyj kodu",
        WARNING_MESSAGE : "Brak kodu promocyjnego!"
    }
  });

  export const setGlobalLanguagePromotion = (language) => {
    localize.setLanguage(language)
  }