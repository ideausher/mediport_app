import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        FAQ : "FAQ",
        WARNING_MESSAGE :"No Faq found yet!"
    },
    en: {
        FAQ : "FAQ",
        WARNING_MESSAGE :"No Faq found yet!"
         },
    pl: {
        FAQ : "FAQ",
        WARNING_MESSAGE :"Nie znaleziono jeszcze Faq!"
    }
  });

  export const setGlobalLanguageFaq = (language) => {
    localize.setLanguage(language)
  }