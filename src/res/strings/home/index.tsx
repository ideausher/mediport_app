import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        isCallingYou: 'is calling you',
        accept: 'Accept',
        reject: 'Reject',
        home: 'Home',
        internetConnectionError: 'Please check your internet connection',
        searchSpeacility: 'Search for Speciality',
        searchSubject: 'Search Subject',
        selectSpeciality: 'Browse by Subject',
    },
    en: {
        isCallingYou: 'is calling you',
        accept: 'Accept',
        reject: 'Reject',
        home: 'Home',
        internetConnectionError: 'Please check your internet connection',
        searchSpeacility: 'Search for Speciality',
        searchSubject: 'Search Subject',
        selectSpeciality: 'Select Speciality',
    }
  });
  
  export const setGlobalLanguageHistory = (language) => {
    localize.setLanguage(language)
  }