import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        HISTORY:'History', 
        MILES_TRAVELLED: "Miles Travelled: ",
        DURATION:'Duration: ',
        WARNING_MESSAGE:"No order history found"
    },
    en: {
        HISTORY:'History', 
        MILES_TRAVELLED: "Miles Travelled: ",
        DURATION:'Duration: ',
        WARNING_MESSAGE:"No order history found"
    },
    pl: {
        HISTORY:'Historia', 
        MILES_TRAVELLED: "Przejechane mile: ",
        DURATION:'Trwanie: ',
        WARNING_MESSAGE:"Nie znaleziono historii zamówień"
    }
  });
  
  export const setGlobalLanguageHistory = (language) => {
    localize.setLanguage(language)
  }