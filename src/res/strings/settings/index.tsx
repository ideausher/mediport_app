import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        SETTINGS : 'Settings',
        TERMS_CONDITIONS : "Terms & Conditions",
        PRIVACY_POLICY : "Privacy Policy",
        FAQ : "Faq's"
    },
    en: {
        SETTINGS : 'Settings',
        TERMS_CONDITIONS : "Terms & Conditions",
        PRIVACY_POLICY : "Privacy Policy",
        FAQ : "Faq's"
         },
    pl: {
        SETTINGS : 'Ustawienia',
        TERMS_CONDITIONS : "Zasady i Warunki",
        PRIVACY_POLICY : "Polityka prywatności",
        FAQ : "Często zadawane pytania"
    }
  });

  export const setGlobalLanguageSetting = (language) => {
    localize.setLanguage(language)
  }