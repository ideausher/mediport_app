import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        ENTER_DESTINATION_PLACEHOLDER:'Enter your destination', 
        TURN_RIGHT : "Turn Right"
    },
    en: {
        ENTER_DESTINATION_PLACEHOLDER:'Enter your destination',      
        TURN_RIGHT : "Turn Right"  
    },
    pl: {
        ENTER_DESTINATION_PLACEHOLDER:'Wpisz swój cel podróży',     
        TURN_RIGHT : "Skręć w prawo"
    },
  });

  export const setGlobalLanguageDestination = (language) => {
    localize.setLanguage(language)
  }