import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        ACCESS_PERMISSION_MSG:'We want to access your current location', 
        ALLOW: "Allow",
        NOT_NOW:'Not Now',
    },
    en: {
        ACCESS_PERMISSION_MSG:'We want to access your current location', 
        ALLOW: "Allow",
        NOT_NOW:'Not Now',
       
    },
    pl: {
        ACCESS_PERMISSION_MSG:'Chcemy uzyskać dostęp do Twojej bieżącej lokalizacji', 
        ALLOW: "Dopuszczać",
        NOT_NOW:'Nie teraz',
    }
  });

  export const setGlobalLanguageLocation = (language) => {
    localize.setLanguage(language)
  }