import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
       SELECT_CARD_ERROR_MSG : "Please select a card",
       ENTER_AMOUNT_ERROR_MSG : "Please enter amount",
       CHOOSE_CARD : "Choose card",
       ADD_NEW_CARD : "Add new card",
       ENDING_WITH : "ending with",
       EXPIRES : "expires",
       NO_CARD_FOUND : "No card found, please add!",
       PAYMENT : "Payment",
       ENTER_AMOUNT_PLACEHOLDER : "Enter Amount",
       AMOUNT_TITLE : "Amount :"
    },
    en: {
        SELECT_CARD_ERROR_MSG : "Please select a card",
        ENTER_AMOUNT_ERROR_MSG : "Please enter amount",
        CHOOSE_CARD : "Choose card",
        ADD_NEW_CARD : "Add new card",
        ENDING_WITH : "ending with",
        EXPIRES : "expires",
        NO_CARD_FOUND : "No card found, please add!",
        PAYMENT : "Payment",
        ENTER_AMOUNT_PLACEHOLDER : "Enter Amount",
        AMOUNT_TITLE : "Amount :"
    },
    pl: {
        SELECT_CARD_ERROR_MSG : "Wybierz kartę",
        ENTER_AMOUNT_ERROR_MSG : "Proszę podać kwotę",
        CHOOSE_CARD : "Wybierz kartę",
        ADD_NEW_CARD : "Dodaj nową kartę",
        ENDING_WITH : "kończąc na",
        EXPIRES : "wygasa",
        NO_CARD_FOUND : "Nie znaleziono karty, dodaj!",
        PAYMENT : "Zapłata",
        ENTER_AMOUNT_PLACEHOLDER : "Wprowadź ilość",
        AMOUNT_TITLE : "Ilość :"
    },
  });

  export const setGlobalLanguageCardList = (language) => {
    localize.setLanguage(language)
  }