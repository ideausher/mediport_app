import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
/********************************ERROR MSGS********************************************** */
        PLEASE_ENTER_NAME : "Please enter name",
        PLEASE_ENTER_CARD : "Please enter card number",
        PLEASE_ENTER_EXPIRE_YEAR :"Please enter year of card expiration",
        PLEASE_ENTER_EXPIRE_MONTH:"Please enter month of card expiration",
        PLEASE_ENTER_CVC:"Please enter cvc",
        INVALID_MONTH:"Invalid month of expiration",
        INVALID_YEAR : "invalid year of expiration",
        INVALID_CARD:"invalid card number",
/********************************ERROR MSGS********************************************** */
        FILL_CARD_DETAIL:"Fill card detail",
        NAME_PLACEHOLDER:"Name on card",
        CARD_PLACEHOLDER:"Card Number",
        MONTH_PLACEHOLDER:"MM",
        YEAR_PLACEHOLDER:"YY",
        CVC_PLACEHOLDER:"CVC",
        ADD_CARD:'Add Card',
        SUCCESS_MSG: "Card added successfully"
    },

    en: {
  /********************************ERROR MSGS********************************************** */
  PLEASE_ENTER_NAME : "Please enter name",
  PLEASE_ENTER_CARD : "Please enter card number",
  PLEASE_ENTER_EXPIRE_YEAR :"Please enter year of card expiration",
  PLEASE_ENTER_EXPIRE_MONTH:"Please enter month of card expiration",
  PLEASE_ENTER_CVC:"Please enter cvc",
  INVALID_MONTH:"Invalid month of expiration",
  INVALID_YEAR : "invalid year of expiration",
  INVALID_CARD:"invalid card number",
/********************************ERROR MSGS********************************************** */
  FILL_CARD_DETAIL:"Fill card detail",
  NAME_PLACEHOLDER:"Name on card",
  CARD_PLACEHOLDER:"Card Number",
  MONTH_PLACEHOLDER:"MM",
  YEAR_PLACEHOLDER:"YY",
  CVC_PLACEHOLDER:"CVC",
  ADD_CARD:'Add Card',
  SUCCESS_MSG: "Card added successfully"
    },
    
    pl: {
  /********************************ERROR MSGS********************************************** */
  PLEASE_ENTER_NAME : "Proszę podać imię",
  PLEASE_ENTER_CARD : "Proszę podać numer karty",
  PLEASE_ENTER_EXPIRE_YEAR :"Podaj rok wygaśnięcia karty",
  PLEASE_ENTER_EXPIRE_MONTH:"Wprowadź miesiąc wygaśnięcia karty",
  PLEASE_ENTER_CVC:"Proszę wpisać cvc",
  INVALID_MONTH:"Nieprawidłowy miesiąc ważności",
  INVALID_YEAR : "niepoprawny rok ważności",
  INVALID_CARD:"nieprawidłowy numer karty",
  SUCCESS_MSG: "Karta dodana pomyślnie",
/********************************ERROR MSGS END********************************************** */

  FILL_CARD_DETAIL:"Wypełnij szczegóły karty",
  NAME_PLACEHOLDER:"Imię na karcie",
  CARD_PLACEHOLDER:"Numer karty",
  MONTH_PLACEHOLDER:"MM",
  YEAR_PLACEHOLDER:"YY",
  CVC_PLACEHOLDER:"CVC",
  ADD_CARD:'Dodaj kartę'
    },

  });

  export const setGlobalLanguageAddCard = (language) => {
    localize.setLanguage(language)
  }
  