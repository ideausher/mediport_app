import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        PAYMENT:'Payments',
        WALLET:"Wallet"
    },
    en: {
        PAYMENT:'Payments',
        WALLET:"Wallet"
    },
    pl: {
        PAYMENT:'Płatności',
        WALLET:"Portfel"
    }
  });

  export const setGlobalLanguagePayment = (language) => {
    localize.setLanguage(language)
  }