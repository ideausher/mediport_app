import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        SCAN_TO_UNLOCK : "Scan to unlock",
        MESSAGE : "For how much time , you want to hire?",
        CAMERA_PERMISSION_MSG:'Scoot Rent app needs access to your camera.',
        CAMERA_PERMISSION:"Camera Permission",
        ASK_LATER:"Ask Me Later",
        CANCEL:"Cancel",
        OK : "Ok",
        WALLET_AMOUNT_ALERT_MSG:"Your wallet balance is less, kindly add minimum amount of ",
        WALLET_BALANCE_LOW:"Wallet balance low"
    },
    en: {
        SCAN_TO_UNLOCK : "Scan to unlock",
        MESSAGE : "For how much time , you want to hire?",
        CAMERA_PERMISSION_MSG:'Scoot Rent app needs access to your camera.',
        CAMERA_PERMISSION:"Camera Permission",
        ASK_LATER:"Ask Me Later",
        CANCEL:"Cancel",
        OK : "Ok",
        WALLET_AMOUNT_ALERT_MSG:"Your wallet balance is less, kindly add minimum amount of ",
        WALLET_BALANCE_LOW:"Wallet balance low"
         },
    pl: {
        SCAN_TO_UNLOCK : "Skanuj aby odblokować",
        MESSAGE : "Na ile czasu , chcesz zatrudnić?",
        CAMERA_PERMISSION_MSG:'Aplikacja Scoot Rent wymaga dostępu do aparatu.',
        CAMERA_PERMISSION:"Pozwolenie na kamerę",
        ASK_LATER:"Zapytaj mnie później",
        CANCEL:"Anuluj",
        OK : "Ok",
        WALLET_AMOUNT_ALERT_MSG:"Saldo w portfelu jest mniejsze, dodaj minimalną kwotę ",
        WALLET_BALANCE_LOW:"Saldo portfela niskie"
    }
  });

  export const setGlobalLanguageScanQR = (language) => {
    localize.setLanguage(language)
  }