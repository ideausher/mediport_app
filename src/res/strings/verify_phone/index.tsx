import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        INSTRUCTION_MESSAGE : "Enter the OTP sent to your phone number",
        RESEND_CODE : "Resend a new code",
        VERIFY : "Submit",
        internetConnectionError : "Please check your internet connection",
        verify_phone_msg : "Please enter the phone number already registered with Skiedo"

    },
    en: {
        INSTRUCTION_MESSAGE : "Enter the OTP sent to your phone number",
        RESEND_CODE : "Resend a new code",
        VERIFY : "Submit",
        internetConnectionError : "Please check your internet connection",
        verify_phone_msg : "Please enter the phone number already registered with Skiedo"
    },
    pl: {
        INSTRUCTION_MESSAGE : "Wprowadź numer OTP wysłany na Twój numer telefonu",
        RESEND_CODE : "Wyślij ponownie OTP",
        VERIFY : "Kontyntynuj"
    }
  });

  export const setGlobalLanguageVerifyPhone = (language) => {
    localize.setLanguage(language)
  }