import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        PRIVACY_POLICY : "Privacy Policy"
    },
    en: {
        PRIVACY_POLICY : "Privacy Policy"
         },
    pl: {
        PRIVACY_POLICY : "Polityka prywatności"
    }
  });

  export const setGlobalLanguagePrivacyPloicy = (language) => {
    localize.setLanguage(language)
  }