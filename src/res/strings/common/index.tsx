import LocalizedStrings from 'react-native-localization';
export const DEFAULT_LANGUAGE = 'en';

const translations = new LocalizedStrings({
    en: {
        APP_NAME: 'Scoot Rent',
        SCAN_QR: "Scan QR",
        RESPORT_MSG:"If you have any issue regarding the vehicle.Please ",
        REPORT_HERE:"Report here",
        DISTANCE:"Distance",
        DURATION:"Duration",
        STOP:"Stop",
        STOP_RIDE:"Stop Ride",
        CANCEL:"Cancel",
        RIDE_STARTED:"Ride Started",
        RIDE_START_MSG:"Your ride has been started.You can add your destination location for navigation",
        OKAY:"Okay",
        MAKE_YOUR_PAYMENT:"Make your payment",
        APPLY_VOUCHER_CODE:"Apply Voucher Code",
        DISCOUNT:"Discount",
        TOTAL_AMOUNT:"Total amount",
        MAKE_PAYMENT:"Make Payment",
        SEND_FEEDBACK:"Send us your feedback !",
        HOW_SATISFY: "How satisfy are you with us?",
        FEEDBACK_PLACEHOLDER: "Tell us your feedback(optional)",
        SUBMIT:"Submit",
        ENTER_CODE_PLACEHOLDER:"Enter your code here",
        INVALID : "Invalid",
        USE_CODE : "Use Code",
        CURRENCY : "$"
    },

    pl: {
        APP_NAME: 'Scoot Rent',
        SCAN_QR: "Zeskanuj QR",
        RESPORT_MSG:"Jeśli masz jakiekolwiek problemy dotyczące pojazdu, proszę ",
        REPORT_HERE:"Zgłoś tutaj",
        DISTANCE:"Dystans",
        DURATION:"Dystans",
        STOP:"Zatrzymać",
        STOP_RIDE:"Zatrzymaj jazdę",
        CANCEL:"Anuluj",
        RIDE_STARTED:"Rozpoczęto jazdę",
        RIDE_START_MSG:"Twoja jazda została rozpoczęta. Możesz dodać lokalizację docelową do nawigacji",
        OKAY:"w porządku",
        MAKE_YOUR_PAYMENT:"Dokonaj płatności",
        APPLY_VOUCHER_CODE:"Zastosuj kod kuponu",
        DISCOUNT:"Zniżka",
        TOTAL_AMOUNT:"Łączna kwota",
        MAKE_PAYMENT:"Dokonać płatności",
        SEND_FEEDBACK:"Wyślij nam swoją opinię !",
        HOW_SATISFY: "Jak jesteś zadowolony z nas?",
        FEEDBACK_PLACEHOLDER: "Przekaż nam swoją opinię (opcjonalnie)",
        SUBMIT:"Zatwierdź",
        ENTER_CODE_PLACEHOLDER:"Wpisz swój kod tutaj",
        INVALID : "Nieważny",
        USE_CODE : "Użyj kodu",
        CURRENCY : "zł"
    },

    de: {
        APP_NAME: 'Buddy Finder',
    }
  });
  
  export const setGlobalLanguageCommon = (language) => {
    translations.setLanguage(language)
  }

  export default translations
