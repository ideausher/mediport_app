import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        WEEK:"/ Week",
        MONTH:"/ Month",
        YEAR:"/ Year",
        VERIFY : "Select",
        CHOOSE_PLAN : "Choose Plan",
        WALLET_BALANCE_LOW:"Wallet balance low",
        SUBSCRIPTION_ALREADY:"Subscription already purchased",
        NEED_ADD_MORE_MONEY:"Need to Add minimum amount!",
        SUBSCRIPTIONS:"Subscription",
        MSG:"as kjhasdf aksj ajsd fasdf aaa a d adasd asdhad as da dajsdhk asdd ad akd skashs dfsa fkj asdf asd fjkasd fhs df asf"
    },
    en: {
        WEEK:"/ Week",
        MONTH:"/ Month",
        YEAR:"/ Year",
        VERIFY : "Select",
        CHOOSE_PLAN : "Choose Plan",
        WALLET_BALANCE_LOW:"Wallet balance low",
        SUBSCRIPTION_ALREADY:"Subscription already purchased",
        NEED_ADD_MORE_MONEY:"Need to Add minimum amount!",
        SUBSCRIPTIONS:"Subscription",
        MSG:"as kjhasdf aksj ajsd fasdf aaa a d adasd asdhad as da dajsdhk asdd ad akd skashs dfsa fkj asdf asd fjkasd fhs df asf"
       
    },
    pl: {
        WEEK:"/ Tydzień",
        MONTH:"/ Miesiąc",
        YEAR:"/ Rok",
        VERIFY : "Wybierz",
        CHOOSE_PLAN : "Wybierz Plan",
        WALLET_BALANCE_LOW:"Saldo portfela niskie",
        SUBSCRIPTION_ALREADY:"Subskrypcja już zakupiona",
        NEED_ADD_MORE_MONEY:"Musisz dodać minimalną kwotę!",
        SUBSCRIPTIONS:"Subskrypcja",
        MSG:"as kjhasdf aksj ajsd fasdf aaa a d adasd asdhad as da dajsdhk asdd ad akd skashs dfsa fkj asdf asd fjkasd fhs df asf"
    },
  });

  export const setGlobalLanguageSubscription = (language) => {
    localize.setLanguage(language)
  }