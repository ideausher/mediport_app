import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        MY_SESSION:'My Session', 
        T_C : "Terms and Conditions",
        FAQ:"FAQ",
        PROMOTIONS:"Promotions",
        SETTINGS : "Settings",
        LOGOUT:"Sign Out",
        PAYMENT: "Payment",
        BECOME_TUTOR:"Become a tutor",
        LOGIN: "Login"
    },
    en: {
      MY_SESSION:'My Session', 
      T_C : "Terms and Conditions",
      FAQ:"FAQ",
      PROMOTIONS:"Promotions",
      SETTINGS : "Settings",
      LOGOUT:"Sign Out",
      PAYMENT: "Payment",
      BECOME_TUTOR:"Become a Tutor",
      LOGIN: "Login"
    },

  });

  export const setGlobalLanguageDrawer = (language) => {
    localize.setLanguage(language)
  }