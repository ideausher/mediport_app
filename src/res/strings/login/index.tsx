import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        PHONE:'Enter your phone number', 
        PHONE_PLACEHOLDER: "Phone",
        REFERAL_CODE_PLACEHOLDER:'Referral Code [Optional]',
        TERMS_CONDITION_MSG:"By creating an account with Scoot Rent. you are accepting our",
        TERMS_CONDITIONS:' Terms and conditions',
        OR_SIGN_IN:"or Sign in with",
        CONTINUE:'Continue'

    },
    en: {
        PHONE:'Enter your phone number', 
        PHONE_PLACEHOLDER: "Phone",
        REFERAL_CODE_PLACEHOLDER:'Referral Code [Optional]',
        TERMS_CONDITION_MSG:"By creating an account with Scoot Rent. you are accepting our",
        TERMS_CONDITIONS:' Terms and conditions',
        OR_SIGN_IN:"or Sign in with",
        CONTINUE:'Continue'
       
    },
    pl: {
        PHONE:'Wprowadź swój numer telefonu', 
        PHONE_PLACEHOLDER: "Telefon",
        REFERAL_CODE_PLACEHOLDER:'Kod polecający [Opcjonalnie]',
        TERMS_CONDITION_MSG:"Tworząc konto w Scoot Rent. akceptujesz nasz",
        TERMS_CONDITIONS:' Zasady i warunki',
        OR_SIGN_IN:"Lub Zaloguj się za pomocą",
        CONTINUE:'Kontyntynuj'
    }
  });

  export const setGlobalLanguageLogin = (language) => {
    localize.setLanguage(language)
  }