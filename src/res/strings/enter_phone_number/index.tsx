import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        APP_NAME: 'Scoot Rent',
        TITLE : "Enter your phone number that you are registered while sign up !",
        MESSAGE :"We just mailed you with the instructions to reset password",
        PLACE_HOLDER_TEXT: "Enter your phone number"
    },
    en: {
        APP_NAME: 'Scoot Rent',
        TITLE : "Enter your phone number that you are registered while sign up !",
        MESSAGE :"We just mailed you with the instructions to reset password",
        PLACE_HOLDER_TEXT: "Enter your phone number",
        ENTER_PHONE_NUMBER : "Enter Phone Number"       ,
        SEND_OTP : "Send OTP"
    },
    pl: {
        APP_NAME: 'Scoot Rent',
        TITLE : "Podaj swój numer telefonu, który jesteś zarejestrowany podczas rejestracji !",
        MESSAGE :"Właśnie wysłaliśmy Ci instrukcje resetowania hasła",
        PLACE_HOLDER_TEXT: "Wprowadź swój numer telefonu"

    },
  });

  export const setGlobalLanguageEnterPhoneNumber = (language) => {
    localize.setLanguage(language)
  }