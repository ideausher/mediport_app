import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        INSTRUCTION_MESSAGE : "Enter the pincode to unlock your vehicle",
        VERIFY : "Continue"

    },
    en: {
        INSTRUCTION_MESSAGE : "Enter the pincode to unlock your vehicle",
        VERIFY : "Continue"
       
    },
    pl: {
        INSTRUCTION_MESSAGE : "Wprowadź kod PIN, aby odblokować pojazd",
        VERIFY : "Kontyntynuj"
    }
  });

  export const setGlobalLanguageUnlockVehicle = (language) => {
    localize.setLanguage(language)
  }