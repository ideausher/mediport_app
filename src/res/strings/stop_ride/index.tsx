import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        ENTER_DESTINATION_PLACEHOLDER:'Enter your destination', 
WARNING:"Warning",
MSG_1: "your package has left ",
MSG_2: "days to expire. For stop ride, Press OK",
MSG_3: "Are you sure to stop ride,"
    },
    en: {
        ENTER_DESTINATION_PLACEHOLDER:'Enter your destination', 
        WARNING:"Warning",
        MSG_1: "your package has left ",
        MSG_2: "days to expire. For stop ride, Press OK",
        MSG_3: "Are you sure to stop ride,"        
    },
    pl: {
        ENTER_DESTINATION_PLACEHOLDER:'Wpisz swój cel podróży', 
        WARNING:"Ostrzeżenie",
        MSG_1: "Twoja paczka opuściła ",
        MSG_2: "dni wygasają. Aby zatrzymać jazdę, naciśnij OK",
        MSG_3: "Czy na pewno zatrzymasz jazdę,"     
    }
  });

  export const setGlobalLanguageStopRide = (language) => {
    localize.setLanguage(language)
  }