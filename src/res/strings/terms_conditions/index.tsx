import LocalizedStrings from 'react-native-localization';


  export let localize = new LocalizedStrings({
    "en-US": {
        T_C : "T & C"
    },
    en: {
        T_C : "T & C"
         },
    pl: {
        T_C : "O W H"
    }
  });

  export const setGlobalLanguageTC = (language) => {
    localize.setLanguage(language)
  }