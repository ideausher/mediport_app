import ImagePicker from 'react-native-image-picker';

const options = {
  // title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class CustomImagePicker {

  static showImgPicker() {

    return new Promise(function (resolve, reject) {
      ImagePicker.showImagePicker({}, (response: any) => {
        console.log('Selcted Image Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled image picker');
          reject()

        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
          reject()
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          reject()
        } else {
          console.log('Rsponse URI: ', response.uri);
          resolve(response)
        }
      });
    });
  }

}

export default CustomImagePicker