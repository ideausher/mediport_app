import LocalizedStrings from 'react-native-localization';

export let localize = new LocalizedStrings({
  "en-US": {
    //Common Error's
    internetConnectionError: 'Please check your internet connection',

    //Welcome
    skip: 'Skip',
    welcome: 'Welcome',
    mediportNeedsAccessToLocation: 'Mediport App needs access to your location',
    askMeLater: 'Ask Me Later',

    //Sign IN
    helloSign: 'Hello Sign ',
    withUs: 'In with us',
    email: 'E-mail',
    password: 'Password',
    forgotPassword: 'Forgot Password',
    signIn: "Sign in",
    or: 'OR',
    dontHaveAccount: 'Don\'t have an account?',
    signUp: "Sign Up",

    //Sign Up
    name: "Name",
    cnfrmPassword: "Confirm Password",
    phoneNumber: "Phone Number",
    signup: "Sign up",

    // Verification
    verification: 'Verification',
    verifyOTP: 'OTP Verification',
    verifyMsg: 'Enter the OTP Sent to',
    haveNotReceivedCode: 'I haven\'t received the OTP?',
    resendOTP: 'Resend OTP',
    confirm: 'Confirm',

    // Forgot Password
    submit: 'Submit',
    enterNewPhoneNumber: 'Enter New Phone Number',

    //Home
    home: 'Home',
    searchSpeacility: 'Search for Speciality',
    searchDoctor: 'Search for Doctor, Speciality',
    selectSpeciality: 'Select Speciality',

    //Doctor List
    doctors: 'Doctors',
    viewProfile: 'View Profile',
    myDoctors: 'My Doctors',
    message: 'Message',
    min: 'Min',
    max: 'Max',

    // Doctor Profile
    doctorProfile: 'Doctor Profile',
    ratings: 'Ratings',
    appointments: 'Appointments',
    suggested: 'Suggested',
    office: 'Office',
    education: 'Education',
    language: 'Language',
    patientReviews: 'Patient Reviews',
    bookAppointment: 'Book Appointment',

    // Book Appointment
    bookAnAppointment: 'Book an Appointment',
    appointmentFor: 'Appointment For',
    agePlaceholder: 'Age: eg 27',
    booldGroupPlaceholder: 'Boold Group: A+',
    patientName: 'Patient Name',
    contactNumber: 'Contact Number',
    typeHealthIssue: 'Please type your health issue in detail.',
    uploadReports: 'Upload Reports/Medical Record',
    add: 'Add',
    next: 'Next',
    selectAppointmentDate: 'Select Appointment Date',
    selectAppointmentTime: 'Select Appointment Time',
    date: 'Date',
    to: 'To',
    referenceId: 'Reference ID',
    fees: 'Fees',
    continue: 'Continue',
    bookingSuccessful: 'Booking Successful',
    done: 'Done',
    time: 'Time',
    confirmMsg: 'Please confirm your booking details',

    //Payment
    paymentMethod: 'Payment Method',
    addPaymentMethod: 'Add Payment Method',

    // Add Card
    addCard: 'Add Card',
    selectyourCard: 'Select your Card',
    crditCard: 'Credit Card',
    debitCard: 'Debit Card',
    enterAmount: 'Enter Amount',
    cardDetails: 'Card Details',
    cardHolderName: 'Card Holder Name',
    cardNumber: 'Card Number',
    expirationDate: 'Expiration Date',
    cvv: 'CVV/CVC',
    saveCardForFuture: 'Save Card for future',
    amount: 'Amount',

    // Saved Cards
    savedCards: 'Saved Cards',
    startWithCardNumber: 'let\'s start with your card number',
    applyDicountCode: 'Apply Discount Code',

    //Discount
    discount: 'Discount',
    availableDiscounts: 'Available Discounts',
    mediport: 'MEDIPORT',
    copyCode: 'COPY CODE',

    // Appointments
    upcoming: 'Upcoming',
    history: 'History',
    dayLeft: 'Day Left',
    viewReports: 'View Reports',
    patientsName: 'Patient\'s Name',
    cancelAppointment: 'Cancel Appointment',
    selectReasonCancelAppointment: 'Select your reason for cancelling the appointment',
    enterReasonHere: 'Enter your reason here',

    // Reports
    reports: 'Reports',
    clickHereToDownload: 'Click Here To Download',

    //Chats
    chats: 'Chats',
    ago: 'Ago',
    writeAMsg: 'Write a message',
    youRequestVideoCall: 'You request a video call',
    isCallingYou: 'is calling you',
    accept: 'Accept',
    reject: 'Reject',

    //Feedback
    feedback: 'Feedback',
    howWasThe: 'How was the',
    behaviour: 'behaviour',
    pleaseGiveFeedback: 'Please give us a feedback',
    submitRating: 'Submit Rating',
    suggestDoctor: 'Suggest this Doctor?',

    // Reveal
    myProfile: 'My Profile',
    myReports: 'My Reports',
    chatSupport: 'Chat Support',
    mySubscription: 'My Subscription',
    settings: 'Settings',
    termsConditions: 'Terms and Conditions',
    faq: 'FAQ',
    signOut: 'Sign Out',

    //Profile
    profile: 'Profile',
    edit: 'Edit',
    Email: 'E-mail',
    gender: 'Gender',
    phone: 'Phone',
    bloodGroup: 'Blood Group',

    //Settings
    notifications: 'Notifications',
    changePswd: 'Change Password',
    oldPassword: 'Old Password',
    changePassword: 'Change Password',
    updatePassword: 'Update Password',
    retypePassword: 'Retype Password',
    save: 'Save',
    successChangePswdMsg: 'You have successfully Changed your Password',
    ok: 'OK',

    // Sign Out
    wantToSignOut: 'Are you sure you want to Sign out?',
    cancel: 'Cancel',

    // View Reports 
    age: 'Age',
    DoctorsName: 'Doctor\'s name',
    disease: 'Disease',
    doctorFeedback: 'Doctor Feedback',

    // Messages
    pleaseEnterName: 'Please enter name',
    pleaseEnterEmail: 'Please enter email',
    pleaseEnterValidEmail: 'Please enter valid email',
    pleaseEnterPswd: 'Please enter Password',
    pleaseEnterCardNum: 'Please enter card number',
    pleaseEnterExpMonth: 'Please enter expiry month',
    pleaseEnterExpYear: 'Please enter expiry year',
    pleaseEnterCVV: 'Please enter cvv/cvc',
    pleaseSelectReasonOFCancel: 'Please select your reason for cancelling the appointment',
    pleaseEnterAge: 'Please enter age',
    pleaseEnterBloodGroup: 'Please enter blood group',
    pleaseEnterPatientsName: 'Please enter patient\'s name',
    pleaseEnterContactNum: 'Please enter contact number',
    pleaseEnterValidContactNum: 'Please enter valid contact number',
    slotsNotAvailable: 'Slots are not available for selected date',
    pleaseSelectAppointmentTime: 'Please select Appointment time',
    youSuccessfullyBookedAppointment: 'You have successfully Booked your Appointment',
    pleaseAllowAppToAccessLocation: 'Please allow the application to access your location',
    feedbackMessageCantBeEmpty: 'Feedback message cannot be empty',
    pleaseEnterCountryCode: 'Please enter country code',
    pleaseEnterPhoneNum: 'Please enter phone number',
    pleaseEnterValidPhoneNum: 'Please enter valid phone number',
    newNumIsSameAsPrevious: 'New number is same as previous one',
    pleaseEnterValidEmailPhoneNum: 'Please enter valid E-Mail/Phone Number',
    pleaseEnterPswdOf6Digits: 'Please enter Password of atleast 6 digits',
    pleaseEnterCnfmPswd: 'Please enter confirm Password',
    confirmPswdDoesntMatched: 'Confirm password doesn\'t matched',
    pleaseEnterOldPswd: 'Please enter Old Password',

    // AppointmentFor options
    mySelf: 'Myself',
    wife: 'Wife',
    son: 'Son',
    daughter: 'Daughter',

    noThanks: 'No thanks',
  },
  en: {
    //Common Error's
    internetConnectionError: 'Please check your internet connection',

    //Welcome
    skip: 'Skip',
    welcome: 'Welcome',
    mediportNeedsAccessToLocation: 'Mediport App needs access to your location',
    askMeLater: 'Ask Me Later',

    //Sign IN
    helloSign: 'Hello Sign ',
    withUs: 'In with us',
    email: 'E-mail',
    password: 'Password',
    forgotPassword: 'Forgot Password',
    signIn: "Sign in",
    or: 'OR',
    dontHaveAccount: 'Don\'t have an account?',
    signUp: "Sign Up",

    //Sign Up
    name: "Name",
    cnfrmPassword: "Confirm Password",
    phoneNumber: "Phone Number",
    signup: "Sign up",

    // Verification
    verification: 'Verification',
    verifyOTP: 'OTP Verification',
    verifyMsg: 'Enter the OTP Sent to',
    haveNotReceivedCode: 'I haven\'t received the OTP?',
    resendOTP: 'Resend OTP',
    confirm: 'Confirm',

    // Forgot Password
    submit: 'Submit',
    enterNewPhoneNumber: 'Enter New Phone Number',

    //Home
    home: 'Home',
    searchSpeacility: 'Search for Speciality',
    searchDoctor: 'Search for Doctor, Speciality',
    selectSpeciality: 'Select Speciality',

    //Doctor List
    doctors: 'Doctors',
    viewProfile: 'View Profile',
    myDoctors: 'My Doctors',
    message: 'Message',
    min: 'Min',
    max: 'Max',

    // Doctor Profile
    doctorProfile: 'Doctor Profile',
    ratings: 'Ratings',
    appointments: 'Appointments',
    suggested: 'Suggested',
    office: 'Office',
    education: 'Education',
    language: 'Language',
    patientReviews: 'Patient Reviews',
    bookAppointment: 'Book Appointment',

    // Book Appointment
    bookAnAppointment: 'Book an Appointment',
    appointmentFor: 'Appointment For',
    agePlaceholder: 'Age: eg 27',
    booldGroupPlaceholder: 'Boold Group: A+',
    patientName: 'Patient Name',
    contactNumber: 'Contact Number',
    typeHealthIssue: 'Please type your health issue in detail.',
    uploadReports: 'Upload Reports/Medical Record',
    add: 'Add',
    next: 'Next',
    selectAppointmentDate: 'Select Appointment Date',
    selectAppointmentTime: 'Select Appointment Time',
    date: 'Date',
    to: 'To',
    referenceId: 'Reference ID',
    fees: 'Fees',
    continue: 'Continue',
    bookingSuccessful: 'Booking Successful',
    done: 'Done',
    time: 'Time',
    confirmMsg: 'Please confirm your booking details',

    //Payment
    paymentMethod: 'Payment Method',
    addPaymentMethod: 'Add Payment Method',

    // Add Card
    addCard: 'Add Card',
    selectyourCard: 'Select your Card',
    crditCard: 'Credit Card',
    debitCard: 'Debit Card',
    enterAmount: 'Enter Amount',
    cardDetails: 'Card Details',
    cardHolderName: 'Card Holder Name',
    cardNumber: 'Card Number',
    expirationDate: 'Expiration Date',
    cvv: 'CVV/CVC',
    saveCardForFuture: 'Save Card for future',
    amount: 'Amount',

    // Saved Cards
    savedCards: 'Saved Cards',
    startWithCardNumber: 'let\'s start with your card number',
    applyDicountCode: 'Apply Discount Code',

    //Discount
    discount: 'Discount',
    availableDiscounts: 'Available Discounts',
    mediport: 'MEDIPORT',
    copyCode: 'COPY CODE',

    // Appointments
    upcoming: 'Upcoming',
    history: 'History',
    dayLeft: 'Day Left',
    viewReports: 'View Reports',
    patientsName: 'Patient\'s Name',
    cancelAppointment: 'Cancel Appointment',
    selectReasonCancelAppointment: 'Select your reason for cancelling the appointment',
    enterReasonHere: 'Enter your reason here',

    // Reports
    reports: 'Reports',
    clickHereToDownload: 'Click Here To Download',

    //Chats
    chats: 'Chats',
    ago: 'Ago',
    writeAMsg: 'Write a message',
    youRequestVideoCall: 'You request a video call',
    isCallingYou: 'is calling you',
    accept: 'Accept',
    reject: 'Reject',

    //Feedback
    feedback: 'Feedback',
    howWasThe: 'How was the',
    behaviour: 'behaviour',
    pleaseGiveFeedback: 'Please give us a feedback',
    submitRating: 'Submit Rating',
    suggestDoctor: 'Suggest this Doctor?',

    // Reveal
    myProfile: 'My Profile',
    myReports: 'My Reports',
    chatSupport: 'Chat Support',
    mySubscription: 'My Subscription',
    settings: 'Settings',
    termsConditions: 'Terms and Conditions',
    faq: 'FAQ',
    signOut: 'Sign Out',

    //Profile
    profile: 'Profile',
    edit: 'Edit',
    Email: 'E-mail',
    gender: 'Gender',
    phone: 'Phone',
    bloodGroup: 'Blood Group',

    //Settings
    notifications: 'Notifications',
    changePswd: 'Change Password',
    oldPassword: 'Old Password',
    changePassword: 'Change Password',
    updatePassword: 'Update Password',
    retypePassword: 'Retype Password',
    save: 'Save',
    successChangePswdMsg: 'You have successfully Changed your Password',
    ok: 'OK',

    // Sign Out
    wantToSignOut: 'Are you sure you want to Sign out?',
    cancel: 'Cancel',

    // View Reports 
    age: 'Age',
    DoctorsName: 'Doctor\'s name',
    disease: 'Disease',
    doctorFeedback: 'Doctor Feedback',

    // Messages
    pleaseEnterName: 'Please enter name',
    pleaseEnterEmail: 'Please enter email',
    pleaseEnterValidEmail: 'Please enter valid email',
    pleaseEnterPswd: 'Please enter Password',
    pleaseEnterCardNum: 'Please enter card number',
    pleaseEnterExpMonth: 'Please enter expiry month',
    pleaseEnterExpYear: 'Please enter expiry year',
    pleaseEnterCVV: 'Please enter cvv/cvc',
    pleaseSelectReasonOFCancel: 'Please select your reason for cancelling the appointment',
    pleaseEnterAge: 'Please enter age',
    pleaseEnterBloodGroup: 'Please enter blood group',
    pleaseEnterPatientsName: 'Please enter patient\'s name',
    pleaseEnterContactNum: 'Please enter contact number',
    pleaseEnterValidContactNum: 'Please enter valid contact number',
    slotsNotAvailable: 'Slots are not available for selected date',
    pleaseSelectAppointmentTime: 'Please select Appointment time',
    youSuccessfullyBookedAppointment: 'You have successfully Booked your Appointment',
    pleaseAllowAppToAccessLocation: 'Please allow the application to access your location',
    feedbackMessageCantBeEmpty: 'Feedback message cannot be empty',
    pleaseEnterCountryCode: 'Please enter country code',
    pleaseEnterPhoneNum: 'Please enter phone number',
    pleaseEnterValidPhoneNum: 'Please enter valid phone number',
    newNumIsSameAsPrevious: 'New number is same as previous one',
    pleaseEnterValidEmailPhoneNum: 'Please enter valid E-Mail/Phone Number',
    pleaseEnterPswdOf6Digits: 'Please enter Password of atleast 6 digits',
    pleaseEnterCnfmPswd: 'Please enter confirm Password',
    confirmPswdDoesntMatched: 'Confirm password doesn\'t matched',
    pleaseEnterOldPswd: 'Please enter Old Password',

    // AppointmentFor options
    mySelf: 'Myself',
    wife: 'Wife',
    son: 'Son',
    daughter: 'Daughter',

    noThanks: 'No thanks',
  },
  ar: {
    //Common Error's
    internetConnectionError: 'المرجو التحقق من اتصالك بالإنترنت',

    //Welcome
    skip: 'تخطي',
    welcome: 'أهلاً وسهلا',
    mediportNeedsAccessToLocation: 'يحتاج تطبيق Mediport إلى الولوج لموقعك الحالي',
    askMeLater: 'اسألني لاحقًا',

    //Sign IN
    helloSign: 'إشارة الترحيب',
    withUs: 'معنا',
    email: 'البريد الإلكتروني',
    password: 'كلمة المرور',
    forgotPassword: 'نسيت كلمة المرور',
    signIn: "تسجيل الدخول",
    or: 'أو',
    dontHaveAccount: 'لا تتوفر على حساب؟',
    signUp: "اشتراك",

    //Sign Up
    name: "الاسم",
    cnfrmPassword: "تأكيد كلمة المرور",
    phoneNumber: "رقم الهاتف",
    signup: "اشتراك",

    // Verification
    verification: 'التحقق',
    verifyOTP: 'التحقق من كلمة مرور لمرة واحدة',
    verifyMsg: 'أدخل كلمة المرور التي تم إرسالها إلى',
    haveNotReceivedCode: 'أدخل كلمة المرور التي تم إرسالها إلى',
    resendOTP: 'أعد إرسال كلمة مرور للمرة الواحدة',
    confirm: 'تأكيد',

    // Forgot Password
    submit: 'إرسال',
    enterNewPhoneNumber: 'أدخل رقم هاتف جديد',

    //Home
    home: 'الصفحة الرئيسية',
    searchSpeacility: 'البحث عن التخصص',
    searchDoctor: 'البحث عن طبيب، التخصص',
    selectSpeciality: 'اختر التخصص',

    //Doctor List
    doctors: 'الأطباء',
    viewProfile: 'عرض الملف الشخصي',
    myDoctors: 'أطبائي',
    message: 'الرسالة',
    min: 'الحد الأدنى',
    max: 'الحد الأقصى',

    // Doctor Profile
    doctorProfile: 'الملف الشخصي الخاص بالطبيب',
    ratings: 'التصنيفات',
    appointments: 'المواعيد',
    suggested: 'مقترح',
    office: 'المكتب',
    education: 'التعليم',
    language: 'اللغة',
    patientReviews: 'ملاحظات المرضى',
    bookAppointment: 'حجز الموعد',

    // Book Appointment
    bookAnAppointment: 'احجز موعدًا',
    appointmentFor: 'موعد لـ',
    agePlaceholder: 'العمر: eg 27',
    booldGroupPlaceholder: 'فصيلة الدم: A+',
    patientName: 'اسم المريض',
    contactNumber: 'رقم الاتصال',
    typeHealthIssue: 'المرجو الحديث عن مشكلك الصحي بالتفصيل.',
    uploadReports: 'تحميل التقارير/السجل الطبي',
    add: 'أضف',
    next: 'التالي',
    selectAppointmentDate: 'حدد تاريخ الموعد',
    selectAppointmentTime: 'حدد وقت الموعد',
    date: 'التاريخ',
    to: 'إلى',
    referenceId: 'الرقم المرجعي',
    fees: 'الرسوم',
    continue: 'تابع',
    bookingSuccessful: 'تم الحجز بنجاح',
    done: 'تم الأمر بنجاح',
    time: 'الوقت',
    confirmMsg: 'المرجو تأكيد تفاصيل حجزك',

    //Payment
    paymentMethod: 'طريقة الدفع',
    addPaymentMethod: 'إضافة طريقة الدفع',

    // Add Card
    addCard: 'إضافة البطاقة',
    selectyourCard: 'حدد البطاقة الخاصة بك',
    crditCard: 'بطاقة الائتمان',
    debitCard: 'بطاقة الائتمان',
    enterAmount: 'أدخل المبلغ',
    cardDetails: 'تفاصيل البطاقة',
    cardHolderName: 'اسم حامل البطاقة',
    cardNumber: 'رقم البطاقة',
    expirationDate: 'تاريخ انتهاء الصلاحية',
    cvv: '(رمز التحقق) CVV/CV',
    saveCardForFuture: 'احتفظ بالبطاقة للمستقبل',
    amount: 'المبلغ',

    // Saved Cards
    savedCards: 'البطاقات المحفوظة',
    startWithCardNumber: 'لنبدأ برقم بطاقتك',
    applyDicountCode: 'تطبيق رمز الخصم',

    //Discount
    discount: 'الخصم',
    availableDiscounts: 'الخصومات المتوفرة',
    mediport: 'MEDIPORT',
    copyCode: 'نسخ الرمز ',

    // Appointments
    upcoming: 'القادمة',
    history: 'التاريخ',
    dayLeft: 'اليوم المتبقي',
    viewReports: 'عرض التقارير',
    patientsName: 'اسم المريض',
    cancelAppointment: 'إلغاء الموعد',
    selectReasonCancelAppointment: 'حدد سبب إلغاء الموعد',
    enterReasonHere: 'أدخل السبب هنا',

    // Reports
    reports: 'التقارير',
    clickHereToDownload: 'انقر هنا للتنزيل',

    //Chats
    chats: 'الدردشات',
    ago: 'منذ',
    writeAMsg: 'اكتب رسالة',
    youRequestVideoCall: 'أنت تطلب مكالمة فيديو',
    isCallingYou: 'يتصل بك',
    accept: 'قبول',
    reject: 'رفض',

    //Feedback
    feedback: 'التقييم',
    howWasThe: 'كيف كان',
    behaviour: 'السلوك',
    pleaseGiveFeedback: 'المرجو تقديم تقييما لنا',
    submitRating: 'إرسال التصنيف',
    suggestDoctor: 'هل تقترح هذا الطبيب؟',

    // Reveal
    myProfile: 'ملف التعريف الخاص بي',
    myReports: 'تقاريري',
    chatSupport: 'الدعم الخاص بالدردشة',
    mySubscription: 'اشتراكاتي',
    settings: 'الإعدادات',
    termsConditions: 'البنود والشروط',
    faq: 'الأسئلة المتداولة',
    signOut: 'تسجيل الخروج',

    //Profile
    profile: 'الملف الشخصي',
    edit: 'تعديل',
    Email: 'البريد الإلكتروني',
    gender: 'الجنس',
    phone: 'الهاتف',
    bloodGroup: 'فصيلة الدم',

    //Settings
    notifications: 'الإشعارات',
    changePswd: 'تغيير كلمة المرور',
    oldPassword: 'كلمة المرور القديمة',
    changePassword: 'تغيير كلمة المرور',
    updatePassword: 'تحديث كلمة المرور',
    retypePassword: 'أعد كتابة كلمة المرور',
    save: 'حفظ',
    successChangePswdMsg: 'لقد قمت بتغيير كلمة المرور بنجاح',
    ok: 'حسنا',

    // Sign Out
    wantToSignOut: 'هل تريد بالتأكيد تسجيل الخروج؟',
    cancel: 'إلغاء',

    // View Reports 
    age: 'العمر',
    DoctorsName: 'اسم الطبيب',
    disease: 'المرض',
    doctorFeedback: 'ملاحظات الطبيب',

    // Messages
    pleaseEnterName: 'المرجو إدخال الاسم',
    pleaseEnterEmail: 'المرجو إدخال البريد الإلكتروني',
    pleaseEnterValidEmail: 'المرجو إدخال بريد إلكتروني صالح',
    pleaseEnterPswd: 'المرجو إدخال كلمة المرور',
    pleaseEnterCardNum: 'من فضلك أدخل رقم البطاقة',
    pleaseEnterExpMonth: 'المرجو إدخال شهر انتهاء الصلاحية',
    pleaseEnterExpYear: 'المرجو إدخال سنة انتهاء الصلاحية',
    pleaseEnterCVV: 'المرجو إدخال (رمز التحقق) CVV/CV',
    pleaseSelectReasonOFCancel: 'المرجو تحديد سبب إلغاء الموعد',
    pleaseEnterAge: 'المرجو إدخال العمر',
    pleaseEnterBloodGroup: 'المرجو إدخال فصيلة الدم',
    pleaseEnterPatientsName: 'المرجو إدخال اسم المريض',
    pleaseEnterContactNum: 'المرجو إدخال رقم الهاتف',
    pleaseEnterValidContactNum: 'المرجو إدخال رقم هاتف صالح',
    slotsNotAvailable: 'لا تتوفر المقاعد للتاريخ المحدد',
    pleaseSelectAppointmentTime: 'من فضلك حدد وقت الموعد',
    youSuccessfullyBookedAppointment: 'لقد قمت بحجز موعدك بنجاح',
    pleaseAllowAppToAccessLocation: 'المرجو السماح للتطبيق بالولوج إلى موقعك',
    feedbackMessageCantBeEmpty: 'رسالة التقييم لا يمكن أن تكون فارغة',
    pleaseEnterCountryCode: 'المرجو إدخال رمز البلد',
    pleaseEnterPhoneNum: 'المرجو إدخال رقم الاتصال',
    pleaseEnterValidPhoneNum: 'المرجو إدخال رقم اتصال صالح',
    newNumIsSameAsPrevious: 'المرجو إدخال رقم اتصال صالح',
    pleaseEnterValidEmailPhoneNum: 'المرجو إدخال رقم هاتف/بريد إلكتروني صالح',
    pleaseEnterPswdOf6Digits: 'المرجو إدخال كلمة المرور التي تتألف من 6 أرقام على الأقل',
    pleaseEnterCnfmPswd: 'المرجو إدخال كلمة المرور الصحيحة',
    confirmPswdDoesntMatched: 'كلمة المرور غير متطابقة',
    pleaseEnterOldPswd: 'المرجو إدخال كلمة المرور القديمة',

    // AppointmentFor options
    mySelf: 'أنا',
    wife: 'الزوجة',
    son: 'الابن',
    daughter: 'الابنة',

    noThanks: 'لا شكرا',
  },
  es: {
    //Common Error's
    internetConnectionError: 'Por favor, comprueba tu conexión a Internet.',

    //Welcome
    skip: 'Omitir',
    welcome: 'Bienvenido',
    mediportNeedsAccessToLocation: 'Mediport App necesita acceso a tu ubicación',
    askMeLater: 'Recordarme después',

    //Sign IN
    helloSign: 'Hola, entra',
    withUs: 'Con nosotros',
    email: 'Correo electrónico',
    password: 'Contraseña',
    forgotPassword: 'Olvidé mi contraseña',
    signIn: "Iniciar sesión",
    or: 'O',
    dontHaveAccount: '¿No tienes una cuenta?',
    signUp: "Regístrate",

    //Sign Up
    name: "Nombre",
    cnfrmPassword: "Confirmar contraseña",
    phoneNumber: "Número de teléfono",
    signup: "Regístrate",

    // Verification
    verification: 'Verificación',
    verifyOTP: 'Verificación OTP',
    verifyMsg: 'Introduce el OTP Enviado a',
    haveNotReceivedCode: '¿No has recibido el OTP?',
    resendOTP: 'Reenviar OTP',
    confirm: 'Confirmar',

    // Forgot Password
    submit: 'Enviar',
    enterNewPhoneNumber: 'Ingrese un nuevo número de teléfono',

    //Home
    home: 'Inicio',
    searchSpeacility: 'Búsqueda de especialidad',
    searchDoctor: 'Búsqueda de Doctor, Especialidad',
    selectSpeciality: 'Seleccionar especialidad',

    //Doctor List
    doctors: 'Doctores',
    viewProfile: 'Ver Perfil',
    myDoctors: 'Mis Doctores',
    message: 'Mensaje',
    min: 'Min',
    max: 'Max',

    // Doctor Profile
    doctorProfile: 'Perfil del Doctor',
    ratings: 'Calificaciones',
    appointments: 'Citas',
    suggested: 'Sugerido',
    office: 'Oficina',
    education: 'Educación',
    language: 'Idioma',
    patientReviews: 'Comentarios de los pacientes',
    bookAppointment: 'Reservar cita',

    // Book Appointment
    bookAnAppointment: 'Reservar una cita',
    appointmentFor: 'Cita para',
    agePlaceholder: 'Edad: eg 27',
    booldGroupPlaceholder: 'Grupo sanguíneo: A+',
    patientName: 'Nombre del paciente',
    contactNumber: 'Número de contacto',
    typeHealthIssue: 'Por favor, escribe tu problema de salud en detalle.',
    uploadReports: 'Subir informes/registro médico',
    add: 'Añadir',
    next: 'Siguiente',
    selectAppointmentDate: 'Seleccionar la fecha de la cita',
    selectAppointmentTime: 'Seleccionar la hora de la cita',
    date: 'Fecha',
    to: 'A',
    referenceId: 'ID de referencia',
    fees: 'Tarifas',
    continue: 'Continuar',
    bookingSuccessful: 'Reservación exitosa',
    done: 'Hecho',
    time: 'Hora',
    confirmMsg: 'Por favor, confirma los detalles de tu reserva',

    //Payment
    paymentMethod: 'Método de pago',
    addPaymentMethod: 'Añadir método de pago',

    // Add Card
    addCard: 'Añadir tarjeta',
    selectyourCard: 'Seleccionar tarjeta',
    crditCard: 'Tarjeta de Crédito',
    debitCard: 'Tarjeta de Débito',
    enterAmount: 'Introduce el monto',
    cardDetails: 'Detalles de la tarjeta',
    cardHolderName: 'Nombre del titular de la tarjeta',
    cardNumber: 'Número de la tarjeta',
    expirationDate: 'Fecha de vencimiento',
    cvv: 'CVV/CVC',
    saveCardForFuture: 'Guardar la tarjeta para futuro',
    amount: 'Monto',

    // Saved Cards
    savedCards: 'Tarjetas guardadas',
    startWithCardNumber: 'empecemos con el número de tarjeta',
    applyDicountCode: 'Aplicar el código de descuento',

    //Discount
    discount: 'Descuento',
    availableDiscounts: 'Descuentos disponibles',
    mediport: 'MEDIPORT',
    copyCode: 'COPIAR CÓDIGO',

    // Appointments
    upcoming: 'Próximamente',
    history: 'Historia',
    dayLeft: 'Días',
    viewReports: 'Ver informes',
    patientsName: 'Nombre del paciente',
    cancelAppointment: 'Cancelar Cita',
    selectReasonCancelAppointment: 'Selecciona el motivo de la cancelación de la cita',
    enterReasonHere: 'Introduce tu razón aquí ',

    // Reports
    reports: 'Informes',
    clickHereToDownload: 'Haz clic aquí para descargar',

    //Chats
    chats: 'Chats',
    ago: 'Hace',
    writeAMsg: 'Escribe un mensaje',
    youRequestVideoCall: 'Solicita una videollamada',
    isCallingYou: 'te esta llamando',
    accept: 'Aceptar',
    reject: 'Rechazar',

    //Feedback
    feedback: 'Comentarios',
    howWasThe: '¿Cómo fue el ',
    behaviour: 'conducta',
    pleaseGiveFeedback: 'Por favor, danos tu opinión',
    submitRating: 'Enviar calificación',
    suggestDoctor: '¿Sugerir este Doctor?',
    // Reveal
    myProfile: 'Mi perfil',
    myReports: 'Mis informes',
    chatSupport: 'Soporte de chat',
    mySubscription: 'Mi suscripción',
    settings: 'Ajustes',
    termsConditions: 'Términos y Condiciones',
    faq: 'FAQ',
    signOut: 'Cerrar sesión',

    //Profile
    profile: 'Perfil',
    edit: 'Editar',
    Email: 'Correo electrónico',
    gender: 'Género',
    phone: 'Teléfono',
    bloodGroup: 'Grupo sanguíneo',

    //Settings
    notifications: 'Notificaciones',
    changePswd: 'Cambiar Contraseña',
    oldPassword: 'Contraseña antigua',
    changePassword: 'Cambiar Contraseña',
    updatePassword: 'Cambiar Contraseña',
    retypePassword: 'Vuelve a escribir la contraseña',
    save: 'Guardar',
    successChangePswdMsg: 'Has cambiado con éxito tu contraseña',
    ok: 'OK',

    // Sign Out
    wantToSignOut: '¿Estás seguro de que quieres cerrar sesión?',
    cancel: 'Cancelar',

    // View Reports 
    age: 'Edad',
    DoctorsName: 'Nombre del doctor',
    disease: 'Enfermedad',
    doctorFeedback: 'Opinión del médico',

    // Messages
    pleaseEnterName: 'Por favor, introduce el nombre',
    pleaseEnterEmail: 'Por favor, introduce el correo electrónico',
    pleaseEnterValidEmail: 'Por favor, introduce un correo electrónico válido',
    pleaseEnterPswd: 'Por favor, introduce una contraseña',
    pleaseEnterCardNum: 'Por favor, introduce el número de la tarjeta',
    pleaseEnterExpMonth: 'Por favor, introduce el mes de caducidad',
    pleaseEnterExpYear: 'Por favor, introduce el año de caducidad',
    pleaseEnterCVV: 'Por favor, introduce cvv/cvc',
    pleaseSelectReasonOFCancel: 'Por favor, selecciona el motivo de la cancelación de la cita',
    pleaseEnterAge: 'Por favor, introduce la edad',
    pleaseEnterBloodGroup: 'Por favor, introduce el grupo sanguíneo',
    pleaseEnterPatientsName: 'Por favor, introduce el nombre del paciente',
    pleaseEnterContactNum: 'Por favor, introduce el nombre del paciente',
    pleaseEnterValidContactNum: 'Por favor, introduce un número de contacto válido',
    slotsNotAvailable: 'No se dispone de vacantes para la fecha seleccionada',
    pleaseSelectAppointmentTime: 'Por favor, selecciona la hora de la cita',
    youSuccessfullyBookedAppointment: 'Has reservado tu cita con éxito',
    pleaseAllowAppToAccessLocation: 'Por favor, permite que la aplicación acceda a tu ubicación',
    feedbackMessageCantBeEmpty: 'El mensaje de opinión no puede estar vacío',
    pleaseEnterCountryCode: 'Por favor, introduce el código de país',
    pleaseEnterPhoneNum: 'Por favor, introduce el número de teléfono',
    pleaseEnterValidPhoneNum: 'Por favor, introduce un número de teléfono válido',
    newNumIsSameAsPrevious: 'El nuevo número es el mismo que el anterior',
    pleaseEnterValidEmailPhoneNum: 'Por favor, introduce un correo electrónico/número de teléfono válido',
    pleaseEnterPswdOf6Digits: 'Por favor, introduce una contraseña de al menos 6 dígitos',
    pleaseEnterCnfmPswd: 'Por favor, introduce y confirma la contraseña',
    confirmPswdDoesntMatched: 'La contraseña de confirmación no coincide',
    pleaseEnterOldPswd: 'Por favor, introduce la Contraseña Antigua',

    // AppointmentFor options
    mySelf: 'Yo mismo',
    wife: 'Esposa',
    son: 'Hijo',
    daughter: 'Hija',

    noThanks: 'No, gracias',
  },
});