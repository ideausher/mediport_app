import { Alert, NativeModules } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import LocalDataManager from '../../Utils/LocalDataManager';
import { localize } from '../../Utils/LocalisedManager';
import General from "../General";
import NetInfo from "@react-native-community/netinfo";
//@ts-ignore
import TimeZone from 'react-native-timezone';
import RNFetchBlob from 'rn-fetch-blob'
//import I18n from 'react-native-i18n';

const axios = require('axios');
const requestTime = 10000

const deviceLocale = General.getLocale()

class RequestManager {

  static async  getRequest(url: any, params: any) {

    const timeZone = await TimeZone.getTimeZone()
    const retrievedItem = await AsyncStorage.getItem('loginInfo') || 'none';
    const tokenStr = JSON.parse(retrievedItem).token

    console.log('Request URl  >>>>', url, timeZone, url == "https://mediportvideocall.iphoneapps.co.in:9530/token?");
    console.log('Request URl login info >>>>', url, JSON.parse(retrievedItem));

    if(url == "https://mediportvideocall.iphoneapps.co.in:9530/token?"){
      var instance = axios.create({
        baseURL: url,
      });
    }else{
    var instance = axios.create({
      baseURL: url,
      timeout: requestTime,
      headers: {
        'Authorization': 'Bearer ' + tokenStr,
        Accept: 'application/json',
        'Content-Type': 'application/json',
        language: deviceLocale,
        'time-zone': timeZone,
        trustAllHosts: true
      }
    });
}
    return new Promise(function (resolve, reject) {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          console.log('net connected')
          instance.get(url, {
            params: params
          })
            .then(function (response: any) {
              console.log('Response getRequest >>>>')
              console.log('Response getRequest >>>>', response)
              if (response.status == 200 || response.data.status == 1) {
                resolve(response.data)
              }
              else {
                reject(response.data)
              }
            })
            .catch(function (error: any) {
              console.log('Error in GET REQUEST >>>>', error)
              console.log('Error RESPOnse GET REQUEST >>>>', error.response)

              if (error.response != null && error.response != undefined) {
                reject(error.response.data)
              }
              else {
                reject(localize.internetConnectionError)
              }
            })
        }
        else {
          console.log('Net not Conencted')
          reject(localize.internetConnectionError)
        }
      });
    });
  }

  static postRequest(url: any, params: any, headers: boolean) {

    return new Promise(function (resolve, reject) {

      NetInfo.fetch().then(state => {
        if (state.isConnected) {

          var instance = axios;
          LocalDataManager.getDataAsyncStorage('fcmToken').then((data: any) => {
            console.log('FCM Toke in post request  >>>>', data)
            if (headers) {
              instance = axios.create({
                baseURL: url,
                timeout: requestTime,
                headers: {
                  'device-token': data,
                  language: deviceLocale,
                }
              });
            }
            else {
              instance = axios.create({
                baseURL: url,
                timeout: requestTime,
              });
            }

            instance.post(url, params)
              .then(function (response: any) {
                console.log('Response >>>>', response)
                if (response.data.status == 1) {
                  resolve(response.data)
                }
                else {
                  reject(response.data)
                }
              })
              .catch(function (error: any) {
                console.log('Error in Post REQUEST >>>>', error)
                if (error.response != null || error.response != undefined) {
                  reject(error.response.data)
                }
                else {
                  reject(localize.internetConnectionError)
                }
              });
          })
        }
        else {
          console.log('Net not Conencted')
          reject(localize.internetConnectionError)
        }
      })
    });
  }

  static postRequestWithHeaders(url: any, params: any) {

    console.log('URL in postRequestWithHeaders>>', url)

    return new Promise(function (resolve, reject) {
      console.log('Called promise>>>')

      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          var instance = axios;
          LocalDataManager.getLoginInfo().then((data: any) => {
            if (data != null && data != {}) {

              console.log('Data Token>>>', data.token)
              instance = axios.create({
                baseURL: url,
                timeout: requestTime,
                headers: {
                  'Authorization': 'Bearer ' + data.token,
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  language: deviceLocale,
                }
              })

              instance.post(url, params)
                .then(function (response: any) {
                  console.log('Response postRequestWithHeaders>>>>', response)
                  if (response.data.status == 1) {
                    resolve(response.data)
                  }
                  else {
                    reject(response.data)
                  }
                })
                .catch(function (error: any) {
                  console.log('Error in postRequestWithHeaders REQUEST >>>>', error)
                  if (error.response != null && error.response != undefined) {
                    reject(error.response.data)
                  }
                  else {
                    reject(localize.internetConnectionError)
                  }
                });
            }
          })
        }
        else {
          console.log('Net not Conencted')
          reject(localize.internetConnectionError)
        }

      })
    });
  }

  static uploadImage(url: any, bodyFormData: any) {
    return new Promise(function (resolve, reject) {


      NetInfo.fetch().then(state => {

        if (state.isConnected) {
          LocalDataManager.getLoginInfo().then((data: any) => {
            if (data != null && data != {}) {
              axios({
                method: 'post',
                url: url,
                data: bodyFormData,
                headers: {
                  'Authorization': 'Bearer ' + data.token,
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                  language: deviceLocale,
                }
              }).then(function (response: any) {
                console.log('Response Upoad Image >>>>', response)
                if (response.data.status == 1) {
                  resolve(response.data)
                }
                else {
                  reject(response.data)
                }
              })
                .catch(function (error: any) {
                  console.log('Error in uploadImage REQUEST >>>>', error)
                  if (error.response != null && error.response != undefined) {
                    reject(error.response.data)
                  }
                  else {
                    reject(localize.internetConnectionError)
                  }
                });
            }
          })
        }
        else {
          console.log('Net not Conencted')
          reject(localize.internetConnectionError)
        }
      })
    });
  }

  static putRequest(url: any, params: any) {

    return new Promise(function (resolve, reject) {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          RequestManager.callPutRequest(url, params, axios, resolve, reject)
        }
        else {
          console.log('Net not Conencted')
          reject(localize.internetConnectionError)
        }
      })
    });
  }

  static async putRequestWithHeaders(url: any, params: any) {

    return new Promise(function (resolve, reject) {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          var instance = axios;
          LocalDataManager.getLoginInfo().then((data: any) => {
            if (data != null && data != {}) {

              instance = axios.create({
                baseURL: url,
                timeout: requestTime,
                headers: {
                  'Authorization': 'Bearer ' + data.token,
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  language: deviceLocale,
                }
              })
              RequestManager.callPutRequest(url, params, instance, resolve, reject)
            }
          })
        }
        else {
          console.log('Net not Conencted')
          reject(localize.internetConnectionError)
        }
      })
    });
  }

  static async callPutRequest(url: any, params: any, instance: any, resolve: any, reject: any) {

    console.log('callPutRequest func called');

    instance.put(url, params)
      .then(function (response: any) {
        console.log('Response callPutRequest >>>>', response)
        if (response.data.status == 1) {
          resolve(response.data)
        }
        else {
          reject(response.data)
        }
      })
      .catch(function (error: any) {
        console.log('Error in callPutRequest REQUEST >>>>', error)
        if (error.response != null && error.response != undefined) {
          reject(error.response.data)
        }
        else {
          reject(localize.internetConnectionError)
        }
      });
  }

  static async  patchRequest(url: any, params: any) {


    const timeZone = await TimeZone.getTimeZone()
    const retrievedItem = await AsyncStorage.getItem('loginInfo') || 'none';
    const tokenStr = JSON.parse(retrievedItem).token

    console.log('Request URl  >>>>', url);
    console.log('PAtch request params >>>>', params);

    const instance = axios.create({
      baseURL: url,
      timeout: requestTime,
      headers: {
        'Authorization': 'Bearer ' + tokenStr,
        Accept: 'application/json',
        'Content-Type': 'application/json',
        language: deviceLocale,
        'time-zone': timeZone,
      }
    });

    return new Promise(function (resolve, reject) {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          console.log('net connected')
          instance.patch(url, params)
            .then(function (response: any) {
              console.log('Response patchRequest >>>>', response)
              if (response.status == 200 || response.data.status == 1) {
                resolve(response.data)
              }
              else {
                reject(response.data)
              }
            })
            .catch(function (error: any) {
              console.log('Error in PATCH REQUEST >>>>', error)
              console.log('Error RESPOnse PATCH REQUEST >>>>', error.response)

              if (error.response != null && error.response != undefined) {
                reject(error.response.data)
              }
              else {
                reject(localize.internetConnectionError)
              }
            })
        }
        else {
          console.log('Net not Conencted')
          reject(localize.internetConnectionError)
        }
      });
    });
  }

  static uploadPDF(url: string, path: string) {

    console.log('UPload PDf FUnc Called')

    return new Promise(function (resolve, reject) {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          console.log('net connected')
          LocalDataManager.getLoginInfo().then((data: any) => {
            if (data != null && data != {}) {

              console.log('Data in uploadPDF >>>', data)
              RNFetchBlob.fetch('POST', url, {
                Authorization: 'Bearer ' + data.token,
                Accept: 'application/json',
                // otherHeader: "foo",
                // this is required, otherwise it won't be process as a multipart/form-data request
                'Content-Type': 'application/x-www-form-urlencoded'
              }, [
                // append field data from file path
                {
                  name: 'file',
                  filename: 'pdf.pdf',
                  data: RNFetchBlob.wrap(path)
                },

              ]).then((resp) => {
                console.log("success" + resp.text())
                resolve(resp.text())
                // ...
              }).catch((err) => {
                // ...
                console.log("error" + err.text())
                reject(err.text())
              })
            }
          })
        }
        else {
          console.log('Net not Conencted')
          reject(localize.internetConnectionError)
        }
      })
    })
  }



  static async  getRequestToken(url: any, params: any) {

    const timeZone = await TimeZone.getTimeZone()
    const retrievedItem = await AsyncStorage.getItem('loginInfo') || 'none';
    const tokenStr = JSON.parse(retrievedItem).token

    console.log('Request URl  >>>>', url, timeZone, url == "https://mediportvideocall.iphoneapps.co.in:9530/token?");
    console.log('Request URl login info >>>>', params);

    var instance = axios.create({
      baseURL: url,
      timeout: requestTime,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        language: deviceLocale,
        'time-zone': timeZone,
        trustAllHosts: true
      }
    });
    
    return new Promise(function (resolve, reject) {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          console.log('net connected')
          instance.get(url, {
            params: params
          })
            .then(function (response: any) {
              console.log('Response getRequest >>>>')
              console.log('Response getRequest >>>>', response)
              if (response.status == 200 || response.data.status == 1) {
                resolve(response.data)
              }
              else {
                reject(response.data)
              }
            })
            .catch(function (error: any) {
              console.log('Error in GET REQUEST >>>>', error)
              console.log('Error RESPOnse GET REQUEST >>>>', error.response)

              if (error.response != null && error.response != undefined) {
                reject(error.response.data)
              }
              else {
                reject(localize.internetConnectionError)
              }
            })
        }
        else {
          console.log('Net not Conencted')
          reject(localize.internetConnectionError)
        }
      });
    });
  }

}

export default RequestManager

