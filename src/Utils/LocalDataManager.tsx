import { Alert } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

const axios = require('axios');

class LocalDataManager {

   static getLoginInfo = async () => {

        console.log('Get Email Called >>>')
        let loginInfo = {}

        try {
            const retrievedItem =  await AsyncStorage.getItem('loginInfo') || 'none';
            loginInfo = JSON.parse(retrievedItem);
        } catch (error) {
            // Error retrieving data
            console.log('Error while retriving login info >>>', error.message);
            return null
        }

        console.log('Get login Info Called>>>', loginInfo)
        
        return loginInfo;
    }

    static saveDataAsyncStorage = async (key: string , data: any) => {

        console.log('key in saveDataAsyncStorage>>>', key)
        console.log('data in saveDataAsyncStorage>>>', data)
        try {
            await AsyncStorage.setItem(key, data);
        } catch (error) {
            // Error while Saving data
            console.log(error.message);
        }
    };

    static getDataAsyncStorage = async (key: string) => {

        console.log('getDataAsyncStorage Fun called for key', key)
        let data = {}

        try {
            const retrievedItem =  await AsyncStorage.getItem(key) || 'none';
            data = JSON.parse(retrievedItem);

            console.log('Data in try', data)
        } catch (error) {
            // Error retrieving data
            console.log('Error while retriving Async Data >>>', error.message);
            return null
        }

        console.log('Get getDataAsyncStorage Called>>>', data)
        
        return data;
    }

    static removeLocalData = async (key: string) => {
        try {
            await AsyncStorage.removeItem(key);
        } catch (error) {
            // Error retrieving data
            console.log(error.message);
        }
    }

    static removeAllLocalData = async () => {
        try {
            await AsyncStorage.removeItem('loginInfo')
            //await AsyncStorage.removeItem('isFirstTime')
            await AsyncStorage.removeItem('signupInfo')
            await AsyncStorage.removeItem('fcmToken')
        } catch (error) {
            // Error retrieving data
            console.log(error.message);
        }
    }
}

export default LocalDataManager

