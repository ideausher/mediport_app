export const BASE_URL = 'http://iphoneapps.co.in/mediport/website/public/api/v1';
export const IMAGE_URL = 'http://iphoneapps.co.in/mediport/website/public/images/';
export const AVATAR_URL = 'http://iphoneapps.co.in/mediport/website/public/images/avatars/';
export const DOCTOR_IMAGE_URL = 'http://iphoneapps.co.in/mediport/website/public/doctor_images/';

// export const BASE_URL = 'http://iphoneapps.co.in/devmediport/website/public/api/v1';
// export const IMAGE_URL = 'http://iphoneapps.co.in/devmediport/website/public/images/';
// export const AVATAR_URL = 'http://iphoneapps.co.in/devmediport/website/public/images/avatars/';
// export const DOCTOR_IMAGE_URL = 'http://iphoneapps.co.in/mediport/website/public/doctor_images/';

export const API_SENDOTP = BASE_URL +  '/sendotp';
export const API_SIGNUP = BASE_URL + '/user';
export const API_SIGNIN = BASE_URL + '/login';
export const API_FORGOT_PSWD = BASE_URL + '/forgetpassword';
export const API_CHECK_OTP = BASE_URL + '/checkotp';

export const API_UPDATE_FORGOT_PASSWORD = BASE_URL + '/updateforgetpassword';
export const API_UPDATE_PASSWORD = BASE_URL + '/updatepassword';
export const API_SPECIALITY_LIST = BASE_URL + '/category';
export const API_DOCTORS_LIST = BASE_URL  + '/getServices';
export const API_DOCTOR_PROFILE = BASE_URL + '/doctor';
export const API_CHECK_PHONE_OTP = BASE_URL + '/checkPhoneOtp';
export const API_GET_SLOTS = BASE_URL + '/slots';
export const API_UPLOAD_IMAGE = BASE_URL + '/uploadImage';
export const API_GET_CARDS = BASE_URL + '/card';
export const API_GET_COUPENS = BASE_URL + '/coupons';
export const API_VALIDATE_COUPENS = BASE_URL + '/validate_coupon';
export const API_TERMS_COND = BASE_URL + '/term_condition';
export const API_MY_DOCTORS_LIST = BASE_URL + '/mydoctors';
export const API_NOTIFICATIONS_LIST = BASE_URL + '/notification';
export const API_GET_REPORTS = BASE_URL + '/reports';
export const API_DOCTOR_REPORTS_LIST = BASE_URL + '/doctor_reports_list';
export const API_DOCTOR_REPORTS = BASE_URL + '/doctor_reports';
export const API_LOGOUT = BASE_URL + '/logout';
export const API_GET_CHATS = 'https://pt.iphoneapps.co.in:3002/api/v1.0/chat/getchat?user_id=';

/////////
export const API_GET_FAQ = BASE_URL + '/faq';
export const API_ADD_REVIEW = BASE_URL + '/addReview';
export const API_APPOINTMENT_LIST = BASE_URL + '/appointment';
export const API_UPLOAD_PDF = BASE_URL + '/upload'; 
// export const API_GET_TOKEN =  "https://sessions.skiedo.com:8080/token?";
export const API_GET_TOKEN =  "https://mediportvideocall.iphoneapps.co.in:9530/token?";

/**
 * Video Chat URL
 */
export const API_CHAT_GET_URL = 'https://livesmartvideochat.iphoneapps.co.in:3240/api/sendnotification?';