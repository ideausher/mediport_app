//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_SHADOW_COLOR } from '../Constants'
import { Dimensions, Platform } from 'react-native';

export const CommonStyles = ScaleSheet.create({
    doctorImgCntnrStyle: {
        width: 60,
        height: 60,
        backgroundColor:'white',
        ...Platform.select({
            ios: {
                shadowColor: APP_SHADOW_COLOR,
                shadowOffset: { height: 0, width: 0 },
                shadowOpacity: 2,
                shadowRadius: 2,
            },
            android: {
                elevation: 2,
            },
        }),
        borderRadius: 30,
    },
    smallDoctorImgCntnrStyle: {
        width: 50,
        height: 50,
        backgroundColor:'white',
        ...Platform.select({
            ios: {
                shadowColor: APP_SHADOW_COLOR,
                shadowOffset: { height: 0, width: 0 },
                shadowOpacity: 2,
                shadowRadius: 2,
            },
            android: {
                elevation: 2,
            },
        }),
        borderRadius: 25,
    },
    doctorImg: {
        width:'100%',
        height:'100%',      
        borderRadius: Math.round(Dimensions.get('window').width * 0.2) / 2,  
    },
})
