
export enum VerificationFor {
    signup,
    payment,
    forgotPswd,
    editPhoneNumber,
}

export enum ForGotScreenType {
    forgotPswd,
    editPhoneNumber,
}