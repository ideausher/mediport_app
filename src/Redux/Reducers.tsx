import * as Actions from './Constants';
import { combineReducers } from 'redux';
import { Alert, AlertAndroid } from 'react-native';
import { act } from 'react-test-renderer';

const INITIAL_STATE = {
    id: '',
    token: '',
    name: '',
    email: '',
    phoneNumber: '',
    countryCode: '',
    bloodGroup: '',
    gender: '',
    image: '', 
}

const saveLoginInfo = (state = INITIAL_STATE, action: any) => {

    switch (action.type) {
        case Actions.SAVE_LOGIN_INFO:

            console.log('Login Info Saved >>>', action.payload)
            return {
                ...state,
                id: action.payload.id,
                token: action.payload.token,
                name: action.payload.firstname,
                email: action.payload.email,
                phoneNumber: action.payload.phone_number,
                countryCode: action.payload.phone_country_code,
                bloodGroup: (action.payload.blood_group != undefined ? action.payload.blood_group : ''),
                gender: (action.payload.gender != undefined ? action.payload.gender : ''),
                image: (action.payload.image != undefined ? action.payload.image : ''),
            }
        default:
            return state;
    }
}

const DOCTOR_INITIAL_STATE = {
    id: '',
    name: '',
    speciality: '',
    address: [],
    price: '',
    description: '',
    ratingCount: 0,
    appointmentsCount: 0,
    suggestedCount: 0,
    office: '',
    education: [],
    languages: [],
    avgRating: '',
    patientReviews: [],
    image: '',
    categoryName: '',
    selectedService: {},
    serviceId: '',
    venderServices: []
}

// Selcted Doctor Profile data
const saveDoctorInfo = (state = DOCTOR_INITIAL_STATE, action: any) => {

    switch (action.type) {
        case Actions.SAVE_DOCTOR_INFO:
            console.log('Doctor data Payload >>>', action.payload)
            console.log('Doctor data CatID >>>', action.catId)
            

            if (action.payload == undefined) {
                console.log('Inside If Condition')
                state = DOCTOR_INITIAL_STATE
                return state
            }
            else {
                console.log('Inside Else Condition')
                console.log('USer MANy Address >>', action.payload.user_many_address)
                let vendorServices = action.payload.vender_services

                let selectedService: any

                if (action.doctorsType == 1) {
                    vendorServices.map((item: any) => {
                        if (item.cat_id == action.catId) {
                            selectedService = item
                        }
                    })
                }
                else {
                    selectedService = vendorServices[0]
                }

                console.log('Selected Servcie >>>', selectedService)

                return {
                    ...state,
                    id: action.payload.id,
                    name: action.payload.firstname,
                    speciality: '',
                    address: (action.payload.user_many_address != null && action.payload.user_many_address != undefined ? action.payload.user_many_address : []),
                    price: (selectedService != null && selectedService != undefined ? selectedService.price : ''),
                    description: (selectedService != null && selectedService != undefined ? selectedService.description : ''),
                    ratingCount: action.payload.vendor_reviews_count,
                    appointmentsCount: action.payload.vender_booking_count,
                    suggestedCount: 0,
                    office: '',
                    education: action.payload.vendor_education,
                    languages: action.payload.vender_langauages,
                    avgRating: (action.payload.vendor_reviews.length > 0 ? action.payload.vendor_reviews_avg[0].avg_rating : 0),
                    patientReviews: action.payload.vendor_reviews,
                    image: action.payload.image,
                    categoryName: (selectedService != null && selectedService != undefined ? selectedService.servicecategory.cat_name : ''),
                    selectedService: selectedService,
                    serviceId: (selectedService != null && selectedService != undefined ? selectedService.id : ''),
                    venderServices: vendorServices
                }
            }
        default:
            return state;
    }
}

const CAT_INITIAL_STATE = {
    category: {},
}

const saveCategoryInfo = (state = CAT_INITIAL_STATE, action: any) => {
    switch (action.type) {
        case Actions.SAVE_CATEGORY_INFO:
            return {
                ...state,
                category: action.payload
            }
        default:
            return state;
    }
}

const BOOKING_INITIAL_STATE = {
    appointmentFor: '',
    age: '',
    bloodGroup: '',
    patientName: '',
    contactNumber: '',
    description: '',
    reportIds: '',
    gender: '',
    bookingDate: '',
    bookingTime: '',
    slotId: '',
}

const saveBookingInfo = (state = BOOKING_INITIAL_STATE, action: any) => {
    switch (action.type) {
        case Actions.SAVE_BOOKING_INFO:
            return {
                ...state,
                appointmentFor: action.payload.appointmentFor,
                age: action.payload.age,
                bloodGroup: action.payload.bloodGroup,
                patientName: action.payload.patientName,
                contactNumber: action.payload.contactNumber,
                description: action.payload.description,
                reportIds: action.payload.reportIds,
                gender: action.payload.gender,
            }
        case Actions.SAVE_DATE_TIME:
            return {
                ...state,
                bookingDate: action.payload.bookingDate,
                bookingTime: action.payload.bookingTime,
                slotId: action.payload.slotId,
            }
        default:
            return state;
    }
}

export default combineReducers({
    saveLoginInfo: saveLoginInfo,
    saveDoctorInfo: saveDoctorInfo,
    saveCategoryInfo: saveCategoryInfo,
    saveBookingInfo: saveBookingInfo,
    //companyInfo: companyInfoReducer,
});
