
import { put, takeLatest } from 'redux-saga/effects'
import { LOGIN, DOCTOR_INFO, CATEGORY_INFO, BOOKING_INFO, DATE_TIME_INFO } from './Constants';
import { saveLoginInfo, saveDoctorInfo, saveCategoryInfo, saveBookingInfo, saveBookingDateTimeInfo} from './UsersAction';

function* loginUserToApp(data: any) {
  yield put(saveLoginInfo(data.payload));
}

function* setDoctorInfo(data: any) {
  console.log('RESPONSE SAGA: ', data.payload)
  yield put(saveDoctorInfo(data.payload, data.catId, data.doctorsType)); // Type is for if Doctor comes from MyDoctors or by selcting Category
}

function* setCategoryInfo(data: any) {
  console.log('RESPONSE SAGA: ', data.payload)
  yield put(saveCategoryInfo(data.payload));
}

function* setBookingInfo(data: any) {
  console.log('RESPONSE SAGA: ', data.payload)
  yield put(saveBookingInfo(data.payload));
}

function* setBookingDateTimeInfo(data: any) {
  console.log('RESPONSE SAGA: ', data.payload)
  yield put(saveBookingDateTimeInfo(data.payload));
}

export default function* rootSaga(data: any) {
  yield takeLatest(LOGIN, loginUserToApp);
  yield takeLatest(DOCTOR_INFO, setDoctorInfo);
  yield takeLatest(CATEGORY_INFO, setCategoryInfo);
  yield takeLatest(BOOKING_INFO, setBookingInfo);
  yield takeLatest(DATE_TIME_INFO, setBookingDateTimeInfo);
}
