
import { SAVE_LOGIN_INFO, SAVE_DOCTOR_INFO, SAVE_CATEGORY_INFO, SAVE_BOOKING_INFO, SAVE_DATE_TIME } from './Constants';

export const saveLoginInfo = (data: string) => ({
  type: SAVE_LOGIN_INFO,
  payload: data,
});

export const saveDoctorInfo = (data: string, catId: string, doctorsType: string) => ({
  type: SAVE_DOCTOR_INFO,
  payload: data,
  catId: catId,
  doctorsType: doctorsType
});

export const saveCategoryInfo = (data: string) => ({
  type: SAVE_CATEGORY_INFO,
  payload: data,
});

export const saveBookingInfo = (data: string) => ({
  type: SAVE_BOOKING_INFO,
  payload: data,
});

export const saveBookingDateTimeInfo = (data: string) => ({
  type: SAVE_DATE_TIME,
  payload: data,
});