
export interface NotificationsModal {
    vender_id: Number,
    type: Number
    title: String,
    message: NotMsg,
    created_at: String
    vendor: NotVendor
}

export interface NotMsg {
    user_name: String,
    booking_id: Number,
    vender_id: Number,
}

export interface NotVendor {
    id: Number, 
    image: String, 
    firstname: String
}