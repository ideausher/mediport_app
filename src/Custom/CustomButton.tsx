import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREEN_COLOR, APP_GREY_TEXT_COLOR, APP_LIGHT_GREY_COLOR, App_MIDIUM_GREY_COLOR } from '../Constants';

export const CommonBtn = (props: any) => {

  return (
    <TouchableOpacity
      style={[styles.Btn, {width: props.width}, (props.marginTop != null ? {marginTop: props.marginTop} : null), (props.height != null ? {height: props.height} : null), (props.alignSelf != null ? {alignSelf: props.alignSelf} : null), (props.marginRight != null ? {marginRight: props.marginRight} : null), (props.position != null ? {position:props.position} : null), (props.marginBottom != null ? {marginTop: props.marginBottom} : null), (props.bottom != null ? {bottom: props.bottom} : null)]}
      onPress={() => props.func()}>
      <Text style={styles.text}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export const CommonBtnWhite = (props: any) => {

  return (
    <TouchableOpacity
      style={[styles.btnWhite, (props.marginTop != null ? {marginTop: props.marginTop} : null), (props.marginBottom != null ? {marginBottom: props.marginBottom} : null)]}
      onPress={() => props.func()}>
      <Text style={[styles.text, { color:App_MIDIUM_GREY_COLOR, fontSize: 14}]}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export const GreenTextBtn = (props: any) => {

  return (
    <TouchableOpacity onPress={() => props.func()} style={[styles.greenTextBtn, (props.width != null ? { width: props.width, alignSelf: 'center' } : null), (props.marginRight != null ? { marginRight: props.marginRight} : null), (props.marginTop != null ? { marginTop: props.marginTop} : null), (props.textAlign != null ? { textAlign: props.textAlign} : null)]}>
      <Text style={[styles.greenText, (props.textAlign != null ? {textAlign: props.textAlign} : 'center'), (props.width != null ? {width: props.width} : null)]}>{props.title}</Text>
    </TouchableOpacity>
  )
}

const styles = ScaleSheet.create({
  //Button
  Btn: {
    width: '60%',
    height: 50,
    backgroundColor: APP_GREEN_COLOR,
    marginTop: 20,
    justifyContent: 'center',
    color: 'white',
    borderRadius: 25,
    alignSelf: 'center',
    marginBottom: 16
  },
  btnWhite: {
    width: '80%',
    height: 50,
    backgroundColor: 'white',
    marginTop: 20,
    justifyContent: 'center',
    borderRadius: 25,
    borderWidth:2,
    borderColor: APP_GREEN_COLOR,
    alignSelf: 'center',
   // marginBottom: 80
  },
  text: {
    textAlign: 'center',
    color: 'white',
    fontSize: 16,
    fontWeight: "500"
  },
  greenTextBtn: {
    marginTop: 20,
    justifyContent: 'center',
    width: 120,
    height: 30,
    alignSelf: 'flex-end',
  },
  greenText: {
    textAlign: 'center',
    color: APP_GREEN_COLOR,
    fontSize: 14,
    fontWeight: "500",
  }
})
