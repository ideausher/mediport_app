import React from 'react';         
//@ts-ignore
import DatePicker from 'react-native-datepicker';

const MyDatePicker = (props: any) => {

  return (
    <DatePicker
      //date={props.dob}
      // style={{  btnTextText: {
      //   color: 'green',
      // }}}
      mode="date"
      androidMode='default'
      format="DD/MM/YYYY"
      //minDate="2016-05-01"
      maxDate={props.currentDate}
      showIcon={false}
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      hideText={false}
      customStyles={{
       
        btnTextConfirm: {
          color: 'black',         
        },
        dateInput: {
          borderWidth: 0,
          alignItems: 'flex-start',
        }
      }}
      onDateChange={(date: any) => props.func(date)}
    />
  )
}

export default MyDatePicker; 
