import React from 'react';         

import DatePicker from 'react-native-datepicker';

const MyTimePicker = (props: any) => {

  return (
    <DatePicker
      //date={props.dob}
      style={{ border: null}}
      mode="time"
      androidMode='default'
      format="hh:mm"
      //minDate="2016-05-01"
      maxDate={props.currentDate}
      showIcon={false}
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      hideText={false}
      customStyles={{
       
        btnTextConfirm: {
          color: 'black',
        },
        dateInput: {
          borderWidth: 0,
          alignItems: 'flex-end',
        }
      }}
      onDateChange={(date: any) => props.func(date)}
    />
  )
}

export default MyTimePicker; 
