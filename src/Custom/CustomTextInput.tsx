
import React, { useRef } from 'react';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { View, TextInput, TouchableOpacity, Image, Text, I18nManager } from 'react-native';
import MyDatePicker from './MyDatePicker';
import { APP_BORDER_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_LIGHT_GREY_COLOR, commonShadowStyle, APP_GREY_PLACEHOLDER_COLOR, APP_THEME_LIGHT_COLOR } from '../Constants';
import { CustomPicker, CustomPickerAndroid } from './CustomPicker';

export const TextInputWithoutImage = (props: any) => {
  return (
    <View style={[styles.inputContainer, commonShadowStyle, (props.marginTop != null ? { marginTop: props.marginTop } : null)]}>
      {props.func != null ?
        <TextInput style={styles.txtFld} placeholder={props.placeholder} placeholderTextColor={APP_GREY_PLACEHOLDER_COLOR} keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} onFocus={() => props.func()} onChangeText={(text) => props.onChange(text)} secureTextEntry={props.secureTextEntry != null ? props.secureTextEntry : false} autoCapitalize={props.autoCapitalize != null ? props.autoCapitalize : "words"} /> :
        <TextInput style={styles.txtFld} placeholder={props.placeholder} placeholderTextColor={APP_GREY_PLACEHOLDER_COLOR} keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} onChangeText={(text) => props.onChange(text)} secureTextEntry={props.secureTextEntry != null ? props.secureTextEntry : false} autoCapitalize={props.autoCapitalize != null ? props.autoCapitalize : "words"} />}
    </View>
  )
}

export const TextInputWithoutBorder = (props: any) => {
  return (
    <TextInput style={styles.txtFldWithoutBorder} placeholder={props.placeholder} placeholderTextColor='grey' keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} onChangeText={(text) => props.onChange(text)} />
  )
}


export const TextInputWithoutImageCustomWidthHeight = (props: any) => {

  console.log('PRICE IN CLAASSS', props.value)
  return (
    <View ref={props.ref != null ? props.ref : null} style={[styles.inputContainer, ((props.editable != null && props.editable == false) ? null : commonShadowStyle), { width: props.width }, (props.height != null ? { height: props.height } : null)]}>
      <TextInput style={[styles.txtFld, (props.textAlign != null ? { textAlign: props.textAlign } : null), (props.multiline != null ? { multiline: props.multiline, height: props.height, textAlignVertical: 'top', lineHeight: 20 } : null), (props.fontWeight != null ? { fontWeight: props.fontWeight } : null)]} placeholder={props.placeholder} placeholderTextColor={APP_LIGHT_GREY_COLOR} keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} onChangeText={(text) => props.onChange(text)} editable={props.editable != null ? props.editable : true} returnKeyType={props.returnKeyType != null ? props.returnKeyType : 'default'} maxLength={props.maxLength != null ? props.maxLength : null} onEndEditing={props.onEndEditing != null ? (() => props.onEndEditing()) : (() => { })} />
    </View>
  )
}

export const TextInputMultiLine = (props: any) => {
  return (
    <View style={[styles.inputContainer, commonShadowStyle, { width: props.width, height: props.height }]}>
      <TextInput style={[styles.txtFldMultiLine, { minHeight: props.height - 20 }]} placeholder={props.placeholder} placeholderTextColor='grey' keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} onChangeText={(text) => props.onChange(text)} multiline={true} blurOnSubmit={true} />
    </View>
  )
}

export const TextInputPhone = (props: any) => {
  return (
    <View style={[styles.inputContainer, commonShadowStyle, { width: '60%', alignSelf: 'flex-end' }]}>
      <TextInput style={styles.txtFld} placeholder={props.placeholder} placeholderTextColor='grey' keyboardType='phone-pad' value={props.value} onChangeText={(text) => props.onChange(text)} returnKeyType="done" />
    </View>
  )
}

export const TextInputWithDropdown = (props: any) => {

  return (
    <View style={[styles.inputContainer, commonShadowStyle, { width: '35%', alignSelf: 'flex-start' }, (props.marginTop != null ? { marginTop: 8, width: '100%' } : null), (props.width != null ? { width: props.width } : null)]}>
      <TouchableOpacity style={styles.dropdownBtn} onPress={() => props.func()} disabled={props.disabled != null ? props.disabled : false}>
        {props.img != null ?
          <Image
            style={styles.imgBw}
            source={props.img}
          /> : null
        }
        <Text style={[styles.dropdownText, (props.textAlign != null ? { textAlign: props.textAlign } : null), (props.marginTop != null ? { marginTop: props.marginTop } : null), (props.value == '' ? { color: APP_LIGHT_GREY_COLOR } : null)]}>{props.value != '' ? props.value : props.placeholder}</Text>
        <Image
          style={{
            height: 16,
            width: 16,
            marginRight: 4,
            resizeMode: 'contain'
          }}
          source={require('../assets/arrow.png')}
        />
      </TouchableOpacity>
    </View>
  )
}

export const TextInputWithDropdownIOS = (props: any) => {

  return (
    <View style={[styles.inputContainer, commonShadowStyle, { width: '35%', alignSelf: 'flex-start' }, (props.marginTop != null ? { marginTop: props.marginTop, width: '100%' } : null), (props.width != null ? { width: props.width } : null)]}>
      <TouchableOpacity style={styles.dropdownBtn} onPress={() => props.func()} disabled={props.disabled != null ? props.disabled : false}>
        {props.img != null ?
          <Image
            style={styles.imgBw}
            source={props.img}
          /> : null
        }
        <Text style={[styles.dropdownText, (props.textAlign != null ? { textAlign: props.textAlign } : null), (props.value == '' ? { color: APP_LIGHT_GREY_COLOR } : null)]}>{props.value != '' ? props.value : props.placeholder}</Text>
        <Image
          style={{
            height: 16,
            width: 16,
            marginRight: 4,
            resizeMode: 'contain'
          }}
          source={require('../assets/arrow.png')}
        />
      </TouchableOpacity>
    </View>
  )
}

export const TextInputWithDropdownAndroid = (props: any) => {

  return (
    <View style={[styles.inputContainer, commonShadowStyle, { width: '35%', alignSelf: 'flex-start' }, (props.marginTop != null ? { marginTop: props.marginTop, width: '100%' } : null), (props.width != null ? { width: props.width } : null)]}>
      {props.img != null ?
        <Image
          style={styles.imgBw}
          source={props.img}
        /> : null
      }
      <CustomPickerAndroid disabled={props.disabled} data={props.data} value={props.value} placeholder={props.placeholder} func={props.func} style={[styles.dropdownText, (props.textAlign != null ? { textAlign: props.textAlign } : null), (props.value == '' ? { color: APP_LIGHT_GREY_COLOR } : null)]} />
    </View>
  )
}

export const TextInputVerification = (props: any) => {
  return (
    <View style={[styles.inputContainer, commonShadowStyle, { width: '18%', height: 80 }]}>
      <TextInput ref={props.ref} style={styles.verificationTxtFld} placeholder={props.placeholder} placeholderTextColor='grey' keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} onChangeText={(text) => props.onChange(text)} />
    </View>
  )
}

export const TextInputWithImage = (props: any) => {

  return (
    <View style={[styles.inputContainer, commonShadowStyle, { width: '100%', marginTop: 8 }]}>
      {props.func != null ?
        <TouchableOpacity onPress={() => props.func()}>
          <Image
            style={styles.imgBw}
            source={props.icon}
          />
        </TouchableOpacity> :
        null
      }

      {props.rightImage == null ?
        <Image
          style={styles.imgBw}
          source={props.icon}
        /> : null}

      <TextInput ref={props.ref != null ? props.ref : null} style={[styles.txtFld, {
        // adjustsFontSizeToFitWidth: true,
        flexWrap: 'wrap'
      }, { width: '90%', fontSize: 13 }
      ]} placeholder={props.placeholder} placeholderTextColor={props.placeholderColor} keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} onChangeText={(text) => props.onChange(text)} value={props.value} autoCapitalize={props.autoCapitalize != null ? props.autoCapitalize : 'words'} secureTextEntry={props.secureTextEntry != null ? props.secureTextEntry : false} editable={props.editable} returnKeyType={props.returnKeyType != null ? props.returnKeyType : 'default'} maxLength={props.maxLength != null ? props.maxLength : null} />

      {props.rightImage != null ?
        <Image
          style={styles.imgBw}
          source={props.icon}
        /> : null}

    </View>
  )
}

export const TextWithImage = (props: any) => {

  return (
    <View style={[styles.inputContainer, commonShadowStyle, { width: '100%', marginTop: 8 }]}>
      {props.func != null ?
        <TouchableOpacity onPress={() => props.func()}>
          <Image
            style={styles.imgBw}
            source={props.icon}
          />
        </TouchableOpacity> :
        null
      }

      {props.rightImage == null ?
        <Image
          style={styles.imgBw}
          source={props.icon}
        /> : null}

      <Text style={{
        marginRight: 8,
        textAlign: 'left',
        height: 40,
        fontSize: 14,
        color: APP_GREY_TEXT_COLOR,
      }}>{props.value}</Text>

      {props.rightImage != null ?
        <Image
          style={styles.imgBw}
          source={props.icon}
        /> : null}

    </View>
  )
}

export const TextInputWithImageFunc = (props: any) => {

  let textColor = props.placeholderColor
  let text = props.placeholder
  if (props.value.length > 0) {
    textColor = APP_GREY_TEXT_COLOR
    text = props.value
  }

  return (

    <TouchableOpacity onPress={() => props.func()} style={[styles.inputContainer, commonShadowStyle, { width: '100%', marginTop: 8 }]} disabled={props.disabled != null ? props.disabled : false}>
      <Image
        style={[styles.imgBw, { transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] }]}
        source={props.icon}
      />
      <Text style={[styles.txtFld, { width: '90%', fontSize: 13, color: textColor, lineHeight: 40, textAlign: 'left' }
      ]}>{text}</Text>
    </TouchableOpacity>
  )
}

export const TextInputWithVisibility = (props: any) => {

  if (props.show != null && props.show == true) {
    return (
      <View style={[styles.inputContainer, commonShadowStyle]}>
        {props.func != null ?
          <TextInput style={styles.txtFld} placeholder={props.placeholder} placeholderTextColor={APP_GREY_PLACEHOLDER_COLOR} keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} onFocus={() => props.func()} onChangeText={(text) => props.onChange(text)} secureTextEntry={props.secureTextEntry != null ? props.secureTextEntry : false} /> :
          <TextInput style={styles.txtFld} placeholder={props.placeholder} placeholderTextColor={APP_GREY_PLACEHOLDER_COLOR} keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} onChangeText={(text) => props.onChange(text)} secureTextEntry={props.secureTextEntry != null ? props.secureTextEntry : false} />}
      </View>
    )
  } else {
    return null;
  }
}

const styles = ScaleSheet.create({

  //View Components
  imgBw: {
    height: 16,
    width: 16,
    marginRight: 4,
    resizeMode: 'contain'
  },
  inputContainer: {
    flexDirection: 'row',
    marginTop: 16,
    alignItems: 'center',
    height: 50,
    padding: 10,
    borderWidth: 0.2,
    borderColor: APP_THEME_LIGHT_COLOR,
    width: '90%',
    borderRadius: 10,
    backgroundColor: 'white',
  },
  // Text Input Components
  txtFld: {
    textAlign: I18nManager.isRTL ? 'right' : 'left',
    width: '100%',
    height: 40,
    fontSize: 14,
    color: APP_GREY_TEXT_COLOR,
    //backgroundColor: 'red'
  },
  dropdownText: {
    textAlign: 'center',
    color: APP_GREY_TEXT_COLOR,
    flex: 1,
    fontSize: 14,
  },
  dropdownBtn: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  verificationTxtFld: {
    textAlign: 'center',
    width: '100%',
    height: 100,
    fontSize: 30,
    color: APP_GREY_TEXT_COLOR,
  },
  txtFldWithoutBorder: {
    textAlign: 'left',
    width: '80%',
    height: 50,
    fontSize: 14,
    color: APP_GREY_TEXT_COLOR,
  },
  txtFldMultiLine: {
    textAlign: 'left',
    width: '100%',
    height: 'auto',
    fontSize: 14,
    color: APP_GREY_TEXT_COLOR,
    // Android only Property
    textAlignVertical: 'top'
  },
})
