import React from 'react';
import { View, StyleSheet, Text, FlatList, Dimensions, SafeAreaView, TouchableOpacity } from 'react-native';
import ListCell from '../Custom Cells/ListCell';
import { CommonBtn } from '../Custom/CustomButton';

export const DropdownList = (props: any) => {

  console.log('Props data in Dropdownlist', props)
  return (

    <SafeAreaView style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' }}>
      <View style={[styles.listVw, shadowStyle, { marginTop: props.topMargin }]}>
        <FlatList
          style={{ width: '100%' }}
          data={props.data}
          keyExtractor={(item: any, index: any) => index.toString()}
          renderItem={({ item, index }) => <ListCell cellData={item} index={index} func={(item: any) => props.func(item)} />}
        />
        <CommonBtn title={'Cancel'} func={() => props.removeList()} width='60%' height={40} />
      </View>
    </SafeAreaView>
  )
}

export const OptionsMenu = (props: any) => {

  return (
    <View style={[styles.optionsMenu, shadowStyle]}>
      <FlatList
        data={props.data}
        keyExtractor={(item: any, index: any) => index.toString()}
        renderItem={({ item, index }) => <ListCell cellData={item} type={props.type} index={index} func={(props.func)} />}
      /></View>
  )
}

const styles = StyleSheet.create({
  //Button
  listVw: {
    width: Dimensions.get('window').width - 40,
    padding: 10,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
  },
  optionsMenu: {
    height: 80,
    width: 200,
    padding: 10,
    marginRight: 20,
    marginTop: 10,
    backgroundColor: 'white',
    position: 'absolute',
  },
  btnStyle: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  }
})

export const shadowStyle = {
  shadowColor: 'black',
  shadowRadius: 2,
  shadowOpacity: 0.5,
  shadowOffset: { width: 0, height: 2 },
  elevation: 2,
}
