import React, { useRef } from 'react';
import { View, TouchableOpacity, Image, Text, I18nManager } from 'react-native';
import { APP_LIGHT_GREY_COLOR, APP_GREY_TEXT_COLOR, App_MIDIUM_GREY_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_SHADOW_COLOR, APP_HEADER_TITLE_COLOR } from '../Constants';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { CommonBtn } from './CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import { localize } from '../Utils/LocalisedManager';

export const LineVw = () => {
  return (
    <View style={{ height: 1.0, alignSelf: 'center', backgroundColor: APP_LIGHT_GREY_COLOR, width: '100%', marginTop: 10 }}></View>
  )
}

export const BackHeaderBtn = (props: any) => {
  return (
    <TouchableOpacity style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.9} onPress={props.func}>
      <Image style={{ marginLeft: 10, height: 20, width: 20, resizeMode: 'contain', transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] }} source={require('../assets/back-arrow.png')} />
    </TouchableOpacity>
  )
}

export const OpenDrawerHeaderBtn = (props: any) => {
  return (
    <TouchableOpacity style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.9} onPress={props.func}>
      <Image style={{ marginLeft: 10, height: 20, width: 20, resizeMode: 'contain' }} source={require('../assets/menu.png')} />
    </TouchableOpacity>
  )
}

export const NotificationsHeaderBtn = (props: any) => {
  return (
    <TouchableOpacity style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.9} onPress={props.func}>
      <Image style={{ marginRight: 10, height: 20, width: 20, resizeMode: 'contain' }} source={require('../assets/noti-bell.png')} />
    </TouchableOpacity>
  )
}

export const NoDataFoundView = (props: any) => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text style={styles.text}>No data found</Text>
      {/* {props.showFilterBtn ? <CommonBtn title='Refresh' func={() => props.func()} width='40%'/> : null} */}
    </View>
  )
}

export const NoInternetFoundView = (props: any) => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Image style={{ height: 70, width: 70, resizeMode: 'contain' }} source={require('../assets/internet_error.png')} />
      <Text style={styles.light_text}>{localize.internetConnectionError}</Text>
      <TouchableOpacity style={{ padding: 30 }} onPress={props.func}>
        <Text style={{ color: APP_HEADER_BACK_COLOR, textDecorationLine: 'underline', fontWeight: "bold" }}>Try Again</Text>
      </TouchableOpacity>
    </View>
  )
}

export const BottomShadowView = (props: any) => {
  return (
    //<View />
    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['transparent', '#eee',]} style={styles.gradient} />
  )
}

export const BackButtonHeader = (props: any) => {
  const { style, title, onClick, bellClick } = props
  return (
      <View style={[styles.header, style]}>
          <TouchableOpacity onPress={onClick} style={styles.backBtn}>
              <Image source={require('../assets/back-arrow.png')} style={styles.backArrowImg} />
          </TouchableOpacity>

          <Text style={styles.title}>{title}</Text>
      </View>
  )
}

const styles = ScaleSheet.create({
  backArrowImg: {
    width: 20,
    height: 20,
    resizeMode: 'contain'
},
title:{
  textAlign: 'center',
  color: APP_HEADER_TITLE_COLOR,
  width:'60%',
  fontSize: 18,
},
  header: {
    flexDirection: "row",
    // backgroundColor:'red',
    alignItems: 'center',
    height: 80,
    paddingHorizontal: 10,
    paddingTop: 30,
    width:'100%',
    marginBottom:25
},
backBtn: {
    // backgroundColor:'red',
    width: '20%'
},
  text: {
    fontSize: 18,
    fontWeight: '400',
    color: APP_GREY_TEXT_COLOR,
    textAlign: 'center',
  },
  light_text: {
    fontSize: 15,
    fontWeight: '400',
    color: App_MIDIUM_GREY_COLOR,
    textAlign: 'center',
  },
  gradient: {
    bottom:0,
    position:'absolute',
    height: 5,
    backgroundColor: 'red',
  }
})

export const bottomShadowVwStyle = {
  position: 'absolute',
  bottom: 5,
  backgroundColor: 'green',
  height: 0,
  width: '100%',
  shadowColor: APP_SHADOW_COLOR,
  shadowOffset: { width: 0, height: 7 },
  shadowRadius: 7,
  shadowOpacity: 1.0,
  elevation: 7,
}
