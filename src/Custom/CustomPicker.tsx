import React from 'react';
import { Picker } from 'react-native';

export const CustomPicker = (props: any) => {

    console.log('Props in Custom Picker >>>', props)
    return (
        <Picker
        style={props.style}
        selectedValue={'s'}
            onValueChange={(itemValue, itemIndex) => props.func(itemValue)} mode='dropdown'>
            {props.data.map((item: any, index: number) => (
                <Picker.Item label={item} value={item} />
            ))}
        </Picker>
    )
}

export const CustomPickerAndroid = (props: any) => {
    //ToastAndroid.show(JSON.stringify(!props.disabled),1000)
    console.log('Props in Custom Picker >>>' + props, props)

    return (
        <Picker
            enabled={!props.disabled}
            style={props.style}
            selectedValue={props.value != '' ? props.value : props.placeholder}
            onValueChange={(itemValue, itemIndex) => props.func(itemValue, itemIndex)} mode='dropdown'>
            {props.data.map((item: any, index: number) => (
                <Picker.Item label={item} value={item} />
            ))}
        </Picker>
    )
}
export const CustomPickerIOS = (props: any) => {
    console.log('Props in Custom Picker >>>' + props, props)

    return (
        <Picker
        style={props.style}
        selectedValue={'s'}
            onValueChange={(itemValue, itemIndex) => props.func(itemValue, itemIndex)} mode='dropdown'>
            {props.data.map((item: any, index: number) => (
                <Picker.Item label={item} value={item} />
            ))}
        </Picker>
    )
}
