
import { SafeAreaView, ScrollView, View, Text, Image, TouchableOpacity } from 'react-native';
import React, { Component } from 'react';
import { NavigationScreenProp } from "react-navigation";
import { DrawerItems } from 'react-navigation-drawer';
import { DrawerNavigatorItems, DrawerView } from 'react-navigation-drawer';
//@ts-ignore
import { connect } from 'react-redux';
import { APP_HEADER_BACK_COLOR, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, App_MIDIUM_GREY_COLOR, APP_DARK_GREY_COLOR } from '../Constants';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { thisExpression } from '@babel/types';
import { localize } from '../Utils/LocalisedManager';

export interface Props {
  navigation: NavigationScreenProp<any, any>
  name: '',
  email: '',
  avatarSource: '',
};

class DrawerComponent extends Component<Props>
{

  logout() {
    this.props.navigation.navigate('Signout')
  }

  myProfileAction() {
    this.props.navigation.closeDrawer()

    setTimeout(() => {
      this.props.navigation.navigate('Profile')
    }, 50);
  }

  revealOptionSelected(item: any) {
    this.props.navigation.closeDrawer()

    // console.log('This props >>>', this.props)
    // console.log('Item in revealOptionSelected  >>>', item)

    setTimeout(() => {
      if (item.routeName == 'Home') {
        // item.index = 0
        // item.routes[0].index = 0
         this.props.navigation.navigate('Homes')

        // console.log('This props after>>>', this.props)
        // console.log('Item in revealOptionSelected  after>>>', item)
      }
      else if (item.routeName == 'Reports') {
        this.props.navigation.navigate('Reports')
      }
      else if (item.routeName == 'Chats') {
        // item.index = 0
        // item.routes[0].index = 0
        // item.routes[1].index = 0
        // item.routes[2].index = 0
        // item.routes[3].index = 0
        this.props.navigation.navigate('Chatss')

        // console.log('This props after>>>', this.props)
        // console.log('Item in revealOptionSelected after>>>', item)
      }
      else if (item.routeName == 'Subscription') {
        this.props.navigation.navigate('Subscription')
      }
      else if (item.routeName == 'Settings') {
        this.props.navigation.navigate('Settings')
      }
      else if (item.routeName == 'TermsAndConditions') {
        this.props.navigation.navigate('TermsAndConditions')
      }
      else if (item.routeName == 'FAQ') {
        this.props.navigation.navigate('FAQ')
      }

      else if (item.routeName == 'Signout') {
        this.props.navigation.navigate('Signout')
      }
    }, 200);
  }

  render() {

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.topContainer}>
          <View style={styles.imgVw}>
            {this.props.avatarSource != '' ?
              <Image
                source={{ uri: this.props.avatarSource }}
                style={styles.img}
                resizeMode='cover' /> :
              <Image
                source={require('../assets/avatar.png')}
                style={[styles.img, { backgroundColor: 'transparent' }]}
                resizeMode='cover' />}
          </View>
          <Text style={styles.nameTxt}>{this.props.name}</Text>
          <Text style={styles.emailTxt}>{this.props.email}</Text>
          <TouchableOpacity
            style={styles.myProfileBtn}
            onPress={() => this.myProfileAction()}>
            <Text style={{ color: APP_GREEN_COLOR, textAlign: 'center', fontWeight: '600' }}>{localize.myProfile}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.blue_container} >
          <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
            {this.props.items.map((item: any, index: any) => {
              ///console.log('Item  Image>>>>', item.params.image)
              return item.routeName != 'Signout' && item.routeName != 'Profile' ?
                <TouchableOpacity key={index} onPress={() => this.revealOptionSelected(item)} style={{ flexDirection: 'row', marginLeft: 16, alignItems: 'center', height: 50, marginRight: 16, backgroundColor: 'white' }}>
                  <Image
                    source={item.params.image}
                    style={[styles.revealOptionImage, { tintColor: '#333333' }]} />
                  <Text style={styles.revealOptionText}>{item.params.name}</Text>
                </TouchableOpacity> : null
            }
            )}
            {/* <DrawerItems {...this.props} /> */}
            <TouchableOpacity onPress={() => this.logout()} style={{ flexDirection: 'row', marginLeft: 16, alignItems: 'center', height: 50, marginRight: 16, backgroundColor: 'white' }}>
              <Image
                source={require('../assets/sign-out.png')}
                style={styles.revealOptionImage} />
              <Text style={[styles.revealOptionText, { color: APP_GREEN_COLOR }]}>{localize.signOut}</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: any) => ({
  name: state.saveLoginInfo.name,
  email: state.saveLoginInfo.email,
  avatarSource: state.saveLoginInfo.image,
});

export default connect(mapStateToProps)(DrawerComponent);

export const styles = ScaleSheet.create({
  blue_container: {
    backgroundColor: APP_HEADER_BACK_COLOR
  },
  container: {
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  topContainer: {
    alignItems: 'center',
    backgroundColor: APP_HEADER_BACK_COLOR,
    justifyContent: 'center'
  },
  imgVw: {
    marginTop: 30,
    overflow: 'hidden',
    height: 80,
    width: 80,
    borderRadius: 40,
    backgroundColor: 'black',
    borderColor: 'black'
  },
  img: {
    height: 100,
    width: 100,
    borderRadius: 50,
    borderColor: 'black',
    backgroundColor: 'transparent'
  },
  revealOptionImage: {
    height: 20,
    width: 30,
    resizeMode: 'contain',
  },
  revealOptionText: {
    marginLeft: 8,
    fontWeight: '500',
    color: App_MIDIUM_GREY_COLOR,
    fontSize: 16,
    // backgroundColor: 'red',
    flex: 1
  },
  nameTxt: {
    lineHeight: 22,
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 8
  },
  emailTxt: {
    lineHeight: 20,
    color: 'white',
    fontSize: 14,
    marginTop: 4
  },
  myProfileBtn: {
    width: 120,
    height: 30,
    backgroundColor: 'white',
    borderRadius: 15,
    justifyContent: 'center',
    marginTop: 24,
    marginBottom: 30,
  },
});
