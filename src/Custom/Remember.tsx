import React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';

export const Remember = (props: any) => {

  return (
    <View style={styles.rememberContainer}>
      <TouchableOpacity onPress={() => props.func()}>
        <Image
          style={styles.smallIcon}
          source={props.checked ? require('../../assets_new/check.png') : require('../../assets_new/uncheck.png')}
        />
      </TouchableOpacity>
      <Text style={styles.greyTxt}>{props.title}</Text>
    </View>
  )
}

const styles = ScaleSheet.create({
  //Button
  rememberContainer: {
    flexDirection: 'row',
    marginTop: 0,
    paddingRight: 20,
    backgroundColor: 'transparent',
    alignItems: 'center',
    height: 30,
    width: '80%',
  },
  smallIcon: {
    height: 20,
    width: 20,
  },
  greyTxt: {
    marginLeft: 10,
    color: 'white',
    fontSize: 14,

  },
})