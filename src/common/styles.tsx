import { Dimensions } from "react-native";
import { APP_SHADOW_COLOR } from '../res/colors';
import ScaleSheet from 'react-native-scalesheet';

export const commonShadowStyle = {
    shadowColor: APP_SHADOW_COLOR,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    elevation: 3,
}




export const navHdrTxtStyle = ScaleSheet.create({
    style: {
        textAlign: 'center',
        color: '#A2A2A2',
        fontSize: 18,
          width: Dimensions.get('screen').width - 140,
       // width: 40,
    }
})

export const headerTitleStyle = {
    alignSelf: 'center',
    textAlign: "center",
    justifyContent: 'center',
    flex: 1,
    textAlignVertical: 'center',
}

export const headerStyle = ScaleSheet.create({
    style: {
        height: 50,
        textAlign: 'center',
        justifyContent: 'center',
    },
})