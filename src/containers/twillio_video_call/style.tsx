//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import * as color from "../../res/colors"

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        padding:10
    },
    room: {
        flex: 1,
        backgroundColor: color.APP_THEME_BLUE
    },
    preview: {
        width:100,
        height:100,
        color: color.TEXT_COLOR_GRAY_DARK,
    },
    optionsContainer:{
        flexDirection:'row',
        backgroundColor: 'white',
        height:100,
        alignItems:'center',
        
        
    },
    optionButton:{
        width:60,
        height:60,
        // padding:10,
        alignItems:'center',
        color: color.APP_THEME_BLUE,
        borderWidth:1,
        borderRadius:35,
        justifyContent : 'center',
        marginRight:10
    },
    callContainer:{
       flex:1,
    },remoteVideo:{
        // width:100,
        // height:100,
        flex:1,
        zIndex:1
    },remoteGrid:{
        flex:1,
    },localVideo:{
        width:100,
        height:100,
        backgroundColor:'white',
        zIndex:1,
        position:'absolute',
        right:0
    },callButton:{
        width:'100%',
        padding:10,
        alignItems:'center',
        marginTop:10,
        backgroundColor: color.APP_THEME_GREEN,
    },call:{
        color:'white',
        fontSize:14,
        
    }



});