import React, { Component } from 'react';
import { Text, View, TextInput, TouchableOpacity, Button, PermissionsAndroid, Image } from 'react-native'
import { NavigationScreenProp } from 'react-navigation';


import {
    TwilioVideoLocalView,
    TwilioVideoParticipantView,
    TwilioVideo
  } from 'react-native-twilio-video-webrtc'
import { styles } from './style';
//import { ControlButton } from './TWIVideo.ControllButton';
import { API_GET_TOKEN } from '../../Utils/APIs';
// import LocalDataManager from '../../utils/LocalDataManager';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import AsyncStorage from '@react-native-community/async-storage'
import LocalDataManager from '../../Utils/LocalDataManager';


export interface props {
    navigation: NavigationScreenProp<any, any>
    // loginInfo: any,
};
class TWIVideoView extends Component<props, object> {

    state = {
        isAudioEnabled: true,
        isVideoEnabled: true,
        status: 'disconnected',
        participants: new Map(),
        videoTracks: new Map(),
        token: ''
    }

    componentDidMount() {
        console.log("Twillio Video ref > ",this.refs.twilioVideo);
        this.requestAudioRecordPermission()
        
    }
  
    getParams(data:any){
      console.log('DATA ROOM : ', data.replace('"', ''))
        return {
            identity:"Rakesh1",
            // roomName:"jshdfjkhasdfjh"
            roomName:data.replace('"', '')
        } 
    }
    getBookingID=()=>{
        console.log('get Booking ID')
        AsyncStorage.getItem('videoCallInfo').then((data: any) => { 
            console.log('Booking ID Retrived from Storage success >>>>', data)
            if (data != null ) {
               this.getToken(JSON.parse(data))
            }
        })
    }


    getToken=(data:any)=>{
      // this.setState({token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzIyNzEyZTdhZTM4ZjhhODVmMmQxMGFlMGZkZTU1NDU0LTE2MDcwMDAyNjkiLCJncmFudHMiOnsiaWRlbnRpdHkiOiJyYWtlc2gxMSIsInZpZGVvIjp7InJvb20iOiI3NTEzMiJ9fSwiaWF0IjoxNjA3MDAwMjY5LCJleHAiOjE2MDcwMTQ2NjksImlzcyI6IlNLMjI3MTJlN2FlMzhmOGE4NWYyZDEwYWUwZmRlNTU0NTQiLCJzdWIiOiJBQzJjMmVjMGNhNTllY2U2NWViNjgwYzNmMTI2ZjU5NmNlIn0.8-QJvv3Ed5vfUHlmtORgch97RXT-TJ-d2jhGfQ0jsJ0"})
      // this._onConnectButtonPress("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzIyNzEyZTdhZTM4ZjhhODVmMmQxMGFlMGZkZTU1NDU0LTE2MDc1ODYyMDgiLCJncmFudHMiOnsiaWRlbnRpdHkiOiJSYWtlc2gxIiwidmlkZW8iOnsicm9vbSI6Ijc1MTMyIn19LCJpYXQiOjE2MDc1ODYyMDgsImV4cCI6MTYwNzYwMDYwOCwiaXNzIjoiU0syMjcxMmU3YWUzOGY4YTg1ZjJkMTBhZTBmZGU1NTQ1NCIsInN1YiI6IkFDMmMyZWMwY2E1OWVjZTY1ZWI2ODBjM2YxMjZmNTk2Y2UifQ._sRQdTKbEB-oqkj70-qnSGeD7S8Cn2VKqSQqlor5z8k")     
        RequestManager.getRequestToken(API_GET_TOKEN, this.getParams(data))
        .then((response: any) => {
            console.log('REsponse of Get Token API >>>>', response);
             this.setState({token:response})
             return response
        })
        .then((token) => {
          this._onConnectButtonPress(token)
        })
        .catch((error: any) => {
            this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
            General.showErroMsg(this, error)
        })
    }

    requestAudioRecordPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                {
                    title: "Mediport app Record Audio permission",
                    message:
                        "Mediport App needs access to your Record Audio ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the camera");
                this.requestCameraPermission()
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    };
    requestCameraPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "Mediport app Camera permission",
                    message:
                        "Mediport App needs access to your camera ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the camera");

                this.getBookingID()
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    };


    _onConnectButtonPress = (token : any) => {
        console.log(this.refs.twilioVideo);
        // this.refs.twilioVideo.connect({ accessToken: this.state.token })
        this.refs.twilioVideo.connect({ accessToken: token })
        // this.setState({ status: 'connecting' })
        this.setState({ status: 'connected' })
    }

    _onEndButtonPress = () => {
        this.refs.twilioVideo.disconnect()
        // this.refs.twilioVideo.current.disconnect()

        LocalDataManager.saveDataAsyncStorage('shouldAskForVideoCall', JSON.stringify(false))
        AsyncStorage.removeItem('shouldAskForVideoCall')
        AsyncStorage.removeItem('videoCallInfo')
        
        //Self Disconnected
        this.setState({ status: 'disconnected' })
        this.props.navigation.goBack()
    }

    _onMuteButtonPress = () => {
      // this.refs.twilioVideo.setLocalVideoEnabled
        this.refs.twilioVideo.setLocalAudioEnabled(!this.state.isAudioEnabled)
            .then(isEnabled => this.setState({ isAudioEnabled: isEnabled }))
    }

    _onFlipButtonPress = () => {
        this.refs.twilioVideo.flipCamera()
    }

    _onRoomDidConnect = ({ roomName, error }) => {
        console.log('onRoomDidConnect: ', roomName);

        this.setState({ status: 'connected' });
    };

    _onRoomDidDisconnect = ({ roomName, error }) => {
        console.log("ERROR: _onRoomDidDisconnect ", error)
 
        this.setState({ status: 'disconnected' })
        AsyncStorage.removeItem('shouldAskForVideoCall')
        AsyncStorage.removeItem('videoCallInfo')
        this.props.navigation.goBack()
    }

    _onRoomDidFailToConnect = (error) => {
        console.log("ERROR: ", error)

        this.setState({ status: 'disconnected' })
    }

    _onParticipantAddedVideoTrack = ({ participant, track }) => {
        console.log("onParticipantAddedVideoTrack: ", participant, track)

        this.setState({
            videoTracks: new Map([
                ...this.state.videoTracks,
                [track.trackSid, { participantSid: participant.sid, videoTrackSid: track.trackSid }]
            ]),
        });
    }

    _onParticipantRemovedVideoTrack = ({ participant, track }) => {
        console.log("onParticipantRemovedVideoTrack: ", participant, track)

        const videoTracks = this.state.videoTracks
        videoTracks.delete(track.trackSid)
        this.refs.twilioVideo.disconnect()
        this.setState({ videoTracks: { ...videoTracks } ,status: 'disconnected'})
        AsyncStorage.removeItem('shouldAskForVideoCall')
        AsyncStorage.removeItem('videoCallInfo')
        this.props.navigation.goBack()
                
    }


    _onParticipantDisconnected = (room) =>{
      console.log("_onParticipantDisconnected: ", room)

      // const videoTracks = this.state.videoTracks
      // videoTracks.delete(remoteParticipant.trackSid)
      // this.refs.twilioVideo.disconnect()
      // this.setState({ status: 'disconnected'})
      // this.props.navigation.goBack()
    }

    render() {
        return (
            <View style={styles.container}>
            {
              this.state.status === 'disconnected' &&
              <View>
             
               
                {/* <Button
                  title="Connect"
                  style={styles.button}
                  onPress={this.getToken}
                //   onPress={this._onConnectButtonPress}
                  >
                </Button> */}
              </View>
            }
    
            {
              (this.state.status === 'connected' || this.state.status === 'connecting') &&
              <View style={[styles.callContainer]}>
                {
                  this.state.status === 'connected' &&
                  <View style={[styles.remoteGrid]}>
                    {
                      Array.from(this.state.videoTracks, ([trackSid, trackIdentifier]) => {
                        return (
                          <TwilioVideoParticipantView
                            style={[styles.remoteVideo]}
                            key={trackSid}
                            trackIdentifier={trackIdentifier}
                          />
                        )
                      })
                    }
                  </View>
                }
                <View
                  style={styles.optionsContainer}>
                  <TouchableOpacity
                    style={styles.optionButton}
                    onPress={this._onEndButtonPress}>
                    <Image style={{ width:40 , height: 40 }} source={ require('../../res/drawable/end.png')} />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.optionButton}
                    onPress={this._onMuteButtonPress}>
                      <Image style={{ width:30 , height: 30 }} source={this.state.isAudioEnabled ? require('../../res/drawable/unmute.png') : require('../../res/drawable/mute.png')} />
                    {/* <Text style={{ fontSize: 12 }}>{this.state.isAudioEnabled ? "Mute" : "Unmute"}</Text> */}
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.optionButton}
                    onPress={this._onFlipButtonPress}>
                    <Image style={{ width:30 , height: 30 }} source={ require('../../res/drawable/flip.png')} />
                  </TouchableOpacity>
                  
                  <TwilioVideoLocalView
                    enabled={true}
                    style={[styles.localVideo, { backgroundColor: "green" }]}
                  />
                </View>
              </View>
            }
    
            <TwilioVideo
              ref="twilioVideo"
              onRoomDidConnect={this._onRoomDidConnect}
              onRoomDidDisconnect={this._onRoomDidDisconnect}
              onRoomDidFailToConnect={this._onRoomDidFailToConnect}
              onParticipantAddedVideoTrack={this._onParticipantAddedVideoTrack}
              onParticipantRemovedVideoTrack={this._onParticipantRemovedVideoTrack}
             // onParticipantDisconnected = {this._onParticipantDisconnected()}
            />
          </View>
        );
    }

}

export default TWIVideoView