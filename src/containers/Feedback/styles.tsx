//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_LIGHT_GREY_COLOR, App_MIDIUM_GREY_COLOR} from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,        
        backgroundColor: APP_GREY_BACK,
        padding: 8,
        alignItems:'center'
    },
    image: {
        marginTop: 30,
        height: 100,
        width: '100%',
        resizeMode: 'contain',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontSize: 14,
        fontWeight: '400',
        color: App_MIDIUM_GREY_COLOR,
        textAlign: 'center',
        marginTop: 8,
        marginBottom : 16,
        width: '100%',
      },
      smallImg: {
        width: 24,
        height: 24,
        marginLeft: 8,
        marginright: 8
    },
    lightTxt: {
        textAlign: 'left',
        color: App_MIDIUM_GREY_COLOR,
        alignSelf: 'center',
        marginLeft: -8,
    },
});