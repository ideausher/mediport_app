import React, { Component } from 'react';
import { View, Text, Image, KeyboardAvoidingView, Modal, Platform, Alert, NativeModules, StatusBarIOS } from 'react-native';
import { APP_MEDIUM_GREY_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../../src/Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles'
import { CommonBtn, GreenTextBtn } from '../../Custom/CustomButton';
import { TextInputMultiLine } from '../../Custom/CustomTextInput';
import { localize } from '../../Utils/LocalisedManager';
import { AirbnbRating } from 'react-native-ratings';
import { Loader } from '../../Utils/Loader';
import RequestManager from '../../Utils/RequestManager'
import { API_ADD_REVIEW } from '../../Utils/APIs';
import { CheckBox } from 'react-native-elements'
import Toast from 'react-native-simple-toast';
import General from '../../Utils/General';

const { StatusBarManager } = NativeModules;

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class Feedback extends Component<props, object> {
    constructor(props: any) {
        super(props)
        this.ratingCompleted = this.ratingCompleted.bind(this)
    }
    state = {
        isImageSeleted: false,
        showLoader: false,
        feedback: '',
        rating: 1,
        isDoctorSuggested: false,
        statusBarHeight: 0,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.feedback}</Text>
                </View>
            ),
            headerLeft: null,
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }
    }

    getApiParams() {

        return {
            booking_id: this.props.navigation.getParam('vendorId'),
            rating: this.state.rating,
            feedback_message: this.state.feedback,
            is_like: this.state.isDoctorSuggested ? 1 : 0,
        }
    }
    ratingCompleted(__rating: any) {

        this.setState({ rating: __rating })

    }
    updateLikeUnlike() {

        var updated = !this.state.isImageSeleted;
        this.setState({ isImageSeleted: updated })
    }

    isValidFields() {

        if (this.state.feedback.length == 0) {
            Alert.alert(localize.feedbackMessageCantBeEmpty)
            return false;
        }
        //  else if (this.state.feedback.length < 2) {
        //     Alert.alert('Please enter atleast 250 charcter')
        //     return false;
        // }

        return true;
    }

    sumbitRating() {

        if (!this.isValidFields()) {
            return;
        }

        this.setState({ showLoader: true })
        console.log('Params >>>>', this.getApiParams())
        RequestManager.postRequestWithHeaders(API_ADD_REVIEW, this.getApiParams())
            .then((response: any) => {

                console.log('REsponse of sumbitRating API >>>>', response);
                this.setState({ showLoader: false })

                setTimeout(() => {
                    Alert.alert(response.message)
                    this.props.navigation.navigate('Home')
                }, 200);
            }).catch((error: any) => {
                General.showErroMsg(this, error)
            })

    }

    noThanksBtnAction() {
        this.props.navigation.goBack()
        
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                        <Image style={styles.image} source={require('../../assets/skin-care.png')} />
                        <Text style={[styles.text, { color: APP_MEDIUM_GREY_COLOR, marginBottom: 16, fontSize: 15 }]}>Dr. Gyana</Text>
                        <Text style={styles.text}>{localize.howWasThe} Gyana {localize.behaviour}?</Text>
                        <AirbnbRating
                            style={{ paddingVertical: 10 }}
                            showRating={false}
                            onFinishRating={this.ratingCompleted}
                            size={22} />
                        <TextInputMultiLine placeholder={localize.pleaseGiveFeedback} onChange={(text: any) => this.setState({ feedback: text })} value={this.state.feedback} autoCapitalize='none' width='75%' height={150} />
                        <View style={{ flexDirection: 'row', marginBottom: -20 }}>
                            <CheckBox
                                checkedIcon={<Image source={require('../../assets/checked.png')} />}
                                uncheckedIcon={<Image source={require('../../assets/unchecked.png')} />}
                                checked={this.state.isDoctorSuggested}
                                onPress={() => this.setState({ isDoctorSuggested: !this.state.isDoctorSuggested })}
                            />
                            <Text style={styles.lightTxt}>{localize.suggestDoctor}</Text>
                        </View>
                        <CommonBtn title={localize.submitRating} func={() => this.sumbitRating()} width='75%' />
                        <GreenTextBtn title={localize.noThanks} func={() => this.noThanksBtnAction()} textAlign='center' marginTop={-8} width={150} />

                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}
