//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREEN_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_HEADER_TITLE_COLOR, App_MIDIUM_GREY_COLOR, APP_MEDIUM_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        // padding: 8,
        // alignItems: 'center'
    },
    scrollContainer: {
        padding: 8,
        alignItems: 'center',
    },
    popupVw: {
        borderRadius: 10,
        backgroundColor: 'white',
        padding: 16,
        height: '50%',
        margin: 16,
        alignItems: 'center'
    },
    imgContainer: {
        backgroundColor: 'rgba(66, 202, 162, 0.7)',
        marginTop: 60,
        height: 62,
        width: 62,
        borderRadius: 31,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        height: 24,
        width: 24,
        resizeMode: 'contain',
    },
    signInTxt: {
        fontSize: 20,
        fontWeight: "500",
        textAlign: 'center',
        marginTop: 8,

    },
    text: {
        fontSize: 16,
        fontWeight: "500",
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        marginTop: 8,
    },
    greyText: {
        fontSize: 16,
        fontWeight: "500",
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'center',
    },
    greyTextPopup: {
        marginTop: 20,
        fontSize: 16,
        color: APP_MEDIUM_GREY_COLOR,
        textAlign: 'center',
        width: '60%',
        marginBottom: '30'
    },
    greyLine: {
        width: '35%',
        height: 1,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: APP_GREY_TEXT_COLOR,
    },
    greenTextBtn: {
        justifyContent: 'center',
        width: 120,
        height: 30,
    },
    greenText: {
        color: APP_GREEN_COLOR,
        fontSize: 14,
        fontWeight: "500",
        marginLeft: 4
    }
});