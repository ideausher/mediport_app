import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, Alert, Modal, Keyboard, ScrollView,StatusBar } from 'react-native';
import { APP_HEADER_BACK_COLOR, commonShadowStyle, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { TextInputWithoutImage, TextInputWithVisibility } from '../../Custom/CustomTextInput';
import { CommonBtn } from '../../Custom/CustomButton';
//@ts-ignore
import validator from 'validator';
import { Loader } from '../../Utils/Loader';
import RequestManager from '../../Utils/RequestManager'
//@ts-ignore
import { ConfirmDialog } from 'react-native-simple-dialogs';
import { API_UPDATE_FORGOT_PASSWORD, API_UPDATE_PASSWORD } from '../../Utils/APIs'
import General from '../../Utils/General';
import { BackHeaderBtn } from '../../Custom/CustomComponents';
import { localize } from '../../Utils/LocalisedManager';
import { tsPropertySignature } from '@babel/types';
import Toast from 'react-native-simple-toast';


export interface LoginProps {
    navigation: NavigationScreenProp<any, any>
};

const { StatusBarManager } = NativeModules;

const ChangePswdSuccessPopup = (props: any ) => {
    console.log('Funcccc Called')
    return (
        <View style={[styles.blueConatiner, { justifyContent: 'center', backgroundColor: 'rgba(47, 103, 157, 0.9)', }]}>
            <View style={[styles.popupVw, commonShadowStyle]}>
                <View style={styles.imgContainer}>
                    <Image
                        source={require('../../assets/sucess-check.png')}
                        style={styles.image}
                        resizeMode={'contain'}
                    />
                </View>
                <Text style={styles.greyTextPopup}>{localize.successChangePswdMsg}</Text>
                <CommonBtn title={localize.ok} func={() => props.func()} width='60%' height={45} />
            </View>
        </View>
    )
}

export default class UpdatePassword extends Component<LoginProps, object> {

    state = {
        old_password: '',
        password: '',
        cnfrmPswd: '',
        statusBarHeight: 0,
        showLoader: false,
        isForgotPassword: false,
        showChangePSwdSuccessPopUp: false,
    };

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{navigation.getParam('isForgotPassword') == true ? localize.updatePassword : localize.changePassword}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        var param = this.props.navigation.getParam('params');


        console.log('PArams >>>>>', param);

        const forgotPassword = param['forgotPassword'];
        this.setState({ isForgotPassword: (forgotPassword) })

        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler(), 'isForgotPassword': forgotPassword });
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    isValidateDetail() {
        if (!(this.state.isForgotPassword) && this.state.old_password.length <= 0) {
            Toast.show(localize.pleaseEnterOldPswd, Toast.SHORT);
            return false;
        }
        else if (this.state.password.length <= 0) {
            Toast.show(localize.pleaseEnterPswd, Toast.SHORT);
            return false;
        }
        else if (this.state.password.length < 6) {
            Toast.show(localize.pleaseEnterPswdOf6Digits, Toast.SHORT);
            return false;
        }
        else if (this.state.cnfrmPswd.length <= 0) {
            Toast.show(localize.pleaseEnterCnfmPswd, Toast.SHORT);
            return false;
        }
        else if (this.state.cnfrmPswd != this.state.password) {
            Toast.show(localize.confirmPswdDoesntMatched, Toast.SHORT);
            return false;
        }

        return true;
    }

    resetPassword() {
        Keyboard.dismiss()
        if (this.state.isForgotPassword && this.isValidateDetail()) {

            this.setState({ showLoader: true })
            this.makeForgotPasswordResetRequest()

        } else if (!this.state.isForgotPassword && this.isValidateDetail()) {

            this.setState({ showLoader: true })
            this.makePasswordResetRequest()
        }
    }

    getForgotPasswordParams() {
        var param = this.props.navigation.getParam('params');
        const id = param['id'];
        return {
            password: this.state.password,
            user_id: id
        }
    }

    getResetPasswordParams() {

        return {
            password: this.state.password,
            old_password: this.state.old_password
        }
    }

    makeForgotPasswordResetRequest() {
        RequestManager.putRequest(API_UPDATE_FORGOT_PASSWORD, this.getForgotPasswordParams()).then((response: any) => {
            console.log('REsponse of Send OTP API >>>>', response);

            this.setState({ showLoader: false });

            setTimeout(() => {
                Alert.alert(response.message)
                this.props.navigation.navigate('Login')
            }, 200);

        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    makePasswordResetRequest() {
        RequestManager.putRequestWithHeaders(API_UPDATE_PASSWORD, this.getResetPasswordParams()).then((response: any) => {
            console.log('Response of makePasswordResetRequest >>>>', response);
            this.setState({ showLoader: false })

            setTimeout(() => {
                this.setState({ old_password: '', password: '', cnfrmPswd: '', showChangePSwdSuccessPopUp: true })
            }, 800);

        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    okClicked() {
        this.setState({ showChangePSwdSuccessPopUp: false })
        this.props.navigation.goBack();
    }

    render() {
        var old_password_visibility = !this.state.isForgotPassword;
        return (

            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showChangePSwdSuccessPopUp}>
                    <ChangePswdSuccessPopup func={() => this.okClicked()} />
                </Modal>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }} scrollEnabled={false}>
                            <View style={styles.scrollContainer}>
                                <TextInputWithVisibility placeholder={localize.oldPassword} show={old_password_visibility} onChange={(text: any) => this.setState({ old_password: text })} value={this.state.old_password} autoCapitalize='none' secureTextEntry={true} />
                                <TextInputWithoutImage placeholder={this.state.isForgotPassword ? localize.updatePassword :localize.changePassword} onChange={(text: any) => this.setState({ password: text })} value={this.state.password} autoCapitalize='none' secureTextEntry={true} />
                                <TextInputWithoutImage placeholder={localize.retypePassword} onChange={(text: any) => this.setState({ cnfrmPswd: text })} value={this.state.cnfrmPswd} autoCapitalize='none' secureTextEntry={true} />

                                <CommonBtn   marginTop={40} title={localize.save} func={() => this.resetPassword()} width='40%'   />
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}
