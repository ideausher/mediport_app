import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity, Image, SafeAreaView } from 'react-native';
import { APP_HEADER_TITLE_COLOR, APP_HEADER_BACK_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { TextInputWithoutBorder } from '../../Custom/CustomTextInput';
import { LineVw, BackHeaderBtn } from '../../Custom/CustomComponents';
import { localize } from '../../Utils/LocalisedManager';

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class ChangePswd extends Component<props, object> {

    state = {
        oldPswd: '',
        newPswd: '',
        cnfrmPswd: ''
    }
    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.changePassword}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                        <TextInputWithoutBorder placeholder={localize.oldPassword} onChange={(text: any) => this.setState({ oldPswd: text })} value={this.state.oldPswd} autoCapitalize='none' />
                        <LineVw />
                        <TextInputWithoutBorder placeholder={localize.changePassword}onChange={(text: any) => this.setState({ newPswd: text })} value={this.state.newPswd} autoCapitalize='none' />
                        <LineVw />
                        <TextInputWithoutBorder placeholder={localize.retypePassword} onChange={(text: any) => this.setState({ cnfrmPswd: text })} value={this.state.oldPswd} autoCapitalize='none' />
                        <LineVw />
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}