//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, App_MIDIUM_GREY_COLOR, APP_LIGHT_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        padding: 16,
    },
    quesText: {
        marginLeft: 16,
        fontSize: 18,
        fontWeight: '400',
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'center',
        marginTop: 8,
        marginBottom:8,
    },
    ansText: {
        fontSize: 13,
        fontWeight:'400',
        color: App_MIDIUM_GREY_COLOR,
        textAlign: 'left',
        marginTop: 8,
        marginBottom:8,
        lineHeight:18
    },

});