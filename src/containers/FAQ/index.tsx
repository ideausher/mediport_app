import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity, Image, KeyboardAvoidingView, Platform, Modal, ToastAndroid } from 'react-native';
import { APP_HEADER_TITLE_COLOR, APP_HEADER_BACK_COLOR, APP_LIGHT_GREY_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
//import { Content } from './Constants'
import { styles } from './styles';
import { isTemplateElement } from '@babel/types';
import { localize } from '../../Utils/LocalisedManager';
import { OpenDrawerHeaderBtn, NoInternetFoundView } from '../../Custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager'
import { API_GET_FAQ } from '../../Utils/APIs';
import { Loader } from '../../Utils/Loader';
import General from '../../Utils/General';


const AnswerView = (props: any) => {

    console.log('Is expandec >>>>', props.item.isExpanded)
    if (props.item.isExpanded) {
        return (
            <View style={{ width: '100%' }}>
                <Text style={styles.ansText}>{props.item.answer}</Text>

                <View style={{ height: 0.2, alignSelf: 'center', backgroundColor: APP_LIGHT_GREY_COLOR, width: '100%' }}></View>
            </View>
        );
    }

    return null
};

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class FAQ extends Component<props, object> {

    state = {
        showLoader: false,
        content: [] as any[],
        isInternetError: false,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.faq}</Text>
                </View>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {

        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler() });
        this.apiGetFAQ()
    }


    async apiGetFAQ() {
        this.setState({ showLoader: true, isInternetError: false })
        RequestManager.getRequest(API_GET_FAQ, {}).then((response: any) => {

            console.log('Response of FAQ APi >>>', response)
            var arr = response.data;
            for (let i = 0; i < response.data.length; i++) {
                arr[i].isExpanded = false;
            }
            this.setState({ showLoader: false, content: arr })

        }).catch((error: any) => {

            if (error == localize.internetConnectionError) {
                this.setState({ isInternetError: true })
            }
            else {
                General.showErroMsg(this, error)
            }
        })

    }
    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    cellSelected(index: number) {

        let items = this.state.content

        for (var i = 0; i < items.length; i++) {
            if (i == index) {
                items[i].isExpanded = !items[i].isExpanded
            }
            else {
                items[i].isExpanded = false
            }
        }
        this.setState({ content: items })
    }

    render() {
        return (
            <View style={styles.blueConatiner}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    {this.state.isInternetError ?
                        <NoInternetFoundView func={() => this.apiGetFAQ()} />
                        :
                        <View >
                            {this.state.content.map((item: any, index: number) => (
                                <TouchableOpacity onPress={() => this.cellSelected(index)} style={{ alignItems: 'center', width: '100%' }}>
                                    <Text style={styles.quesText}>{item.question}</Text>
                                    <View style={{ height: 0.5, alignSelf: 'center', backgroundColor: APP_LIGHT_GREY_COLOR, width: '100%' }}></View>
                                    <AnswerView item={item} />
                                </TouchableOpacity>
                            )
                            )}
                        </View>
                    }
                </View>
            </View>
        );
    }
}
