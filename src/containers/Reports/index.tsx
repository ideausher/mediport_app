import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList, Modal } from 'react-native';
import { navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import ReportsCell from '../../Custom Cells/ReportsCell';
import { localize } from '../../Utils/LocalisedManager';
import { OpenDrawerHeaderBtn, NoDataFoundView, NoInternetFoundView } from '../../Custom/CustomComponents';
import { API_DOCTOR_REPORTS } from '../../Utils/APIs';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import { Loader } from '../../Utils/Loader';

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class Reports extends Component<props, object> {
    state = {
        page: 0,
        isRefreshing: false,
        loadMore: false,
        isCalled: false,
        prevPage: -1,
        isInternetError: false,
        isDataNotFoundError: false,
        reportsListArr: [] as any[],
        showLoader: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.reports}</Text>
                </View>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }
    componentDidMount() {
        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler() });

        this.setState({ showLoader: true })
        this.apiGetReportsList(false)
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    // Get paarmeters 
    private getParams(refresh: boolean) {

        return {
            page: refresh ? 0 : this.state.page,
            limit: 10
        }
    }

    async apiGetReportsList(refresh: boolean) {
        this.setState({ isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this
        RequestManager.getRequest(API_DOCTOR_REPORTS, this.getParams(refresh)).then((response: any) => {
            console.log('REsponse of API_APPOINTMENT_LIST API >>>>', response);

            let reportsArrData: any[] = []
            if (refresh) {
                weakSelf.setState({ page: 0, prevPage: -1, reportsListArr: [] })
                reportsArrData = response.data
            }
            else {
                reportsArrData = this.state.reportsListArr.concat(response.data)
            }

            setTimeout(() => {
                weakSelf.setState({ showLoader: false, reportsListArr: reportsArrData, loadMore: false, prevPage: this.state.page, isRefreshing: false })

                setTimeout(() => {

                    if (weakSelf.state.reportsListArr.length == 0) {
                        weakSelf.setState({ isInternetError: false, isDataNotFoundError: true })
                    }
                    weakSelf.setState({ isCalled: false })
                }, 200)

                if (response.data.length > 0) {
                    let nextPage = this.state.page + 1
                    weakSelf.setState({ page: nextPage })
                }
            }, 200)
        }).catch((error: any) => {
            this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
            if (error == localize.internetConnectionError) {
                this.setState({ isInternetError: true, isDataNotFoundError: false })
            }
            else {
                General.showErroMsg(this, error)
            }
        })
    }

    private viewReports(index: any) {
        this.props.navigation.navigate('ViewReports', { venderId: this.state.reportsListArr[index].vender_id })
    }

    //Will call when scroll view pulled down to refresh
    private refreshList() {
        this.setState({ isRefreshing: true })
        setTimeout(() => {
            this.apiGetReportsList(true)
        }, 200)
    }

    //Will call when reached last cell
    private loadMoreData = () => {

        if (this.state.prevPage == this.state.page) {
            return
        }
        this.setState({ loadMore: true, isCalled: true })
        setTimeout(() => {
            this.apiGetReportsList(false)
        }, 200)
    }

    private isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }: any) => {
        const paddingToBottom = 50; // how far from the bottom

        let value1 = layoutMeasurement.height + contentOffset.y
        let value2 = contentSize.height - paddingToBottom
        let isVAlue2GreaterThanLayout = value2 > layoutMeasurement.height
        let value1GreterthanValue2 = value1 >= value2
        let isClose = (isVAlue2GreaterThanLayout && value1GreterthanValue2)

        return isClose
    };

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.blueConatiner}>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showLoader}>
                        <Loader />
                    </Modal>
                    <View style={styles.container}>
                        {this.state.reportsListArr.length > 0 ? <FlatList style={{ marginTop: 16, }}
                            onRefresh={() => this.refreshList()}
                            refreshing={this.state.isRefreshing}
                            data={this.state.reportsListArr}
                            keyExtractor={(item: any, index: any) => index.toString()}
                            renderItem={({ item, index }: any) => <ReportsCell
                                onClickEvent={(index: any) => this.viewReports(index)}
                                item={item} index={index} />}
                            onEndReachedThreshold={0.5}
                            onScroll={({ nativeEvent }) => {
                                if (this.state.isCalled == false && this.isCloseToBottom(nativeEvent) && this.state.isRefreshing == false) {
                                    this.loadMoreData()
                                }
                            }}
                        /> : (this.state.showLoader ? null :
                            (this.state.isDataNotFoundError ? <NoDataFoundView /> :
                                (this.state.isInternetError ?
                                    <NoInternetFoundView func={() => this.apiGetReportsList(false)} /> : null)))}
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}