import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity, Image, SafeAreaView, FlatList, NativeModules, Platform, StatusBarIOS } from 'react-native';
import { APP_HEADER_TITLE_COLOR, APP_HEADER_BACK_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle, LOGIN_DATA } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import ChatsCell from '../../Custom Cells/ChatsCell';
import { Content } from './Constants';
import { localize } from '../../Utils/LocalisedManager';
import { OpenDrawerHeaderBtn } from '../../Custom/CustomComponents';
import { API_GET_CHATS } from '../../Utils/APIs';
import RequestManager from '../../Utils/RequestManager'
import General from '../../Utils/General';
//@ts-ignore
import { connect } from 'react-redux';
import LocalDataManager from '../../Utils/LocalDataManager';
import firestore from '@react-native-firebase/firestore';
import { updateReadUnreadStatus } from '../../Firebase/FirestoreHandler';


const { StatusBarManager } = NativeModules;
let loginData : any;

export interface props {
    navigation: NavigationScreenProp<any, any>
    id: any

    notificationsInfo: any
};

class Chats extends Component<props, object> {
    notificationListener: any = undefined

    state = {
        chatsArr: [] as any[],
        showLoader: false,
        isInternetError: false,
        isDataNotFoundError: false,
        isHighlighted: false,
        isCalled: false,
        statusBarHeight: 0,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.chats}</Text>
                </View>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerRight: (
                <TouchableOpacity onPress={navigation.getParam('notificationsHandler')}>
                    <Image style={{ marginRight: 10, height: 20, width: 20, resizeMode: 'contain' }} source={require('../../assets/noti-bell.png')} />
                </TouchableOpacity>
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount = async() => {
        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler() });

        await LocalDataManager.getDataAsyncStorage(LOGIN_DATA).then(data => {
            console.log("Current User Details >>> ", data)
            loginData = data
        });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }


        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': this.state.isHighlighted });


        this.props.navigation.addListener('willFocus', (route) => this.refreshData());

        this.getUsers()
        // this.apiGetChatsList()
    }

    private refreshData() {
        this.getUsers()
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    notificationsHandler() {
        this.props.navigation.navigate('Notifications')
    }

    getUsers() {

        let loginUserId = loginData.id.toString()
        console.log("get User id", loginUserId);
        console.log("get User data", loginData);

        firestore().collection('users').doc(loginUserId).collection('chats').onSnapshot((response) => {

            console.log("user success", response);
            const listData: any[] = [];
            var chatData: any = {};

            response.docs.forEach((doc: any) => {
                chatData = doc.data();
                listData.push(chatData);
            });

            console.log('Users >>>>>', listData)

            this.setState({ isCalled: true, chatsArr: listData })
            //Get Last msg of Each User 

            this.getLastMessageforChatArr(listData)

            // this.getImageforChatArr(listData)
        }, (error) => {
            console.log("user error", error);
            Alert.alert(error.message);
        });
    }

    getLastMessageforChatArr(chatsArr: any) {

        console.log("chatsArr >>>>>", chatsArr)

        chatsArr.forEach((chat: any, index: any) => {

            console.log("chat.chatId >>>>>", chat.chatId)
            
            firestore().collection('messages').doc(chat.chatId.toString()).collection('lastMsg').doc('lastMsg').onSnapshot((response) => {
                
                console.log('getLastMessageforChatId Response >>>>>', response.data())

                let lastMsgData: any = response.data()

                console.log("lastMsgData >>>>>", lastMsgData)

                if (lastMsgData != undefined) {
                    chat['lastMsg'] = lastMsgData.message
                    chat['sentDate'] = lastMsgData.sentDate
                    chat['lastImage'] = lastMsgData.image
                    chat['senderID'] = lastMsgData.senderID
                    chat['status'] = lastMsgData.status
                    chat['chatID'] = chat.chatId

                    let chatsArr = this.state.chatsArr

                    chatsArr[index] = chat

                    this.setState({ chatsArr: chatsArr })
                    //this.apiGetUserDetailsById(chat.id, index)
                }

            }, (error) => {
                Alert.alert(error.message);
            });
        });
    }

    getImageforChatArr(chatsArr: any) {

        console.log("chatsArr >>>>>", chatsArr)

        chatsArr.forEach((chat: any, index: any) => {


        });
    }

    // async apiGetChatsList() {
    //     this.setState({ showLoader: true, isInternetError: false })

    //     console.log('APIS CHats URl >>>', API_GET_CHATS + this.props.id)
    //     RequestManager.getRequest(API_GET_CHATS + '75', {}).then((response: any) => {

    //         console.log('Response of apiGetChatsList APi >>>', response)

    //         this.setState({ showLoader: false, chatsArr: response.data })

    //     }).catch((error: any) => {

    //         if (error == localize.internetConnectionError) {
    //             this.setState({ isInternetError: true })
    //         }
    //         else {
    //             General.showErroMsg(this, error)
    //         }
    //     })
    // }

    cellSelected(index: any) {

        
        console.log("Chat data on cell click >>>>", this.state.chatsArr, index)
        console.log("Chat data on cell click >>>>", this.state.chatsArr[index])
        if (loginData.id != this.state.chatsArr[index].senderID) {
            updateReadUnreadStatus(this.state.chatsArr[index].chatID)
        }


        this.props.navigation.navigate('Messages', { chatData: this.state.chatsArr[index] })
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                        <FlatList
                            style={{ marginTop: 24, width: '100%', marginBottom: 6 }}
                            data={this.state.chatsArr}
                            keyExtractor={(item: any, index: any) => index.toString()}
                            renderItem={({ item, index }) => <ChatsCell
                                onClickEvent={() => this.cellSelected(index)}
                                item={item} />}
                        />
                        {/* {Platform.OS == 'android' ? <BottomShadowView /> : null } */}
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    id: state.saveLoginInfo.id,
  });
  
  export default connect(mapStateToProps)(Chats);