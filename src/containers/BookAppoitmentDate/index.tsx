import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, FlatList, Modal, StatusBar, Dimensions, Alert, I18nManager } from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_GREEN_COLOR, APP_DARK_GREY_COLOR, App_MIDIUM_GREY_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle, commonShadowStyle } from '../../Constants';
import { NavigationScreenProp, ScrollView } from 'react-navigation';
import { styles } from './styles';
import { LineVw, BackHeaderBtn, NoDataFoundView, NoInternetFoundView } from '../../Custom/CustomComponents';
import AppointmentTimeCell from '../../Custom Cells/AppointmentTimeCell';
import { localize } from '../../Utils/LocalisedManager';
import { API_GET_SLOTS } from '../../Utils/APIs';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import { CommonBtn } from '../../Custom/CustomButton';
import moment from 'moment';
import CalendarStrip from 'react-native-calendar-strip';
import _ from 'react-native-google-places';
import { Loader } from '../../Utils/Loader';
import { DATE_TIME_INFO } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { CommonStyles } from '../../Utils/CommonStyles';
import Toast from 'react-native-simple-toast';

const ConfirmPopup = (props: any) => {

    return (
        <View style={[{
            width: '90%',
            height: Math.round(Dimensions.get('screen').height - (Dimensions.get('screen').width * 0.1) - 110),
            position: 'absolute',
            marginTop: (Dimensions.get('screen').width * 0.05),
            marginBottom: (Dimensions.get('screen').width * 0.05),
            borderRadius: 10,
            backgroundColor: 'white',
            padding: 16,
            alignSelf: 'center',
        }, commonShadowStyle]}>
            <Image
                source={require('../../assets/grad-circle.png')}
                style={styles.image}
                resizeMode={'contain'}
            />
            <Text style={styles.greyText}>{localize.confirmMsg}</Text>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 16 }}>
                <View style={{}}>
                    <View style={styles.ratingVw}>
                        <Text style={styles.ratingTxt}>{localize.to}</Text>
                        <Text style={[styles.smallText, { marginTop: 4 }]}>Dr. {props.doctorName}</Text>
                    </View>
                    <View style={styles.ratingVw}>
                        <Text style={styles.ratingTxt}>{localize.date}</Text>
                        <Text style={[styles.smallText, { marginTop: 4 }]}>{moment(props.date, "YYYY-MM-DD").format('DD MMM YYYY')}</Text>
                    </View>

                </View>
                <View style={{}}>
                    <View style={styles.ratingVw}>
                        <Text style={styles.ratingTxt}>{localize.fees}</Text>
                        <Text style={[styles.smallText, { marginTop: 4 }]}>${props.price.toFixed(2)}</Text>
                    </View>
                    <View style={styles.ratingVw}>
                        <Text style={[styles.ratingTxt]}>{localize.time}</Text>
                        <Text style={[styles.smallText, { marginTop: 4 }]}>{props.time}</Text>
                    </View>
                </View>
            </View>
            {/* <View style={CommonStyles.doctorImgCntnrStyle}>
                    {props.image == '' ?
                        <Image
                            source={require('../../assets/avatar.png')}
                            style={CommonStyles.doctorImg} /> :
                        <Image style={CommonStyles.doctorImg} source={{ uri: props.image }} />
                        // <ImageLoad style={CommonStyles.doctorImg} placeholderSource={require('../../assets/avatar.png')} source={{ uri: this.props.doctorInfo.image }} />
                    }
                </View> */}

            
            <CommonBtn title={localize.continue} func={() => props.onClickEvent()} width='80%' position='absolute' bottom={16} />
        </View>
    );
};

export interface props {
    navigation: NavigationScreenProp<any, any>
    saveBookingInfo: any,
    doctorInfo: any,
    bookingInfo: any,
    categoryInfo: any,
};

class BookAppoitmentDate extends Component<props, object> {

    state = {
        datesArr: [],
        timeSlotsArr: [] as any[],
        showConfirmPopup: false,
        showLoader: false,
        selectedDate: new Date() as any,
        selectedSlotId: '',
        selectedTime: '',
        isInternetError: false,
        isDataNotFoundError: false
    }
    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.bookAnAppointment}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });
        this.setState({ selectedDate: moment().format('YYYY-MM-DD') })
        this.apiGetSlots(moment().format('YYYY-MM-DD'))

        this.props.navigation.addListener('willFocus', (route) => { this.setState({ showConfirmPopup: false }) });
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    private getParams(selectedDate: any) {
        return {
            //service_id: this.props.categoryInfo.id,
            vender_id: this.props.doctorInfo.id,
            date: selectedDate
        }
    }

    async apiGetSlots(selectedDate: any) {
        this.setState({ showLoader: true, isInternetError: false, isDataNotFoundError: false })
        RequestManager.getRequest(API_GET_SLOTS, this.getParams(selectedDate)).then((response: any) => {
            console.log('REsponse of API_GET_SLOTS API >>>>', response);
            this.setState({ showLoader: false })
            this.manipulateTimeSlots(response)
        }).catch((error: any) => {
            if (error == localize.internetConnectionError) {
                this.setState({ isInternetError: true, isDataNotFoundError: false })
            }
            else {
                General.showErroMsg(this, error)
            }
        })
    }

    private manipulateTimeSlots(response: any) {
        let slotsArr = response.data
        if (slotsArr.length > 0) {
            // Get Min and max Hour
            let minHour = moment(slotsArr[0].start_time, "HH:mm")
            let maxHour = moment(slotsArr[0].start_time, "HH:mm")

            var timeSlots = [];

            for (var i = 0; i < slotsArr.length; i++) {
                let slot = slotsArr[i]
                let date = moment(slot.start_time, "HH:mm")

                console.log('Slot.start time >>>', slot.start_time)
                console.log('date aftr moment convert >>>', date)

                if (minHour > date) {
                    minHour = date
                }

                if (maxHour < date) {
                    maxHour = date
                }

                slot.start_time = date.format('hh:mm A')
                slot.end_time = date.add(1, 'hour').format('hh:mm A')
                slot.isSelected = false

                // check if Booking count is greater than max count if yes disable that slot
                if (slot.bookings_count >= slot.max_patient) {
                    slot.is_active = "0"
                }
                // If selcted date is Today date then need to check if slot are lessa than or grater than current time
                else if (this.state.selectedDate == moment().format('YYYY-MM-DD')) {
                    var currentTime = moment()

                    // Add Date with Strt time
                    let dateStr = this.state.selectedDate + ' ' + slot.start_time
                    // Now convert String to Momnet Object 
                    let dateMoment = moment(dateStr, 'YYYY-MM-DD hh:mm A')

                    // Check if Curremt time is after dateMoment
                    let isAfter = currentTime.isAfter(dateMoment)

                    console.log('IS AFTERRRRRR >>>', isAfter)

                    slot.is_active = isAfter ? "0" : "1"
                }

                timeSlots.push(slot);
            }

            while (minHour < maxHour) {

                let filteredArr = timeSlots.filter((timeSlot: any) => this.isMatched(timeSlot, minHour.format('hh:mm A')))

                if (filteredArr.length == 0) {
                    timeSlots.push({ start_time: minHour.format('hh:mm A'), end_time: minHour.add(1, 'hour').format('hh:mm A'), is_active: '0', isSelected: false });
                }
                else {
                    minHour.add(1, 'hour');
                }
            }
            console.log('Array before Sorting >>>', timeSlots)
            let sortedSlots = timeSlots.sort((a: any, b: any) => this.sortArr(a, b));
            this.setState({ timeSlotsArr: sortedSlots })
            if (sortedSlots.length == 0) {
                this.setState({ isInternetError: false, isDataNotFoundError: true })
            }
        }
        else {
            this.setState({ timeSlotsArr: slotsArr, isInternetError: false, isDataNotFoundError: true })
        }
    }

    private sortArr(a: any, b: any) {
        let first = moment(a.start_time, "hh:mm A")
        let second = moment(b.start_time, "hh:mm A")

        let firstFormattedStr = first.format('HH:mm')
        let secondFormattedStr = second.format('HH:mm')


        let firstFormattedMoment = moment(firstFormattedStr, "HH:mm")
        let secondFormattedMoment = moment(secondFormattedStr, "HH:mm")

        return firstFormattedMoment.hour() - secondFormattedMoment.hour()
    }

    private isMatched(slot: any, hour: any) {
        return slot.start_time == hour;
    }

    cellSelected(index: any) {
        this.setState({ selectedSlotId: this.state.timeSlotsArr[index].id, selectedTime: this.state.timeSlotsArr[index].start_time })

        this.state.timeSlotsArr.map((slot: any, slotIndex: number) => {

            if (index == slotIndex) {
                slot.isSelected = true
            }
            else {
                slot.isSelected = false
            }
        })
    }

    confirmBtnAction() {

        if (this.state.timeSlotsArr.length == 0) {
            Toast.show(localize.slotsNotAvailable, Toast.SHORT);
        }
        else if (this.state.selectedSlotId == "") {
            Toast.show(localize.pleaseSelectAppointmentTime, Toast.SHORT);
        }
        else {
            this.props.saveBookingInfo({ bookingDate: this.state.selectedDate, slotId: this.state.selectedSlotId, bookingTime: this.state.selectedTime })
            this.setState({ showConfirmPopup: true })
        }
    }

    continueBtnAction() {
        this.props.navigation.navigate('PaymentMethod');
    }

    private daySelected(date: any) {
        this.setState({ showLoader: true, selectedDate: date })
        this.apiGetSlots(date)
    }

    private getMonday() {
        let today = new Date();
        var day = today.getDay(),
            diff = today.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
        return new Date(today.setDate(diff));
    }

    render() {
        const { navigation } = this.props;

        let startDate = this.getMonday()
        let end = new Date()

        let blackDatelist = [{
            start: moment().subtract(7, 'days'), // yesterday
            end: moment().subtract(1, 'days')  // tomorrow
        }];

        return (
            <View style={styles.blueConatiner}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    <Text style={styles.text}>{localize.selectAppointmentDate}</Text>
                    <CalendarStrip
                        // calendarAnimation={{ type: 'sequence', duration: 30 }}
                        style={{ height: 110, paddingTop: 10, paddingBottom: 10 }}
                        calendarHeaderStyle={[styles.text, { marginTop: 0 }]}
                        //calendarColor={'red'}
                        dateNumberStyle={styles.dateText}
                        dateNameStyle={styles.lightText}
                        highlightDateNumberStyle={styles.selectedDateText}
                        highlightDateNameStyle={styles.selectedLightText}
                        disabledDateNameStyle={styles.lightText}
                        disabledDateNumberStyle={styles.disabledDateText}
                        iconContainer={{ flex: 0.1 }}
                        startingDate={this.state.selectedDate}
                        minDate={new Date()}
                        iconLeft={I18nManager.isRTL ? require('../../assets/arow-right.png') : require('../../assets/arow-left.png')}
                        iconLeftStyle={styles.smallImg}
                        iconRight={I18nManager.isRTL ? require('../../assets/arow-left.png') : require('../../assets/arow-right.png')}
                        iconRightStyle={styles.smallImg}
                        onDateSelected={(event: any) => this.daySelected(event.format('YYYY-MM-DD'))}
                        datesBlacklist={blackDatelist}
                    />
                    <LineVw />
                    {this.state.timeSlotsArr.length > 0 ? <Text style={styles.text}>{localize.selectAppointmentTime}</Text> : null}
                    {this.state.timeSlotsArr.length > 0 ? <View style={{ flex: 1, marginBottom: 98 }}>
                        <FlatList
                            style={{ width: '100%', }}
                            data={this.state.timeSlotsArr}
                            keyExtractor={(item: any, index: any) => index.toString()}
                            renderItem={({ item, index }) => <AppointmentTimeCell
                                onClickEvent={(index: any) => this.cellSelected(index)}
                                item={item} index={index} />}
                            numColumns={3}
                        />
                    </View> : (this.state.showLoader ? null :
                        (this.state.isDataNotFoundError ?
                            <NoDataFoundView /> :
                            (this.state.isInternetError ?
                                <NoInternetFoundView func={() => this.apiGetSlots(this.state.selectedDate)} /> : null)))}
                    <CommonBtn title={localize.confirm} func={() => this.confirmBtnAction()} width='70%' position='absolute' bottom={16} />
                </View>
                {this.state.showConfirmPopup ? <ConfirmPopup onClickEvent={() => this.continueBtnAction()} date={this.state.selectedDate} time={this.state.selectedTime} doctorName={this.props.doctorInfo.name} price={this.props.doctorInfo.price} image={this.props.doctorInfo.image} /> : null}
            </View>
        );
    }
}

const mapStateToProps = (state: any) => ({
    doctorInfo: state.saveDoctorInfo,
    bookingInfo: state.saveBookingInfo,
    categoryInfo: state.saveCategoryInfo
});

const mapDispatchToProps = (dispatch: any) => ({
    saveBookingInfo: (info: string) => dispatch({ type: DATE_TIME_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(BookAppoitmentDate);