//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, App_MIDIUM_GREY_COLOR, APP_DARK_GREY_COLOR, App_LIGHT_BLUE_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        padding: 16,
    },
    text: {
        fontSize: 13,
        fontWeight: 'bold',
        color: APP_DARK_GREY_COLOR,
        textAlign: 'left',
        marginTop: 24,
        marginBottom: 16,
        marginLeft: 8
    },
    lightText: {
        fontSize: 10,
        fontWeight: '600',
        color: App_MIDIUM_GREY_COLOR,
        textAlign: 'center',
    },
    selectedLightText: {
        fontSize: 10,
        fontWeight: '600',
        color: APP_HEADER_BACK_COLOR,
        textAlign: 'center',
    },
    dateText: {
        fontSize: 12,
        fontWeight: '600',
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'center',
        borderColor: APP_HEADER_BACK_COLOR,
        borderWidth: 1,
        width: 30,
        height: 30,
        borderRadius: 15,
        marginTop: 8,
        marginBottom: 8,
        lineHeight: 30,
        margin: 8,
    },
    selectedDateText: {
        fontSize: 12,
        fontWeight: '600',
        color: APP_HEADER_BACK_COLOR,
        textAlign: 'center',
        width: 30,
        height: 30,
        borderRadius: 15,
        marginTop: 8,
        marginBottom: 8,
        lineHeight: 30,
        backgroundColor: App_LIGHT_BLUE_COLOR,
        margin: 8,
    },
    disabledDateText: {
        // fontSize: 16,
        // fontWeight: 'bold',
        //color: APP_HEADER_BACK_COLOR,
        //textAlign: 'center',
        width: 30,
        height: 30,
        marginTop: 8,
        marginBottom: 8,
        lineHeight: 30,
        margin: 8,
    },
    smallImg: {
        width: 16,
        height: 16,
    },
    confirmBtn: {
        width: 200,
        height: 40,
        backgroundColor: APP_GREEN_COLOR,
        borderRadius: 20,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 16,
        marginBottom: 16
    },
    popupVw: {
        width: '90%',
        height: Math.round(Dimensions.get('screen').height - (Dimensions.get('screen').width * 0.1) - 110),
        position: 'absolute',
        marginTop: (Dimensions.get('screen').width * 0.05),
        marginBottom: (Dimensions.get('screen').width * 0.05),
        borderRadius: 10,
        backgroundColor: 'white',
        padding: 16,
        alignSelf: 'center'
    },
    image: {
        marginTop: 40,
        width: 100,
        height: 100,
        alignSelf: 'center',
    },
    greyText: {
        fontSize: 18,
        fontWeight: '400',
        color: App_MIDIUM_GREY_COLOR,
        marginTop: 30,
        textAlign: 'center',
    },
    ratingVw: {
        height: 50,
        justifyContent: 'center',
    },
    ratingTxt: {
        fontSize: 12,
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',

    },
    smallText: {
        fontSize: 12,
        color: App_MIDIUM_GREY_COLOR,
        textAlign: 'left',
    },
    doctorImg: {
        width: 60,
        height: 60,
    },
});