import React, { Component } from 'react';
import { View, Text, Modal } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles'
import { CommonBtn, CommonBtnWhite } from '../../Custom/CustomButton';
import { localize } from '../../Utils/LocalisedManager';
import LocalDataManager from '../../Utils/LocalDataManager'
import { commonShadowStyle } from '../../Constants';
import RequestManager from '../../Utils/RequestManager';
import { API_LOGOUT } from '../../Utils/APIs';
import General from '../../Utils/General';
import { Loader } from '../../Utils/Loader';

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class Signout extends Component<props, object> {

    state = {
        showLoader: false
    }

    signoutBtnAction() {
        this.apiLogout()
    }

    async apiLogout() {
        this.setState({ showLoader: true })
        let weakSelf = this
        RequestManager.getRequest(API_LOGOUT, {}).then((response: any) => {
            console.log('REsponse of API_LOGOUT API >>>>', response);
            weakSelf.setState({ showLoader: false })
            setTimeout(function () {
                LocalDataManager.removeAllLocalData()
                weakSelf.props.navigation.navigate('Login')
            }, 200);
            
        }).catch((error: any) => {
            if (error == localize.internetConnectionError) {
                this.setState({ isInternetError: true })
            }
            else {
                General.showErroMsg(this, error)
            }
        })
    }

    cancelBtnAction() {
        this.props.navigation.navigate('Home')
    }

    render() {
        return (
            <View style={styles.blueConatiner}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={[styles.popupVw, commonShadowStyle]}>
                    <Text style={styles.text}>{localize.wantToSignOut}</Text>
                    <CommonBtn title={localize.signOut} func={() => this.signoutBtnAction()} width='80%' />
                    <CommonBtnWhite title={localize.cancel} func={() => this.cancelBtnAction()} width='80%' marginTop={0} />
                </View>
            </View>
        );
    }
}