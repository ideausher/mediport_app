//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK} from '../../Constants'

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: '#146195',
        justifyContent:'center',
        alignItems: 'center'
    },
    popupVw: {
        position: 'absolute',
        margin: 16,
        borderRadius: 10,
        backgroundColor: 'white',
        padding: 16,
        
    },
    text: {
        marginTop: 80,
        marginLeft: 50,
        marginRight: 50,
        marginBottom: 40,
        fontSize: 18,
        fontWeight: '400',
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'center',
        
      },
});