//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREEN_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_HEADER_TITLE_COLOR, APP_LIGHT_GREY_COLOR,App_MIDIUM_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,        
        backgroundColor: APP_GREY_BACK,
        // padding: 8,
        // alignItems: 'center'
    },
    scrollContainer: {
        padding: 8,
        alignItems: 'center',
    },
    text: {
        fontSize: 16,
        fontWeight: "500",
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        marginTop: 8,
    },
    greyText: {
        fontSize: 13,
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        marginTop: 8,
        width: '80%'
    },
    lightGreyText: {
        fontSize: 13,
        color: App_MIDIUM_GREY_COLOR,
        textAlign: 'center',
        width: '60%'
    },
});