import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, Alert, Modal, ScrollView ,StatusBar} from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_GREY_TEXT_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { TextInputWithoutImage, TextInputPhone, TextInputWithDropdown } from '../../Custom/CustomTextInput';
import { CommonBtn } from '../../Custom/CustomButton';
//@ts-ignore
import validator from 'validator';
import { Loader } from '../../Utils/Loader';
import RequestManager from '../../Utils/RequestManager'
import { API_FORGOT_PSWD, API_SENDOTP } from '../../Utils/APIs'
import { VerificationFor, ForGotScreenType } from '../../Utils/Enums';
import { localize } from '../../Utils/LocalisedManager';
//@ts-ignore
import { connect } from 'react-redux';
import { func } from 'prop-types';
import General from '../../Utils/General';
//@ts-ignore
import { CountrySelection } from 'react-native-country-list';
import { BackHeaderBtn } from '../../Custom/CustomComponents';
import Toast from 'react-native-simple-toast';

export interface LoginProps {
    navigation: NavigationScreenProp<any, any>
    id: ''
};

const { StatusBarManager } = NativeModules;

class ForgotPassword extends Component<LoginProps, object> {

    state = {
        phoneNumber: '',
        countryCode: '+91',
        statusBarHeight: 0,
        showLoader: false,
        forgotScreenType: ForGotScreenType.forgotPswd,
        showCodeList: false,
        oldPhoneNumber: '',
        flag: undefined
    };

    static navigationOptions = ({ navigation }: any) => {

        let title = localize.forgotPassword

        if (navigation.getParam('forgotScreenType') == ForGotScreenType.editPhoneNumber) {
            title = localize.phoneNumber
        }

        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{title}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        this.setState({ forgotScreenType: this.props.navigation.getParam('forgotScreenType') })

        if (this.props.navigation.getParam('forgotScreenType') == ForGotScreenType.editPhoneNumber) {
            this.setState({ oldPhoneNumber: this.props.navigation.getParam('params').phone })
            //countryCode: this.props.navigation.getParam('params').countryCode
        }
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    validateDetail() {
        if (this.state.countryCode.length <= 0) {
            Toast.show(localize.pleaseEnterCountryCode, Toast.SHORT);
            return false;
        }
        else if (this.state.phoneNumber.length <= 0) {
            Toast.show(localize.pleaseEnterPhoneNum, Toast.SHORT);
            return false;
        }
        else if (!validator.isMobilePhone(this.state.phoneNumber, 'any', { strictMode: false })) {
            Toast.show(localize.pleaseEnterValidPhoneNum, Toast.SHORT);
            return false;
        }
        else if (this.state.phoneNumber == this.state.oldPhoneNumber) {
            //Alert.alert('New number is same as previous one');
            Toast.show(localize.newNumIsSameAsPrevious, Toast.SHORT);
            return false;
        }

        return true;
    }

    submitRequest() {

        if (this.validateDetail()) {
            this.setState({ showLoader: true })

            if (this.state.forgotScreenType == ForGotScreenType.editPhoneNumber) {
                this.apiSendOTP()
            }
            else {
                this.apiForgotPassword()
            }
        }
    }

    getOTPParams() {
        return {
            phone_country_code: this.state.countryCode,
            phone_number: this.state.phoneNumber,
        }
    }

    apiSendOTP() {

        RequestManager.postRequest(API_SENDOTP, this.getOTPParams(), false).then((response: any) => {
            console.log('REsponse of Send OTP API >>>>', response);

            this.setState({ showLoader: false })

            setTimeout(() => {
                this.props.navigation.navigate('Verification', { params: { id: response.data.id }, verificationFor: VerificationFor.editPhoneNumber, phoneNumber: this.state.countryCode + ' ' + this.state.phoneNumber, func: this.props.navigation.getParam('func') })
            }, 200);
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    getParams() {
        return {
            phone_no: this.state.phoneNumber
        }
    }

    async apiForgotPassword() {

        RequestManager.postRequest(API_FORGOT_PSWD, this.getParams(), false).then((response: any) => {

            console.log('REsponse of apiForgotPassword >>>>', response);

            this.setState({ showLoader: false })
            this.props.navigation.navigate('Verification', { params: { id: response.data.id }, verificationFor: VerificationFor.forgotPswd, phoneNumber: this.state.countryCode + ' ' + this.state.phoneNumber })


        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    // Called to show Country Code picker
    showCountryCodeList() {
        this.setState({ showCodeList: true })
    }

    // Caleld when Country Code is selected and save selected country code in state
    countryCodeSelcted(item: any) {

        this.setState({ countryCode: '+' + item.callingCode, showCodeList: false, flag: item.flag })
    }

    render() {

        let btnTitle = localize.submit
        if (this.state.forgotScreenType == ForGotScreenType.editPhoneNumber) {
            btnTitle = localize.next
        }

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showCodeList}>
                    <View style={{ flex: 1, marginTop: 20 }}>
                        <TouchableOpacity
                            style={{ backgroundColor: 'white' }}
                            onPress={() => this.setState({ showCodeList: false })}>
                            <Text style={{ alignSelf: 'flex-end', lineHeight: 30,  textAlign: 'center', fontSize: 14, color: APP_GREY_TEXT_COLOR,padding:10 }}>{localize.cancel}</Text>
                        </TouchableOpacity>
                        <CountrySelection action={(item: any) => this.countryCodeSelcted(item)} selected={true} />
                    </View>

                </Modal>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }} scrollEnabled={false}>
                        <View style={styles.scrollContainer}>
                            {this.state.forgotScreenType == ForGotScreenType.editPhoneNumber ? <Text style={styles.greyText}>{localize.enterNewPhoneNumber}</Text> : null}
                            <View style={{ width: '80%', flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TextInputWithDropdown func={(text: any) => this.showCountryCodeList()} value={this.state.countryCode} placeholder='+91' img={{ uri: this.state.flag }} />
                                <TextInputPhone placeholder={localize.phoneNumber} onChange={(text: any) => this.setState({ phoneNumber: text })} value={this.state.phoneNumber} />
                            </View>
                            <CommonBtn title={btnTitle} func={() => this.submitRequest()} width='60%' marginTop={40} />
                            <Text style={styles.lightGreyText}>You will receive a OTP to verify your Phone Number</Text>
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    id: state.saveLoginInfo.id,
});

export default connect(mapStateToProps)(ForgotPassword);
