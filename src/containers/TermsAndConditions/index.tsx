import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity, Image } from 'react-native';
import { APP_HEADER_TITLE_COLOR, APP_HEADER_BACK_COLOR, headerTitleStyle, navHdrTxtStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { localize } from '../../Utils/LocalisedManager';
import { OpenDrawerHeaderBtn, NoInternetFoundView } from '../../Custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager';
import { API_TERMS_COND } from '../../Utils/APIs';
import General from '../../Utils/General';

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class TermsAndConditions extends Component<props, object> {

    state = {
        data: '',
        isInternetError: false,
    }
    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.termsConditions}</Text>
                </View>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler() });

        this.apiGetTermsConditions()
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    async apiGetTermsConditions() {
        this.setState({ showLoader: true, isInternetError: false })
        let weakSelf = this
        RequestManager.getRequest(API_TERMS_COND, {}).then((response: any) => {
            console.log('REsponse of apiGetTermsConditions API >>>>', response);
            setTimeout(function () {
                weakSelf.setState({ showLoader: false, data: response.data })
            }, 200);
        }).catch((error: any) => {
            if (error == localize.internetConnectionError) {
                this.setState({ isInternetError: true })
            }
            else {
                General.showErroMsg(this, error)
            }
        })
    }

    render() {
        return (
            <View style={styles.blueConatiner}>
                <View style={styles.container}>
                    {this.state.isInternetError ?
                        <NoInternetFoundView func={() => this.apiGetTermsConditions()} />
                        :
                        <Text style={styles.text}>{this.state.data}</Text>
                    }
                </View>
            </View>
        );
    }
}
