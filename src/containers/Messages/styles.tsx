//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { TEXT_COLOR_GRAY_MEDIUM, TEXT_COLOR_GRAY_LIGHT, APP_THEME_GREEN } from '../../res/colors';
import { Platform } from 'react-native';
//import { APP_FONT_REGULAR } from '../../constants/fonts';
import * as color from '../../res/colors'
import { APP_BLUE_COLOR, APP_DARK_BLUE_COLOR, APP_HEADER_BACK_COLOR } from '../../Constants';


export const styles = ScaleSheet.create({
  blueContainer: {     width:'100%',
  height:'100%', backgroundColor: APP_HEADER_BACK_COLOR, },
  container: {
    // flex: 1,
    width:'100%',
    height:'100%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: color.WHITE_COLOR,
  },
  flatListStyle: {
    // flex: 1, 
    height:'100%',
    width: '100%', marginBottom: (Platform.OS === "ios" ? 58+80 : 68+80),
  },
  textVwCntnr: {
    width: '100%',
    height: (Platform.OS === "ios" ? 50 : 60),
    marginBottom:100,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    zIndex: 1,
  },

  msgTxtInpt: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 10,
    textAlign: 'left',
    height: '100%',
    fontSize: 12,
    //fontFamily: APP_FONT_REGULAR,
    color: TEXT_COLOR_GRAY_MEDIUM,
    // Android only Property
    textAlignVertical: 'top',
    backgroundColor: '#fff',
    borderRadius: 10,
    borderColor: TEXT_COLOR_GRAY_LIGHT,
    borderWidth: 1
  },
  //Button Touch Styles
  sendBtn: {
    width: 35,
    height: '80%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  //Image Styles
  sendImg: {
    height: '80%',
    width: '80%',
    resizeMode: 'contain',
  },
  fullImageContainer: {
    flex: 1,
    //justifyContent: 'center',
    alignItems: 'center',
    padding: 32,
    backgroundColor: "#140F2693",
  },
  crossBtn: {
    // backgroundColor: 'white',
    marginTop: 10,
    marginBottom:5,
    width: 30,
    height: 30,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  crossText: {
    fontSize: 16,
    textAlign: 'center',
    //fontFamily: APP_FONT_REGULAR,
    color: APP_THEME_GREEN,
  },
  fullImage: {
    flex: 1,
    width: "100%",
    backgroundColor: '#717173'
  },
});
