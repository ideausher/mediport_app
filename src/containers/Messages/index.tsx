import React, { Component } from 'react';
import { View, Text, SafeAreaView, Alert, Modal, FlatList, TouchableOpacity, TextInput, Image, KeyboardAvoidingView, Platform, StatusBar, NativeModules, StatusBarIOS, Keyboard, Animated, PermissionsAndroid, AsyncStorage } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { BackHeaderBtn, NoDataFoundView } from '../../Custom/CustomComponents';
import { Loader } from '../../Utils/Loader';
//import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import MessagaCell from '../../Custom Cells/MessagaCell';
import CustomImagePicker from '../../Utils/CustomImagePicker';
import ImageResizer from 'react-native-image-resizer';
import { sendMessage, updateLastMessage } from '../../Firebase/FirestoreHandler';
//@ts-ignore
import { connect } from 'react-redux';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import { AutoId } from '../../Firebase/AutoId';
import FastImage from 'react-native-fast-image';
//import { AVATAR_URL } from '../../constants/api_constants';
import { API_DOCTOR_PROFILE } from '../../Utils/APIs'
import { checkAndAddUser } from '../../Firebase/FirestoreHandler';
import LocalDataManager from '../../Utils/LocalDataManager'
import { headerStyle, headerTitleStyle, LOGIN_DATA, navHdrTxtStyle } from '../../Constants';
import { BackButtonHeader } from '../../Custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager';
import CameraRoll from '@react-native-community/cameraroll';
import RNFetchBlob from 'rn-fetch-blob';
import SimpleToast from 'react-native-simple-toast';
import * as Progress from 'react-native-progress';


const { StatusBarManager } = NativeModules;
let loginData:any;

export interface props {
    navigation: NavigationScreenProp<any, any>
    // loginInfo: any
};

class Messages extends Component<props, object> {

    flatList: any;
    keyboardWillShowSub: any;
    keyboardWillHideSub: any;

    state = {
        messages: [] as any[],
        showLoader: false,
        isInternetError: false,
        isDataNotFoundError: false,
        message: '',
        statusBarHeight: 0,
        isCalled: false,
        showFullImage: false,
        selectedImage: "",
        receiverInfo: {} as any,
        downloadLoader:false
        // imagesdownloadedCount: 0
    }

    senderId = ''
    receiverId = ''
    chatId = ''
    chatData = {} as any



    // static navigationOptions = ({ navigation }: any) => {
    //     return {
    //         headerTitle: (
    //             <View >
    //                 <Text style={navHdrTxtStyle}>{'Doctor name'}</Text>
    //             </View>
    //         ),
    //         headerLeft: (
    //             <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
    //         ),
    //         headerStyle: headerStyle.style,
    //         headerTitleStyle: headerTitleStyle
    //     }
    // }

    async componentDidMount() {

        // StatusBar.setHidden(true);
        await LocalDataManager.getDataAsyncStorage(LOGIN_DATA).then(data => {
            console.log("Current User Details message>>> ", data)
            loginData = data
        });


        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler(), 'name': this.props.navigation.getParam('chatData').firstName + ' ' + this.props.navigation.getParam('chatData').lastName });

        this.senderId = loginData.id
        this.receiverId = this.props.navigation.getParam('chatData').id
        this.chatId = this.props.navigation.getParam('chatData').chatId
        this.chatData = this.props.navigation.getParam('chatData')

        //Update receiver id when got control by notification
        if (this.props.navigation.getParam('isFromNotification') == true) {
            this.receiverId = this.props.navigation.getParam('chatData').senderID
        }

        console.log("Chat Data in Messages >>>> ", this.props.navigation.getParam('chatData'));


        // this.apiGetUserDetailsById()
        // console.log('chat id >>>>', this.chatId)


        this.getMessages(this.chatId)

        // this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        // this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
    }

    requestAudioRecordPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: "Mediport app Record Write",
                    message:
                        "Mediport App needs access to your External Storage ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the camera");
                this.handleDownload()
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    };

    componentWillUnmount() {
        // this.keyboardWillShowSub.remove();
        // this.keyboardWillHideSub.remove();
    }

    //   keyboardWillShow = (event: any) => {

    //     let contentHeight = this.flatList.contentHeight

    //     Animated.timing(this.imageHeight, {
    //       duration: event.duration,
    //       toValue: IMAGE_HEIGHT_SMALL,
    //     }).start();
    //   };

    //   keyboardWillHide = (event: any) => {
    //     Animated.timing(this.imageHeight, {
    //       duration: event.duration,
    //       toValue: IMAGE_HEIGHT,
    //     }).start();
    //   };

    backBtnHandler() {
        this.props.navigation.goBack()
    }

    getMessages(chatId: string) {
        console.log('messages chatId>>>>>', chatId)

        firestore().collection('messages').doc(chatId).collection('messages').orderBy('sentDate').onSnapshot((response) => {
            console.log('Response getMessages>>>>>', response)

            const listData: any[] = [];
            var chatData: any = {};

            response.docs.forEach((doc: any) => {
                chatData = doc.data();
                chatData.message_id = doc.id;
                listData.push(chatData);
            });

            console.log('Messages >>>>>', listData)
            this.setState({ isCalled: true, messages: listData })

            this.updateMessagesStatus(chatId);
        }, (error) => {
            console.log('Error getMessages>>>>>', error)
            Alert.alert(error.message);
        });
    }

    updateMessagesStatus(chatId: string) {
        var localList: any = [];
        this.state.messages.forEach((msg: any, index: any) => {
            if (msg.status == 2) {
                return;
            }
            else if (msg.status == 0 || msg.status == 1) {
                localList.push(msg);
            }
        });

        //  console.log('After updatingstatus Mesages >>>>>', this.state.messages)
        this.updateFirestore(localList, chatId);
    }

    updateFirestore(mesagesArr: any[], chatId: string) {

        let db = firestore();
        let batch = db.batch();

        let ref = db.collection('messages').doc(chatId.toString()).collection('messages')

        mesagesArr.forEach((msg: any, index: any) => {
            if (msg.status == 2 && msg.sender_id != this.senderId) {
                return;
            }
            else if ((msg.status == 0 || msg.status == 1) && msg.sender_id != this.senderId) {
                let sfRef = ref.doc(msg.message_id);
                batch.update(sfRef, { status: '2' });
            }
        });

        return batch.commit().then(function () {

        });
    }

    private async selectImg() {
        Keyboard.dismiss()
        CustomImagePicker.showImgPicker().then((response: any) => {

            // console.log('Image Selced >>>', response)
            setTimeout(() => {
                this.resizeImageandUpload(response)
            }, 200);

        }).catch((error: any) => {
            //  console.log('Error While CApture image >>>', error)
            if (error != undefined) {
                General.showErroMsg(this, error)
            }
        })
    }

    private resizeImageandUpload(image: any) {
        let newWidth = 800; 
        let newHeight = 800 / (image.width / image.height);

        let rotation = 0

        if (image.originalRotation === 90) {
            rotation = 90
        } else if (image.originalRotation === 270) {
            rotation = -90
        }


        ImageResizer.createResizedImage(image.uri, newWidth, newHeight, 'JPEG', 100, rotation).then((response) => {

            this.sendImage(response)
        }).catch((err) => {
            //  console.log('Image Resize error  >>>', err)
        });
    }

    sendImage(image: any) {
console.log("Image URI >>> ", image)
        let localData = { chatId: this.chatId, senderId: this.senderId, receiverId: this.receiverId, message: '', image: image.uri };
        let messagesArr = this.state.messages
        messagesArr.push(localData)
        this.setState({ message: '', messages: messagesArr });

        let imageName = AutoId.imageName() + ".jpeg"
        console.log("Imageeeeeee", ">> sumit", image);
        console.log("Imageeeeeee Name", ">> sumit", imageName);
        
        var firebaseStorageRef = storage().ref('chat/images')

        console.log("Imageeeeeee", ">> sumit 1111", firebaseStorageRef);
        const imageRef = firebaseStorageRef.child(imageName);


        let weakSelf = this

        imageRef.putFile(image.uri, { contentType: 'image/jpeg' }).then(function () {
            return imageRef.getDownloadURL();
        }).then(function (url) {
            console.log("Image url", { url: url });

            let data = { chatId: weakSelf.chatId, senderId: weakSelf.senderId, receiverId: weakSelf.receiverId, message: '', image: url };
            sendMessage(data);
            updateLastMessage(data);
            weakSelf.setState({ message: '' });
        }).catch(function (error) {
            console.log("Error while saving the image.. ", error);
        });
    }

    async sendMessage() {
        if (this.state.message.trim().length <= 0) {
            Alert.alert('Please type a message to send.');
            return;
        }

        checkAndAddUser(loginData.id, this.receiverId, this.props.navigation.getParam("chatData")).then((data_: any) => {
            var receiverDict = this.props.navigation.getParam("chatData")
            if (data_.exists) {
                receiverDict["chatId"] = data_.chatId

                let data = { chatId: data_.chatId, senderId: loginData.id, receiverId: this.receiverId, message: this.state.message.trim(), image: '' };
                //let data = { chatId: this.chatId, senderId: this.senderId, receiverId: this.receiverId, message: this.state.message.trim(), image: '' };
                let messagesArr = this.state.messages
                messagesArr.push(data)
                this.setState({ message: '', messages: messagesArr });
                sendMessage(data);
                updateLastMessage(data);

            } else {
                // Now AddChat for Receiver means Driver

                let senderDict = {
                    firstName: loginData.firstname,
                    lastName: "",
                    // lastName: loginData.firstname,
                    image: loginData.image,
                    // email: this.props.loginInfo.email,
                    createdOn: Date.now(),
                    id: loginData.id,
                    chatId: this.chatId
                }

                console.log('senderDict Sent >>>>>>', senderDict)

                checkAndAddUser(this.receiverId, loginData.id, senderDict)

                let data = { chatId: data_.chatId, senderId: loginData.id, receiverId: this.receiverId, message: this.state.message.trim(), image: '' };
                //let data = { chatId: this.chatId, senderId: this.senderId, receiverId: this.receiverId, message: this.state.message.trim(), image: '' };
                let messagesArr = this.state.messages
                messagesArr.push(data)
                this.setState({ message: '', messages: messagesArr });
                sendMessage(data);
                updateLastMessage(data);
            }

        })


        // let data = { chatId: this.chatId, senderId: this.senderId, receiverId: this.receiverId, message: this.state.message.trim(), image: '' };
        // let messagesArr = this.state.messages
        // messagesArr.push(data)
        // this.setState({ message: '', messages: messagesArr });
        // sendMessage(data);
        // updateLastMessage(data);
        //  this.sendNotification(data);
    }





    getPushParams(data: any) {
        console.log("chat Data >>", this.chatData);
        console.log("simple Data >>", data);
        var new_data = this.chatData
        new_data.senderID = data.senderId
        new_data.id = data.receiverId
        return {
            title: "You got a Message",
            recieverID: data.receiverId,
            message: data.message,
            data: new_data
        }
    }

    // sendNotification(data: any) {
    //     console.log('send notification params >>>>', this.getPushParams(data));
    //     RequestManager.postRequestWithHeaders(API_SEND_MESSAGE_NOTIFICATION, this.getPushParams(data)).then((response: any) => {
    //         //console.log('REsponse of Send Notification API >>>>', response);
    //     }).catch((error) => {
    //         // console.log('Error of Send Notification API >>>>', error);
    //         // General.showErroMsg(this, error)
    //     })
    // }

    check = () => {
        try {
            PermissionsAndroid.checkPermission( PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE )
            .then(result => {
                console.log(result)
            }).catch(error => {
                console.log(error)
            })
            // if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //     console.log("You can use the camera");
            //     this.handleDownload()
            // } else {
            //     console.log("Camera permission denied");
            // }
        } catch (err) {
            console.warn(err);
        }
    }

    handleDownload = async () => {
        PermissionsAndroid.checkPermission( PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE )
        .then(result => {
            if(result){
            console.log("IF case>>> ",result)

         this.setState({ ...this.state, downloadLoader : true})
        console.log("Image Selcted >>>>>", this.state.selectedImage)
        RNFetchBlob.config({
          fileCache: true,
          appendExt: 'jpeg',
        })
          .fetch('GET', this.state.selectedImage)
          .then(res => {
            CameraRoll.save(res.data, {type : 'photo',album:"Mediport"})
              .then(res => {
                  console.log(res)
                  this.setState({ ...this.state, downloadLoader : false})
                SimpleToast.show("Download Successfully")
                })
              .catch(err => {
                this.setState({ ...this.state, downloadLoader : false})
                console.log(err)
                SimpleToast.show("Download error")
                })
          })
          .catch(error => console.log(error));
          
            }else{
            console.log("ELSE case>>> ",result)

                this.requestAudioRecordPermission()
            }
        }).catch(error => {
            console.log("Catch case>>> ",error)
        })

      };

    showFullImage(image: any) {
        
        // this.handleDownload(image)
        this.setState({ selectedImage: image, showFullImage: true })
    }

    removeImage() {
        this.setState({ selectedImage: "", showFullImage: false })
    }

    // Get paarmeters for Login API
    private getParams() {
        return {
            id: this.receiverId
        }
    }

    async apiGetUserDetailsById() {
        // console.log('GET apiGetUserDetailsById called with params Mesages>>>', this.getParams())
        // this.setState({ showLoader: true })
        let weakSelf = this

        // Get Login User's Detail
        RequestManager.getRequest(`${API_DOCTOR_PROFILE}/${this.receiverId}`, this.getParams())
            .then((response: any) => {
                 console.log('REsponse of apiGetUserDetailsById API Messages>>>>', response);
                weakSelf.setState({ receiverInfo: response.data })
            }).catch((error: any) => {
                console.log("Erro While erecieving user details >>>>>", error)
                //General.showErroMsg(this, error)
            })

        // GetUserDetails(this, this.getParams())
        //     .then((response: any) => {

        //         weakSelf.setState({ receiverInfo: response.data })
        //     }).catch((error: any) => {
        //         console.log("Erro While erecieving user details >>>>>", error)
        //     })



    }

    goBack = () => {
        this.props.navigation.goBack()
    }

    render() {

        let receiverImageUrl =  this.chatData.image
        let title=this.props.navigation.getParam('chatData').firstName+" "+this.props.navigation.getParam('chatData').lastName
        return (

            <SafeAreaView style={styles.blueContainer}>
                
                <BackButtonHeader
                    title={title}
                   // style={{position:'absolute',top:0}}
                    onClick={() => this.goBack()}
                />
                <KeyboardAvoidingView 
                behavior={Platform.OS === 'ios' ? 'padding' : undefined}
                    keyboardVerticalOffset={(Platform.OS === 'android') ? this.state.statusBarHeight + 60 : this.state.statusBarHeight + 44}
                    style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }}
                    enabled
                >
          <Modal
            animated={true}
            transparent={true}
            visible={this.state.downloadLoader}
            // visible={this.state.downloadLoader}
          >
          
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <View style={{ height:'100%', width: '100%', backgroundColor: 'rgba(0, 0, 0, 0.5)' }} />
      <View style={{ backgroundColor: 'transparent', position: 'absolute'}}>
      <Progress.Circle size={50} indeterminate={true} borderColor={'white'} borderWidth={3} />
      </View>
    </View>
          </Modal>
                    <Modal
                        animated={true}
                        //transparent={true}
                        visible={this.state.showFullImage}>
                        <View style={styles.fullImageContainer}>
                            <View style={{ flexDirection:'row', justifyContent:'space-between', width:'100%' }}>
                            <TouchableOpacity onPress={() => this.removeImage()} style={[styles.crossBtn,{ alignSelf:'flex-end' }]}>
                            <Image style={styles.sendImg} source={require('../../res/drawable/back.png')} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.handleDownload()} style={styles.crossBtn}>
                            <Image style={styles.sendImg} source={require('../../res/drawable/save.png')} />
                            </TouchableOpacity>       
                            </View>
 
                            <FastImage
                                style={styles.fullImage}
                                source={{ uri: this.state.selectedImage }}
                            />

                            
                        </View>
                    </Modal>

                    <View style={styles.container}>
                        {
                        this.state.messages.length > 0 ?
                            <FlatList
                                ref={ref => this.flatList = ref}
                                style={styles.flatListStyle}
                                contentContainerStyle={{
                                    flexGrow: 1,
                                    justifyContent: 'flex-start'
                                }}
                                automaticallyAdjustContentInsets={false}
                                data={this.state.messages}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item, index }: any) => 
                                <MessagaCell item={item} index={index} 
                                senderId={this.senderId} senderImage={loginData.image} 
                                receiverImage={receiverImageUrl} 
                                imageClickEvent={(image: any) => this.showFullImage(image)} />}
                                onContentSizeChange={() => this.flatList.scrollToEnd({ animated: false })}
                                onLayout={() => this.flatList.scrollToEnd({ animated: false })}
                            /> : (this.state.isCalled ? <NoDataFoundView warning_message='NO MESSAGES YET !' message='' img={require('../../assets/noMessages.png')} marginTop={80} /> 
                            : null)
                        }
                        <View style={[styles.textVwCntnr]}>
                            <TouchableOpacity onPress={() => this.selectImg()} style={styles.sendBtn}>
                                <Image style={styles.sendImg} source={require('../../assets/camera.png')} />
                            </TouchableOpacity>
                            <TextInput multiline={true} style={styles.msgTxtInpt} value={this.state.message} onChangeText={(text) => this.setState({ message: text })} placeholder={'Enter your message'} returnKeyType="done" blurOnSubmit={true} underlineColorAndroid="transparent" />

                            <TouchableOpacity onPress={() => this.sendMessage()} style={styles.sendBtn}>
                                <Image style={styles.sendImg} source={require('../../assets/send.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </KeyboardAvoidingView>

            </SafeAreaView>
        );
    }
}

//  const mapStateToProps =async (state: any) => ({
//      //loginInfo: state.saveLoginInfo,
// });

const mapDispatchToProps = (dispatch: any) => ({

});

export default connect(mapDispatchToProps)(Messages);
