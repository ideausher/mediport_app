import React, { Component } from 'react';
import { View, Text, TextInput, Image, FlatList, StatusBar, Modal, NativeModules } from 'react-native';
import { APP_GREY_PLACEHOLDER_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle, commonShadowStyle, APP_LIGHT_GREY_COLOR } from '../../../src/Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles'
import SpecialityCell from '../../Custom Cells/SpecialityCell';
import { localize } from '../../Utils/LocalisedManager';
import RequestManager from '../../Utils/RequestManager'
import { API_SPECIALITY_LIST } from '../../Utils/APIs';
//@ts-ignore
import { connect } from 'react-redux';
import General from '../../Utils/General';
import { OpenDrawerHeaderBtn, NoInternetFoundView, NotificationsHeaderBtn, NoDataFoundView, BottomShadowView } from '../../Custom/CustomComponents';
import PaymentMethod from '../PaymentMethod';
import moment from 'moment';
import { CATEGORY_INFO } from '../../Redux/Constants';
import { Loader } from '../../Utils/Loader';
import { CommonBtn, CommonBtnWhite } from '../../Custom/CustomButton';
import LocalDataManager from '../../Utils/LocalDataManager';
import AsyncStorage from '@react-native-community/async-storage';
import { localNotificationservice } from '../../Firebase/LocalNotificationService';

var Locale = require('react-native-locale');

const I18n = require('react-native-i18n')

export interface props {
    navigation: NavigationScreenProp<any, any>,
    token: '',
    saveCategoryInfo: any,
};

const VideoCalingPopup = (props: any) => {
    console.log('Funcccc Called')

    let docName = 'Laurice'

    return (
        <View style={[{flex: 1}, { justifyContent: 'center', backgroundColor: 'rgba(47, 103, 157, 0.9)', }]}>
            <View style={[styles.popupVw, commonShadowStyle]}>
                <View style={styles.imgContainer}>
                    <Image
                        source={require('../../assets/video.png')}
                        style={styles.image}
                        resizeMode={'contain'}
                    />
                </View>
                <Text style={styles.greyTextPopup}>{docName + ' ' + localize.isCallingYou}</Text>
                <CommonBtn title={localize.accept} func={() => props.acceptFunc()} width='80%' />
                <CommonBtnWhite title={localize.reject} func={() => props.rejectFunc()} width='80%' marginTop={0} marginBottom={8}/>

            </View>
        </View>
    )
}

class Home extends Component<props, object> {

    state = {
        specialityList: [],
        filteredSpecialityList: [],
        showLoader: false,
        isInternetError: false,
        isDataNotFoundError: false, 
        showVideoCallPopup: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View>
                    <Text style={navHdrTxtStyle}>{localize.home}</Text>
                </View>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerRight: (
                <NotificationsHeaderBtn func={navigation.getParam('notificationsHandler')} />
                //  <TouchableOpacity style={{backgroundColor: 'red'}} onPress={navigation.getParam('notificationsHandler')}>
                //     <Image style={{ marginRight: 10, height: 20, width: 20, resizeMode: 'contain' }} source={require('../../assets/noti-bell.png')} />
                // </TouchableOpacity>
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle,
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler() });

        this.apiGetSpecialityList()
        
        this.props.navigation.addListener('willFocus', (route) => this.checkForVideoCall());
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    notificationsHandler() {
        this.props.navigation.navigate('Notifications')
    }

    checkForVideoCall() {
        console.log('checkForVideoCall func called')
        LocalDataManager.getDataAsyncStorage('shouldAskForVideoCall').then((data: any) => {
            console.log('Data Retrived from Storage >>>>', data)
            if (data != null && data == true) {
                LocalDataManager.saveDataAsyncStorage('shouldAskForVideoCall', JSON.stringify(false))
                this.setState({showVideoCallPopup: true})
            }
        })
    }

    searching(text: string) {

        let object: any = {};
        let searchedList: any[] = [];

        this.state.specialityList.map((item: any) => {
            if (item.cat_name.toLowerCase().replace(" ", "").includes(text.toLowerCase().replace(" ", ""))) {
                searchedList.push(item);
            }
        })

        if (searchedList.length > 0) {
            this.setState({ filteredSpecialityList: searchedList });
        }
        else {
            // this.setState({ filteredSpecialityList: this.state.specialityList });
            this.setState({ filteredSpecialityList: [] });
        }
    }

    cellSelected(item: any) {

        this.props.saveCategoryInfo(item);
        this.props.navigation.navigate('Doctor', { type: 1 })
    }

    async apiGetSpecialityList() {

        this.setState({ showLoader: true, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this

        RequestManager.getRequest(API_SPECIALITY_LIST, {})
            .then((response: any) => {
                console.log('REsponse of apiGetSpecialityList API >>>>', response);
                setTimeout(function () {
                    weakSelf.setState({ showLoader: false, specialityList: response.data, filteredSpecialityList: response.data })

                    if (weakSelf.state.filteredSpecialityList.length == 0)
                        weakSelf.setState({ isInternetError: false, isDataNotFoundError: true })
                }, 200);

            }).catch((error: any) => {
                if (error == localize.internetConnectionError) {
                    this.setState({ isInternetError: true, isDataNotFoundError: false })
                }
                else {
                    General.showErroMsg(this, error)
                }
            })
    }

    acceptVideoCall() {
        this.setState({showVideoCallPopup : false})
        this.props.navigation.navigate('VideoCall')
        localNotificationservice.cancelAllNotification()
    }

    rejectVideoCall() {
        AsyncStorage.removeItem('shouldAskForVideoCall')
        AsyncStorage.removeItem('videoCallInfo')
        this.setState({showVideoCallPopup : false})
    }

    render() {
        return (
            <View style={styles.blueConatiner}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showVideoCallPopup}>
                    <VideoCalingPopup acceptFunc={() => this.acceptVideoCall()} rejectFunc={() => this.rejectVideoCall()}/>
                </Modal>
                <View style={styles.container}>
                    <View style={{ flex: 1 }}>
                        <View style={{ backgroundColor: 'white', borderTopLeftRadius: 20, borderTopRightRadius: 20, }}>
                            <View style={[styles.searchVw, commonShadowStyle]}>
                                <Image source={require('../../assets/search-icon.png')} resizeMode={'contain'} style={{ height: 16, width: 16 }} />
                                <TextInput placeholder={localize.searchDoctor}
                                    placeholderTextColor={APP_LIGHT_GREY_COLOR} style={{ flex: 1, marginLeft: 16, padding: 5, fontSize: 14, color: APP_GREY_PLACEHOLDER_COLOR }} onChangeText={(text) => this.searching(text)} />
                            </View>
                            {this.state.filteredSpecialityList.length > 0 ? <Text style={styles.text}>{localize.selectSpeciality}</Text> : null}
                        </View>
                        {this.state.filteredSpecialityList.length > 0 ?
                            <FlatList
                                style={{ marginLeft: 8, marginRight: 8, marginTop: 24 }}
                                data={this.state.filteredSpecialityList}
                                keyExtractor={(item: any, index: any) => index.toString()}
                                renderItem={({ item }) =>
                                    <SpecialityCell
                                        onClickEvent={(item: any) => this.cellSelected(item)}
                                        item={item}
                                    />
                                }
                                numColumns={2}
                            /> :
                            (this.state.showLoader ? null :
                                (this.state.isDataNotFoundError ? <NoDataFoundView /> :
                                    (this.state.isInternetError ?
                                        <NoInternetFoundView func={() => this.apiGetSpecialityList()} /> : null)))}
                    </View>
                    {/* {Platform.OS == 'android' ? <BottomShadowView /> : null } */}
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state: any) => ({
    token: state.saveLoginInfo.token,
});


const mapDispatchToProps = (dispatch: any) => ({
    saveCategoryInfo: (info: string) => dispatch({ type: CATEGORY_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
