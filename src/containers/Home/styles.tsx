//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_LIGHT_GREY_COLOR, APP_SHADOW_COLOR, APP_MEDIUM_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
    },
    searchVw: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 20,
        padding: 5,
        borderRadius: 10,
        backgroundColor: 'white',
        marginLeft: 16,
        marginRight: 16,
        marginTop: 26,
    },
    text: {
        fontSize: 18,
        fontWeight: '400',
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        margin: 16
    },

    popupVw: {
        borderRadius: 10,
        backgroundColor: 'white',
        padding: 16,
       // height: '50%',
        margin: 16,
        alignItems: 'center'
    },
    imgContainer: {
        backgroundColor: 'rgba(66, 202, 162, 0.7)',
        marginTop: 60,
        height: 62,
        width: 62,
        borderRadius: 31,
        alignItems: 'center',
        justifyContent: 'center',
    },
    greyTextPopup: {
        marginTop: 20,
        fontSize: 16,
        color: APP_MEDIUM_GREY_COLOR,
        textAlign: 'center',
        width: '60%',
        marginBottom: '30'
    },
    image: {
        height: 24,
        width: 24,
        resizeMode: 'contain',
    },
});