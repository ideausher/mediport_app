//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, App_MIDIUM_GREY_COLOR } from '../../Constants'
import { Dimensions, Platform } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
       padding: 16,
    },
    nameTxt: {
        textAlign: 'left',
        fontSize: 16,
        color: APP_GREY_TEXT_COLOR,
        fontWeight: '600',
    },
    smallText: {
        fontSize: 12,
        color: App_MIDIUM_GREY_COLOR,
        textAlign: 'left',
        marginTop: 2,
        lineHeight: 17,
        // backgroundColor:'red'
    },
    smallImg: {
        width: 16,
        height: 16,
    },
    ratingVw: {
        height: 60,
        justifyContent: 'center',
    },
    ratingTxt: {
        fontSize: 12,
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'center',
        flexWrap: 'wrap', 
        marginTop: 10
    },
    blueText: {
        fontSize: 16,
        fontWeight: '300',
        color: APP_HEADER_BACK_COLOR,
        textAlign: 'left',
    },
    text: {
        fontSize: 16,
        fontWeight: "bold",
        color: APP_HEADER_BACK_COLOR,
        
    },
    bookAppointmentBtn:{
        width: 200,
        height: 40,
        backgroundColor: APP_GREEN_COLOR,
        borderRadius:20,
        justifyContent: 'center',
        alignSelf:'center',
        marginTop: 16
    },
});