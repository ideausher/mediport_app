import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, Alert, StatusBar, Modal } from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_GREY_TEXT_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp, ScrollView } from 'react-navigation';
import { styles } from './styles';
import { LineVw, BackHeaderBtn, NoDataFoundView } from '../../Custom/CustomComponents';
import { localize } from '../../Utils/LocalisedManager';
import { API_DOCTOR_PROFILE } from '../../Utils/APIs';
import RequestManager from '../../Utils/RequestManager';
import Doctors from '../Doctors';
import { DOCTOR_INFO } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { Rating } from 'react-native-ratings';
import General from '../../Utils/General';
//@ts-ignore
import ImageLoad from 'react-native-image-placeholder';
import { CommonStyles } from '../../Utils/CommonStyles';
import { Loader } from '../../Utils/Loader';

const PatientReviewVw = (props: any) => {

    return (
        <View style={{ marginTop: 8 }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <Text style={[styles.ratingTxt, { textAlign: 'left' }]}>{props.data.user.firstname}</Text>

                <Rating
                    startingValue={props.data.rating}
                    imageSize={20}
                    ratingCount={5}
                    isDisabled={true}
                    readonly={true}
                />
            </View>
            <Text style={styles.smallText}>{props.data.treatment_info}</Text>
            <Text style={[styles.smallText, { color: APP_GREY_TEXT_COLOR, marginTop: 8 }]}>{props.data.feedback_message}</Text>
            <Text style={[styles.smallText, { alignSelf: 'flex-end' }]}>2 days ago</Text>
        </View>
    );
};

const RatingStarVw = (props: any) => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <Image
                source={require('../../assets/skin-care.png')}
                style={styles.smallImg}
                resizeMode={'contain'}
            />
            <Image
                source={require('../../assets/skin-care.png')}
                style={styles.smallImg}
                resizeMode={'contain'}
            />
            <Image
                source={require('../../assets/skin-care.png')}
                style={styles.smallImg}
                resizeMode={'contain'}
            />
            <Image
                source={require('../../assets/skin-care.png')}
                style={styles.smallImg}
                resizeMode={'contain'}
            />
            <Image
                source={require('../../assets/skin-care.png')}
                style={styles.smallImg}
                resizeMode={'contain'}
            />
        </View>
    );
}

const AddInfoView = (props: any) => {
    return (
        <View style={{ flexDirection: 'row', marginTop: 8, alignItems: 'center' }}>
            <Image
                source={props.icon}
                style={styles.smallImg}
                resizeMode={'contain'}
            />
            <Text style={[styles.blueText, { marginLeft: 8 }]}>
                {props.heading}</Text>
        </View>
    );
};

const AddDataView = (props: any) => {

    return (
        <View style={{ justifyContent: 'center', }}>
            <Text style={[styles.ratingTxt, { textAlign: 'left' }]}>{props.data.heading}</Text>
            <Text style={styles.smallText}>{props.data.subHeading}</Text>
        </View>
    );
};


export interface props {
    navigation: NavigationScreenProp<any, any>
    saveDoctorInfo: any,
    doctorInfo: any,
    categoryInfo: any
};

class DoctorsProfile extends Component<props, object> {

    state = {
        doctorId: '',
        doctorData: {},
        isAPICalledOnce: false,
        showLoader: false,

    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.doctorProfile}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });
        this.props.saveDoctorInfo();
        this.setState({ doctorId: this.props.navigation.getParam('doctorId') })

        setTimeout(() => {
            this.getDoctorProfileApi()
        }, 200);
    }

    private clearData() {

    }

    private backBtnHandler() {
        this.props.navigation.goBack();
    }

    private bookAppointmentAction() {

        if(this.props.doctorInfo.name != '')
        {
            this.props.navigation.navigate('BookAppointment');
        }
    }

    private getDoctorProfileApi() {

        this.setState({ showLoader: true, isAPICalledOnce: true})
        console.log('Params in Doctor Profile >>>>', this.props.navigation.getParam('doctorId'))
        
        RequestManager.getRequest(API_DOCTOR_PROFILE + '/' + this.props.navigation.getParam('doctorId'), {}).then((response: any) => {
            console.log('Response of getDoctorProfileApi API >>>>', response);
            this.setState({ showLoader: false, doctorData: response.data})
            this.props.saveDoctorInfo(response.data, this.props.categoryInfo.category.id, this.props.navigation.getParam('type'));
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    render() {

        let doctorData: any = this.props.doctorInfo

        return (
            <View style={styles.blueConatiner}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                {this.props.doctorInfo.name != '' ? <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ flexDirection: 'row', marginTop: 16 }}>
                            <View style={CommonStyles.doctorImgCntnrStyle}>
                                {this.props.doctorInfo.image == '' ?
                                    <Image
                                        source={require('../../assets/avatar.png')}
                                        style={CommonStyles.doctorImg}
                                        resizeMode={'contain'} /> :
                                    <Image style={CommonStyles.doctorImg} source={{uri: this.props.doctorInfo.image}} />
                                    // <ImageLoad style={CommonStyles.doctorImg} placeholderSource={require('../../assets/avatar.png')} source={{ uri: this.props.doctorInfo.image }} />
                                }
                            </View>
                            <View style={{ width: '60%', paddingLeft: 20, paddingTop: 3 }}>
                                <Text style={styles.text}>{doctorData.name}</Text>
                                <Text style={[styles.smallText, { color: APP_HEADER_BACK_COLOR, marginTop: 8 }]}>{this.props.doctorInfo.categoryName}</Text>
                                {doctorData.address.length > 0 ? <View style={{ flexDirection: 'row', marginTop: 8, alignItems: 'center' }}>
                                    <Image
                                        source={require('../../assets/loc.png')}
                                        style={styles.smallImg}
                                        resizeMode={'contain'}
                                    />
                                    <Text style={[styles.smallText, { marginTop: 0, marginLeft: 4 }]}> {doctorData.address[0].full_address}</Text>
                                </View> : null}
                                <Text style={[styles.blueText, { marginTop: 8 }]}>{doctorData.price != undefined &&  doctorData.price != '' ? `$${doctorData.price}` : ''}</Text>
                            </View>
                        </View>
                        <Text style={[styles.smallText, { marginTop: 10, color: APP_GREY_TEXT_COLOR }]}>{doctorData.description}</Text>
                        <View style={{ marginTop: 8 }}>
                            <LineVw />
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <View style={styles.ratingVw}>
                                    <Text style={styles.ratingTxt}>{localize.ratings}</Text>
                                    <Text style={[styles.smallText, { textAlign: 'center' }]}>{doctorData.ratingCount}</Text>
                                </View>
                                <View style={styles.ratingVw}>
                                    <Text style={styles.ratingTxt}>{localize.appointments}</Text>
                                    <Text style={[styles.smallText, { textAlign: 'center' }]}>{doctorData.appointmentsCount}</Text>
                                </View>
                                <View style={styles.ratingVw}>
                                    <Text style={styles.ratingTxt}>{localize.suggested}</Text>
                                    <Text style={[styles.smallText, { textAlign: 'center' }]}>{doctorData.suggestedCount}</Text>
                                </View>
                            </View>
                            <LineVw />
                        </View>
                        {doctorData.address.length > 0 ? <AddInfoView heading={localize.office} icon={require('../../assets/office.png')} /> : null}

                        {doctorData.address.length > 0 ? doctorData.address.map((item: any, index: number) => (

                            <AddDataView data={{ heading: item.full_address + ', ' + item.city + ', ' + item.state + ', ' + item.pincode, subHeading: item.country }} />
                        )) : null}

                        {doctorData.education.length > 0 ?
                            <AddInfoView heading={localize.education} icon={require('../../assets/education.png')} /> : null}
                        {doctorData.education.map((item: any, index: number) => (
                            <AddDataView data={{ heading: item.degree, subHeading: item.edu_desc }} />
                        ))}

                        {doctorData.languages.length > 0 ? <AddInfoView heading={localize.language} icon={require('../../assets/language.png')} /> : null}
                        {doctorData.languages.map((item: any, index: number) => (

                            <AddDataView data={{ heading: item, subHeading: 'Fluent' }} />
                        ))}
                        {Number(doctorData.avgRating) > 0 ? <Text style={[styles.blueText, { marginTop: 8 }]}>
                            {localize.ratings}</Text> : null}
                        {Number(doctorData.avgRating) > 0 ? <Text style={[styles.blueText, { marginTop: 8, fontWeight: '600' }]}>{doctorData.avgRating}</Text> : null}
                        {Number(doctorData.avgRating) > 0 ? <View style={{ alignItems: 'flex-start' }}>
                            <Rating
                                startingValue={doctorData.avgRating}
                                imageSize={20}
                                ratingCount={5}
                                isDisabled={true}
                                readonly={true}
                            />
                        </View> : null}
                        {doctorData.patientReviews.length > 0 ? <Text style={[styles.blueText, { marginTop: 8 }]}>
                            {localize.patientReviews}</Text> : null}

                        {doctorData.patientReviews.map((item: any, index: number) => (
                            <PatientReviewVw data={item} />
                        ))}

                        {this.props.navigation.getParam('type') ? <TouchableOpacity
                            style={styles.bookAppointmentBtn}
                            onPress={() => this.bookAppointmentAction()}>
                            <Text style={{ color: 'white', textAlign: 'center' }}>{localize.bookAppointment}</Text>
                        </TouchableOpacity> : null }
                    </ScrollView> : 
                    <View style={{flex:1}}>{
                        (this.state.showLoader || this.state.isAPICalledOnce ==false) ?
                            null :
                            <NoDataFoundView func={() => this.getDoctorProfileApi()} showFilterBtn={true}/>
                    }
                    </View>}
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state: any) => ({
    doctorInfo: state.saveDoctorInfo,
    categoryInfo: state.saveCategoryInfo
});

const mapDispatchToProps = (dispatch: any) => ({
    saveDoctorInfo: (info: string, catId: string, doctorsType: string) => dispatch({ type: DOCTOR_INFO, payload: info, catId: catId, doctorsType:doctorsType  }),
});

export default connect(mapStateToProps, mapDispatchToProps)(DoctorsProfile);