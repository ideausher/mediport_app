import { localize } from "../../Utils/LocalisedManager";

export const Content = [
    {
        appointmentOption: localize.mySelf, 
        isSelected: true,
    },
    {
        appointmentOption: localize.wife, 
        isSelected: false,
    },
    {
        appointmentOption: localize.son, 
        isSelected: false,
    },
    {
        appointmentOption: localize.daughter, 
        isSelected: false,
    },
]