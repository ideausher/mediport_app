//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, App_LIGHT_BLUE_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        padding: 16,
    },
    nameTxt: {
        textAlign: 'left',
        fontSize: 16,
        color: APP_GREY_TEXT_COLOR,
        fontWeight: '600',
    },
    smallText: {
        fontSize: 12,
        color: APP_LIGHT_GREY_COLOR,
        textAlign: 'left',
    },
    smallImg: {
        width: 18,
        height: 18,
    },
    ratingVw: {
        height: 60,
        justifyContent: 'center',
    },
    ratingTxt: {
        fontSize: 12,
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'center'
    },
    blueText: {
        fontSize: 16,
        fontWeight: '300',
        color: APP_HEADER_BACK_COLOR,
        textAlign: 'left',
    },
    nextBtn:{
        width: 200,
        height: 40,
        backgroundColor: APP_GREEN_COLOR,
        borderRadius:20,
        justifyContent: 'center',
        alignSelf:'center',
        fontWeight: "bold",
        marginTop: 25,
       
    },
    appointmentForSelectedBtn:{
        width:80,
        height:30,
        backgroundColor: 'white',
        borderRadius:15,
        borderColor: APP_HEADER_BACK_COLOR,
        borderWidth:0.2,
        justifyContent: 'center',
        margin:8,
        marginTop: 16
    },
    appointmentForBtn:{
        width:80,
        height:30,
        backgroundColor: 'white',
        borderRadius:15,
        borderColor: APP_LIGHT_GREY_COLOR,
        borderWidth:0.2,
        justifyContent: 'center',
        margin:8,
        marginTop: 16
    },
    addBtn:{
        width:80,
        height: 80,
        borderRadius: 10,
       // marginTop: 22,
        backgroundColor:App_LIGHT_BLUE_COLOR,
        justifyContent:'center'
    },
    genderBtn: {
        width: 30,
        height: 30,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent:'center',
        marginRight: 16,
        marginTop: 16,
    },
    text: {
        fontSize: 16,
        fontWeight: "bold",
        color: APP_HEADER_BACK_COLOR,
        
    },
});