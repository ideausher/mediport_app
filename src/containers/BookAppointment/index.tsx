import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, Alert, StatusBar, Modal } from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_LIGHT_GREY_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle, commonShadowStyle } from '../../Constants';
import { NavigationScreenProp, ScrollView } from 'react-navigation';
import { styles } from './styles';
import { Content } from './Constants';
import { LineVw, BackHeaderBtn } from '../../Custom/CustomComponents';
import { TextInputWithoutImageCustomWidthHeight, TextInputWithDropdown, TextInputMultiLine, TextInputWithDropdownIOS, TextInputWithDropdownAndroid } from '../../Custom/CustomTextInput';
import { localize } from '../../Utils/LocalisedManager';
//@ts-ignore
import { connect } from 'react-redux';
import { CustomPicker, CustomPickerIOS } from '../../Custom/CustomPicker';
import { BOOKING_INFO, } from '../../Redux/Constants';
//@ts-ignore
import validator from 'validator';
import { CommonStyles } from '../../Utils/CommonStyles';
import { CommonBtn } from '../../Custom/CustomButton';
import Toast from 'react-native-simple-toast';
import CustomImagePicker from '../../Utils/CustomImagePicker';
import General from '../../Utils/General';
import ImageResizer from 'react-native-image-resizer';
//@ts-ignore
import PDFLib, { PDFDocument, PDFPage } from 'react-native-pdf-lib';
import RequestManager from '../../Utils/RequestManager'
import { API_UPLOAD_PDF } from '../../Utils/APIs';
import { FlatList } from 'react-native-gesture-handler';
import PDFImagesCell from '../../Custom Cells/PDFImagesCell';
import { Loader } from '../../Utils/Loader';

const { StatusBarManager } = NativeModules;
const bloodGroups = ['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-']
const bloodGroupsIOS = ['- Choose Blood Group -', 'A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-']

export interface props {
    navigation: NavigationScreenProp<any, any>,
    doctorInfo: any,
    saveBookingInfo: any,
};

class BookAppointment extends Component<props, object> {

    state = {
        appointmentForArr: [] as any[],
        age: '23',
        bloodGroup: 'A+',
        conatctNo: '9842156985',
        gender: 1,
        patientName: 'asasa',
        healthIssue: 'asasa',
        statusBarHeight: 0,
        showBloodGroupPicker: false,
        selectedAppointmentFor: 'Myself',
        images: [],
        showLoader: false,
        isImagePickerShown: false,
        selectedImagesArr: [] as any[], 
        recentSelectedImageType: '',
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.bookAnAppointment}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        this.setState({ appointmentForArr: Content })

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    appoitmentFor(indx: number) {
        this.state.appointmentForArr.map((item: any, index: number) => {
            if (indx == index) {
                item.isSelected = true
            }
            else {
                item.isSelected = false
            }
        })

        this.setState({ appointmentForArr: this.state.appointmentForArr, selectedAppointmentFor: this.state.appointmentForArr[indx].appointmentOption })
    }

    nextBtnAction() {
        // if (this.state.images.length > 0) {
        //     this.generatePDF();
        // }
        // else {
        //     this.moveToNextScreen('')
        // }

        this.moveToNextScreen()
    }

    moveToNextScreen() {
        console.log('Move to next screen Func called With Dict >>>>', this.getAppointmentInfoDict())
        if (this.validateDetail()) {
            this.props.saveBookingInfo(this.getAppointmentInfoDict())
            this.props.navigation.navigate('BookAppoitmentDate')
        }
    }

    // async  generatePDF() {

    //     var arr: any[] = []
    //     console.log('image path >>>> ' + this.state.images[0])
    //     for (var i = 0; i < this.state.images.length; i++) {
    //         arr[i] = PDFPage
    //             .create()
    //             .setMediaBox(200, 200)
    //             .drawImage(String(this.state.images[i]).replace("file://", ""), 'png', {
    //                 x: 5,
    //                 y: 125,
    //                 width: 200,
    //                 height: 100,
    //             })
    //     }
    //     var page1, page2, page3
    //     if (arr.length == 1)
    //         page1 = arr[0]
    //     else if (arr.length == 2) {
    //         page1 = arr[0]
    //         page2 = arr[1]
    //     }
    //     if (arr.length == 3) {
    //         page1 = arr[0]
    //         page2 = arr[1]
    //         page3 = arr[2]
    //     }

    //     console.log('pages arrayyyyyyy' + JSON.stringify(arr))
    //     console.log('PDFLib', PDFLib)
    //     console.log('PDFDocument', PDFDocument)
    //     console.log('PDFPage', PDFPage)

    //     // Create a new PDF in your app's private Documents directory
    //     let docsDir = await PDFLib.getDocumentsDirectory()

    //     const pdfPath = `${docsDir}/pdf.pdf`;
    //     PDFDocument
    //         .create(pdfPath)
    //         .addPages(page1)
    //         .write() // Returns a promise that resolves with the PDF's path
    //         .then((path: any) => {
    //             console.log('PDF created at: ' + path);
    //             // Do stuff with your shiny new PDF!
    //             // ToastAndroid.show('pdf generated', 1000)
    //             this.moveToNextScreen(path)
    //         }).catch((error: any) => {

    //             //ToastAndroid.show("PDF error >>>" + error, 1000)
    //         });
    // }
    
    private getParams(path: any) {

        const formData = new FormData();
        //formData.append('file', path); 

        formData.append("profile_image", {
            name: 'pdf_file',
            type: 'image/PNG',
            uri: '/data/user/0/com.mediport/cache/1578298497486.PNG'
        });

        console.log('Forma data >>>>', formData)

        return formData
    }

    private getAppointmentInfoDict() {
        return {
            appointmentFor: this.state.selectedAppointmentFor,
            age: this.state.age,
            bloodGroup: this.state.bloodGroup,
            patientName: this.state.patientName,
            contactNumber: this.state.conatctNo,
            description: this.state.healthIssue,
            reportIds: this.state.images,
            gender: this.state.gender
        }
    }

    private genderSelected(gender: Number) {
        this.setState({ gender: gender })
    }

    // Validate Login Credentials eneterd
    private validateDetail() {

        if (this.state.age.length <= 0) {
            Toast.show(localize.pleaseEnterAge, Toast.SHORT);
            return false;
        }
        else if (this.state.bloodGroup.length <= 0) {
            Toast.show(localize.pleaseEnterBloodGroup, Toast.SHORT);
            return false;
        }
        else if (this.state.patientName.length <= 0) {
            Toast.show(localize.pleaseEnterPatientsName, Toast.SHORT);
            return false;
        }
        else if (this.state.conatctNo.length <= 0) {
            Toast.show(localize.pleaseEnterContactNum, Toast.SHORT);
            return false;
        }
        else if (!validator.isMobilePhone(this.state.conatctNo, 'any', { strictMode: false })) {
            Toast.show(localize.pleaseEnterValidContactNum, Toast.SHORT);
            return false;
        }

        return true;
    }

    addBtnAction() {

        if (this.state.isImagePickerShown == true) {
            return
        }

        this.setState({ isImagePickerShown: true })

        CustomImagePicker.showImgPicker().then((response: any) => {

            console.log('Image Selced >>>', response)

            this.setState({ recentSelectedImageType: response.type })
            this.resizeImage(response)

        }).catch((error: any) => {
            this.setState({ isImagePickerShown: false })
            if (error != undefined) {
                General.showErroMsg(this, error)
            }
        })

    }
    private resizeImage(image: any) {
        let newWidth = 800;
        let newHeight = 800 / (image.width / image.height);

        console.log('resizeImageandUpload func called >>', image)

        ImageResizer.createResizedImage(image.uri, 800, 650, 'PNG', 100).then((response: any) => {
            console.log('Image Resize REsponse  >>>', response)
            this.setState({ images: [...this.state.images, {type: this.state.recentSelectedImageType, uri: response.uri, name: response.name}], isImagePickerShown: false })
        }).catch((err: any) => {
            this.setState({ isImagePickerShown: false })
            console.log('Image Resize error  >>>', err)
        });
    }

    // Called when Blod group selected from Picker
    private bloodGroupSelected(value: any, index: any) {

        console.log('Blood Group  Selcted >>>', value)
        
        if (index == 0 && Platform.OS === 'ios') {
            this.setState({ showBloodGroupPicker: false })
            return
        }
        else
        {
            this.setState({ showBloodGroupPicker: false, bloodGroup: value })
        }
    }

    // private bloodGroupSelected(value: any) {
    //     this.setState({ showBloodGroupPicker: false, bloodGroup: value })
    // }

    cellSelected(index: any) {
        //ToastAndroid.show(index + "", 1000)
        var array = [...this.state.images];

        array.splice(index, 1);
        this.setState({ images: array });
    }

    render() {
        let doctorData: any = this.props.doctorInfo

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={{ flexDirection: 'row', marginTop: 16 }}>
                                <View style={CommonStyles.doctorImgCntnrStyle}>
                                    {this.props.doctorInfo.image == '' ?
                                        <Image
                                            source={require('../../assets/avatar.png')}
                                            style={CommonStyles.doctorImg} /> :
                                        <Image style={CommonStyles.doctorImg} source={{ uri: this.props.doctorInfo.image }} />
                                        // <ImageLoad style={CommonStyles.doctorImg} placeholderSource={require('../../assets/avatar.png')} source={{ uri: this.props.doctorInfo.image }} />
                                    }
                                </View>
                                <View style={{ width: '60%', paddingLeft: 20, paddingTop: 3 }}>
                                    <Text style={styles.text}>{doctorData.name}</Text>
                                    <Text style={[styles.smallText, { color: APP_HEADER_BACK_COLOR, marginTop: 8 }]}>{this.props.doctorInfo.categoryName}</Text>
                                    {doctorData.address.count > 0 ? <View style={{ flexDirection: 'row', marginTop: 8 }}>
                                        <Image
                                            source={require('../../assets/location.png')}
                                            style={styles.smallImg}
                                            resizeMode={'contain'}
                                        />
                                        <Text style={styles.smallText}>{doctorData.address.count > 0 ? doctorData.address[0].full_address : ''}</Text>
                                    </View> : null}
                                    <Text style={[styles.blueText, { marginTop: 8 }]}>
                                        $ {doctorData.price}</Text>
                                </View>
                            </View>
                            <LineVw />
                            <Text style={[styles.ratingTxt, { textAlign: 'left', marginTop: 10, fontSize: 15 }]}>{localize.appointmentFor}</Text>
                            <ScrollView style={{ marginTop: 5 }}
                                horizontal={true} showsHorizontalScrollIndicator={false}>
                                {this.state.appointmentForArr.map((item: any, index: number) => (
                                    <TouchableOpacity
                                        style={[item.isSelected ? styles.appointmentForSelectedBtn : styles.appointmentForBtn, (index == 0 ? { marginLeft: 0 } : null), (index == this.state.appointmentForArr.length ? { marginRight: 0 } : null)]}
                                        onPress={() => this.appoitmentFor(index)}>
                                        <Text style={[{ textAlign: 'center' }, (item.isSelected ? { color: APP_HEADER_BACK_COLOR } : { color: APP_LIGHT_GREY_COLOR })]}>{item.appointmentOption}</Text>
                                    </TouchableOpacity>
                                )
                                )}
                            </ScrollView>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <TextInputWithoutImageCustomWidthHeight placeholder={localize.agePlaceholder} onChange={(text: any) => this.setState({ age: text })} value={this.state.age} autoCapitalize='none' width='40%' keyboardType='numeric' returnKeyType="done" maxLength={3} />
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity
                                        style={[styles.genderBtn, commonShadowStyle, (this.state.gender == 1 ? { backgroundColor: APP_HEADER_BACK_COLOR } : { backgroundColor: APP_LIGHT_GREY_COLOR })]}
                                        onPress={() => this.genderSelected(1)}>
                                        <Image
                                            source={require('../../assets/male.png')}
                                            style={styles.smallImg}
                                            resizeMode={'contain'}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[styles.genderBtn, commonShadowStyle, (this.state.gender == 2 ? { backgroundColor: APP_HEADER_BACK_COLOR } : { backgroundColor: APP_LIGHT_GREY_COLOR })]}
                                        onPress={() => this.genderSelected(2)}>
                                        <Image
                                            source={require('../../assets/female.png')}
                                            style={styles.smallImg}
                                            resizeMode={'contain'}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>


                            {/* <TextInputWithoutImageCustomWidthHeight placeholder={localize.booldGroupPlaceholder} onChange={(text: any) => this.setState({ bloodGroup: text })} value={this.state.bloodGroup} autoCapitalize='none' width='50%' /> */}

                            {Platform.OS == 'android' ?
                                <TextInputWithDropdownAndroid data={bloodGroups} func={(value: any, index: any) => this.bloodGroupSelected(value, index)} value={this.state.bloodGroup} placeholder='Blood Group A+'  width='50%' />
                                :
                                <TextInputWithDropdownIOS func={(text: any) => this.setState({ showBloodGroupPicker: true, showGenderPicker: false })} value={this.state.bloodGroup} placeholder='Blood Group A+' width='50%' textAlign='left' />
                            }

                            {/* <TextInputWithDropdown func={(text: any) => this.setState({ showBloodGroupPicker: true })} value={this.state.bloodGroup} placeholder='Blood Group A+'  textAlign='left' /> */}

                            <TextInputWithoutImageCustomWidthHeight placeholder={localize.patientsName} onChange={(text: any) => this.setState({ patientName: text })} value={this.state.patientName} autoCapitalize='none' width='100%' />

                            <TextInputWithoutImageCustomWidthHeight placeholder={localize.contactNumber} onChange={(text: any) => this.setState({ conatctNo: text })} value={this.state.conatctNo} autoCapitalize='none' width='100%' keyboardType='phone-pad' returnKeyType="done" />

                            <TextInputMultiLine placeholder={localize.typeHealthIssue} onChange={(text: any) => this.setState({ healthIssue: text })} value={this.state.healthIssue} autoCapitalize='none' width='100%' height={150} returnKeyType='done' />

                            {/* <TextInputWithoutImageCustomWidthHeight placeholder={localize.typeHealthIssue} onChange={(text: any) => this.setState({ healthIssue: text })} value={this.state.healthIssue} autoCapitalize='none' width='100%' height={120} mutliline={true} /> */}

                            <Text style={[styles.ratingTxt, { textAlign: 'left', marginTop: 23, fontSize: 14 }]}>{localize.uploadReports}</Text>

                            <View style={{ flexDirection: "row", marginTop: 22 }}>
                                {this.state.images.length < 3 ?

                                    <TouchableOpacity
                                        style={styles.addBtn}
                                        onPress={() => this.addBtnAction()}>
                                        <Text style={{ color: APP_HEADER_BACK_COLOR, textAlign: 'center' }}>+</Text>
                                        <Text style={{ color: APP_HEADER_BACK_COLOR, textAlign: 'center' }}>{localize.add}</Text>
                                    </TouchableOpacity>
                                    : null}
                                <FlatList
                                    style={{ marginLeft:8 }}
                                    data={this.state.images}
                                    keyExtractor={(item: any, index: any) => index.toString()}
                                    renderItem={({ item, index }) => <PDFImagesCell
                                        onClickEvent={(index: any) => this.cellSelected(index)}
                                        item={item} index={index} />}
                                    numColumns={3}

                                />
                            </View>

                            <CommonBtn title={localize.next} func={() => this.nextBtnAction()} width='70%' />
                        </ScrollView>
                    </View>
                </View>
                {this.state.showBloodGroupPicker ?
                    <CustomPickerIOS data={bloodGroupsIOS} func={(value: any, index: any) => this.bloodGroupSelected(value, index)} /> : null}
                    
                {/* {this.state.showBloodGroupPicker ?
                    <CustomPicker data={bloodGroupsIOS} func={(value: any) => this.bloodGroupSelected(value)} /> : null} */}
            </KeyboardAvoidingView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    doctorInfo: state.saveDoctorInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
    saveBookingInfo: (info: string) => dispatch({ type: BOOKING_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(BookAppointment);
