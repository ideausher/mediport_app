import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, FlatList, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, ScrollView, Modal, Alert, ImageStore, StatusBar } from 'react-native';
import { APP_HEADER_BACK_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles'
import { localize } from '../../Utils/LocalisedManager';
import { CustomPickerIOS } from '../../Custom/CustomPicker';
import { CommonBtn } from '../../Custom/CustomButton';
import RequestManager from '../../Utils/RequestManager'
import { API_SIGNUP, API_UPLOAD_IMAGE } from '../../Utils/APIs'
import LocalDataManager from '../../Utils/LocalDataManager';
import { ForGotScreenType } from '../../Utils/Enums';
import General from '../../Utils/General';
//@ts-ignore
import { connect } from 'react-redux';
import { LOGIN } from '../../Redux/Constants';
import { OpenDrawerHeaderBtn } from '../../Custom/CustomComponents';
import { TextInputWithImage, TextInputWithImageFunc, TextInputWithDropdownIOS, TextInputWithDropdownAndroid, TextWithImage } from '../../Custom/CustomTextInput';
import CustomImagePicker from '../../Utils/CustomImagePicker';
import ImageResizer from 'react-native-image-resizer';
import { Loader } from '../../Utils/Loader';

const { StatusBarManager } = NativeModules;
const genderList = ['Male', 'Female']
const bloodGroups = ['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-']
const genderListIOS = ['- Choose Gender -', 'Male', 'Female']
const bloodGroupsIOS = ['- Choose Blood Group -', 'A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-']

export interface props {
    navigation: NavigationScreenProp<any, any>
    loginInfo: any,
    saveLoginInfo: any,
};

class Profile extends Component<props, object> {

    state = {
        statusBarHeight: 0,
        appointments: '0',
        doctors: '0',
        isEditable: false,
        editBtnText: 'Edit',
        showBloodGroupPicker: false,
        showGenderPicker: false,
        otp: '',
        avatarSource: undefined as any,
        uploadImageType: '',
        showLoader: false
    };

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.profile}</Text>
                </View>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {

        console.log('Component Did mount Called')
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        this.setState({ editBtnText: localize.edit, isEditable: false })

        //this.setUserInfo()
    }

    // OPen Side Menu
    private revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    componentWillReceiveProps(prop: any) {
        console.log('Props data: ', prop)
    }

    // // Get Login User info from Local data and Set in state
    // private setUserInfo() {
    //     LocalDataManager.getLoginInfo().then((data: any) => {

    //         this.setState({
    //             name: data.firstname,
    //             email: data.email,
    //             phone: data.phone_number,
    //             countryCode: data.phone_country_code
    //         })
    //     })
    // }

    // This Func is passed to upadte Phone number
    private updatePhoneNumber(phoneNum: string, countryCod: string, otpStr: string) {
        console.log('Update Phone Num func called?>>>>', phoneNum, countryCod, otpStr);
        this.setState({ otp: otpStr })

        this.updateLoginInfo(phoneNum, countryCod, this.props.loginInfo.gender, this.props.loginInfo.bloodGroup, this.props.loginInfo.image)
    }

    private updateLoginInfo(phoneNum: string, countryCod: string, genderStr: string, bloodGroup: string, ImageStr: string) {

        let loginDict = this.getLoginInfoDict(phoneNum, countryCod, genderStr, bloodGroup, ImageStr)
        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(loginDict))
        this.props.saveLoginInfo(loginDict)

    }
    private getLoginInfoDict(phoneNum: string, countryCod: string, genderStr: string, bloodGroup: string, ImageStr: string) {

        return {
            email: this.props.loginInfo.email,
            firstname: this.props.loginInfo.name,
            id: this.props.loginInfo.id,
            phone_country_code: countryCod,
            phone_number: phoneNum,
            token: this.props.loginInfo.token,
            blood_group: bloodGroup,
            gender: genderStr,
            image: ImageStr
        }
    }

    // On edit Btn Click set all fields to editable
    private editBtnAction() {
        this.setState({ isEditable: true })
    }

    // Called when Blod group selected from Picker
    bloodGroupSelected(value: any, index: any) {

        console.log('Blood Group  Selcted >>>', value)
        this.setState({ showBloodGroupPicker: false })
        if (index == 0 && Platform.OS === 'ios') {
            return
        }

        this.updateLoginInfo(this.props.loginInfo.phoneNumber, this.props.loginInfo.countryCode, this.props.loginInfo.gender, value, this.props.loginInfo.image)
    }

    // Called when Gender selected from Picker
    genderSelected(value: any, index: any) {

        this.setState({ showGenderPicker: false })
        if (index == 0 && Platform.OS === 'ios') {
            return
        }

        this.updateLoginInfo(this.props.loginInfo.phoneNumber, this.props.loginInfo.countryCode, value, this.props.loginInfo.bloodGroup, this.props.loginInfo.image)
    }

    // Called on Save  btn Click
    private saveBtnAction() {
        this.setState({ showLoader: true })
        this.callProfileUpdateAPI()
    }

    // paarmeters for API
    private getParams() {

        var params: any = {
            "name": this.props.loginInfo.name,
            "gender": this.props.loginInfo.gender,
            "phone_country_code": this.props.loginInfo.countryCode,
            "phone_number": this.props.loginInfo.phoneNumber,
            "blood_group": this.props.loginInfo.bloodGroup,
        }

        if (this.state.otp != '') {
            params['otp'] = this.state.otp
        }

        return params
    }

    // Call API to update User Data
    private callProfileUpdateAPI() {
        RequestManager.putRequestWithHeaders(API_SIGNUP, this.getParams()).then((response: any) => {
            this.setState({ isEditable: false, showLoader: false, otp: '' })
            General.showMsgWithDelay(response.message)
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    // Moved on next screen to update Phone Number
    private EditPhoneNumber() {
        console
        this.props.navigation.navigate('ForgotPassword', { forgotScreenType: ForGotScreenType.editPhoneNumber, params: { phone: this.props.loginInfo.phoneNumber, countryCode: this.props.loginInfo.countryCode }, func: this.updatePhoneNumber.bind(this) });
    }

    private async updateImg() {
        CustomImagePicker.showImgPicker().then((response: any) => {

            console.log('Image Selced >>>', response)
            this.setState({ uploadImageType: response.type, showLoader: true })
            this.resizeImageandUpload(response)

        }).catch((error: any) => {
            if (error != undefined) {
                General.showErroMsg(this, error)
            }
        })
    }

    private resizeImageandUpload(image: any) {
        let newWidth = 800;
        let newHeight = 800 / (image.width / image.height);

        let rotation = 0

        if (image.originalRotation === 90) {
            rotation = 90
        } else if (image.originalRotation === 270) {
            rotation = -90
        }

        ImageResizer.createResizedImage(image.uri, newWidth, newHeight, 'JPEG', 100, rotation).then((response) => {
            this.setState({ avatarSource: response })

            setTimeout(() => {
                this.apiUploadImage()
            }, 200);

        }).catch((err) => {
            console.log('Image Resize error  >>>', err)
        });
    }

    private uploadImageParams() {

        const formData = new FormData();
        // formData.append('profile_image', this.state.avatarSource.data); 

        formData.append("profile_image", {
            name: this.state.avatarSource.name,
            type: this.state.uploadImageType,
            uri:
                Platform.OS === "android" ? this.state.avatarSource.uri : this.state.avatarSource.uri.replace("file://", "")
        });

        return formData
    }

    // API tp send OTp on added Phone Number for verification
    apiUploadImage() {
        RequestManager.uploadImage(API_UPLOAD_IMAGE, this.uploadImageParams()).then((response: any) => {
            console.log('REsponse of apiUploadImage >>>>', response);
            this.updateLoginInfo(this.props.loginInfo.phoneNumber, this.props.loginInfo.countryCode, this.props.loginInfo.gender, this.props.loginInfo.bloodGroup, response.data.profile_image)

            this.setState({ showLoader: false })

            setTimeout(() => {
                Alert.alert(response.message)
            }, 200);

        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    render() {

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                            {this.state.isEditable ? null : <TouchableOpacity onPress={() => this.editBtnAction()}><Text style={styles.editText}>{this.state.editBtnText}</Text></TouchableOpacity>}

                            <View style={styles.imgVw}>
                                <TouchableOpacity onPress={() => this.updateImg()}>
                                    {this.state.avatarSource != undefined ?
                                        <Image
                                            source={{ uri: this.state.avatarSource.uri }}
                                            style={styles.img}
                                            resizeMode='cover' /> :
                                        this.props.loginInfo.image != '' ?
                                            <Image
                                                source={{ uri: this.props.loginInfo.image }}
                                                style={[styles.img, { backgroundColor: 'transparent' }]}
                                                resizeMode='cover' /> :
                                            <Image
                                                source={require('../../assets/avatar.png')}
                                                style={[styles.img, { backgroundColor: 'transparent' }]}
                                                resizeMode='cover' />
                                    }

                                </TouchableOpacity>
                                <TextInput style={styles.text}>{this.props.loginInfo.name}</TextInput>
                            </View>
                            <View style={styles.emailVw}>
                                <View style={{ width: '48%' }}>
                                    <Text style={styles.emailText}>{localize.Email}</Text>
                                    <TextWithImage placeholder={localize.Email} onChange={(text: any) => this.setState({ email: text })} value={this.props.loginInfo.email} icon={require('../../assets/mail.png')} autoCapitalize='none' />
                                    {/* <TextInputMultiLine placeholder={localize.typeHealthIssue} onChange={(text: any) => this.setState({ healthIssue: text })} value={this.state.healthIssue} autoCapitalize='none' width='100%' height={150} returnKeyType='done' /> */}
                                    {/* <TextInputWithImage placeholder={localize.Email} onChange={(text: any) => this.setState({ email: text })} value={'this.props.loginInfo.email sdisuidsidusidu'} autoCapitalize='none' icon={require('../../assets/mail.png')} editable={false} /> */}
                                </View>
                                <View style={{ width: '48%' }}>
                                    <Text style={styles.emailText}>{localize.phone}</Text>
                                    <TextInputWithImageFunc placeholder={localize.phone} value={this.props.loginInfo.countryCode + ' ' + this.props.loginInfo.phoneNumber} icon={require('../../assets/phone.png')} editable={false} func={() => this.EditPhoneNumber()} disabled={!this.state.isEditable} />
                                </View>
                            </View>

                            {Platform.OS == 'android' ?
                                <View style={styles.emailVw}>
                                    <View style={{ width: '48%' }}>
                                        <Text style={styles.emailText}>{localize.gender}</Text>
                                        <TextInputWithDropdownAndroid data={genderList} func={(value: any, index: any) => this.genderSelected(value, index)} value={this.props.loginInfo.gender} placeholder='Male' marginTop={8} disabled={!this.state.isEditable} img={require('../../assets/gender.png')} />
                                    </View>
                                    <View style={{ width: '48%' }}>
                                        <Text style={styles.emailText}>{localize.bloodGroup}</Text>
                                        <TextInputWithDropdownAndroid data={bloodGroups} func={(value: any, index: any) => this.bloodGroupSelected(value, index)} value={this.props.loginInfo.bloodGroup} placeholder='A+' marginTop={8} disabled={!this.state.isEditable} img={require('../../assets/blood-grp.png')} />
                                    </View>
                                </View>
                                :
                                <View style={styles.emailVw}>
                                    <View style={{ width: '48%' }}>
                                        <Text style={styles.emailText}>{localize.gender}</Text>
                                        <TextInputWithDropdownIOS func={(text: any) => this.setState({ showGenderPicker: true, showBloodGroupPicker: false })} value={this.props.loginInfo.gender} placeholder='Male' marginTop={8} disabled={!this.state.isEditable} img={require('../../assets/gender.png')} textAlign='left' />
                                    </View>
                                    <View style={{ width: '48%' }}>
                                        <Text style={styles.emailText}>{localize.bloodGroup}</Text>
                                        <TextInputWithDropdownIOS func={(text: any) => this.setState({ showBloodGroupPicker: true, showGenderPicker: false })} value={this.props.loginInfo.bloodGroup} placeholder='A+' marginTop={8} disabled={!this.state.isEditable} img={require('../../assets/blood-grp.png')} textAlign='left' />
                                    </View>
                                </View>
                            }


                            <View style={[styles.emailVw, { paddingBottom: 10 }]}>
                                <View style={{ width: '48%' }}>
                                    <Text style={styles.emailText}>{localize.appointments}</Text>
                                    <TextInputWithImage placeholder={localize.appointments} onChange={(text: any) => this.setState({ appointments: text })} value={this.state.appointments} autoCapitalize='none' icon={require('../../assets/appointProfile.png')} editable={false} />
                                </View>
                                <View style={{ width: '48%' }}>
                                    <Text style={styles.emailText}>{localize.myDoctors}</Text>
                                    <TextInputWithImage placeholder={localize.myDoctors} onChange={(text: any) => this.setState({ doctors: text })} value={this.state.doctors} autoCapitalize='none' icon={require('../../assets/doc.png')} editable={false} />
                                </View>
                            </View>
                            {this.state.isEditable ? <CommonBtn title={localize.save} func={() => this.saveBtnAction()} width='60%' /> : null}
                        </ScrollView>
                    </View>
                </View>

                {this.state.showGenderPicker ?
                    <CustomPickerIOS data={genderListIOS} func={(value: any, index: any) => this.genderSelected(value, index)} /> : null}
                {this.state.showBloodGroupPicker ?
                    <CustomPickerIOS data={bloodGroupsIOS} func={(value: any, index: any) => this.bloodGroupSelected(value, index)} /> : null}

                {/* <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showGenderPicker}>
                    <DropdownList data={genderList} topMargin={50} func={(item: any) => this.genderSelected(item)} removeList={() => this.setState({ showGenderPicker: false })} />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showBloodGroupPicker}>
                    <DropdownList data={bloodGroups} topMargin={50} func={(item: any) => this.bloodGroupSelected(item)} removeList={() => this.setState({ showBloodGroupPicker: false })} />
                </Modal> */}

            </KeyboardAvoidingView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    loginInfo: state.saveLoginInfo,
});

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
