//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, App_MIDIUM_GREY_COLOR, APP_LIGHT_GREY_COLOR, APP_DARK_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        padding: 16,
    },
    imgVw: {
        marginTop: 16,
        flexDirection: 'row',
        alignItems: 'center',
    },
    img: {
        height: 100,
        width: 100,
        borderRadius: 50,
        borderColor: 'black',
    },
    text: {
        marginLeft: 16,
        marginRight: 8,
        fontSize: 21,
        fontWeight: 'bold',
        color: APP_DARK_GREY_COLOR,
        textAlign: 'left',
        marginTop: 8,
    },
    editText: {
        alignSelf: 'flex-end',
        color: APP_GREY_TEXT_COLOR,
        fontSize: 14,
        fontWeight: '400'
    },
    emailVw: {
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    emailText: {
        color: APP_GREY_TEXT_COLOR,
        fontSize: 14,
    }
});