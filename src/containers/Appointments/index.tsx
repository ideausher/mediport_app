import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity, Image, SafeAreaView, FlatList, Modal, KeyboardAvoidingView, Platform, ToastAndroid, Dimensions } from 'react-native';
import { APP_HEADER_TITLE_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREY_TEXT_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle, commonShadowStyle, App_MIDIUM_GREY_COLOR, APP_LIGHT_GREY_COLOR, APP_GREEN_COLOR, LOGIN_DATA } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { TabView, TabBar } from 'react-native-tab-view';
import { styles } from './styles';
import AppointmentCell from '../../Custom Cells/AppointmentCell';
import HistoryAppointmentCell from '../../Custom Cells/HistoryAppointmentCell';
import { localize } from '../../Utils/LocalisedManager';
import { OpenDrawerHeaderBtn, NotificationsHeaderBtn, BottomShadowView, NoInternetFoundView, NoDataFoundView } from '../../Custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager'
import { API_APPOINTMENT_LIST } from '../../Utils/APIs';
import { Loader } from '../../Utils/Loader';
import General from '../../Utils/General';
import { CommonBtn } from '../../Custom/CustomButton';
import { TextInput, State } from 'react-native-gesture-handler';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const cancelReasonsArr = ['Don\'t want to consult the doctor', 'Don\'t trust the online consultation', 'Feeling better now', 'Can\'t attend the call today', 'Others']

const CancelAppointmentPopup = (props: any) => {

    let selectedCancelReasonIndex = props.selectedReasonIndex
    let otherReason = ''//props.reason

    console.log('Confirm Pop up Called >>>', props.selectedReasonIndex)

    return (
        <View style={[styles.blueConatiner, { justifyContent: 'center', backgroundColor: 'rgba(134, 145, 160, 0.9)', }]}>
            <View style={[styles.popupVw, commonShadowStyle]}>
                <TouchableOpacity style={{ width: 40, height: 40, borderRadius: 20, borderColor: APP_GREEN_COLOR, borderWidth: 2, alignSelf: 'flex-end', marginLeft: 8, justifyContent: 'center', alignItems: 'center' }} onPress={() => props.removePopup()}>
                    <Text style={{ fontSize: 24, fontWeight: '500', color: APP_GREEN_COLOR, textAlign: 'center' }}>X</Text>
                </TouchableOpacity>
                <Text style={styles.selectReasonText}>{localize.selectReasonCancelAppointment}</Text>
                {cancelReasonsArr.map((item: any, index: number) => (
                    <TouchableOpacity style={{ flexDirection: 'row', marginTop: 16, width: '94%', alignItems: 'center' }} onPress={() => props.reasonSelected(index)}>
                        <View style={{ width: 14, height: 14, borderRadius: 7, backgroundColor: (selectedCancelReasonIndex == index ? APP_HEADER_BACK_COLOR : 'white'), borderColor: App_MIDIUM_GREY_COLOR, borderWidth: 1 }} />
                        <Text style={styles.greyText}>{item}</Text>
                    </TouchableOpacity>
                ))}

                {selectedCancelReasonIndex == 4 ? <TextInput style={styles.txtFld} placeholder={localize.enterReasonHere} placeholderTextColor={App_MIDIUM_GREY_COLOR} onChangeText={(text) => { otherReason = text }} /> : null}
                
                <CommonBtn title={localize.cancelAppointment} func={() => props.onClickEvent(otherReason)} width='60%' />
            </View>
        </View>
    );
};

const Upcoming = (props: any) => {
    return (
        <View style={{ flex: 1 }} >
            <FlatList
                onRefresh={() => props.refreshList()}
                refreshing={props.isRefreshing}
                style={{ marginTop: 8, width: '100%' }}
                data={props.data}
                keyExtractor={(item: any, index: any) => index.toString()}
                renderItem={({ item, index }: any) => <AppointmentCell
                    onClickEvent={(indx: any) => props.onPress(indx)}
                    messageEvent={(index: any) => props.messageEvent(index)}
                    item={item} index={index} />}
                onEndReachedThreshold={0.5}
                onScroll={({ nativeEvent }) => {
                    if (props.isCalled == false && props.isCloseToBottom(nativeEvent) && props.isRefreshing == false) {
                        props.loadMoreData()
                    }
                }}
            />
        </View>
    )
};

const History = (props: any) => {
    return (
        <View style={{ flex: 1 }} >

            <FlatList
                onRefresh={() => props.refreshList()}
                refreshing={props.isRefreshing}
                style={{ marginTop: 8, width: '100%' }}
                data={props.data}
                keyExtractor={(item: any, index: any) => index.toString()}
                renderItem={({ item, index }: any) => <HistoryAppointmentCell
                    viewReportsEvent={(index: any) => props.viewReportsEvent(index)} 
                    messageEvent={(index: any) => props.messageEvent(index)} 
                    feedbackEvent={(index: any) => props.feedbackEvent(index)}
                    item={item} index={index} />}
                onEndReachedThreshold={0.5}
                onScroll={({ nativeEvent }) => {
                    if (props.isCalled == false && props.isCloseToBottom(nativeEvent) && props.isRefreshing == false) {
                        props.loadMoreData()
                    }
                }}
            />
        </View>
    )
};

export interface props {
    navigation: NavigationScreenProp<any, any>
};

interface stateStruct {
    index: number,
    routes: any,
    showLoader: false
    statusBarHeight: any,
    isRefreshing: boolean,
    loadMore: boolean,
    isCalled: boolean,
    upcomingPage: number,
    upcomingPrevPage: number,
    historyPage: number,
    historyPrevPage: number,
    isInternetError: boolean,
    isDataNotFoundError: boolean,
    upcomingAptArr: any[],
    historyAptArr: any[],
    showCancelAppointmentPopup: boolean,
    selectedCancelReasonIndex: number,
    selectedAppointmentIndex: number,
    //otherReason: String,
}

export default class Appointments extends Component<props, object> {

    state: stateStruct = {
        showLoader: false,
        routes: [
            { key: 'first', title: localize.upcoming },
            { key: 'second', title: localize.history },
        ],
        statusBarHeight: 0,
        index: 0,
        isRefreshing: false,
        loadMore: false,
        isCalled: false,
        upcomingPage: 0,
        upcomingPrevPage: -1,
        historyPage: 0,
        historyPrevPage: -1,
        isInternetError: false,
        isDataNotFoundError: false,
        upcomingAptArr: [],
        historyAptArr: [],
        showCancelAppointmentPopup: false,
        selectedCancelReasonIndex: -1,
        selectedAppointmentIndex: 0,
       // otherReason: ''
    };

    otherReason = ''

    static navigationOptions = ({ navigation }: any) => {

        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.appointments}</Text>
                </View>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerRight: (
                <NotificationsHeaderBtn func={navigation.getParam('notificationsHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler() });

        this.props.navigation.addListener('willFocus', (route) => this.handleIndexChange(this.state.index));
    }

    // All Funs for refersh and Load More
    private refreshList() {
        console.log('refreshList func called >>>')
        this.setState({ isRefreshing: true })
        setTimeout(() => {
            this.apiGetAppointmentList(this.state.index, true)
        }, 200)
    }

    //Will call when reached last cell
    private loadMoreData = () => {

        if (this.state.index == 0 && this.state.upcomingPrevPage == this.state.upcomingPage) {
            return
        }
        else if (this.state.index == 1 && this.state.historyPrevPage == this.state.historyPage) {
            return
        }

        this.setState({ loadMore: true, isCalled: true })
        setTimeout(() => {
            this.apiGetAppointmentList(this.state.index, false)
        }, 200)
    }

    private isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }: any) => {

        const paddingToBottom = 50; // how far from the bottom
        let value1 = layoutMeasurement.height + contentOffset.y
        let value2 = contentSize.height - paddingToBottom
        let isVAlue2GreaterThanLayout = value2 > layoutMeasurement.height
        let value1GreterthanValue2 = value1 >= value2
        let isClose = (isVAlue2GreaterThanLayout && value1GreterthanValue2)

        return isClose
    };

    getApiParams(index: number) {
        return {
            type: index == 0 ? 1 : 2,
            page: this.state.isRefreshing ? 0 : (index == 0 ? this.state.upcomingPage : this.state.historyPage),
            limit: 10
        }
    }

    async apiGetAppointmentList(index: number, refresh: boolean) {

        let weakSelf = this
        this.setState({ isInternetError: false, isDataNotFoundError: false })
        console.log('apiGetAppointmentList called with params >>>>', this.getApiParams(index))
        RequestManager.getRequest(API_APPOINTMENT_LIST, this.getApiParams(index)).then((response: any) => {

            if (index == 0) {
                this.updateUpcomingAptData(refresh, response)
            }
            else {
                this.updateHistoryAptData(refresh, response)
            }
        }).catch((error: any) => {
            this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
            General.showErroMsg(this, error)
        })
    }

    private updateUpcomingAptData(refresh: boolean, response: any) {
        let upcomingAptArrData: any[] = []
        if (refresh) {
            this.setState({ upcomingPage: 0, upcomingPrevPage: -1, upcomingAptArr: [] })
            upcomingAptArrData = response.data
        }
        else {
            upcomingAptArrData = this.state.upcomingAptArr.concat(response.data)
        }

        setTimeout(() => {
            this.setState({ showLoader: false, upcomingAptArr: upcomingAptArrData, loadMore: false, upcomingPrevPage: this.state.upcomingPage, isRefreshing: false })

            setTimeout(() => {
                if (this.state.upcomingAptArr.length == 0) {
                    this.setState({ isInternetError: false, isDataNotFoundError: true })
                }

                this.setState({ isCalled: false })
            }, 200)

            if (response.data.length > 0) {
                let nextPage = this.state.upcomingPage + 1
                this.setState({ upcomingPage: nextPage })
            }
        }, 200)
    }

    private updateHistoryAptData(refresh: boolean, response: any) {

        let historyAptArrData: any[] = []
        if (refresh) {
            this.setState({ historyPage: 0, historyPrevPage: -1, historyAptArr: [] })
            historyAptArrData = response.data
        }
        else {
            historyAptArrData = this.state.historyAptArr.concat(response.data)
        }

        setTimeout(() => {

            this.setState({ showLoader: false, historyAptArr: historyAptArrData, loadMore: false, historyPrevPage: this.state.historyPage, isRefreshing: false })

            setTimeout(() => {
                if (this.state.historyAptArr.length == 0) {
                    this.setState({ isInternetError: false, isDataNotFoundError: true })
                }
                this.setState({ isCalled: false })
            }, 200)

            if (response.data.length > 0) {
                let nextPage = this.state.historyPage + 1
                this.setState({ historyPage: nextPage })
            }
        }, 200)
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    private notificationsHandler() {
        this.props.navigation.navigate('Notifications')
    }

    handleIndexChange(indx: number) {

        this.setState({ index: indx })
        if (indx == 0 && this.state.upcomingAptArr.length > 0) {
            return
        }
        else if (indx == 1 && this.state.historyAptArr.length > 0) {
            return
        }

        this.setState({ showLoader: true })
        this.apiGetAppointmentList(indx, false)
    };

    viewReportsAction(index: any) {
        this.props.navigation.navigate('ViewReports', { venderId: this.state.historyAptArr[index].vender_id })
    }

    // messageAction(index: any) {
    //     console.log('Message Action Called with Index: ', index, this.state.historyAptArr[index].vender_id)
    //     this.props.navigation.navigate('ChatScreen', {'vendorId': this.state.historyAptArr[index].vender_id});
    // }

    messageActionForUpcoming=(index: any)=>{
        console.log(index)
        AsyncStorage.getItem(LOGIN_DATA).then((res:any) => JSON.parse(res)).then((data) => {
           console.log("selected Doctor Detail >>>>>",this.state.upcomingAptArr[index].vender_id)
            let chat_id=data.id+""+this.state.upcomingAptArr[index].vender_id
            let senderDict = {
                firstName: this.state.upcomingAptArr[index].vendor.firstname,
                lastName: this.state.upcomingAptArr[index].lastName != undefined ? this.state.upcomingAptArr[index].lastName: "",
                image: this.state.upcomingAptArr[index].vendor.image,
                // email: this.props.loginInfo.email,
                createdOn: Date.now(),
                id: this.state.upcomingAptArr[index].vender_id,
                chatId: chat_id
            }
    
            this.props.navigation.navigate('Messages', { chatData: senderDict })
        })

    }

    messageAction=(index: any)=>{
        AsyncStorage.getItem(LOGIN_DATA).then((res:any) => JSON.parse(res)).then((data) => {
           console.log("selected Doctor Detail >>>>>",this.state.historyAptArr[index].vender_id)
            let chat_id=data.id+""+this.state.historyAptArr[index].vender_id
            let senderDict = {
                firstName: this.state.historyAptArr[index].vendor.firstname,
                lastName: this.state.historyAptArr[index].lastName != undefined ? this.state.historyAptArr[index].lastName: "",
                image: this.state.historyAptArr[index].vendor.image,
                // email: this.props.loginInfo.email,
                createdOn: Date.now(),
                id: this.state.historyAptArr[index].vender_id,
                chatId: chat_id
            }
    
            this.props.navigation.navigate('Messages', { chatData: senderDict })
        })

        
    }

    feedbackAction(index: any) {
        console.log('feedback Action Called with Index: ', index, this.state.historyAptArr[index].id)
        this.props.navigation.navigate('Feedback', {'vendorId': this.state.historyAptArr[index].id});
    }

    reasonSelected(index: any) {
        this.setState({ selectedCancelReasonIndex: index })
    }

    cancelAppointment(index: any) {

        if(this.state.upcomingAptArr[index].status == "1")
        {
            this.setState({ showCancelAppointmentPopup: true, selectedAppointmentIndex: index })
        }
    }

    getCancelApiParams(selectedCancelReason: any) {

        return {
            "booking_id": this.state.upcomingAptArr[this.state.selectedAppointmentIndex].id,
            "note": selectedCancelReason
        }
    }

    callCancelAppointmentAPI(selectedCancelReason: any) {

        if (this.state.selectedCancelReasonIndex == -1) {
            Toast.show(localize.pleaseSelectReasonOFCancel, Toast.SHORT);

            return;
        }

        this.setState({ showCancelAppointmentPopup: false })
        let reason = selectedCancelReason

        if (this.state.selectedCancelReasonIndex < 4) {
            reason = cancelReasonsArr[this.state.selectedCancelReasonIndex]
        }

        let weakSelf = this
        this.setState({ isInternetError: false, isDataNotFoundError: false })
        RequestManager.patchRequest(API_APPOINTMENT_LIST, this.getCancelApiParams(reason)).then((response: any) => {

            console.log('REsponse of Cancel appointment API >>>>', response);
            Toast.show((response.message), Toast.SHORT);
            this.state.upcomingAptArr[this.state.selectedAppointmentIndex].status = "2"
            this.setState({ selectedCancelReasonIndex: -1 })
            // appt.status = "2"
            // this.setState({upc})
             console.log('Appointments count After>>', this.state.upcomingAptArr)
        }).catch((error: any) => {
            this.setState({ selectedCancelReasonIndex: -1 })
            General.showErroMsg(this, error)
        })
    }

    private removePopup() {
        this.setState({ selectedCancelReasonIndex: -1, showCancelAppointmentPopup: false })
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showCancelAppointmentPopup}>
                    <CancelAppointmentPopup onClickEvent={(reason: any) => this.callCancelAppointmentAPI(reason)} reasonSelected={(index: any) => this.reasonSelected(index)} selectedReasonIndex={this.state.selectedCancelReasonIndex} removePopup={() => this.removePopup()}/>
                </Modal>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={styles.blueConatiner}>
                        <View style={styles.container}>
                            <TabView
                                navigationState={this.state}
                                renderScene={({ route }) => {
                                    switch (route.key) {
                                        case 'first':
                                            return (this.state.upcomingAptArr.length > 0 ? 
                                            <Upcoming data={this.state.upcomingAptArr} 
                                            onPress={(index: any) => this.cancelAppointment(index)} 
                                            messageEvent={(index: any) => this.messageActionForUpcoming(index)}
                                            refreshList={() => this.refreshList()} loadMoreData={() => this.loadMoreData()} isCloseToBottom={(nativeEvent: any) => this.isCloseToBottom(nativeEvent)} isRefreshing={this.state.isRefreshing} isCalled={this.state.isCalled} /> : (this.state.showLoader ? null :
                                                (this.state.isDataNotFoundError ? <NoDataFoundView /> :
                                                    (this.state.isInternetError ?
                                                        <NoInternetFoundView func={() => this.apiGetAppointmentList(this.state.index, false)} /> : null))))
                                        case 'second':
                                            return (this.state.historyAptArr.length > 0 ? 
                                            <History data={this.state.historyAptArr} viewReportsEvent={(index: any) => this.viewReportsAction(index)} 
                                            messageEvent={(index: any) => this.messageAction(index)} 
                                            feedbackEvent={(index: any) => this.feedbackAction(index)} 
                                            refreshList={() => this.refreshList()} loadMoreData={() => this.loadMoreData()} isCloseToBottom={(nativeEvent: any) => this.isCloseToBottom(nativeEvent)} isRefreshing={this.state.isRefreshing} isCalled={this.state.isCalled} /> : (this.state.showLoader ? null :
                                                (this.state.isDataNotFoundError ? <NoDataFoundView /> :
                                                    (this.state.isInternetError ?
                                                        <NoInternetFoundView func={() => this.apiGetAppointmentList(this.state.index, false)} /> : null))))
                                        default:
                                            return null;
                                    }
                                }}
                                renderTabBar={props =>
                                    <View style={{ backgroundColor: 'red' }}>
                                        <TabBar
                                            {...props}
                                            activeColor={'black'}
                                            inactiveColor={APP_GREY_TEXT_COLOR}
                                            indicatorStyle={{ backgroundColor: APP_HEADER_BACK_COLOR, width: 100, marginLeft: 50, marginBottom: -1}}
                                            style={{ backgroundColor: APP_GREY_BACK, }}
                                        />
                                    </View>
                                }
                                onIndexChange={(index) => this.handleIndexChange(index)}
                                style={{ marginBottom: 6 }}
                            />
                            {/* {Platform.OS == 'android' ? <BottomShadowView /> : null } */}
                        </View>
                    </View>
                </SafeAreaView>
            </KeyboardAvoidingView>
        );
    }
}
