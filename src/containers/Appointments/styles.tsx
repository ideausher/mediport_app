import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_LIGHT_SILVER_COLOR, APP_LIGHT_GREY_COLOR, App_MIDIUM_GREY_COLOR} from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,        
        backgroundColor: APP_GREY_BACK,
        padding: 8,
    },
    selectReasonText:{ 
        fontSize: 15,
        fontWeight:'600',
        marginTop: 8,
        marginBottom: 16,
        textAlign:'center',
        width: '80%'
    },
    popupVw: {
        borderRadius: 10,
        backgroundColor: 'white',
        padding: 16,
        margin: 16,
        alignItems: 'center'
    },
    greyText: {
        fontSize: 14, 
        color:App_MIDIUM_GREY_COLOR,
        marginLeft: 10
    },
    txtFld: {
        marginTop:24,
        textAlign: 'left',
        width: '94%',
        height: 40,
        fontSize: 12,
        color: App_MIDIUM_GREY_COLOR,
        borderBottomColor: APP_LIGHT_GREY_COLOR,
        borderBottomWidth: 1,
        lineHeight:20
      },
})
