//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, App_LIGHT_BLUE_COLOR, App_MIDIUM_GREY_COLOR, APP_BLUE_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        padding: 16,
    },
    cardVw: {
        borderRadius: 10,
        backgroundColor: 'white',
        padding: 16,
        alignItems:'center'
    },
    img: {
        width: 60,
        height: 60,
    },
    darkTxt: {
        textAlign: 'left',
        fontSize: 18,
        color: APP_GREY_TEXT_COLOR,
        fontWeight: 'bold',
        marginTop: 20, 
    },
    lightTxt: {
        textAlign: 'left',
        color: App_MIDIUM_GREY_COLOR,
        marginTop: 20, 
    },    
blueText:{
    textAlign: 'center',
        fontSize: 20,
        color: APP_BLUE_COLOR,
        fontWeight: 'bold',
        marginTop: 20,
}
});