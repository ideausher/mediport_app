import React, { Component } from 'react';
import { View, Text, Image, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, ScrollView, Modal, Alert, Keyboard } from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_GREY_TEXT_COLOR, commonShadowStyle, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { BackHeaderBtn } from '../../Custom/CustomComponents';
import { TextInputWithoutImageCustomWidthHeight } from '../../Custom/CustomTextInput';
import { bool } from 'prop-types';
import { CommonBtn, GreenTextBtn } from '../../Custom/CustomButton';
import { localize } from '../../Utils/LocalisedManager';
//@ts-ignore
import { connect } from 'react-redux';
import RequestManager from '../../Utils/RequestManager';
import { API_VALIDATE_COUPENS, API_APPOINTMENT_LIST, API_UPLOAD_PDF } from '../../Utils/APIs';
import { Loader } from '../../Utils/Loader';
import General from '../../Utils/General';

const { StatusBarManager } = NativeModules;

export interface props {
    navigation: NavigationScreenProp<any, any>,
    doctorInfo: any,
    bookingInfo: any
};

class SavedCards extends Component<props, object> {

    state = {
        expiryMonth: '',
        expiryYear: '',
        cvv: '',
        statusBarHeight: 0,
        cardLast4: '',
        coupenCode: '',
        showLoader: false,
        cardId: '',
        discountedPrice: 0,
        brand: '',
    }

    uploadedImagesArr = [] as String[]

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.savedCards}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        let selectedCard = this.props.navigation.getParam('selectedCard')

        this.setState({ cardLast4: selectedCard.last4, expiryMonth: selectedCard.exp_month, expiryYear: selectedCard.exp_year, cardId: selectedCard.id, brand: selectedCard.brand })

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    validateData() {
        if (this.state.expiryMonth.length == 0) {
            Alert.alert('Please enter expiry month')
            return
        }
        else if (this.state.expiryYear.length == 0) {
            Alert.alert('Please enter expiry year')
            return
        }
        else if (this.state.cvv.length == 0) {
            Alert.alert('Please enter cvv/cvc')
            return
        }

        return true
    }

    async confirmBtnAction() {
        // this.props.navigation.navigate('Verification', { verificationFor: VerificationFor.payment })

        if (this.validateData()) {
            // let cardNum = this.state.cardNo.replace(/ /g, '')

            // var stripe = require('stripe-client')('pk_test_gVTWFUkRg5xoLaNQnCfgY3Zf00Sdn1jBwb'); //pk_test_pku2v8tppoSjIx8zYldEZalN
            // const token = await stripe.createToken({
            //     card: {
            //         "number": cardNum,
            //         "exp_month": this.state.expiryMonth,
            //         "exp_year": this.state.expiryYear,
            //         "cvc": this.state.cvv
            //     }
            // })

            // // const token = PaymentManager.setStripeKey(cardNum, this.state.expiryMonth, this.state.expiryYear, this.state.cvv)
            // console.log('Stripe Token Genrated >>>>', token);
            this.callAPI()
        }
    }

    // Get paarmeters for APPOINTMENT BOOK API
    private getParams(report_ids: String) {

        console.log('IS VAlid Coupon >>>', this.state.discountedPrice)

        let dict: any = {
            "vender_id": this.props.doctorInfo.id,
            "service_id": this.props.doctorInfo.serviceId,
            "vendor_slot_id": this.props.bookingInfo.slotId,
            "price": (this.state.discountedPrice > 0 ? this.state.discountedPrice : this.props.doctorInfo.price),
            "booking_date": this.props.bookingInfo.bookingDate,
            "card_source_id": this.state.cardId,
            "card_id": this.state.cardId,
            "appointment_for": this.props.bookingInfo.appointmentFor,
            "age": this.props.bookingInfo.age,
            "blood_group": this.props.bookingInfo.bloodGroup,
            "contact_number": this.props.bookingInfo.contactNumber,
            "description": this.props.bookingInfo.description,
            "patient_name": this.props.bookingInfo.patientName,
            "card_save": 0,
            "gender": this.props.bookingInfo.gender,
            "currency": "usd",
            "original_amount": this.props.doctorInfo.price,
            "coupon_code": (this.state.discountedPrice > 0 ? this.state.coupenCode.trim() : "")
        }

        if (report_ids != "") {
            dict["report_ids"] = report_ids
        }
        return dict
    }
    private uploadImageParams(image: any) {

        const formData = new FormData();
        // formData.append('profile_image', this.state.avatarSource.data); 

        formData.append("file", {
            name: image.name,
            type: image.type,
            uri:
                Platform.OS === "android" ? image.uri : image.uri.replace("file://", "")
        });

        return formData
    }

    // API tp send OTp on added Phone Number for verification
    apiUploadImage(image: any, index: number) {
        console.log('Upload Image func called with Image >>>>', image)
        console.log('Upload Image func called with params>>>>', this.uploadImageParams(image))
        RequestManager.uploadImage(API_UPLOAD_PDF, this.uploadImageParams(image)).then((response: any) => {
            console.log('REsponse of apiUploadImage >>>>', response);
            console.log('Image Str in response  >>>>', response.data.id);
            console.log('Index >>>>', index);

            this.uploadedImagesArr.push(response.data.id)

            // Means All Images Uploaded if Equal
            if (this.uploadedImagesArr.length == this.props.bookingInfo.reportIds.length) {
                let imageStr = this.uploadedImagesArr.join(',')
                console.log('IMAGESTR >>>', imageStr)

                this.callBookAppointmentAPI(imageStr)
            }

        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }


    private async callAPI() {

        console.log('Call API Func Called', this.props.bookingInfo)

        // Upload PDF if path exits means
        if (this.props.bookingInfo.reportIds != '') {
            this.setState({ showLoader: true })

            {
                this.props.bookingInfo.reportIds.map((item: any, index: number) => (
                    this.apiUploadImage(item, index)
                ))
            }
        }
        else {
            this.callBookAppointmentAPI('')
        }
    }

    private async callBookAppointmentAPI(reportIds: String) {
        console.log('PArams API_APPOINTMENT_LIST >>>>', this.getParams(reportIds))
        this.setState({ showLoader: true })
        let weakSelf = this
        RequestManager.postRequestWithHeaders(API_APPOINTMENT_LIST, this.getParams(reportIds)).then((response: any) => {

            console.log('REsponse of Book APPOINTMENT API >>>>', response);

            setTimeout(function () {
                weakSelf.setState({ showLoader: false })
            }, 200);
            weakSelf.props.navigation.navigate('BookingSuccess', { refId: response.data.reference_id })
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    applyDiscountAction() {

        this.props.navigation.navigate('Discount', {
            onGoBack: (code: string, price: any) => this.refresh(code, price)
        });
    }

    private refresh(code: string, price: any) {
        console.log('refresh Btn Called >>>', code)
        this.setState({ coupenCode: code, discountedPrice: price })
    }

    // Get paarmeters for VALIDATE COUPEN API
    private getValidateCoupenAPIParams() {

        return {
            "code": this.state.coupenCode,
            "price": this.props.doctorInfo.price
        }
    }

    private validateCoupen() {

        if (this.state.coupenCode.trim().length == 0) {
            return;
        }

        this.setState({ showLoader: true })
        let weakSelf = this
        RequestManager.postRequestWithHeaders(API_VALIDATE_COUPENS, weakSelf.getValidateCoupenAPIParams()).then((response: any) => {

            console.log('REsponse of API_VALIDATE_COUPENS API >>>>', response);

            setTimeout(function () {
                weakSelf.setState({ showLoader: false, discountedPrice: response.data.price })
            }, 200);
        }).catch((error: any) => {
            weakSelf.setState({ discountedPrice: 0 })
            General.showErroMsg(this, error)
        })
    }

    render() {

        let year = this.state.expiryYear.toString()
        let yearLastTwoDigits = year.substring(year.length - 2)

        let actualPrice = this.props.doctorInfo.price.toString()

        if (this.state.discountedPrice > 0) {
            actualPrice = this.state.discountedPrice.toString()
        }

        console.log("Actual Price >>>", actualPrice)
        console.log("Discounted Price >>>", this.state.discountedPrice)

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <View style={styles.blueConatiner}>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showLoader}>
                        <Loader />
                    </Modal>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                            <View style={[styles.cardVw, commonShadowStyle]}>
                                <Image
                                    source={this.state.brand == 'Visa' ? require('../../assets/visa.png') : require('../../assets/mastercard.png')}
                                    style={styles.img}
                                    resizeMode={'contain'}
                                />
                                <Text style={[styles.lightTxt, { textAlign: 'center', fontSize: 17, color: APP_GREY_TEXT_COLOR, fontWeight: '300' }]}>{localize.startWithCardNumber}</Text>
                                <Text style={styles.blueText}>XXXX - XXXX - XXXX - {this.state.cardLast4}</Text>
                            </View>

                            <Text style={styles.darkTxt}>{localize.amount}</Text>
                            <TextInputWithoutImageCustomWidthHeight placeholder={localize.amount} onChange={(text: any) => { }} value={actualPrice} width='100%' height={60} editable={false} />

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={styles.lightTxt}>{localize.expirationDate}</Text>
                                <Text style={styles.lightTxt}>{localize.cvv}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ flexDirection: 'row', width: '50%', justifyContent: 'space-between' }}>
                                    <TextInputWithoutImageCustomWidthHeight placeholder='00' onChange={(text: any) => this.setState({ expiryMonth: text })} value={this.state.expiryMonth.toString()} width='45%' editable={false} />
                                    <TextInputWithoutImageCustomWidthHeight placeholder='00' onChange={(text: any) => this.setState({ expiryYear: text })} value={yearLastTwoDigits} width='45%' editable={false} />

                                </View>
                                <TextInputWithoutImageCustomWidthHeight placeholder='000' onChange={(text: any) => { this.setState({ cvv: text }), ((text.length == 3) ? Keyboard.dismiss() : null) }} value={this.state.cvv} width='22.5%' keyboardType='numeric' returnKeyType='done' maxLength={3} />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <GreenTextBtn title={localize.applyDicountCode} func={() => this.applyDiscountAction()} textAlign='left' width={150} />
                                <TextInputWithoutImageCustomWidthHeight placeholder='' onChange={(text: any) => this.setState({ coupenCode: text })} value={this.state.coupenCode} width='50%' height={40} onEndEditing={() => this.validateCoupen()} />
                            </View>
                            <CommonBtn title='Confirm' func={() => this.confirmBtnAction()} width='70%' />
                        </ScrollView>
                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    doctorInfo: state.saveDoctorInfo,
    bookingInfo: state.saveBookingInfo,
});


export default connect(mapStateToProps)(SavedCards);