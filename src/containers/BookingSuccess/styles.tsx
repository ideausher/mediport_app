//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, App_LIGHT_BLUE_COLOR, App_MIDIUM_GREY_COLOR, APP_DARK_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        padding: 16,
    },
    popupVw: {
        borderRadius: 10,
        backgroundColor: APP_GREY_BACK,
        padding: 16,
        marginTop: 16

    },
    image: {
        marginTop: 8,
        width: 100,
        height: 100,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    greyText: {
        fontSize: 14,
        color: APP_DARK_GREY_COLOR,
        marginTop: 50,
        textAlign: 'center',
        width: '60%',
        alignSelf: 'center'
    },
    ratingVw: {
        height: 50,
        justifyContent: 'center',
    },
    ratingTxt: {
        fontSize: 12,
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',

    },
    smallText: {
        fontSize: 12,
        color: App_MIDIUM_GREY_COLOR,
        textAlign: 'left',
    },
});