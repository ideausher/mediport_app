import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, FlatList, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules } from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, APP_GREY_TEXT_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle, commonShadowStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import DoctorCell from '../../Custom Cells/DoctorCell';
import LinearGradient from 'react-native-linear-gradient';
import { LineVw, BackHeaderBtn } from '../../Custom/CustomComponents';
import { TextInputWithoutImageCustomWidthHeight } from '../../Custom/CustomTextInput';
import { CommonBtn } from '../../Custom/CustomButton';
import { localize } from '../../Utils/LocalisedManager';
//@ts-ignore
import { connect } from 'react-redux';
import moment from 'moment';

const { StatusBarManager } = NativeModules;

export interface props {
    navigation: NavigationScreenProp<any, any>,
    doctorInfo: any,
    bookingInfo: any,
    categoryInfo: any,
};

class BookingSuccess extends Component<props, object> {

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.bookingSuccessful}</Text>
                </View>
            ),
            headerLeft: <View style={{width: 40, height: 40,}} />,
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    doneBtnAction() {
        this.props.navigation.navigate('Home')
    }

    render() {

        let refId = this.props.navigation.getParam('refId')

        return (
            <View style={styles.blueConatiner}>
                <View style={styles.container}>
                    <View style={[styles.popupVw, commonShadowStyle]}>
                        <Image
                            source={require('../../assets/grad-circle.png')}
                            style={styles.image}
                            resizeMode={'contain'}
                        />
                        <Text style={styles.greyText}>{localize.youSuccessfullyBookedAppointment}</Text>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 16 }}>
                            <View style={{}}>
                                <View style={styles.ratingVw}>
                                    <Text style={styles.ratingTxt}>{localize.date}</Text>
                                    <Text style={styles.smallText}>{moment(this.props.bookingInfo.bookingDate, "YYYY-MM-DD").format('DD MMM YYYY')}</Text>
                                </View>
                                <View style={styles.ratingVw}>
                                    <Text style={styles.ratingTxt}>{localize.to}</Text>
                                    <Text style={styles.smallText}>Dr. {this.props.doctorInfo.name}</Text>
                                </View>

                            </View>
                            <View style={{}}>
                                <View style={styles.ratingVw}>
                                    <Text style={styles.ratingTxt}>{localize.time}</Text>
                                    <Text style={styles.smallText}>{this.props.bookingInfo.bookingTime}</Text>
                                </View>
                                <View style={styles.ratingVw}>
                                    <Text style={styles.ratingTxt}>{localize.referenceId} </Text>
                                    <Text style={styles.smallText}>{refId}</Text>
                                </View>
                            </View>
                        </View>

                        <CommonBtn title={localize.done} func={() => this.doneBtnAction()} width='80%' />
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state: any) => ({
    doctorInfo: state.saveDoctorInfo,
    bookingInfo: state.saveBookingInfo,
    categoryInfo: state.saveCategoryInfo
});


export default connect(mapStateToProps)(BookingSuccess);