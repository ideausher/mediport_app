import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity, Image, StatusBar, Modal } from 'react-native';
import { APP_HEADER_TITLE_COLOR, APP_HEADER_BACK_COLOR, APP_LIGHT_GREY_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { localize } from '../../Utils/LocalisedManager';
import { OpenDrawerHeaderBtn } from '../../Custom/CustomComponents';
//@ts-ignore
import ToggleSwitch from 'toggle-switch-react-native';
import RequestManager from '../../Utils/RequestManager';
import { API_NOTIFICATIONS_LIST } from '../../Utils/APIs';
import Toast from 'react-native-simple-toast';
import General from '../../Utils/General';
import LocalDataManager from '../../Utils/LocalDataManager';
import { Loader } from '../../Utils/Loader';

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class Settings extends Component<props, object> {

    state = {
        isNotificationsOn: 1,
        showLoader: false,
    };

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View>
                    <Text style={navHdrTxtStyle}>{localize.settings}</Text>
                </View>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler() });

        LocalDataManager.getLoginInfo().then((data: any) => {
            if (data != null && data != {}) {
                this.setState({ isNotificationsOn: data.is_notification })
            }
        })
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    getPassingParams() {
        return {
            forgotPassword: false,
        }
    }

    changePswd() {
        this.props.navigation.navigate('UpdatePassword', { params: this.getPassingParams() })
    }

    onToggle(isOn: any) {
        this.setState({ isNotificationsOn: isOn });
        this.callNotificationsAPI()
    }

    // UPdate Notiofications Status 
    callNotificationsAPI() {
        let weakSelf = this
        this.setState({ showLoader: true })
        RequestManager.patchRequest(API_NOTIFICATIONS_LIST, {}).then((response: any) => {
            console.log('REsponse of API_NOTIFICATIONS_LIST >>>>', response);
            this.setState({showLoader: false})
            this.updateinLoginInfo()
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    updateinLoginInfo() {
        LocalDataManager.getLoginInfo().then((data: any) => {
            if (data != null && data != {}) {
                console.log('Login Dict Before Change >>>>', data)
                data.is_notification = this.state.isNotificationsOn
                console.log('Login Dict After Change >>>>', data)
                LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(data))
            }
        })
    }

    render() {
        return (
            <View style={styles.blueConatiner}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    <View style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>
                        <View style={styles.imgTextContainer}>
                            <Image
                                source={require('../../assets/noti-bell-grey.png')}
                                style={styles.img} />
                            <Text style={styles.text}>{localize.notifications}</Text>
                        </View>
                        <ToggleSwitch
                            isOn={this.state.isNotificationsOn == 1 ? true : false}
                            onColor={APP_HEADER_BACK_COLOR}
                            onToggle={(isOn: any) => {
                                console.log('ISON >>>>', isOn)
                                this.onToggle(isOn == true ? 1 : 2);
                            }}
                        />
                    </View>
                    <View style={{ height: 1, alignSelf: 'center', backgroundColor: APP_LIGHT_GREY_COLOR, width: '100%' }}></View>
                    <TouchableOpacity onPress={() => this.changePswd()} style={styles.imgTextContainer}>
                        <Image
                            source={require('../../assets/change-pswd.png')}
                            style={styles.img} />
                        <Text style={styles.text}>{localize.changePassword}</Text>
                    </TouchableOpacity>
                    <View style={{ height: 1, alignSelf: 'center', backgroundColor: APP_LIGHT_GREY_COLOR, width: '100%' }}></View>
                </View>
            </View>
        );
    }
}
