//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, App_MIDIUM_GREY_COLOR, APP_LIGHT_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        padding: 16,
    },
    imgTextContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
       // backgroundColor: 'green'
    },
    img: {
        height: 23,
        width: 23,
        resizeMode: 'contain'
    },
    text: {
        marginLeft: 16,
        fontSize: 15,
        fontWeight: '400',
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
    },
});