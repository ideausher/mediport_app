import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity, Image, SafeAreaView } from 'react-native';
import { APP_HEADER_TITLE_COLOR, APP_HEADER_BACK_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { OpenDrawerHeaderBtn } from '../../Custom/CustomComponents';

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class Subscription extends Component<props, object> {
    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>My Subscription</Text>
                </View>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')}/>
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler()});
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                        <Text style={styles.text}>Active Plan</Text>
                        <View style= {styles.planVw}>
                            <Text style={styles.circleText}>M</Text>
                            <Text style={styles.planText}>Your plan for 300 is active now.And will be expire soon please renew your plan</Text>
                        </View>
                    </View>
                    </View>
            </SafeAreaView>
                );
            }
}