//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, App_MIDIUM_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        padding: 8,
       // backgroundColor: 'red'
    },
    text: {
        fontSize: 20,
        fontWeight: '500',
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        marginTop: 8,
    },
    planVw: {
        flexDirection: 'row',
        padding: 8,
        marginTop: 20,
        justifyContent:'center',
    },
    circleText: {
        backgroundColor: APP_HEADER_BACK_COLOR,
        width: 60,
        height: 60,
        borderRadius: 30,
        textAlign: 'center',
        color: 'white',
        lineHeight: 50,
        fontSize: 18,
        fontWeight: '500',
    },
    planText: {
        flex: 1, 
        marginLeft: 8,
        color: App_MIDIUM_GREY_COLOR,
        fontSize: 16,
    }
});