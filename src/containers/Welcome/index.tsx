import React, { Component } from 'react';
import { View, Text, Image, Platform, PermissionsAndroid } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
//@ts-ignore
import AppIntroSlider from 'react-native-app-intro-slider';
import { APP_GREY_BACK, APP_BLUE_COLOR, APP_HEADER_BACK_COLOR, APP_GREEN_COLOR } from '../../Constants';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { localize } from '../../Utils/LocalisedManager';
import LocalDataManager from '../../Utils/LocalDataManager';
// import FCM from "react-native-fcm";
//@ts-ignore
import { registerAppListener } from '../../Utils/Listeners';
//@ts-ignore
import { connect } from 'react-redux';
import { VerificationFor } from '../../Utils/Enums';
import { LOGIN } from '../../Redux/Constants';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import RequestManager from '../../Utils/RequestManager';
import { API_SIGNUP } from '../../Utils/APIs';
import General from '../../Utils/General';
import messaging from '@react-native-firebase/messaging';
import { localNotificationservice } from '../../Firebase/LocalNotificationService';
import { fcmService } from '../../Firebase/FCMService';

const slides = [
    {
        key: 'somethun',
        title: 'Welcome',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        image: require('../../assets/welcome1.png'),
        backgroundColor: APP_GREY_BACK,
    },
    {
        key: 'somethun-dos',
        title: 'Welcome',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        image: require('../../assets/welcome2.png'),
        backgroundColor: APP_GREY_BACK,
    },
    {
        key: 'somethun1',
        title: 'Welcome',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        image: require('../../assets/welcome3.png'),
        backgroundColor: APP_GREY_BACK,
    }
];


export interface props {
    navigation: NavigationScreenProp<any, any>
    saveLoginInfo: any,
};

class Welcome extends Component<props, object> {

    notificationData: any = undefined

    state = {
        showWelcomeScreen: false,
        lastSlideIndex: -1,
        slideIndex: 0,
        onSlideFuncCalled: false,
        token: ''
    }

    type = '0';
    notiId = 0;
    receivedNoti = false;
    classification = '';

    static navigationOptions = () => {
        return {
            header: null
        }
    }

    componentDidMount() {

        LocalDataManager.getDataAsyncStorage('isFirstTime').then((data: any) => {
            if (data == null) {
                LocalDataManager.saveDataAsyncStorage('isFirstTime', JSON.stringify(true))
                this.setState({ showWelcomeScreen: true })
            }
            else {
                this.checkIfUserAlreadyLoggedIn()
            }
        })

        // To remove yellow Warnings
        console.disableYellowBox = true

        if (Platform.OS === "ios") {
            this.checkForLocationPermissions()
        }
        else {
            this.requestLocationPermission()
        }
        this.notificationSetup()
        // this.start
    }

    /********************************************************************************************************************************************************* */
    onRegister = (token:any) => {
        console.log("[App] On Register: ", token)
      }

    onNotification = (notify:any) => {
        console.log("[App] On Notifications: ", notify)
        const options = {
          soundName: 'default',
          playSound: true
        }
        localNotificationservice.showNotification(
          0,
          notify.notification.title,
          notify.notification.body,
          notify,
          options
        )
      }
    
    onOpenNotification = (notificationOpen:any) => {
        console.log("[App] On Open Notifications: ", notificationOpen);
    
        // Alert("Open Notification: ", notify);
        console.log("message Listener called notificationOpen >>>>>>>",notificationOpen);
        this.notificationData = notificationOpen;
        console.log("message Listener called notificationOpen booking id>>>>>>>",this.notificationData);
        console.log("message Listener called notificationOpen vendor name>>>>>>>",this.notificationData.sender_id);
        this.manageRedirection()
        
      }

      manageRedirection() {
    
        LocalDataManager.saveDataAsyncStorage('shouldAskForVideoCall', JSON.stringify(true))
        LocalDataManager.saveDataAsyncStorage('videoCallInfo', JSON.stringify(this.notificationData.room))
        // LocalDataManager.saveDataAsyncStorage('videoCallInfo', JSON.stringify("55"))
        this.props.navigation.navigate('Home',{booking_id:this.notificationData.room,vendor_name:"Inder"});
        // this.props.navigation.navigate('Home',{booking_id:this.notificationData.room_id,vendor_name:this.notificationData.vendor_name});
      }

    notificationSetup = async () => {
        // await messaging().registerDeviceForRemoteMessages();
   
       const token = await messaging().getToken();
   
       console.log(token)
       LocalDataManager.saveDataAsyncStorage('fcmToken', JSON.stringify(token));
   
       fcmService.registerAppWithFCM();
       fcmService.register(this.onRegister, this.onNotification, this.onOpenNotification);
       localNotificationservice.configure(this.onOpenNotification);
   
    //    return () => {
    //      console.log(' [App] UnRegister')
    //      fcmService.unRegister();
    //      localNotificationservice.unregister()
    //    }
   
     }

    /********************************************************************************************************************************************************* */

    // async notificationSetup() {
    //     FCM.createNotificationChannel({
    //         id: 'default',
    //         name: 'Default',
    //         description: 'used for example',
    //         priority: 'high'
    //     })
    //     registerAppListener(this.props.navigation);
    //     FCM.getInitialNotification().then((notif: any) => {
    //         console.log('Notification Data: ', notif)
    //         if (notif != undefined && notif.opened_from_tray && notif.room != undefined) {
    //             // this.type = notif.type;
    //             // this.receivedNoti = true
    //             // this.notiId = notif.id
    //             // this.classification = notif.classification || ''

    //             LocalDataManager.saveDataAsyncStorage('shouldAskForVideoCall', JSON.stringify(true))
    //             LocalDataManager.saveDataAsyncStorage('videoCallInfo', JSON.stringify(notif))

    //             FCM.removeAllDeliveredNotifications();
    //         }
    //     });


    //     FCM.on("FCMTokenRefreshed", token => {
    //         console.log('Token refreshed >>>', token)
    //     })

    //     try {
    //         let result = await FCM.requestPermissions();
    //     } catch (e) {
    //         console.log('Error: ', e);
    //     }

    //     if (Platform.OS === "ios") {
    //         FCM.getAPNSToken().then(token => {
    //         }).catch(error => {
    //             console.log("Access", error);
    //         });
    //     }
    // }

    // When user comes to Login Screen first check if User alredy logged in. If yes then save his data in redux state and move Directly to home Screen
    private checkIfUserAlreadyLoggedIn() {
        let weakSelf = this
        LocalDataManager.getLoginInfo().then((data: any) => {
            console.log('Data Retrived from Storage >>>>', data)
            if (data != null && data != {}) {
                // Get Login Data Again to refresh Data
                this.setState({ token: data.token })
                weakSelf.apiGetLoginUserData()
            }
            else {
                this.checkIfUserSignupAndVerificationPending()
            }
        })
    }

    async apiGetLoginUserData() {

        this.setState({ showLoader: true })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_SIGNUP, {})
            .then((response: any) => {
                console.log('REsponse of Get Users Details API >>>>', response);
                weakSelf.setState({ showLoader: false })
                weakSelf.saveLoginUserDetailsAndMove(response.data)
            }).catch((error: any) => {
                General.showErroMsg(this, error)
            })
    }

    // Save Login data in redux and Local Storage(for use after App kill) and Move to Home Screen
    private saveLoginUserDetailsAndMove(data: any) {

        data["token"] = this.state.token
        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(data))
        this.props.saveLoginInfo(data);
        this.props.navigation.navigate('Home');
    }

    // Check if Signup form data exists Means user Signup but verification pending thenmove directly to Verification Screen
    private checkIfUserSignupAndVerificationPending() {
        LocalDataManager.getDataAsyncStorage('signupInfo').then((data: any) => {
            if (data != null && data != {}) {
                this.props.navigation.navigate('Verification', { verificationFor: VerificationFor.signup, phoneNumber: data['phone_country_code'] + ' ' + data['phone_number'] })
            } else {
                this.props.navigation.navigate('Login')
            }
        })
    }

    private checkForLocationPermissions() {
        check(PERMISSIONS.IOS.LOCATION_ALWAYS)
            .then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log(
                            'This feature is not available (on this device / in this context)',
                        );
                        break;
                    case RESULTS.DENIED:
                        console.log(
                            'The permission has not been requested / is denied but requestable',
                            request(PERMISSIONS.IOS.LOCATION_ALWAYS).then(result => {

                                console.log('Results >>>', result);
                            })
                        );
                        break;
                    case RESULTS.GRANTED:
                        console.log('The permission is granted');
                        break;
                    case RESULTS.BLOCKED:
                        console.log('The permission is denied and not requestable anymore');
                        break;
                }
            })
            .catch(error => {
                // …
            });
    }
    async requestLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Mediport',
                    message: String(localize.mediportNeedsAccessToLocation),
                    buttonNeutral: localize.askMeLater,
                    buttonNegative: localize.cancel,
                    buttonPositive: localize.ok,
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the location');
                // Toast.show('You can use the location', Toast.SHORT);
            } else {
                console.log('location permission denied');
                // Toast.show('location permission denied', Toast.SHORT);
            }
        } catch (err) {
            console.warn(err);
        }
    }

    renderItem = (item: any) => {

        return (
            <View style={styles.container}>
                <TouchableOpacity style={{ marginTop: 30, width: 100, alignSelf: 'flex-end' }} onPress={() => this.skipBtnAction()}>
                    <Text style={styles.skipText}>{localize.skip}</Text>
                </TouchableOpacity>
                <Text style={styles.welcomeText}>{item.item.title}</Text>
                <Text style={styles.lightText}>{item.item.text}</Text>
                <Image style={styles.image} source={item.item.image} />
            </View>
        );
    }

    skipBtnAction() {
        setTimeout(() => {
                this.props.navigation.navigate('Login')
        }, 400);
    }

    onSlideChange(index: number, lastIndex: number) {
        this.setState({ lastSlideIndex: lastIndex, slideIndex: index, onSlideFuncCalled: true })
    }

    onScrollEnd() {
        this.setState({ onSlideFuncCalled: false })
        setTimeout(() => {
            if (this.state.lastSlideIndex == 1 && this.state.slideIndex == 2 && !this.state.onSlideFuncCalled) {

                this.props.navigation.navigate('Login')

            }

        }, 400);
    }

    render() {

        if (!this.state.showWelcomeScreen) {
            return null;
        }
        else {
            return <AppIntroSlider
                renderItem={this.renderItem}
                slides={slides}
                showNextButton={false}
                showDoneButton={false}
                dotStyle={{ backgroundColor: APP_GREEN_COLOR, marginBottom: 120, width: 8, height: 8, borderRadius: 4 }}
                activeDotStyle={{ backgroundColor: APP_HEADER_BACK_COLOR, marginBottom: 120 }}
                onSlideChange={(index: number, lastIndex: number) => this.onSlideChange(index, lastIndex)}
                onScrollEndDrag={(nativeEvent: any) => this.onScrollEnd()} />;
        }
    }
}

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
});

export default connect(null, mapDispatchToProps)(Welcome);
