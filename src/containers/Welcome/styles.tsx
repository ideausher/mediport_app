//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_WELCOME_DARK_GREY, APP_GREY_BACK, APP_WELCOME_LIGHT_GREY, APP_GREY_TEXT_COLOR, App_MIDIUM_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: APP_GREY_BACK,
        padding: 20
    },
    welcomeText: {
        color: APP_WELCOME_DARK_GREY,
        fontSize: 14,
        fontWeight: '700',
        textAlign: 'center',
        marginTop: 50, 
        alignSelf: 'center',
    },
    lightText: {
        fontSize: 12,
       // fontWeight: '500',
        color: APP_WELCOME_LIGHT_GREY,
        textAlign: 'center',
        marginTop: 40,
        alignSelf: 'center',
        lineHeight: 20,
    },
    image: {
        marginTop:50,
        width: 160,
        height: 159,
        alignSelf: 'center',
    }, 
    skipText: {
        alignSelf: 'flex-end',
        color: App_MIDIUM_GREY_COLOR,
        fontSize: 13,
        fontWeight: '400', 
        height: 30,
        lineHeight: 30
    },
});