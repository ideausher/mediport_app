//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREEN_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_HEADER_TITLE_COLOR, APP_LIGHT_GREY_COLOR, App_MIDIUM_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
  blueConatiner: {
    flex: 1,
    backgroundColor: APP_HEADER_BACK_COLOR
  },
  container: {
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: APP_GREY_BACK,
    // padding: 8,
    // alignItems: 'center'
  },
  scrollContainer: {
    padding: 8,
    alignItems: 'center',
},
  greyBoldText: {
    fontSize: 20,
    fontWeight: '600',
    color: APP_GREY_TEXT_COLOR,
    textAlign: 'center',
    marginTop: 30,
  },
  greyText: {
    fontSize: 12,
    color: App_MIDIUM_GREY_COLOR,
    textAlign: 'center',
    width: '80%'
  },
  blueText: {
    fontSize: 14,
    color: APP_HEADER_BACK_COLOR,
    textAlign: 'center',
  },
  blueTextBtn: {
    justifyContent: 'center',
    height: 30,
  },
  redText: {
    color: 'red',
    fontSize: 15,
    marginLeft: 4,

  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 70,
    width: '18%',
    padding: 10,
    borderWidth: 0.2,
    borderColor: APP_HEADER_BACK_COLOR,
    borderRadius: 10,
    backgroundColor: 'white',
    textAlign: 'center',
    fontSize: 30,
    color: APP_GREY_TEXT_COLOR,
  },
});