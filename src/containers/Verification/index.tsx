import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, TextInput, Keyboard, Modal, Alert, ScrollView ,StatusBar} from 'react-native';
import { APP_HEADER_BACK_COLOR, commonShadowStyle, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { CommonBtn } from '../../Custom/CustomButton';
import { Loader } from '../../Utils/Loader';
import RequestManager from '../../Utils/RequestManager'
import { API_SIGNUP, API_SENDOTP, API_CHECK_OTP, API_CHECK_PHONE_OTP, API_FORGOT_PSWD } from '../../Utils/APIs'
import AsyncStorage from '@react-native-community/async-storage';
import { LOGIN } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { VerificationFor } from '../../Utils/Enums';
import { localize } from '../../Utils/LocalisedManager';
import LocalDataManager from '../../Utils/LocalDataManager';
import General from '../../Utils/General';
import { BackHeaderBtn } from '../../Custom/CustomComponents';

export interface LoginProps {
    navigation: NavigationScreenProp<any, any>
    saveLoginInfo: any,
};

const { StatusBarManager } = NativeModules;

class Verification extends Component<LoginProps, object> {

    state = {
        firstDigit: '',
        secondDigit: '',
        thirdDigit: '',
        fourthDigit: '',
        statusBarHeight: 0,
        showLoader: false,
        verificationFor: VerificationFor.signup,
        phoneNumber: ''
    };

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.verification}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        this.setState({ verificationFor: this.props.navigation.getParam('verificationFor'), phoneNumber: this.props.navigation.getParam('phoneNumber') })
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    // Called to btn click to verify OTP
    confirm() {
        console.log('Sign up func called')

        let otp = this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit;
        if (otp.length < 4) {
            Alert.alert("Please Enter a valid OTP")
            return;
        }

        if (this.state.verificationFor == VerificationFor.payment) {
            this.props.navigation.navigate('BookingSuccess');
        }
        else if (this.state.verificationFor === VerificationFor.signup) {
            console.log('in Sign up condition')
            this.setState({ showLoader: true })
            this.apiSignup()
        }
        else if (this.state.verificationFor === VerificationFor.forgotPswd) {
            this.setState({ showLoader: true })
            this.apiVerifiyForgotPswdOTP()
        }
        else if (this.state.verificationFor === VerificationFor.editPhoneNumber) {
            this.setState({ showLoader: true })
            this.apiVerifiyEditPhoneOTP()
        }
    }

    // Same parameters used for OTp verification for both Forgot pswd and Edit phone number case.
    getVerifyOTPParams() {
        var param = this.props.navigation.getParam('params');
        const id = param['id'];
        return {
            otp: this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit,
            id: id,
        }
    }

    // Call API to Verify OTP in Edit Phone Number Case
    async apiVerifiyEditPhoneOTP() {
        console.log('getVerifyOTPParams >>>>', this.getVerifyOTPParams())
        RequestManager.postRequestWithHeaders(API_CHECK_PHONE_OTP, this.getVerifyOTPParams()).then((response: any) => {

            console.log('Success Response of apiVerifiyEditPhoneOTP API >>>>', response);
            this.setState({ showLoader: false })

            let otp = this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit
            var phone = this.props.navigation.getParam('phoneNumber')
            this.props.navigation.state.params.func(phone.split(' ')[1], phone.split(' ')[0], otp)
            this.props.navigation.navigate('Profile');
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    // Parameters to send in next class in case of Update Password after forgot pswd
    getPassingParams() {
        var param = this.props.navigation.getParam('params');
        const id = param['id'];
        return {
            forgotPassword: true,
            id: id,
        }
    }

    // Call API to Verify OTP in Forgot Pswd Case
    async apiVerifiyForgotPswdOTP() {
        RequestManager.postRequest(API_CHECK_OTP, this.getVerifyOTPParams(), false).then((response: any) => {

            this.setState({ showLoader: false })
            this.props.navigation.navigate('UpdatePassword', { params: this.getPassingParams() })

        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    // Call signup API with OTp verification
    async apiSignup() {
        LocalDataManager.getDataAsyncStorage('signupInfo').then((data: any) => {
            console.log('Data Retrived from Storage >>>>', data)
            if (data != null && data != {}) {

                let otp = this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit;
                data['otp'] = otp

                RequestManager.postRequest(API_SIGNUP, data, true).then((response: any) => {
                    console.log('REsponse of Signup API >>>>', response);

                    this.setState({ showLoader: false })
                    this.saveLoginUserDetailsAndMove(response.data)
                }).catch((error: any) => {
                    General.showErroMsg(this, error)
                })
            }
        })
    }

    // After Login Save Login data in redux and Local Storage(for use after App kill) and Move to Home Screen
    saveLoginUserDetailsAndMove(data: any) {
        LocalDataManager.removeLocalData('signupInfo')
        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(data))
        this.props.saveLoginInfo(data);
        this.props.navigation.navigate('Home');
    }

    // Called this func on resend Btn Click
    resendOTP() {
        console.log('Resend OTP func called');

        this.setState({ showLoader: true })

        if (this.state.verificationFor === VerificationFor.forgotPswd) {
            this.apiForgotPassword()
        }
        else {
            this.apiResendOTP()
        }
    }

    // Get paramsters for Reesnd OTP API
    getResendOTPParams() {

        var phone = this.props.navigation.getParam('phoneNumber')

        return {
            phone_country_code: phone.split(' ')[0],
            phone_number: phone.split(' ')[1]
        }
    }

    // Call API to Resend OTP
    async apiResendOTP() {
        console.log('Send OTP params >>>>', this.getResendOTPParams())
        RequestManager.postRequest(API_SENDOTP, this.getResendOTPParams(), false).then((response: any) => {
            console.log('REsponse of Send OTP API >>>>', response);

            this.setState({ showLoader: false })
            General.showMsgWithDelay(response.message)

        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    getForgotPswdParams() {
        var phone = this.props.navigation.getParam('phoneNumber')
        return {
            phone_no: phone.split(' ')[1]
        }
    }

    async apiForgotPassword() {

        RequestManager.postRequest(API_FORGOT_PSWD, this.getForgotPswdParams(), false).then((response: any) => {

            console.log('REsponse of apiForgotPassword >>>>', response);

            this.setState({ showLoader: false })
            General.showMsgWithDelay(response.message)
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    // Called when first digit of OTp Changed/entered
    firstDigitChanged(text: string) {

        console.log('First Digit >>>>', this.txtFirst)
        this.setState({ firstDigit: text })

        if (text.length == 1) {
            this.txtSecond.focus()
        }
    }

    // Called when Second digit of OTp Changed/entered
    secondDigitChanged(text: string) {

        console.log('Second Digit >>>>', this.txtSecond)
        this.setState({ secondDigit: text })

        if (text.length == 1) {
            this.txtThird.focus()
        }
    }

    // Called when third digit of OTp Changed/entered
    thirdDigitChanged(text: string) {

        console.log('third Digit >>>>', this.txtThird)
        this.setState({ thirdDigit: text })

        if (text.length == 1) {
            this.txtFourth.focus()
        }
    }

    // Called when fourth digit of OTp Changed/entered
    fourthDigitChanged(text: string) {

        console.log('Fourth Digit >>>>', this.txtFourth)
        this.setState({ fourthDigit: text })

        if (text.length == 1) {
            Keyboard.dismiss()
        }
    }

    // Instances of Textinput  to use for next TextInput focus and dismiss Keyboard
    txtFirst: TextInput;
    txtSecond: TextInput;
    txtThird: TextInput;
    txtFourth: TextInput;

    render() {

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                            <View style={styles.scrollContainer}>
                                <Text style={styles.greyBoldText}>{localize.verifyOTP}</Text>
                                <Text style={{ marginTop: 16 }}>
                                    <Text style={styles.greyText}>{localize.verifyMsg}</Text>
                                    <Text style={[styles.greyBoldText, { fontSize: 12 }]}> {this.state.phoneNumber}</Text>
                                </Text>
                                <View style={{ marginTop: 60, width: '80%', flexDirection: 'row', justifyContent: 'space-around' }}>

                                    <TextInput ref={(input: any) => { this.txtFirst = input as any }} style={[styles.inputContainer, commonShadowStyle]} placeholder='0' placeholderTextColor='grey' keyboardType='numeric' value={this.state.firstDigit} onChangeText={(text: any) => { this.setState({ firstDigit: text }), ((text.length == 1) ? this.txtSecond.focus() : null) }} maxLength={1} returnKeyType="done" />

                                    <TextInput ref={(input: any) => { this.txtSecond = input as any }} style={[styles.inputContainer, commonShadowStyle]} placeholder='0' placeholderTextColor='grey' keyboardType='numeric' value={this.state.secondDigit} onChangeText={(text: any) => { this.setState({ secondDigit: text }), ((text.length == 1) ? this.txtThird.focus() : null) }} maxLength={1} returnKeyType="done" />

                                    <TextInput ref={(input: any) => { this.txtThird = input as any }} style={[styles.inputContainer, commonShadowStyle]} placeholder='0' placeholderTextColor='grey' keyboardType='numeric' value={this.state.thirdDigit} onChangeText={(text: any) => { this.setState({ thirdDigit: text }), ((text.length == 1) ? this.txtFourth.focus() : null) }} maxLength={1} returnKeyType="done" />

                                    <TextInput ref={(input: any) => { this.txtFourth = input as any }} style={[styles.inputContainer, commonShadowStyle]} placeholder='0' placeholderTextColor='grey' keyboardType='numeric' value={this.state.fourthDigit} onChangeText={(text: any) => { this.setState({ fourthDigit: text }), ((text.length == 1) ? Keyboard.dismiss() : null) }} maxLength={1} returnKeyType="done" />
                                </View>
                                <View style={{ marginTop: 20, flexDirection: 'row', alignSelf: 'center', alignItems: 'center', marginBottom: 30 }}>
                                    <Text style={styles.blueText}>{localize.haveNotReceivedCode}</Text>
                                    <TouchableOpacity onPress={() => this.resendOTP()} style={styles.blueTextBtn}>
                                        <Text style={styles.redText}>{localize.resendOTP}</Text>
                                    </TouchableOpacity>
                                </View>

                                <CommonBtn title={localize.confirm} func={() => this.confirm()} width='70%' />
                            </View>
                        </ScrollView>
                    </View>
                </View>

            </KeyboardAvoidingView>
        );
    }
}

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
});

export default connect(null, mapDispatchToProps)(Verification);
