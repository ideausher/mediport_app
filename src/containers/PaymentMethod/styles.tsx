//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, App_MIDIUM_GREY_COLOR, App_LIGHT_BLUE_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
    },
    text: {
        fontSize: 20,
        fontWeight: '500',
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        margin: 24,
        marginTop: 30
    },
});