import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity, SafeAreaView, Modal } from 'react-native';
import { navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { localize } from '../../Utils/LocalisedManager';
import { BackHeaderBtn } from '../../Custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_CARDS } from '../../Utils/APIs';
import General from '../../Utils/General';
import { Loader } from '../../Utils/Loader';
import CardCell from '../../Custom Cells/CardCell';
import { FlatList } from 'react-native-gesture-handler';

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class PaymentMethod extends Component<props, object> {

    state = {
        cardsArr: [] as any[],
        showLoader: false, 
        selectedCardIndex: -1,
    }
    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.paymentMethod}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')}/>
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });


        this.apiGetCards()
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    async apiGetCards() {
        this.setState({ showLoader: true })
        RequestManager.getRequest(API_GET_CARDS, {}).then((response: any) => {
            console.log('REsponse of API_GET_CARDS API >>>>', response);
            this.setState({ showLoader: false, cardsArr: response.data })
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    addPaymentBtnAction() {
        this.props.navigation.navigate('AddCard');
    }

    cardSelected(index: any) {
        console.log('Cele Slected Func Called >>>', index)
        this.setState({selectedCardIndex: index})
        setTimeout(() => {

            this.props.navigation.navigate('SavedCards', {
                selectedCard: this.state.cardsArr[index]
              });              
        }, 200)
        
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.blueConatiner}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                    <View style={styles.container}>
                        <TouchableOpacity

                            onPress={() => this.addPaymentBtnAction()}>
                            <Text style={styles.text}>+ {localize.addPaymentMethod}</Text>
                        </TouchableOpacity>

                        <FlatList
                               // style={{ marginRight: 8, marginLeft: 8 }}
                                data={this.state.cardsArr}
                                keyExtractor={(item: any, index: any) => index.toString()}
                                renderItem={({ item, index }: any) => <CardCell
                                    onClickEvent={(indx: any) => this.cardSelected(indx)} item={item} index={index} selectedCardIndx={this.state.selectedCardIndex}/>
                                }
                            />
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}