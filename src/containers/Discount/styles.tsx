//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR} from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,        
        backgroundColor: APP_GREY_BACK,
       //padding: 16,
    },
    availableText:{
        textAlign: 'left',
        color: APP_GREY_TEXT_COLOR,
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 30,
        backgroundColor: '#F2F2F2',
        paddingLeft: 16,
        height: 40,
        lineHeight: 40
    },
    
});