import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity, Image, SafeAreaView, FlatList, Modal } from 'react-native';
import { APP_HEADER_TITLE_COLOR, APP_HEADER_BACK_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import DiscountCell from '../../Custom Cells/DiscountCell';
import { localize } from '../../Utils/LocalisedManager';
import { BackHeaderBtn, NoDataFoundView } from '../../Custom/CustomComponents';
import { API_GET_COUPENS, API_VALIDATE_COUPENS } from '../../Utils/APIs';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
//@ts-ignore
import { connect } from 'react-redux';
import { Loader } from '../../Utils/Loader';

export interface props {
    navigation: NavigationScreenProp<any, any>,
    doctorInfo: any,
};
class Discount extends Component<props, object> {

    state = {
        coupensArr: [] as any[],
        showLoader: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.discount}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        this.apiGetCoupens()
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    copyCode(index: any) {
        this.callvalidateCoupenAPI(this.state.coupensArr[index].code)
    }

    async apiGetCoupens() {
        this.setState({ showLoader: true })
        RequestManager.getRequest(API_GET_COUPENS, {}).then((response: any) => {
            console.log('REsponse of API_GET_COUPENS API >>>>', response);
            this.setState({ showLoader: false, coupensArr: response.data })
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    // Get paarmeters for VALIDATE COUPEN API
    private getParams(code: string) {

        return {
            "code": code,
            "price": this.props.doctorInfo.price
        }
    }

    private async callvalidateCoupenAPI(code: string) {
        console.log('PArams validate Coupen >>>>', this.getParams(code))
        this.setState({ showLoader: true })
        let weakSelf = this
        RequestManager.postRequestWithHeaders(API_VALIDATE_COUPENS, weakSelf.getParams(code)).then((response: any) => {

            console.log('REsponse of API_VALIDATE_COUPENS API >>>>', response);

            weakSelf.setState({ showLoader: false })

            setTimeout(() => {
                weakSelf.props.navigation.state.params.onGoBack(code, response.data.price);
                weakSelf.props.navigation.goBack();
            }, 200)
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    render() {

        console.log('Coupons Arr >>>', this.state.coupensArr)

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.blueConatiner}>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showLoader}>
                        <Loader />
                    </Modal>
                    <View style={styles.container}>
                        {this.state.coupensArr.length > 0 ? <View>
                            <Text style={styles.availableText}>{localize.availableDiscounts}</Text>
                            <FlatList style={{ margin: 5, width: '100%' }}
                                data={this.state.coupensArr}
                                keyExtractor={(item: any, index: any) => index.toString()}
                                renderItem={({ item, index }: any) => <DiscountCell
                                    onClickEvent={(indx: any) => this.copyCode(indx)}
                                    item={item} index={index} />}
                            />
                        </View> : (this.state.showLoader ? null : <NoDataFoundView func={() => this.apiGetCoupens()} showFilterBtn={this.state.coupensArr.length == 0} />)}
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    doctorInfo: state.saveDoctorInfo,
});


export default connect(mapStateToProps)(Discount);