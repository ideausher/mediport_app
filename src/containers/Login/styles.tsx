//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREEN_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_LIGHT_GREY_COLOR, App_MIDIUM_GREY_COLOR } from '../../Constants'

export const styles = ScaleSheet.create({
    gradientView: {
        flex: 1,
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 16,
        paddingRight: 16,
        justifyContent:'center'
    },
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10,
    },
    image: {
        marginTop: 8,
        height: 200,
        width: '95%',
        resizeMode: 'contain',
        alignItems: 'center',
        justifyContent: 'center',
    },
    signInTxt: {
        fontSize: 20,
        fontWeight: "500",
        textAlign: 'center',
        marginTop: 8
    },
    text: {
        fontSize: 16,
        fontWeight: "500",
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        marginTop: 8,
    },
    greyText: {
        fontSize: 16,
        fontWeight: "500",
        color: App_MIDIUM_GREY_COLOR,
        textAlign: 'center',
    },
    greyLine: {
        width: '35%',
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: '#F3F5F8',
    },
    greenTextBtn: {
        justifyContent: 'center',
        height: 30,
    },
    greenText: {
        color: APP_GREEN_COLOR,
        fontSize: 12,
        fontWeight: "500",
        marginLeft: 4
    },
});