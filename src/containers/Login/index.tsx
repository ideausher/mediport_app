import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, Modal, Keyboard, BackHandler } from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_GREEN_COLOR, App_MIDIUM_GREY_COLOR, commonShadowStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { TextInputWithoutImage } from '../../Custom/CustomTextInput';
import { CommonBtn, GreenTextBtn } from '../../Custom/CustomButton';
import { LOGIN } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { API_SIGNIN } from '../../Utils/APIs';
import { Loader } from '../../Utils/Loader';
import RequestManager from '../../Utils/RequestManager'
import { localize } from '../../Utils/LocalisedManager';
import LocalDataManager from '../../Utils/LocalDataManager';
import { ForGotScreenType } from '../../Utils/Enums';
import LinearGradient from 'react-native-linear-gradient';
// import FCM from "react-native-fcm";

//@ts-ignore
import validator from 'validator';
import General from '../../Utils/General';
import Toast from 'react-native-simple-toast';

export interface LoginProps {
    navigation: NavigationScreenProp<any, any>,
    saveLoginInfo: any,
};

const { StatusBarManager } = NativeModules;

class Login extends Component<LoginProps, object> {

    state = {
        emailListHeight: 0,
        value: '',
        isPswdHide: true,
        rememberMe: '0',
        statusBarHeight: 0,
        email: '',
        password: '',
        showLoader: false,
    };

    token = '';

    static navigationOptions = () => {
        return {
            header: null
        }
    }

    componentDidMount() {
        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }
        else
        {
            // Will Prevent to move move back on hardware back btn clicked
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        return true
    }

    // getFcmToken(hitApi: boolean) {
    //     FCM.getFCMToken().then(token => {
    //         this.token = token || "";
    //         console.log('FCM TOKEn  >>>>', JSON.stringify(this.token))
    //         LocalDataManager.saveDataAsyncStorage('fcmToken', JSON.stringify(this.token))
    //         if (hitApi) {
    //             this.setState({ showLoader: true })
    //             this.apiSignin()
    //         }
    //     });
    // }

    private validateEmailPhone() {

        if (validator.isEmail(this.state.email)) {
            return true
        }
        else if (validator.isMobilePhone(this.state.email, 'any', { strictMode: false }) && this.state.email.length == 10) {
            return true
        }

        return false
    }

    // Validate Login Credentials eneterd
    private validateDetail() {

        if (this.state.email.length <= 0) {
            Toast.show(localize.pleaseEnterEmail, Toast.SHORT);
            return false;
        }
        else if (!this.validateEmailPhone()) {
            Toast.show(localize.pleaseEnterValidEmailPhoneNum, Toast.SHORT);
             return false;
        }
        else if (this.state.password.length <= 0) {
           Toast.show(localize.pleaseEnterPswd, Toast.SHORT);
           return false;
        }

        return true;
    }

    // Caled on Login Btn Clicked
    private loginAction() {

        Keyboard.dismiss()

        this.setState({ showLoader: true })
                this.apiSignin()
        // if (this.token == '') {
            // this.getFcmToken(true)
        //     return
        // }
        // else {
        //     if (this.validateDetail()) {
        //         this.setState({ showLoader: true })
        //         this.apiSignin()
        //     }
        // }
    }

    // Get paarmeters for Login API
    private getParams() {
        return {
            email: this.state.email,
            password: this.state.password,
        }
    }

    // Call Login API
    private async apiSignin() {

        let weakSelf = this
        RequestManager.postRequest(API_SIGNIN, this.getParams(), true).then((response: any) => {

            console.log('REsponse of Login API >>>>', response);
            setTimeout(function () {
                weakSelf.setState({ showLoader: false })
            }, 200);
            this.saveLoginUserDetailsAndMove(response.data)
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    // After Login Save Login data in redux and Local Storage(for use after App kill) and Move to Home Screen
    private saveLoginUserDetailsAndMove(data: any) {

        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(data))
        this.props.saveLoginInfo(data);
        this.props.navigation.navigate('Home');
    }

    // Called on forgot Paswd Btn click -> Moves to forgot Pswd Screen
    private forgotPswd() {
        this.props.navigation.navigate('ForgotPassword', { forgotScreenType: ForGotScreenType.forgotPswd });
    }

    // Call on Signup Btn click -> Move to Signup Screen
    private signup() {
        this.props.navigation.navigate('SignUp')

        // const token = PaymentManager.setStripeKey()
        //console.log('Stripe Token Genrated >>>>', token);
    }

    render() {
            return (
                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -300 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showLoader}>
                        <Loader />
                    </Modal>
                    <View style={styles.gradientView}>
                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['#E0F2EC', '#D4E1E9']} style={styles.linearGradient} >
                            <View style={[styles.container, commonShadowStyle]}>
                                <Image style={styles.image} source={require('../../assets/login-banner.png')} />
                                <Text style={styles.signInTxt}>
                                    <Text style={{ color: APP_HEADER_BACK_COLOR }}>{localize.helloSign + ' '}</Text>
                                    <Text style={{ color: APP_GREEN_COLOR }}>{localize.withUs}</Text>
                                </Text>
                                <TextInputWithoutImage placeholder={localize.email + '/' + localize.phoneNumber} onChange={(text: any) => this.setState({ email: text })} value={this.state.email} autoCapitalize='none' keyboardType='email-address' marginTop={24} />
                                <TextInputWithoutImage placeholder={localize.password} onChange={(text: any) => this.setState({ password: text })} value={this.state.password} autoCapitalize='none' secureTextEntry={true} />
                                <GreenTextBtn title={localize.forgotPassword + '?'} func={() => this.forgotPswd()} marginRight='5%' marginTop={8} textAlign='right' />
                                <CommonBtn title={localize.signIn} func={() => this.loginAction()} width='70%' marginTop={8} />
                                <View style={{ marginTop: 8, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={[styles.greyLine, { height: 0.2 }]} />
                                    <Text style={styles.greyText}>{localize.or}</Text>
                                    <View style={[styles.greyLine, { height: 0.2 }]} />
                                </View>
                                <View style={{ margin: 8, flexDirection: 'row', alignSelf: 'center', alignItems: 'center' }}>
                                    <Text style={[styles.greyText, { fontSize: 12, color: App_MIDIUM_GREY_COLOR }]}>{localize.dontHaveAccount}</Text>
                                    <TouchableOpacity onPress={() => this.signup()} style={styles.greenTextBtn}>
                                        <Text style={styles.greenText}>{localize.signUp}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </LinearGradient>
                    </View>
                </KeyboardAvoidingView>
            );
    }
}

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
});

export default connect(null, mapDispatchToProps)(Login);
