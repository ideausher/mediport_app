import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, Alert, Modal, TouchableHighlight, ScrollView } from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_GREY_TEXT_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { TextInputWithoutImage, TextInputPhone, TextInputWithDropdown } from '../../Custom/CustomTextInput';
import { CommonBtn } from '../../Custom/CustomButton';
//@ts-ignore
import validator from 'validator';
import { Loader } from '../../Utils/Loader';
import RequestManager from '../../Utils/RequestManager'
import { API_SENDOTP } from '../../Utils/APIs'
//@ts-ignore
import { ConfirmDialog } from 'react-native-simple-dialogs';
//@ts-ignore
import { CountrySelection, countries} from 'react-native-country-list';
import { VerificationFor } from '../../Utils/Enums';
import { localize } from '../../Utils/LocalisedManager';
import LocalDataManager from '../../Utils/LocalDataManager';
import General from '../../Utils/General';
import { BackHeaderBtn } from '../../Custom/CustomComponents';
import { StatusBar } from 'react-native';
import Toast from 'react-native-simple-toast';


export interface LoginProps {
    navigation: NavigationScreenProp<any, any>
};

const { StatusBarManager } = NativeModules;

export default class SignUp extends Component<LoginProps, object> {

    state = {
        name: '',
        email: '',
        password: '',
        cnfrmPswd: '',
        phoneNumber: '',
        countryCode: '+91',
        statusBarHeight: 0,
        showLoader: false,
        dialogVisible: false,
        responseMessage: '',
        showCodeList: false,
        flag: undefined
    };

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.signup}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }


        console.log('Countries >>>>>', countries);

    }

    // back Btn Action
    backBtnHandler() {
        this.props.navigation.goBack();
    }

    // Validation for all fields
    validateDetail() {
        if (this.state.name.length <= 0) {
            //Alert.alert('Please enter name.');
            Toast.show(localize.pleaseEnterName, Toast.SHORT);
            return false;
        }
        else if (this.state.email.length <= 0) {
            //Alert.alert('Please enter email.');
            Toast.show(localize.pleaseEnterEmail, Toast.SHORT);
            return false;
        }
        else if (!validator.isEmail(this.state.email)) {
           // Alert.alert('Please enter valid email.');
            Toast.show(localize.pleaseEnterValidEmail, Toast.SHORT);
            return false;
        }
        else if (this.state.password.length <= 0) {
           // Alert.alert('Please enter Password.');
            Toast.show(localize.pleaseEnterPswd, Toast.SHORT);
            return false;
        }
        else if (this.state.password.length < 6) {
            //Alert.alert('Please enter Password of atleast 6 digits');
            Toast.show(localize.pleaseEnterPswdOf6Digits, Toast.SHORT);
            return false;
        }
        else if (this.state.cnfrmPswd.length <= 0) {
        //    Alert.alert('Please enter confirm Password.');
            Toast.show(localize.pleaseEnterCnfmPswd, Toast.SHORT);
            return false;
        }
        else if (this.state.cnfrmPswd != this.state.password) {
          //  Alert.alert('Confirm password doesn\'t matched');
            Toast.show(localize.confirmPswdDoesntMatched, Toast.SHORT);
            return false;
        }
        else if (this.state.countryCode.length <= 0) {
           // Alert.alert('Please enter Country Code.');
            Toast.show(localize.pleaseEnterCountryCode, Toast.SHORT);
            return false;
        }
        else if (this.state.phoneNumber.length <= 0) {
           // Alert.alert('Please enter phone number.');
            Toast.show(localize.pleaseEnterPhoneNum, Toast.SHORT);
            return false;
        }
        else if (!validator.isMobilePhone(this.state.phoneNumber, 'any', { strictMode: false })) {
            Toast.show(localize.pleaseEnterValidPhoneNum, Toast.SHORT);
            return false;
        }

        return true;
    }

    // called in Signup Btn Click
    signup() {
        if (this.validateDetail()) {
            this.setState({ showLoader: true })
            this.apiSendOTP()
        }
    }

    // Gete parameters of send OTp Api
    getOTPParams() {
        return {
            phone_country_code: this.state.countryCode,
            phone_number: this.state.phoneNumber,
            email: this.state.email
        }
    }

    // API tp send OTp on added Phone Number for verification
    apiSendOTP() {
        RequestManager.postRequest(API_SENDOTP, this.getOTPParams(), false).then((response: any) => {
            console.log('REsponse of Send OTP API >>>>', response);

            this.saveSignupDataOnLocal()
            this.setState({ showLoader: false })

            setTimeout(() => {
                Alert.alert(response.message)
                this.props.navigation.navigate('Verification', { verificationFor: VerificationFor.signup, phoneNumber: this.state.countryCode + ' ' + this.state.phoneNumber })
            }, 200);

        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    // Ge all values to Save in Local Storage 
    getAllParams() {
        return {
            firstname: this.state.name,
            email: this.state.email,
            password: this.state.password,
            password_confirmation: this.state.cnfrmPswd,
            phone_country_code: this.state.countryCode,
            phone_number: this.state.phoneNumber,
        }
    }

    // Save Signup data on local storage so that if user Fills Signup form, move to next screen and kills app without verify phone Number with OTp then next time we will show him/her Direct verification Screen
    private saveSignupDataOnLocal() {
        LocalDataManager.saveDataAsyncStorage('signupInfo', JSON.stringify(this.getAllParams()))
    }

    // Called to show Country Code picker
    showCountryCodeList() {
        this.setState({ showCodeList: true })
    }

    // Caleld when Country Code is selected and save selected country code in state
    countryCodeSelcted(item: any) {

        this.setState({ countryCode: '+' + item.callingCode, showCodeList: false, flag: item.flag })
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showCodeList}>
                    <View style={{ flex: 1, marginTop: 20 }}>
                        <TouchableOpacity
                            style={{ backgroundColor: 'white' }}
                            onPress={() => this.setState({ showCodeList: false })}>
                            <Text style={{ alignSelf: 'flex-end', lineHeight: 30, width: 60, textAlign: 'center', fontSize: 14, color: APP_GREY_TEXT_COLOR }}>{localize.cancel}</Text>
                        </TouchableOpacity>
                        <CountrySelection action={(item: any) => this.countryCodeSelcted(item)} selected={true} />
                    </View>
                </Modal>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1}} scrollEnabled={false}>
                            <View style={styles.scrollContainer}>
                                <TextInputWithoutImage placeholder={localize.name} onChange={(text: any) => this.setState({ name: text })} value={this.state.name} />
                                <TextInputWithoutImage placeholder={localize.email} onChange={(text: any) => this.setState({ email: text })} value={this.state.email} autoCapitalize='none' keyboardType='email-address' />
                                <TextInputWithoutImage placeholder={localize.password} onChange={(text: any) => this.setState({ password: text })} value={this.state.password} autoCapitalize='none' secureTextEntry={true} />
                                <TextInputWithoutImage placeholder={localize.cnfrmPassword} onChange={(text: any) => this.setState({ cnfrmPswd: text })} value={this.state.cnfrmPswd} autoCapitalize='none' secureTextEntry={true} />
                                <View style={{ width: '90%', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 32 }}>
                                    <TextInputWithDropdown func={(text: any) => this.showCountryCodeList()} value={this.state.countryCode} placeholder='+91' img={this.state.flag == undefined ? require('../../assets/flag.png') : { uri: this.state.flag }} />
                                    <TextInputPhone placeholder={localize.phoneNumber} onChange={(text: any) => this.setState({ phoneNumber: text })} value={this.state.phoneNumber} returnKeyType="done" />
                                </View>
                                <CommonBtn title={localize.signup} func={() => this.signup()} width='90%' />
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}
