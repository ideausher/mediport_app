//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREEN_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_HEADER_TITLE_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
    },
    scrollContainer: {
        padding: 8,
        paddingTop: 16,
        alignItems: 'center',
    },
    image: {
        marginTop: 50,
        height: 200,
        width: '100%',
        resizeMode: 'contain',
        alignItems: 'center',
        justifyContent: 'center',
    },
    signInTxt: {
        fontSize: 20,
        fontWeight: "500",
        textAlign: 'center',
        marginTop: 8,

    },
    text: {
        fontSize: 16,
        fontWeight: "500",
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        marginTop: 8,
    },
    greyText: {
        fontSize: 16,
        fontWeight: "500",
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'center',
    },
    greyLine: {
        width: '35%',
        height: 1,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: APP_GREY_TEXT_COLOR,
    },
    greenTextBtn: {
        justifyContent: 'center',
        width: 120,
        height: 30,
    },
    greenText: {
        color: APP_GREEN_COLOR,
        fontSize: 14,
        fontWeight: "500",
        marginLeft: 4
    }
});