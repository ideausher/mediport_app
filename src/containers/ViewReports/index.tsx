import React, { Component } from "react";
import {
  View,
  Text,
  Alert,
  TouchableOpacity,
  Image,
  SafeAreaView,
  FlatList,
  Modal,
} from "react-native";
import {
  APP_HEADER_TITLE_COLOR,
  APP_HEADER_BACK_COLOR,
  navHdrTxtStyle,
  headerTitleStyle,
  headerStyle,
} from "../../Constants";
import { NavigationScreenProp } from "react-navigation";
import { styles } from "./styles";
import ReportsCell from "../../Custom Cells/ReportsCell";
import { localize } from "../../Utils/LocalisedManager";
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';
import RNFS from 'react-native-fs';
import {
  OpenDrawerHeaderBtn,
  BackHeaderBtn,
  NoDataFoundView,
  NoInternetFoundView,
} from "../../Custom/CustomComponents";
import { API_DOCTOR_REPORTS_LIST } from "../../Utils/APIs";
import RequestManager from "../../Utils/RequestManager";
import General from "../../Utils/General";
import { Loader } from "../../Utils/Loader";
import ViewReportsCell from "../../Custom Cells/ViewReportsCell";
import PDFLib, { PDFDocument, PDFPage } from "react-native-pdf-lib";
import moment from "moment";
import * as Progress from 'react-native-progress';
import SimpleToast from "react-native-simple-toast";

export interface props {
  navigation: NavigationScreenProp<any, any>;
}

export default class ViewReports extends Component<props, object> {
  state = {
    page: 0,
    isRefreshing: false,
    loadMore: false,
    isCalled: false,
    prevPage: -1,
    isInternetError: false,
    isDataNotFoundError: false,
    reportsListArr: [] as any[],
    showLoader: false,
    downloadLoader : false
  };

  static navigationOptions = ({ navigation }: any) => {
    return {
      headerTitle: (
        <View>
          <Text style={navHdrTxtStyle}>{localize.reports}</Text>
        </View>
      ),
      headerLeft: (
        <BackHeaderBtn func={navigation.getParam("backBtnHandler")} />
      ),
      headerStyle: headerStyle.style,
      headerTitleStyle: headerTitleStyle,
    };
  };
  componentDidMount() {
    this.props.navigation.setParams({
      backBtnHandler: () => this.backBtnHandler(),
    });

    this.setState({ showLoader: true });
    this.apiGetReportsList(false);
  }

  backBtnHandler() {
    this.props.navigation.goBack();
  }

  // Get paarmeters
  private getParams(refresh: boolean) {
    return {
      vendor_id: this.props.navigation.getParam("venderId"),
      page: refresh ? 0 : this.state.page,
      limit: 10,
    };
  }

  async apiGetReportsList(refresh: boolean) {
    this.setState({ isInternetError: false, isDataNotFoundError: false });
    let weakSelf = this;
    RequestManager.getRequest(API_DOCTOR_REPORTS_LIST, this.getParams(refresh))
      .then((response: any) => {
        console.log("REsponse of API_APPOINTMENT_LIST API >>>>", response);

        let reportsArrData: any[] = [];
        if (refresh) {
          weakSelf.setState({ page: 0, prevPage: -1, reportsListArr: [] });
          reportsArrData = response.data;
        } else {
          reportsArrData = this.state.reportsListArr.concat(response.data);
        }

        setTimeout(() => {
          weakSelf.setState({
            showLoader: false,
            reportsListArr: reportsArrData,
            loadMore: false,
            prevPage: this.state.page,
            isRefreshing: false,
          });

          setTimeout(() => {
            if (weakSelf.state.reportsListArr.length == 0) {
              weakSelf.setState({
                isInternetError: false,
                isDataNotFoundError: true,
              });
            }
            weakSelf.setState({ isCalled: false });
          }, 200);

          if (response.data.length > 0) {
            let nextPage = this.state.page + 1;
            weakSelf.setState({ page: nextPage });
          }
        }, 200);
      })
      .catch((error: any) => {
        this.setState({
          showLoader: false,
          loadMore: false,
          isCalled: false,
          isRefreshing: false,
        });
        if (error == localize.internetConnectionError) {
          this.setState({ isInternetError: true, isDataNotFoundError: false });
        } else {
          General.showErroMsg(this, error);
        }
      });
  }

  //Will call when scroll view pulled down to refresh
  private refreshList() {
    this.setState({ isRefreshing: true });
    setTimeout(() => {
      this.apiGetReportsList(true);
    }, 200);
  }

  //Will call when reached last cell
  private loadMoreData = () => {
    if (this.state.prevPage == this.state.page) {
      return;
    }
    this.setState({ loadMore: true, isCalled: true });
    setTimeout(() => {
      this.apiGetReportsList(false);
    }, 200);
  };

  private isCloseToBottom = ({
    layoutMeasurement,
    contentOffset,
    contentSize,
  }: any) => {
    const paddingToBottom = 50; // how far from the bottom

    let value1 = layoutMeasurement.height + contentOffset.y;
    let value2 = contentSize.height - paddingToBottom;
    let isVAlue2GreaterThanLayout = value2 > layoutMeasurement.height;
    let value1GreterthanValue2 = value1 >= value2;
    let isClose = isVAlue2GreaterThanLayout && value1GreterthanValue2;

    return isClose;
  };

  p = (item) => {
request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then(()=>{
    this.permissions(item)
})
  }

  permissions = (items:any) => {
    check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
    .then((result) => {
      switch (result) {
        case RESULTS.UNAVAILABLE:
          console.log(
            'This feature is not available (on this device / in this context)',
          );
          break;
        case RESULTS.DENIED:
          console.log(
            'The permission has not been requested / is denied but requestable',
          );
          break;
        case RESULTS.GRANTED:
          console.log('The permission is granted');
          this.downloadPdf(items)
          break;
        case RESULTS.BLOCKED:
          console.log('The permission is denied and not requestable anymore');
          break;
      }
    })
    .catch((error) => {
      // …
    });
  }

  downloadPdf = async(item: any) => {
    this.setState({...this.state, downloadLoader : true})

    console.log("Reports Detail >>>>>>>", item);
    let bookingDate = moment(item.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY')
    let bookingTime = moment(item.created_at, 'YYYY-MM-DD HH:mm:ss').format('hh:mm a')
    const page1:any = await PDFPage.create()
    .setMediaBox(300, 500)
      .drawText("Name", {
        x: 5,
        y: 480,
        color: "#007386",
        fontName: "Muli",
        fontSize:12
      })
      .drawText(`:   ${item.booking.booking_detail.patient_name}`,{
        x: 100,
        y: 480,
        color: "#aab1b5",
        fontName: "Muli",
        fontSize:12
      })
      .drawText("Age:", {
        x: 5,
        y: 460,
        color: "#007386",
        fontName: "Muli",
        fontSize:12
      })
      .drawText(`:   ${item.booking.booking_detail.age}`,{
        x: 100,
        y: 460,
        color: "#aab1b5",
        fontName: "Muli",
        fontSize:12
      })
      .drawText("Reference ID:", {
        x: 5,
        y: 440,
        color: "#007386",
        fontName: "Muli",
        fontSize:12
      })
      .drawText(`:   ${item.booking.reference_id}`,{
        x: 100,
        y: 440,
        color: "#aab1b5",
        fontName: "Muli",
        fontSize:12
      })
      .drawText("Date:", {
        x: 5,
        y: 420,
        color: "#007386",
        fontName: "Muli",
        fontSize:12
      })
      .drawText(`:   ${bookingDate}`,{
        x: 100,
        y: 420,
        color: "#aab1b5",
        fontName: "Muli",
        fontSize:12
      })
      .drawText("Time:", {
        x: 5,
        y: 400,
        color: "#007386",
        fontName: "Muli",
        fontSize:12
      })
      .drawText(`:   ${bookingTime}`,{
        x: 100,
        y: 400,
        color: "#aab1b5",
        fontName: "Muli",
        fontSize:12
      })
      .drawText("Doctor's name:", {
        x: 5,
        y: 380,
        color: "#007386",
        fontName: "Muli",
        fontSize:12
      })
      .drawText(`:   ${item.booking.vendor.firstname}`,{
        x: 100,
        y: 380,
        color: "#aab1b5",
        fontName: "Muli",
        fontSize:12
      })
      .drawText("Disease:", {
        x: 5,
        y: 360,
        color: "#007386",
        fontName: "Muli",
        fontSize:12
      })
      .drawText(`:   ${item.data.issue}`,{
        x: 100,
        y: 360,
        color: "#aab1b5",
        fontName: "Muli",
        fontSize:12
      })
      .drawText("Doctor Feedback:", {
        x: 5,
        y: 340,
        color: "#007386",
        fontName: "Muli",
        fontSize:12
      })
      .drawText(item.data.report,{
        x: 5,
        y: 315,
        color: "#aab1b5",
        fontName: "Muli",
        fontSize:12,
        
      })

      // Create a new PDF in your app's private Documents directory
var docsDir = await RNFS.DownloadDirectoryPath;
console.log(docsDir)
docsDir = `${docsDir}`;
// const docsDir = await PDFLib.getDocumentsDirectory();
const pdfPath = `${docsDir}/report_${bookingDate}.pdf`;

console.log(pdfPath)
PDFDocument
  .create(pdfPath)  
  .addPages(page1)
  // .addPages(page2)
  .write() // Returns a promise that resolves with the PDF's path
  .then(path => {
    console.log('PDF created at: ' + path);
    // Do stuff with your shiny new PDF!
  }).catch(err => {
    console.log(err)
  })
  
  setTimeout(() => {
    this.setState({...this.state, downloadLoader : false})
    SimpleToast.show("Pdf Downloaded successfully!")
  }, 2000);

  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.blueConatiner}>
          <Modal
            animated={true}
            transparent={true}
            visible={this.state.showLoader}
          >
            <Loader />
          </Modal>
          
          <Modal
            animated={true}
            transparent={true}
            visible={this.state.downloadLoader}
          >
          
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <View style={{ height:'100%', width: '100%', backgroundColor: 'rgba(0, 0, 0, 0.5)' }} />
      <View style={{ backgroundColor: 'transparent', position: 'absolute'}}>
      <Progress.Circle size={50} indeterminate={true} borderColor={'white'} borderWidth={3} />
      </View>
    </View>
          </Modal>

          <View style={styles.container}>
            {this.state.reportsListArr.length > 0 ? (
              <FlatList
                style={{ marginTop: 16 }}
                onRefresh={() => this.refreshList()}
                refreshing={this.state.isRefreshing}
                data={this.state.reportsListArr}
                keyExtractor={(item: any, index: any) => index.toString()}
                renderItem={({ item, index }: any) => (
                  <ViewReportsCell
                    downloadPdf={() => this.p(item)}
                    item={item}
                  />
                )}
                onEndReachedThreshold={0.5}
                onScroll={({ nativeEvent }) => {
                  if (
                    this.state.isCalled == false &&
                    this.isCloseToBottom(nativeEvent) &&
                    this.state.isRefreshing == false
                  ) {
                    this.loadMoreData();
                  }
                }}
              />
            ) : this.state.showLoader ? null : this.state
                .isDataNotFoundError ? (
              <NoDataFoundView />
            ) : this.state.isInternetError ? (
              <NoInternetFoundView func={() => this.apiGetReportsList(false)} />
            ) : null}
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
