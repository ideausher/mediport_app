export const FiltersArr = [
    {
        appointmentOption: 'Price', 
        isSelected: false,
        min: 1,
        max:1000,
        selectedMin: 1,
        selectedMax:1000,
        step: 10
    },
    {
        appointmentOption: 'Rating', 
        isSelected: false,
        min: 1,
        max:5,
        selectedMin: 1,
        selectedMax:5,
        step: 1
    },
    {
        appointmentOption: 'Experience', 
        isSelected: false,
        min: 1,
        max:50,
        selectedMin: 1,
        selectedMax:50,
        step: 1
    },
    {
        appointmentOption: 'Availability', 
        isSelected: false,
        min: 1,
        max:24,
        selectedMin: 1,
        selectedMax:24,
        step: 1
    },
]