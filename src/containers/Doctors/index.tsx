import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, FlatList, Modal, Alert, StatusBar, PermissionsAndroid, Platform } from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, APP_GREY_BACK, APP_GREY_TEXT_COLOR, App_MIDIUM_GREY_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle, commonShadowStyle, LOGIN_DATA } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import DoctorCell from '../../Custom Cells/DoctorCell';
import LinearGradient from 'react-native-linear-gradient';
import { localize } from '../../Utils/LocalisedManager';
import { API_DOCTORS_LIST, API_MY_DOCTORS_LIST } from '../../Utils/APIs';
import RequestManager from '../../Utils/RequestManager'
import RNGooglePlaces from 'react-native-google-places';
import { BackHeaderBtn, OpenDrawerHeaderBtn, NotificationsHeaderBtn, BottomShadowView } from '../../Custom/CustomComponents';
import { ScrollView } from 'react-native-gesture-handler';
import { FiltersArr } from './Constants';
//@ts-ignore
import RangeSlider from 'rn-range-slider';
import { Loader } from '../../Utils/Loader';
import { CommonBtn } from '../../Custom/CustomButton';
import Toast from 'react-native-simple-toast';
//@ts-ignore
import { connect } from 'react-redux';
import { NoDataFoundView, NoInternetFoundView } from '../../Custom/CustomComponents';
import NetInfo from "@react-native-community/netinfo";
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import General from '../../Utils/General';
import AsyncStorage from '@react-native-community/async-storage';

export interface props {
    navigation: NavigationScreenProp<any, any>
    categoryInfo: any,
};

const AddRangeSlider = (props: any) => {

    console.log('AddRangeSlider >>>', props)
    return (
        <RangeSlider
            style={{ height: 50, marginLeft: 32, marginRight: 32 }}
            gravity={'center'}
            min={props.min}
            max={props.max}
            step={props.step}
            initialLowValue={props.selectedMin}
            initialHighValue={props.selectedMax}
            thumbRadius={7}
            labelStyle='none'
            selectionColor={APP_GREEN_COLOR}
            blankColor={APP_LIGHT_GREY_COLOR}
            onValueChanged={(low: any, high: any, fromUser: any) => {
                props.func(low, high)
            }} />
    );
};

class Doctors extends Component<props, object> {

    state = {
        doctorsList: [],
        filteredDoctorsList: [],
        showLoader: false,
        lat: 0,
        long: 0,
        filtersArr: [] as any[],
        showFilters: false,
        currentlySelctedFilterIndex: 0,
        page: 0,
        isRefreshing: false,
        loadMore: false,
        isCalled: false,
        prevPage: -1,
        isAnyFilterSelected: false,
        searchText: '',
        isInternetError: false,
        isDataNotFoundError: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{navigation.getParam('type') == 1 ? navigation.getParam('selectedCat') : localize.doctors}</Text>
                </View>
            ),
            headerLeft: (
                navigation.getParam('type') == 1 ? <BackHeaderBtn func={navigation.getParam('revealBtnHandler')} /> : <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerRight: (
                navigation.getParam('type') == 1 ? null : <NotificationsHeaderBtn func={navigation.getParam('notificationsHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle,
            tabBarVisible: navigation.getParam('type') == 1 ? false : true,
            gesturesEnabled: false,
            drawerLockMode: 'locked-closed',
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.setState({ filtersArr: FiltersArr })

        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'selectedCat': this.props.categoryInfo.category.cat_name });

        this.getCurrentLocation()

        this.resetAllFilters()

        if (this.props.navigation.getParam('type') == 1) {

        }
        else {
            this.props.navigation.addListener('willFocus', (route) => this.refreshData());
        }
    }

    //Actions
    private revealBtnHandler() {

        if (this.props.navigation.getParam('type') == 1) {
            this.props.navigation.goBack()
        }
        else {
            this.props.navigation.openDrawer();
        }
    }

    private notificationsHandler() {
        this.props.navigation.navigate('Notifications')
    }

    searching(text: string) {
        this.setState({ searchText: text })
    }

    private getCurrentLocation() {

        console.log('Current location func called')
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ showLoader: true, isInternetError: false, isDataNotFoundError: false })
                RNGooglePlaces.getCurrentPlace()
                    .then((results) => {
                        const { latitude, longitude } = results[0].location;
                        this.setState({ lat: latitude, long: longitude })
                        this.apiGetDoctorsList(false)
                    })
                    .catch((error: any) => {
                        //Check if permission granted or not otherwise showing slow internet connection error
                        this.isLocationPermissionGranted();
                    });
            }
            else {
                console.log('Net not Conencted')
                this.setState({ isInternetError: true, isDataNotFoundError: false })
            }
        })
    }
    async isLocationPermissionGranted() {
        if (Platform.OS === "ios") {
            this.checkForLocationPermissions()
        } else {
            this.checkAndroidLocationPermission();
        }
    }

    async checkAndroidLocationPermission() {

        await check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
            .then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        break;
                    case RESULTS.DENIED:
                        this.handleLocationError(localize.pleaseAllowAppToAccessLocation)
                        break;
                    case RESULTS.GRANTED:
                        this.handleLocationError('Please enable your Device Location')
                        break;
                    case RESULTS.BLOCKED:
                        break;
                }
            })
            .catch(error => {
                console.warn(error);
            });
    }

    private checkForLocationPermissions() {
        check(PERMISSIONS.IOS.LOCATION_ALWAYS)
            .then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        break;
                    case RESULTS.DENIED:
                        this.handleLocationError(localize.pleaseAllowAppToAccessLocation)
                        break;
                    case RESULTS.GRANTED:
                        this.handleLocationError('Please enable your Device Location')
                        break;
                    case RESULTS.BLOCKED:
                        break;
                }
            })
            .catch(error => {
                // …
            });
    }

    private handleLocationError(error: any) {
        this.setState({ showLoader: false })
        setTimeout(() => {
            Toast.show(error, Toast.SHORT);
        }, 400);

    }

    viewProfile(index: any) {
        let doctorsDetail: any = this.state.filteredDoctorsList[index]
        console.log('Doctor Details >>>', doctorsDetail.vender_id);
        this.props.navigation.navigate('DoctorsProfile', { doctorId: doctorsDetail.vender_id, type: this.props.navigation.getParam('type') })
    }

    sendMessage=(index: any)=>{
        AsyncStorage.getItem(LOGIN_DATA).then((res:any) => JSON.parse(res)).then((data:any) => {
           console.log("selected Doctor Detail >>>>>",this.state.filteredDoctorsList[index])
            let chat_id=data.id+""+this.state.filteredDoctorsList[index].vender_id
            let senderDict = {
                firstName: this.state.filteredDoctorsList[index].name,
                lastName: this.state.filteredDoctorsList[index].lastName != undefined ? this.state.filteredDoctorsList[index].lastName: "",
                image: this.state.filteredDoctorsList[index].profile_image,
                // email: this.props.loginInfo.email,
                createdOn: Date.now(),
                id: this.state.filteredDoctorsList[index].vender_id,
                chatId: chat_id
            }
    
            this.props.navigation.navigate('Messages', { chatData: senderDict })
        })

        
    }

    getLanguage() {
        let locale = General.getLocale()

        let language = 1
        if(locale == "en")
        {
            language = 1
        }
        else if(locale == "ar")
        {
            language = 2
        }
        else if(locale == "es")
        {
            language = 3
        }

        return language
    }

    getParams() {
        let params: any = {
            latitude: this.state.lat,
            longitude: this.state.long,
            limit: 10,
            page: this.state.isRefreshing ? 0 : this.state.page,
            distance: 50000,
            language: this.getLanguage()
            //category: this.props.categoryInfo.category.id
        }

        if (this.props.navigation.getParam('type') == 1) {
            params['category'] = this.props.categoryInfo.category.id
        }

        if (this.state.searchText.length > 0) {
            params['search'] = this.state.searchText
        }

        if (this.state.filtersArr[0].isSelected) {
            params['min_price'] = this.state.filtersArr[0].selectedMin
            params['max_price'] = this.state.filtersArr[0].selectedMax
        }

        if (this.state.filtersArr[1].isSelected) {
            params['min_review'] = this.state.filtersArr[1].selectedMin
            params['max_review'] = this.state.filtersArr[1].selectedMax
        }

        if (this.state.filtersArr[2].isSelected) {
            params['min_experience'] = this.state.filtersArr[2].selectedMin
            params['max_experience'] = this.state.filtersArr[2].selectedMax
        }

        if (this.state.filtersArr[3].isSelected) {
            params['avail_min'] = this.state.filtersArr[3].selectedMin
            params['avail_max'] = this.state.filtersArr[3].selectedMax
        }

        return params
    }

    async apiGetDoctorsList(refresh: boolean) {

        let weakSelf = this
        let apiName = ''

        if (this.props.navigation.getParam('type') == 1) {
            apiName = API_DOCTORS_LIST
        }
        else {
            apiName = API_MY_DOCTORS_LIST
        }

        console.log('apiGetDoctorsList func called with params >>>', this.getParams())
        this.setState({ isInternetError: false, isDataNotFoundError: false })
        RequestManager.getRequest(apiName, this.getParams()).then((response: any) => {
            console.log('Response of apiGetDoctorsList API >>>>', response);

            let doctorsArrData: any[] = []
            if (refresh) {
                weakSelf.setState({ page: 0, prevPage: -1, doctorsList: [], filteredDoctorsList: [] })
                doctorsArrData = response.data
            }
            else {
                doctorsArrData = this.state.doctorsList.concat(response.data)
            }

            setTimeout(() => {

                console.log('Prev and next page >>>', this.state.prevPage, this.state.page)

                weakSelf.setState({ showLoader: false, doctorsList: doctorsArrData, filteredDoctorsList: doctorsArrData, loadMore: false, prevPage: this.state.page, isRefreshing: false })

                setTimeout(() => {

                    if (weakSelf.state.filteredDoctorsList.length == 0) {
                        weakSelf.setState({ isInternetError: false, isDataNotFoundError: true })
                    }

                    console.log('Again Prev and next page >>>', this.state.prevPage, this.state.page)
                    weakSelf.setState({ isCalled: false })
                }, 200)

                if (response.data.length > 0) {
                    let nextPage = this.state.page + 1
                    weakSelf.setState({ page: nextPage })
                }
            }, 200)
        })

            .catch((error: any) => {
                this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
                console.log('Error >>>>', error)
                if (error == localize.internetConnectionError) {
                    this.setState({ isInternetError: true, isDataNotFoundError: false })
                }
                else {
                    if (error.message == null || error.message == undefined) {
                        Toast.show((error), Toast.SHORT);
                    }
                    else if (error.message != "No record found" && this.state.filteredDoctorsList.length == 0) {
                        this.setState({ isInternetError: false, isDataNotFoundError: true })
                    }
                    else {
                        Toast.show((error.message), Toast.SHORT);
                    }
                }
            })
    }

    private showFilters() {

        this.setState({ showFilters: !this.state.showFilters })
    }

    private resetBtnAction() {
        this.resetAllFilters()
        this.resetDataAndGetDoctorsList()
    }

    private resetAllFilters() {

        var filters = this.state.filtersArr

        console.log('Filters 1>>>>', this.state.filtersArr)

        if (filters.length == 0) {
            filters = FiltersArr

            console.log('Filters inner>>>>', FiltersArr)
        }


        console.log('Filters >>>>', filters)

        filters[0].isSelected = false
        filters[0].selectedMin = 1
        filters[0].selectedMax = 1000

        filters[1].isSelected = false
        filters[1].selectedMin = 1
        filters[1].selectedMax = 5

        filters[2].isSelected = false
        filters[2].selectedMin = 1
        filters[2].selectedMax = 50

        filters[3].isSelected = false
        filters[3].selectedMin = 1
        filters[3].selectedMax = 24

        this.setState({ filtersArr: filters, showFilters: false, isAnyFilterSelected: false })
    }

    private resetDataAndGetDoctorsList() {

        console.log('Reset Data func called >>>')
        this.setState({ showLoader: true, page: 0, prevPage: -1, doctorsList: [], filteredDoctorsList: [], showFilters: false, searchText: '' })
        setTimeout(() => {
            this.apiGetDoctorsList(false)
        }, 200)
    }

    private filterAction(indx: number) {

        this.setState({ currentlySelctedFilterIndex: indx })
        this.state.filtersArr[indx].isSelected = !this.state.filtersArr[indx].isSelected

        this.checkIfAnyFilterSelcted()
    }

    private checkIfAnyFilterSelcted() {

        let isAnySelected = false
        for (var i = 0; i < this.state.filtersArr.length; i++) {
            let filter = this.state.filtersArr[i]

            if (filter.isSelected) {
                isAnySelected = true
                break
            }
        }

        this.setState({ isAnyFilterSelected: isAnySelected })
    }
    private setMinMaxValue(min: number, max: number) {

        this.state.filtersArr[this.state.currentlySelctedFilterIndex].selectedMin = min
        this.state.filtersArr[this.state.currentlySelctedFilterIndex].selectedMax = max

        this.setState({ filtersArr: this.state.filtersArr })
    }

    private applyFilters() {
        this.resetDataAndGetDoctorsList()
    }

    private refreshData() {

        this.getCurrentLocation()
    }

    //Will call when scroll view pulled down to refresh
    private refreshList() {
        console.log('refreshList func called >>>')
        this.setState({ isRefreshing: true, showFilters: false })
        setTimeout(() => {
            this.apiGetDoctorsList(true)
        }, 200)
    }

    //Will call when reached last cell
    private loadMoreData = () => {

        console.log('Func Clledv >>>', this.state.prevPage, this.state.page)
        if (this.state.prevPage == this.state.page) {
            console.log('Func  return Clled')
            return
        }
        this.setState({ loadMore: true, isCalled: true })
        console.log('Func Clled2')
        setTimeout(() => {
            console.log('Func Clled3')
            this.apiGetDoctorsList(false)
        }, 200)
    }

    private isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }: any) => {
        const paddingToBottom = 50; // how far from the bottom

        // console.log('Layout Measurement >>', layoutMeasurement.height)
        // console.log('contentOffset >>', contentOffset)
        // console.log('contentSize >>', contentSize)
        let value1 = layoutMeasurement.height + contentOffset.y
        let value2 = contentSize.height - paddingToBottom
        let isVAlue2GreaterThanLayout = value2 > layoutMeasurement.height
        let value1GreterthanValue2 = value1 >= value2
        let isClose = (isVAlue2GreaterThanLayout && value1GreterthanValue2)

        // console.log('Value1 >>> ', value1)
        // console.log('Value2 >>> ', value2)
        // console.log('isVAlue2GreaterThanLayout >>', isVAlue2GreaterThanLayout)
        // console.log('value1GreterthanValue2 >>', value1GreterthanValue2)
        // console.log('IS Close >>>', isClose)
        return isClose
    };

    private searchBtnAction() {

        this.setState({ showLoader: true, page: 0, prevPage: -1, doctorsList: [], filteredDoctorsList: [] })
        setTimeout(() => {
            this.apiGetDoctorsList(false)
        }, 200)
    }

    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.blueConatiner}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    {(this.state.filteredDoctorsList.length > 0) ? <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 16, marginRight: 16, marginTop: 16, justifyContent: 'center', }}>
                        <View style={[styles.searchVw, commonShadowStyle]}>
                            <TextInput style={{ flex: 1, marginLeft: 5, padding: 5, fontSize: 14 }} onChangeText={(text) => this.searching(text)} placeholder={localize.searchDoctor} placeholderTextColor={APP_LIGHT_GREY_COLOR}
                                value={this.state.searchText} onEndEditing={() => this.searchBtnAction()} />
                            <TouchableOpacity
                                style={{ height: 30, width: 40, alignItems: 'flex-end', justifyContent: 'center' }}
                                onPress={() => this.searchBtnAction()}>
                                <Image source={this.state.searchText.length > 0 ? require('../../assets/search-icon.png') : require('../../assets/search-icon-light.png')} resizeMode={'contain'} style={{ height: 16, width: 16, marginRight: 4 }} />
                            </TouchableOpacity>
                        </View>
                        {navigation.getParam('type') == 1 ? <TouchableOpacity
                            style={{ height: 40, width: 40, justifyContent: 'center', alignItems: 'flex-end' }}
                            onPress={() => this.showFilters()}>
                            <Image source={require('../../assets/list-icon.png')} resizeMode={'contain'} style={{ height: 20, width: 20 }} />
                        </TouchableOpacity> : null}
                    </View> : null}


                    {navigation.getParam('type') == 1 && this.state.showFilters ? <View style={[styles.filtersVw, commonShadowStyle]}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ marginLeft: 16, marginRight: 16 }}>
                            {this.state.filtersArr.map((item: any, index: number) => (
                                <TouchableOpacity
                                    style={item.isSelected ? styles.appointmentForSelectedBtn : styles.appointmentForBtn}
                                    onPress={() => this.filterAction(index)}>
                                    <Text style={[{ textAlign: 'center', fontWeight: '600' }, (item.selctedValue != undefined ? { color: APP_GREEN_COLOR } : { color: APP_LIGHT_GREY_COLOR })]}>{item.appointmentOption}</Text>
                                </TouchableOpacity>
                            )
                            )}
                        </ScrollView>

                        {this.state.isAnyFilterSelected && this.state.currentlySelctedFilterIndex == 0 ? <AddRangeSlider min={this.state.filtersArr[0].min} max={this.state.filtersArr[0].max} selectedMin={this.state.filtersArr[0].selectedMin} selectedMax={this.state.filtersArr[0].selectedMax} step={this.state.filtersArr[0].step} func={(low: number, high: number) => this.setMinMaxValue(low, high)} /> : null}

                        {this.state.isAnyFilterSelected && this.state.currentlySelctedFilterIndex == 1 ? <AddRangeSlider min={this.state.filtersArr[1].min} max={this.state.filtersArr[1].max} selectedMin={this.state.filtersArr[1].selectedMin} selectedMax={this.state.filtersArr[1].selectedMax} step={this.state.filtersArr[1].step} func={(low: number, high: number) => this.setMinMaxValue(low, high)} /> : null}

                        {this.state.isAnyFilterSelected && this.state.currentlySelctedFilterIndex == 2 ? <AddRangeSlider min={this.state.filtersArr[2].min} max={this.state.filtersArr[2].max} selectedMin={this.state.filtersArr[2].selectedMin} selectedMax={this.state.filtersArr[2].selectedMax} step={this.state.filtersArr[2].step} func={(low: number, high: number) => this.setMinMaxValue(low, high)} /> : null}

                        {this.state.isAnyFilterSelected && this.state.currentlySelctedFilterIndex == 3 ? <AddRangeSlider min={this.state.filtersArr[3].min} max={this.state.filtersArr[3].max} selectedMin={this.state.filtersArr[3].selectedMin} selectedMax={this.state.filtersArr[3].selectedMax} step={this.state.filtersArr[3].step} func={(low: number, high: number) => this.setMinMaxValue(low, high)} /> : null}

                        {this.state.isAnyFilterSelected ?
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20, marginRight: 20 }}>
                                <Text style={styles.minMaxText}>{localize.min}  {this.state.filtersArr[this.state.currentlySelctedFilterIndex].selectedMin}</Text>
                                <Text style={styles.minMaxText}>{this.state.filtersArr[this.state.currentlySelctedFilterIndex].selectedMax}  {localize.max}</Text>
                            </View> : null}
                        {this.state.isAnyFilterSelected ? <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20, marginRight: 20 }}>
                            <CommonBtn title='Reset' func={() => this.resetBtnAction()} width={120} height={30} alignSelf='flex-start' />
                            <CommonBtn title='Apply' func={() => this.applyFilters()} width={120} height={30} alignSelf='flex-end' />
                        </View> : null}
                    </View> : null}
                    {this.state.filteredDoctorsList.length > 0 ?
                        <View style={{ flex: 1 }}>
                            <View style={{ backgroundColor: APP_GREY_BACK, marginTop: 5, flex: 1 }}>
                                <Text style={styles.text}>{navigation.getParam('type') == 1 ? this.props.categoryInfo.category.cat_name : localize.myDoctors}</Text>
                                <FlatList
                                    onRefresh={() => this.refreshList()}
                                    refreshing={this.state.isRefreshing}
                                    style={[{ marginRight: 8, marginLeft: 8 }, (navigation.getParam('type') == 1 ? null : { marginBottom: 8 })]}
                                    data={this.state.filteredDoctorsList}
                                    keyExtractor={(item: any, index: any) => index.toString()}
                                    renderItem={({ item, index }: any) => <DoctorCell
                                        sendMessage={(indx: any) => this.sendMessage(indx)} viewProfileEvent={(indx: any) => this.viewProfile(indx)}
                                        item={item} index={index} type={navigation.getParam('type')} />
                                    }
                                    onEndReachedThreshold={0.5}
                                    onScroll={({ nativeEvent }) => {
                                        if (this.state.isCalled == false && this.isCloseToBottom(nativeEvent) && this.state.isRefreshing == false) {
                                            this.loadMoreData()
                                        }
                                    }}
                                />
                            </View>
                        </View>
                        :
                        (this.state.showLoader || this.state.isRefreshing ? null :
                            (this.state.isDataNotFoundError ?
                                <NoDataFoundView func={() => this.refreshData()} showFilterBtn={this.state.doctorsList.length == 0} /> :
                                (this.state.isInternetError ?
                                    <NoInternetFoundView func={() => this.refreshData()} showFilterBtn={this.state.doctorsList.length == 0} /> : null)))
                    }
                    {/* {navigation.getParam('type') == 1 ? null : (Platform.OS == 'android' ? <BottomShadowView /> : null)} */}

                </View>
            </View>
        );
    }

}

const mapStateToProps = (state: any) => ({
    categoryInfo: state.saveCategoryInfo,
});

export default connect(mapStateToProps)(Doctors);



/// For Future Subscription

{/* {navigation.getParam('type') == 1 ?
<LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#A5C1D6', APP_GREEN_COLOR]} style={styles.linearGradient} >
<Text style={styles.consultTxt}>Why to wait long When you can easily cosult a doctor. Click on the Consult now for your convenience</Text>
<TouchableOpacity
    style={styles.consultNowBtn}
    onPress={() => this.consultNow()}>
    <Text style={{ color: 'white', textAlign: 'center' }}>Consult Now</Text>
</TouchableOpacity>
</LinearGradient> : null} */}
