//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        //padding: 16,
        // backgroundColor: 'green'
    },
    searchVw: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 20,
        padding: 5,
        borderRadius: 10,
        //marginTop: 10,
        backgroundColor: 'white',
    },
    text: {
        fontSize: 16,
        fontWeight: "400",
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        margin: 16
    },
    linearGradient: {
        width: '100%',
        padding: 8,
    },
    consultTxt: {
        color: 'white'
    },
    consultNowBtn: {
        width: 120,
        height: 30,
        borderColor: 'white',
        borderRadius: 15,
        borderWidth: 2,
        justifyContent: 'center',
        marginRight: 8,
        alignSelf: 'flex-end',
        marginTop: 8,
        marginBottom: 8
    },
    appointmentForSelectedBtn: {
        width: 80,
        height: 30,
        backgroundColor: 'white',
        borderRadius: 15,
        borderColor: APP_GREEN_COLOR,
        borderWidth: 1,
        justifyContent: 'center',
        margin: 8
    },
    appointmentForBtn: {
        width: 80,
        height: 30,
        backgroundColor: 'white',
        borderRadius: 15,
        borderColor: APP_LIGHT_GREY_COLOR,
        borderWidth: 1,
        justifyContent: 'center',
        margin: 8
    },
    filtersVw: {
        backgroundColor: APP_GREY_BACK,
        marginTop: 16,
    },
    minMaxText: {
        fontSize: 12,
        fontWeight: "500",
        color: APP_LIGHT_GREY_COLOR,
        textAlign: 'left',
        marginBottom: 8,
    },

});