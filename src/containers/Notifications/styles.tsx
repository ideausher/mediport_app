//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREEN_COLOR} from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,        
        backgroundColor: APP_GREY_BACK,
    },
    searchVw: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 20,
        padding: 5,
        borderColor: '#C0C0C0',
        borderWidth: 1,
        borderRadius: 10,
        marginTop:18,
        margin:8
    },
    text: {
        fontSize: 16,
        fontWeight: "500",
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        margin:8
      },
      linearGradient:{
          width:'100%',
          padding: 8,

      },
      consultTxt:{
        color:'white'
      },
      consultNowBtn:{
        width: 120,
        height: 30,
        borderColor: 'white',
        borderRadius:15,
        borderWidth:2,
        justifyContent: 'center',
        marginRight:8,
        alignSelf:'flex-end',
        marginTop: 8,
        marginBottom: 8
    },
});