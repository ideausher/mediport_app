import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, FlatList, Modal, BackHandler } from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_GREEN_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle } from '../../Constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import NotificationCell from '../../Custom Cells/NotificationCell';
import LinearGradient from 'react-native-linear-gradient';
import { localize } from '../../Utils/LocalisedManager';
import { BackHeaderBtn } from '../../Custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager';
import { API_NOTIFICATIONS_LIST } from '../../Utils/APIs';
import General from '../../Utils/General';
import { Loader } from '../../Utils/Loader';
import moment from 'moment';
import {notifiationsArr} from '../../Handlers/NotificationsHandler';

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class Notifications extends Component<props, object> {

    state = {
        page: 0,
        isRefreshing: false,
        loadMore: false,
        isCalled: false,
        prevPage: -1,
        isInternetError: false,
        isDataNotFoundError: false,
        notificatiosnArr: [] as any[],
        showLoader: false
    }
    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{localize.notifications}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        this.setState({ showLoader: true })
        this.apiGetNotificationsList(false)
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    // Get paarmeters 
    private getParams(refresh: boolean) {

        return {
            "page": refresh ? 0 : this.state.page,
            "limit": 10,
            "is_read": 3
        }
    }

    async apiGetNotificationsList(refresh: boolean) {
        let weakSelf = this
        RequestManager.getRequest(API_NOTIFICATIONS_LIST, this.getParams(refresh)).then((response: any) => {
            console.log('REsponse of API_NOTIFICATIONS_LIST API >>>>', response);

            notifiationsArr(response.data).then((arr: any) => {

                console.log('Notifications Arr>>>>', arr);
                let notificationsArrData: any[] = []
                if (refresh) {
                    weakSelf.setState({ page: 0, prevPage: -1, notificatiosnArr: [] })
                    notificationsArrData = arr
                }
                else {
                    notificationsArrData = this.state.notificatiosnArr.concat(arr)
                }

                setTimeout(() => {
                    let sortedNotifications = notificationsArrData.sort((a: any, b: any) => this.sortNotifications(a, b));
                    weakSelf.setState({ showLoader: false, notificatiosnArr: sortedNotifications, loadMore: false, prevPage: this.state.page, isRefreshing: false })

                    setTimeout(() => {

                        if (weakSelf.state.notificatiosnArr.length == 0) {
                            weakSelf.setState({ isInternetError: false, isDataNotFoundError: true })
                        }
                        weakSelf.setState({ isCalled: false })
                    }, 200)

                    if (arr.length > 0) {
                        let nextPage = this.state.page + 1
                        weakSelf.setState({ page: nextPage })
                    }
                }, 200)
            })
        }).catch((error: any) => {
            this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
            General.showErroMsg(this, error)
        })
    }

    private sortNotifications(a: any, b: any) {
        let first: any = moment(a.created_at, "YYYY_MM_DD HH:mm:ss")
        let second: any = moment(b.created_at, "YYYY_MM_DD HH:mm:ss")

        return second - first
    }

    //Will call when scroll view pulled down to refresh
    private refreshList() {
        this.setState({ isRefreshing: true })
        setTimeout(() => {
            this.apiGetNotificationsList(true)
        }, 200)
    }

    //Will call when reached last cell
    private loadMoreData = () => {
        if (this.state.prevPage == this.state.page) {
            return
        }
        this.setState({ loadMore: true, isCalled: true })
        setTimeout(() => {
            this.apiGetNotificationsList(false)
        }, 200)
    }

    private isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }: any) => {
        const paddingToBottom = 50; // how far from the bottom

        let value1 = layoutMeasurement.height + contentOffset.y
        let value2 = contentSize.height - paddingToBottom
        let isVAlue2GreaterThanLayout = value2 > layoutMeasurement.height
        let value1GreterthanValue2 = value1 >= value2
        let isClose = (isVAlue2GreaterThanLayout && value1GreterthanValue2)

        return isClose
    };

    render() {
        return (
            <View style={styles.blueConatiner}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    <FlatList style={{ marginTop: 16, }}
                        onRefresh={() => this.refreshList()}
                        refreshing={this.state.isRefreshing}
                        data={this.state.notificatiosnArr}
                        keyExtractor={(item: any, index: any) => index.toString()}
                        renderItem={({ item }: any) => <NotificationCell item={item} />}
                        onEndReachedThreshold={0.5}
                        onScroll={({ nativeEvent }) => {
                            if (this.state.isCalled == false && this.isCloseToBottom(nativeEvent) && this.state.isRefreshing == false) {
                                this.loadMoreData()
                            }
                        }}
                    />
                </View>
            </View>
        );
    }
}