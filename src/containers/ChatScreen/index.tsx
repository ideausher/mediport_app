import React, { Component } from 'react';
import { NavigationScreenProp } from 'react-navigation';
import { View, Text, TouchableOpacity, Image, requireNativeComponent, ViewPropTypes, Platform, PermissionsAndroid } from 'react-native';
import { BackHeaderBtn, OpenDrawerHeaderBtn, NotificationsHeaderBtn, BottomShadowView } from '../../Custom/CustomComponents';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../Constants';
import LocalDataManager from '../../Utils/LocalDataManager';
import { WebView } from 'react-native-webview';
import PermissionWebview from '../PermissionWebView'
//const WebViewAndroid = require('react-native-webview-rtc');
import { localize } from '../../Utils/LocalisedManager';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import Toast from 'react-native-simple-toast';
import RequestManager from '../../Utils/RequestManager'
import { API_CHAT_GET_URL } from '../../Utils/APIs';
import General from '../../Utils/General';


export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class ChatScreen extends Component<props, object> {
    state = {
        URL: ""
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View >
                    <Text style={navHdrTxtStyle}>{'Doctor name'}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler(), 'audioCallBtnHandler': () => this.audioCallBtnHandler(), 'videoCallBtnHandler': () => this.videoCallBtnHandler() });

        // this.isMicrophonePermissionGranted()
        // this.isCameraPermissionGranted()

        LocalDataManager.getDataAsyncStorage('videoCallInfo').then((data: any) => {
            console.log('videoCallInfo Then >>>>', data)
            if (data != null && data != "") {
                console.log('videoCallInfo if called >>>>', data.room)
                this.isMicrophonePermissionGranted()
                this.isCameraPermissionGranted()
                this.setState({ URL: data.room })
                LocalDataManager.saveDataAsyncStorage('videoCallInfo', "")

            } else {
                console.log('videoCallInfo else  called >>>>')
                this.apiVideoChatURL()
            }
        }).catch((error) => {
            console.log('videoCallInfo catch >>>>', error)
            this.apiVideoChatURL()
        })

    }


    getApiParams() {
        return {
            user_id: this.props.navigation.getParam('vendorId')
            // user_id: 9
        }
    }
    async apiVideoChatURL() {
         let weakSelf = this
        //console.log("start >> Vendor ID >>", this.props.navigation.getParam('vendorId'));

        this.setState({ isInternetError: false, isDataNotFoundError: false })
        RequestManager.getRequest(API_CHAT_GET_URL, this.getApiParams()).then((response: any) => {

            console.log("start >>URLLLLLLLLLL then >>", response);
            this.setState({ URL: response.chatUrl })
            this.isMicrophonePermissionGranted()
            this.isCameraPermissionGranted()

        }).catch((error: any) => {
            console.log("start >>URLLLLLLLLLL error >>", error);
            this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
            General.showErroMsg(this, error)
        })
        // let weakSelf = this
        // console.log("start >> URLLLLLLLLLLLLL>>> ", API_CHAT_GET_URL + "user_id=" + this.props.navigation.getParam('vendorId'));

        // fetch(API_CHAT_GET_URL + "user_id=" + this.props.navigation.getParam('vendorId'), {
        //     method: "get",


        // })
        //     .then(function (response: any) {
        //         console.log("start >>URLLLLLLLLLL then >>", response);
        //         weakSelf.setState({ URL: response.chatUrl })
        //         weakSelf.isMicrophonePermissionGranted()
        //         weakSelf.isCameraPermissionGranted()
        //     }).catch((error) => {
        //         console.log("start >>URLLLLLLLLLL error >>", error);
        //         weakSelf.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
        //         General.showErroMsg(this, error)
        //     })
    }


    private backBtnHandler() {
        this.props.navigation.goBack()
    }

    async isMicrophonePermissionGranted() {
        if (Platform.OS === "ios") {
            this.checkForiOSMicrophonePermissions()
        } else {
            this.checkAndroidMicrophonePermission();
        }
    }

    async checkAndroidMicrophonePermission() {
        console.log('Android Permissioin Func called')
        await check(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO)
            .then(result => {
                console.log('Before Switch', result)
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log('Microphone Permissioin UNAVAILABLE >>', result)
                        break;
                    case RESULTS.DENIED:
                        console.log('Microphone Permissioin Denied')
                        this.requestAndroidMicrophonePermission()
                        break;
                    case RESULTS.GRANTED:
                        console.log('Microphone Permissioin Granted')
                        break;
                    case RESULTS.BLOCKED:
                        console.log('Microphone Permissioin BLOCKED')
                        break;
                    default:
                        console.log('Microphone Permissioin Default >>>', result)

                }
            })
            .catch(error => {
                console.log('Microphone Permissioin Catch called')
                console.warn(error);
            });
    }

    checkForiOSMicrophonePermissions() {
        console.log('ios Permissioin Func called')
        check(PERMISSIONS.IOS.MICROPHONE)
            .then(result => {
                console.log('Before Switch', result)
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log('Microphone Permissioin UNAVAILABLE >>', result)
                        break;
                    case RESULTS.DENIED:
                        console.log(
                            'The permission has not been requested / is denied but requestable',
                            request(PERMISSIONS.IOS.MICROPHONE).then(result => {
                                console.log('Results >>>', result);
                            })
                        );
                        break;
                    case RESULTS.GRANTED:
                        console.log('Microphone Permissioin Granted')
                        break;
                    case RESULTS.BLOCKED:
                        console.log('Microphone Permissioin BLOCKED')
                        break;
                    default:
                        console.log('Microphone Permissioin Default >>>', result)
                }
            })
            .catch(error => {
                console.log('Microphone Permissioin Catch called')
                console.warn(error);
            });
    }

    async isCameraPermissionGranted() {
        if (Platform.OS === "ios") {
            this.checkForiOSCameraPermissions()
        } else {
            this.checkAndroidCameraPermission();
        }
    }

    async checkAndroidCameraPermission() {
        console.log('Android CAMERA Permissioin Func called')
        await check(PermissionsAndroid.PERMISSIONS.CAMERA)
            .then(result => {
                console.log('CAMERA Before Switch', result)
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log('CAMERA Permissioin UNAVAILABLE >>', result)
                        break;
                    case RESULTS.DENIED:
                        console.log('CAMERA Permissioin Denied')
                        this.requestAndroidCameraPermission()
                        break;
                    case RESULTS.GRANTED:
                        console.log('CAMERA Permissioin Granted')
                        break;
                    case RESULTS.BLOCKED:
                        console.log('CAMERA Permissioin BLOCKED')
                        break;
                    default:
                        console.log('CAMERA Permissioin Default >>>', result)

                }
            })
            .catch(error => {
                console.log('CAMERA Permissioin Catch called')
                console.warn(error);
            });
    }

    checkForiOSCameraPermissions() {
        console.log('ios CAMERA Permissioin Func called')
        check(PERMISSIONS.IOS.CAMERA)
            .then(result => {
                console.log('Before Switch', result)
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log('CAMERA Permissioin UNAVAILABLE >>', result)
                        break;
                    case RESULTS.DENIED:
                        console.log(
                            'The permission has not been requested / is denied but requestable',
                            request(PERMISSIONS.IOS.CAMERA).then(result => {
                                console.log('Results >>>', result);
                            })
                        );
                        break;
                    case RESULTS.GRANTED:
                        console.log('CAMERA Permissioin Granted')
                        break;
                    case RESULTS.BLOCKED:
                        console.log('CAMERA Permissioin BLOCKED')
                        break;
                    default:
                        console.log('CAMERA Permissioin Default >>>', result)
                }
            })
            .catch(error => {
                console.log('CAMERA Permissioin Catch called')
                console.warn(error);
            });
    }

    async requestAndroidMicrophonePermission() {

        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                {
                    title: 'Mediport',
                    message: String('Mediport needs access to Microphone'),
                    buttonNeutral: localize.askMeLater,
                    buttonNegative: localize.cancel,
                    buttonPositive: localize.ok,
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the location');
                // Toast.show('You can use the location', Toast.SHORT);
            } else {
                console.log('location permission denied');
                // Toast.show('location permission denied', Toast.SHORT);
            }
        } catch (err) {
            console.warn(err);
        }
    }

    async requestAndroidCameraPermission() {

        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: 'Mediport',
                    message: String('Mediport needs access to CAMERA'),
                    buttonNeutral: localize.askMeLater,
                    buttonNegative: localize.cancel,
                    buttonPositive: localize.ok,
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('CAMERA permissiom Granted');
                // Toast.show('You can use the location', Toast.SHORT);
            } else {
                console.log('CAMERA permission denied');
                // Toast.show('location permission denied', Toast.SHORT);
            }
        } catch (err) {
            console.warn(err);
        }
    }

    audioCallBtnHandler() {

    }

    videoCallBtnHandler() {

    }

    // getURL() {

    //     LocalDataManager.getLoginInfo().then((data: any) => {

    //         let vendorId = this.props.navigation.getParam('vendorId')

    //         let url = 'https://pt.iphoneapps.co.in:3002/demos/dashboard/canvas-designer.html?open=' + true + '&sessionid=' + data.id + vendorId + '&publicRoomIdentifier=dashboard&userFullName=testing&sender_id=' + data.id + '&receiver_id=' + vendorId

    //         console.log('url in getURL >>>>', url)

    //         return url
    //     })
    // }

    // onLoadStart() {
    //     console.log('On Load Start Called >>>')

    //     // this.webview.setWebChromeClient(new WebChromeClient(){
    //     //     // Need to accept permissions to use the camera
    //     //     @Override
    //     //     public void onPermissionRequest(final PermissionRequest request) {
    //     //         request.grant(request.getResources());
    //     //     }
    //     // });
    // }

    // javascriptToInject() {
    //     return `
    //       $(document).ready(function() {
    //         $('a').click(function(event) {
    //           if ($(this).attr('href')) {
    //             var href = $(this).attr('href');
    //             window.webView.postMessage('Link tapped: ' + href);
    //           }
    //         })
    //       })
    //     `
    // }

    // onShouldStartLoadWithRequest(event: any) {

    //     console.log('App.js -> onShouldStartLoadWithRequest () ...');
    //     // currently only url & navigationState are returned

    //     console.log('event.url = ', event.url);
    //     console.log('navigatinalstate = ', event.navigationState);

    //     if (event.url.indexOf({ URL_DOMAIN } >= 0)) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    // onNavigationStateChange = (event: any) => {
    //     console.log('App.js -> onNavigationStateChange () ...');
    //     console.log(event);
    //     this.setState({
    //         forwardButtonEnabled: event.canGoForward,
    //         backButtonEnabled: event.canGoBack,
    //         loading: event.loading,
    //         url: event.url,
    //         status: event.title
    //     });
    // }

    // onMessage(event: any) {
    //     console.log('App.js -> onMessage () ...');
    //     this.setState({
    //         messageFromWebView: event.message
    //     });
    // }

    render() {

         console.log('url111111 hit in render>>>>', this.state.URL)
        return (
            // <WebView
            //     // useWebKit={true}
            //     ref={(webview: any) => { this.webview = webview }}
            //     javaScriptEnabled={true}
            //     source={{ uri: 'https://pt.iphoneapps.co.in:3002/demos/dashboard/canvas-designer.html?open=true&sessionid=5669&publicRoomIdentifier=dashboard&userFullName=testing&sender_id=56&receiver_id=69' }}
            //     onMessage={(val: any) => console.log('On MEssage Called >>>>', val)}
            //     onNavigationStateChange={(state: any) => console.log('Navigation State Changed >>>>', state)}
            //     onLoadEnd={(val: any) => console.log('On Load End Called >>>>', val)}
            //     onLoadStart={(val: any) => this.onLoadStart()}
            //     onError={(error: any) => console.log('On Error Called >>>', error)}
            //     domStorageEnabled={true}
            //     startInLoadingState={true}
            //     mixedContentMode={'compatibility'}
            //     mediaPlaybackRequiresUserAction={true}
            //     allowUniversalAccessFromFileURLs={true}
            //     //renderLoading={this.renderLoading.bind(this)}
            //     //scalesPageToFit={true}

            //     // ref={(webview: any) => { this.webview = webview }}
            //     // originWhitelist={['*']}
            //     // domStorageEnabled={true}
            //     // startInLoadingState={true}
            //     // allowUniversalAccessFromFileURLs={true}
            //     // javaScriptEnabled={true}
            //     // useWebKit={true}
            //     // onNavigationStateChange={(state: any) => console.log('Navigation State Changed >>>>', state)}
            //     // onLoadEnd={(val: any) => console.log('On Load End Called >>>>', val)}
            //     // onLoadStart={(val: any) => this.onLoadStart()}
            //     // onError={(error: any) => console.log('On Error Called >>>', error)}
            //     // source={{ uri: 'https://pt.iphoneapps.co.in:3002/demos/dashboard/canvas-designer.html?open=true&sessionid=5669&publicRoomIdentifier=dashboard&userFullName=testing&sender_id=56&receiver_id=69' }}
            //     // style={{ backgroundColor: 'red' }}

            // />

            // <WebView {...this.props} nativeConfig={{component: RCTCustomWebView}} 
            // source={{ uri: 'https://pt.iphoneapps.co.in:3002/demos/dashboard/canvas-designer.html?open=true&sessionid=5669&publicRoomIdentifier=dashboard&userFullName=testing&sender_id=56&receiver_id=69' }}
            // />
            //     <WebView
            //     {...this.props}
            //     nativeConfig={{
            //       component: RCTCustomWebView,
            //       props: {
            //         finalUrl: this.props.finalUrl,
            //         onNavigationCompleted: this._onNavigationCompleted,
            //       },
            //     }}
            //   />

            // <WebViewAndroid
            //     ref="webViewAndroidSample"
            //     javaScriptEnabled={true}
            //     javaScriptEnabledAndroid={true}
            //     geolocationEnabled={false}
            //     builtInZoomControls={false}
            //     mediaPlayUserGesture={false}
            //     injectedJavaScript={this.javascriptToInject()}
            //     onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest}
            //     onNavigationStateChange={this.onNavigationStateChange}
            //     onMessage={this.onMessage}
            //     url={'https://pt.iphoneapps.co.in:3002/demos/dashboard/canvas-designer.html?open=true&sessionid=5675&publicRoomIdentifier=dashboard&userFullName=testing&sender_id=56&receiver_id=75'}
            //     style={{ flex: 1 }}
            // />
            // <View style={{ flex: 1, backgroundColor: 'green' }}>
            <WebView

                style={{ flex: 1,  padding: 19 }}
                // useWebKit={true}
                // originWhitelist={['*']}
                mediaPlaybackRequiresUserAction={true}
                // domStorageEnabled={true}
                // allowsInlineMediaPlayback={true}
                // startInLoadingState={true}
                // onMessage={(val: any) => console.log('On MEssage Called >>>>', val)}
                // onNavigationStateChange={(state: any) => console.log('Navigation State Changed >>>>', state)}
                // onLoadEnd={(val: any) => console.log('On Load End Called >>>>', val)}
                // onLoadStart={(val: any) => console.log('Load Staretd >>>', val)}
                //  onError={(error: any) => console.log('On Error Called >>>', error)}
                // source={{ uri: 'https://pt.iphoneapps.co.in:3002/demos/dashboard/canvas-designer.html?open=true&sessionid=5676&publicRoomIdentifier=dashboard&userFullName=testing&sender_id=56&receiver_id=76&audioconfig=true&videoconfig=true' }}
                source={{ uri: this.state.URL }}
                allowFileAccessFromFileURLs={true}
                allowUniversalAccessFromFileURLs={true}
                mixedContentMode='always'
                cacheEnabled={false}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                nativeConfig={{ props: { webContentsDebuggingEnabled: true } }}
                allowsFullscreenVideo={true}
            />
            // </View>

            // <CustomWebView
            //     {...this.props}
            //     source={{ uri: 'https://pt.iphoneapps.co.in:3002/demos/dashboard/canvas-designer.html?open=true&sessionid=5669&publicRoomIdentifier=dashboard&userFullName=testing&sender_id=56&receiver_id=69' }}

            //   />
        )
    }
}



