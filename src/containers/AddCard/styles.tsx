//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_HEADER_TITLE_COLOR, APP_GREY_TEXT_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, App_MIDIUM_GREY_COLOR } from '../../Constants'
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: APP_HEADER_BACK_COLOR
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: APP_GREY_BACK,
        padding: 16,
    },
    darkTxt: {
        textAlign: 'left',
        fontSize: 18,
        color: APP_GREY_TEXT_COLOR,
        fontWeight: 'bold',
        marginTop: 20,
    },
    lightTxt: {
        textAlign: 'left',
        color: App_MIDIUM_GREY_COLOR,
        marginTop: 20,
    },
    selectedBtn: {
        width: '48%',
        height: 40,
        backgroundColor: 'white',
        borderRadius: 5,
        borderColor: APP_HEADER_BACK_COLOR,
        borderWidth: 0.2,
        justifyContent: 'center'
    },
    unselectedBtn: {
        width: '48%',
        height: 40,
        backgroundColor: 'white',
        borderRadius: 5,
        borderColor: APP_HEADER_BACK_COLOR,
        borderWidth: 0.2,
        justifyContent: 'center'
    },
    nextBtn: {
        width: 200,
        height: 40,
        backgroundColor: APP_GREEN_COLOR,
        borderRadius: 20,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 30
    },
    img: {
        height: 20,
        width: 20,
        resizeMode: 'contain'
    },
});