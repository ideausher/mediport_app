import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, FlatList, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, Keyboard, Alert, Modal } from 'react-native';
import { APP_HEADER_BACK_COLOR, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, APP_GREY_TEXT_COLOR, App_MIDIUM_GREY_COLOR, navHdrTxtStyle, headerTitleStyle, headerStyle, commonShadowStyle } from '../../Constants';
import { NavigationScreenProp, ScrollView } from 'react-navigation';
import { styles } from './styles';
import DoctorCell from '../../Custom Cells/DoctorCell';
import LinearGradient from 'react-native-linear-gradient';
import { LineVw, BackHeaderBtn } from '../../Custom/CustomComponents';
import { TextInputWithoutImageCustomWidthHeight, TextInputWithoutImage, TextInputWithImage } from '../../Custom/CustomTextInput';
import { localize } from '../../Utils/LocalisedManager';
import { VerificationFor } from '../../Utils/Enums';
import PaymentManager from '../../Utils/PaymentManager';
//@ts-ignore
import { Stripe } from 'react-native-stripe-api';
import { CheckBox } from 'react-native-elements'
//@ts-ignore
import { connect } from 'react-redux';
import { API_APPOINTMENT_LIST, API_VALIDATE_COUPENS, API_UPLOAD_PDF, API_UPLOAD_IMAGE } from '../../Utils/APIs';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import { GreenTextBtn, CommonBtn } from '../../Custom/CustomButton';
import { Loader } from '../../Utils/Loader';
//@ts-ignore
import ToggleSwitch from 'toggle-switch-react-native';
import Toast from 'react-native-simple-toast';

const { StatusBarManager } = NativeModules;

const apiKey = 'pk_test_pku2v8tppoSjIx8zYldEZalN';

// const stripe = require('stripe')(apiKey);

export interface props {
    navigation: NavigationScreenProp<any, any>,
    doctorInfo: any,
    bookingInfo: any,
    categoryInfo: any,
};

class AddCard extends Component<props, object> {

    state = {
        isCreditCard: true,
        amount: '',
        cardHolderName: '',
        cardNo: '4242 4242 4242 4242',
        expiryMonth: '06',
        expiryYear: '20',
        cvv: '256',
        statusBarHeight: 0,
        isCardSaved: false,
        showLoader: false,
        coupenCode: '',
        discountedPrice: 0,
        stripeToken: ''
    }

    uploadedImagesArr = [] as String[]


    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <View style={{ paddingStart: 10 }}>
                    <Text style={navHdrTxtStyle}>{localize.addCard}</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    creditCardSelected() {
        this.setState({ isCreditCard: true })
    }

    debitCardSelected() {
        this.setState({ isCreditCard: false })
    }

    validateData() {
        // if (this.state.amount.length == 0) {
        //     Alert.alert('Please enter amount.');
        //     return false
        // }
        if (this.state.cardHolderName.length == 0) {
            Toast.show(localize.pleaseEnterName, Toast.SHORT);
            return false
        }
        else if (this.state.cardNo.length == 0) {
            Toast.show(localize.pleaseEnterCardNum, Toast.SHORT);
            return
        }
        else if (this.state.expiryMonth.length == 0) {
            Toast.show(localize.pleaseEnterExpMonth, Toast.SHORT);
            return
        }
        else if (this.state.expiryYear.length == 0) {
            Toast.show(localize.pleaseEnterExpYear, Toast.SHORT);
            return
        }
        else if (this.state.cvv.length == 0) {
            Toast.show(localize.pleaseEnterCVV, Toast.SHORT);
            return
        }

        return true
    }

    async confirmBtnAction() {
        // this.props.navigation.navigate('Verification', { verificationFor: VerificationFor.payment })

        if (this.validateData()) {
            let cardNum = this.state.cardNo.replace(/ /g, '')

            var stripe = require('stripe-client')('pk_test_gVTWFUkRg5xoLaNQnCfgY3Zf00Sdn1jBwb'); //pk_test_pku2v8tppoSjIx8zYldEZalN
            const token = await stripe.createToken({
                card: {
                    "number": cardNum,
                    "exp_month": this.state.expiryMonth,
                    "exp_year": this.state.expiryYear,
                    "cvc": this.state.cvv
                }
            })

            // const token = PaymentManager.setStripeKey(cardNum, this.state.expiryMonth, this.state.expiryYear, this.state.cvv)
            console.log('Stripe Token Genrated >>>>', token);
            this.setState({ stripeToken: token })
            this.callAPI()
        }
    }

    // Get paarmeters for APPOINTMENT BOOK API
    private getParams(token: any, report_ids: String) {
        console.log('Category Info >>>', this.props.categoryInfo.category)

        let dict: any = {
            "vender_id": this.props.doctorInfo.id,
            "service_id": this.props.doctorInfo.serviceId,
            "vendor_slot_id": this.props.bookingInfo.slotId,
            "price": (this.state.discountedPrice > 0 ? this.state.discountedPrice : this.props.doctorInfo.price),
            "booking_date": this.props.bookingInfo.bookingDate,
            "card_source_id": token.id,
            "card_id": token.card.id,
            "appointment_for": this.props.bookingInfo.appointmentFor,
            "age": this.props.bookingInfo.age,
            "blood_group": this.props.bookingInfo.bloodGroup,
            "contact_number": this.props.bookingInfo.contactNumber,
            "description": this.props.bookingInfo.description,
            "patient_name": this.props.bookingInfo.patientName,
            "card_save": this.state.isCardSaved ? 1 : 0,
            "gender": this.props.bookingInfo.gender,
            "currency": "usd",
            "original_amount": this.props.doctorInfo.price,
            "coupon_code": (this.state.discountedPrice > 0 ? this.state.coupenCode.trim() : "")
        }

        if (report_ids != "") {
            dict["report_ids"] = report_ids
        }
        return dict
    }

    private uploadImageParams(image: any) {

        const formData = new FormData();
        // formData.append('profile_image', this.state.avatarSource.data); 

        formData.append("file", {
            name: image.name,
            type: image.type,
            uri:
                Platform.OS === "android" ? image.uri : image.uri.replace("file://", "")
        });

        return formData
    }

    // API tp send OTp on added Phone Number for verification
    apiUploadImage(image: any, index: number) {
        console.log('Upload Image func called with Image >>>>', image)
        console.log('Upload Image func called with params>>>>', this.uploadImageParams(image))
        RequestManager.uploadImage(API_UPLOAD_PDF, this.uploadImageParams(image)).then((response: any) => {
            console.log('REsponse of apiUploadImage >>>>', response);
            console.log('Image Str in response  >>>>', response.data.id);
            console.log('Index >>>>', index);

            this.uploadedImagesArr.push(response.data.id)

            // Means All Images Uploaded if Equal
            if (this.uploadedImagesArr.length == this.props.bookingInfo.reportIds.length) {
                let imageStr = this.uploadedImagesArr.join(',')
                console.log('IMAGESTR >>>', imageStr)

                this.callBookAppointmentAPI(imageStr)
            }

        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }


    private async callAPI() {

        console.log('Call API Func Called', this.props.bookingInfo)

        // Upload PDF if path exits means
        if (this.props.bookingInfo.reportIds != '') {
            this.setState({ showLoader: true })

            {
                this.props.bookingInfo.reportIds.map((item: any, index: number) => (
                    this.apiUploadImage(item, index)
                ))
            }
        }
        else {
            this.callBookAppointmentAPI('')
        }
    }

    private async callBookAppointmentAPI(reportIds: String) {
        console.log('Stripe Token >>>>>', this.state.stripeToken)
        console.log('PArams API_APPOINTMENT_LIST >>>>', this.getParams(this.state.stripeToken, reportIds))
        this.setState({ showLoader: true })
        let weakSelf = this
        RequestManager.postRequestWithHeaders(API_APPOINTMENT_LIST, this.getParams(this.state.stripeToken, reportIds)).then((response: any) => {

            console.log('REsponse of Book APPOINTMENT API >>>>', response);

            setTimeout(function () {
                weakSelf.setState({ showLoader: false })
            }, 200);
            weakSelf.props.navigation.navigate('BookingSuccess', { refId: response.data.reference_id })
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    private cardNumChanged(value: string) {

        console.log('Card Value >>>>', value);

        this.setState({
            cardNo: value.replace(/\s?/g, '').replace(/(\d{4})/g, '$1 ').trim()
        });

        if (value.length == 19) {
            Keyboard.dismiss()
        }
    }

    applyDiscountAction() {

        this.props.navigation.navigate('Discount', {
            onGoBack: (code: string, price: any) => this.refresh(code, price)
        });
    }

    private refresh(code: string, price: any) {
        console.log('refresh Btn Called >>>', code)
        this.setState({ coupenCode: code, discountedPrice: price })
    }

    // Get paarmeters for VALIDATE COUPEN API
    private getValidateCoupenAPIParams() {

        return {
            "code": this.state.coupenCode,
            "price": this.props.doctorInfo.price
        }
    }

    private validateCoupen() {
        console.log('Coupen after Editing >>>', this, this.state.coupenCode)

        if (this.state.coupenCode.trim().length == 0) {
            return;
        }

        console.log('PArams validate Coupen >>>>', this.getValidateCoupenAPIParams())
        this.setState({ showLoader: true })
        let weakSelf = this
        RequestManager.postRequestWithHeaders(API_VALIDATE_COUPENS, weakSelf.getValidateCoupenAPIParams()).then((response: any) => {

            console.log('REsponse of API_VALID_COUPENS API >>>>', response);
            console.log('REsponse Discounted Price >>>>', response.data.price);

            weakSelf.setState({ showLoader: false, discountedPrice: response.data.price })
        }).catch((error: any) => {
            weakSelf.setState({ discountedPrice: 0 })
            General.showErroMsg(this, error)
        })
    }

    // Instances of Textinput
    txtCardNum: TextInput;
    txtExpiryMonth: TextInput;
    txtExpiryYear: TextInput;
    txtCvv: TextInput;

    render() {

        let actualPrice = this.props.doctorInfo.price.toString()

        if (this.state.discountedPrice > 0) {
            actualPrice = this.state.discountedPrice.toString()
        }

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>

                <View style={styles.blueConatiner}>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showLoader}>
                        <Loader />
                    </Modal>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                            <Text style={styles.darkTxt}>{localize.selectyourCard}</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 16 }}>
                                <TouchableOpacity
                                    style={this.state.isCreditCard ? [styles.selectedBtn, commonShadowStyle] : styles.unselectedBtn}
                                    onPress={() => this.creditCardSelected()}>
                                    <Text style={{ textAlign: 'center', color: App_MIDIUM_GREY_COLOR }}>{localize.crditCard}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    
                                    style={this.state.isCreditCard ? styles.unselectedBtn : [styles.selectedBtn, commonShadowStyle, {opacity: 0.1}]}
                                    onPress={() => this.debitCardSelected()}>
                                    <Text style={{ textAlign: 'center', color: App_MIDIUM_GREY_COLOR }}>{localize.debitCard}</Text>
                                </TouchableOpacity>
                            </View>

                            {/* <Text style={styles.darkTxt}>{localize.amount}</Text>
                            <TextInputWithoutImageCustomWidthHeight placeholder={localize.amount} onChange={(text: any) => { }} value={actualPrice} width='100%' height={60} keyboardType='numeric' returnKeyType='done' editable={false} /> */}

                            <Text style={[styles.darkTxt, { marginTop: 50 }]}>{localize.cardDetails}</Text>

                            <Text style={styles.lightTxt}>{localize.cardHolderName}</Text>
                            <TextInputWithoutImageCustomWidthHeight placeholder='Jackson Murinondo' onChange={(text: any) => this.setState({ cardHolderName: text })} value={this.state.cardHolderName} width='100%' />

                            <Text style={styles.lightTxt}>{localize.cardNumber}</Text>

                            <TextInputWithImage ref={(input: any) => { this.txtCardNum = input as any }} placeholder='xxxx xxxx xxxx xxxx' onChange={(text: any) => { this.setState({ cardNo: text.replace(/\s?/g, '').replace(/(\d{4})/g, '$1 ').trim() }), ((text.length == 19) ? Keyboard.dismiss() : null) }} value={this.state.cardNo} icon={require('../../assets/mastercard.png')} rightImage={true} keyboardType='numeric' returnKeyType='done' maxLength={19} />

                            {/* <TextInputWithoutImageCustomWidthHeight placeholder='xxxx-xxxx-xxxx-xxxx' onChange={(text: any) => this.setState({ cardNo: text })} value={this.state.cardNo} width='100%' /> */}

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={styles.lightTxt}>{localize.expirationDate}</Text>
                                <Text style={styles.lightTxt}>{localize.cvv}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ flexDirection: 'row', width: '50%', justifyContent: 'space-between' }}>
                                    <TextInputWithoutImageCustomWidthHeight ref={(input: any) => { this.txtExpiryMonth = input as any }} placeholder='MM' onChange={(text: any) => { this.setState({ expiryMonth: text }), ((text.length == 2) ? Keyboard.dismiss() : null) }} value={this.state.expiryMonth} width='45%' keyboardType='numeric' returnKeyType='done' maxLength={2} />
                                    <TextInputWithoutImageCustomWidthHeight ref={(input: any) => { this.txtExpiryYear = input as any }} placeholder='YY' onChange={(text: any) => { this.setState({ expiryYear: text }), ((text.length == 2) ? Keyboard.dismiss() : null) }} value={this.state.expiryYear} width='45%' keyboardType='numeric' returnKeyType='done' maxLength={2} />
                                </View>
                                <TextInputWithoutImageCustomWidthHeight ref={(input: any) => { this.txtCvv = input as any }} placeholder='000' onChange={(text: any) => { this.setState({ cvv: text }), ((text.length == 3) ? Keyboard.dismiss() : null) }} value={this.state.cvv} width='22.5%' keyboardType='numeric' returnKeyType='done' maxLength={3} />

                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <GreenTextBtn title={localize.applyDicountCode} func={() => this.applyDiscountAction()} textAlign='left' width={150} />
                                <TextInputWithoutImageCustomWidthHeight placeholder='' onChange={(text: any) => this.setState({ coupenCode: text })} value={this.state.coupenCode} width='50%' height={40} onEndEditing={() => this.validateCoupen()} />
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 16 }}>
                                <ToggleSwitch
                                    isOn={this.state.isCardSaved}
                                    onColor={APP_HEADER_BACK_COLOR}
                                    onToggle={(isCardSaved: any) =>
                                        this.setState({ isCardSaved: isCardSaved })}
                                />
                                <Text style={{ textAlign: 'left', color: App_MIDIUM_GREY_COLOR, marginLeft: 8 }}>{localize.saveCardForFuture}</Text>
                            </View>
                            <CommonBtn title={localize.confirm} func={() => this.confirmBtnAction()} width='70%' />
                        </ScrollView>
                    </View>
                </View>

            </KeyboardAvoidingView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    doctorInfo: state.saveDoctorInfo,
    bookingInfo: state.saveBookingInfo,
    categoryInfo: state.saveCategoryInfo
});


export default connect(mapStateToProps)(AddCard);