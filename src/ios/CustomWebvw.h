//
//  CustomWebvw.h
//  Mediport
//
//  Created by mojave on 31/01/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomWebvw : UIViewController
{
  WKWebView *webView;
}
@end

NS_ASSUME_NONNULL_END
