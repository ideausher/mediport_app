//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREY_TEXT_COLOR,APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, App_MIDIUM_GREY_COLOR, APP_MEDIUM_GREY_COLOR } from '../../Constants';
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
      //  backgroundColor: 'red',
      paddingTop: 4,
      paddingBottom: 12,
      paddingLeft: 8,
      paddingRight: 8,       
    },
    shadowVw: {
        borderRadius: 15,
    },
    itemView: {
        backgroundColor: 'white',
        padding: 16,
        borderRadius: 15,
        width: '100%',
    },
    nameTxt: {
        textAlign: 'left',
        fontSize: 14,
        color: APP_MEDIUM_GREY_COLOR,
        fontWeight: '400',
        marginLeft: 8,
    },
    smallTxt: {
        textAlign: 'left',
        fontSize: 12,
        color: APP_MEDIUM_GREY_COLOR,
        fontWeight: '300',
        marginLeft: 8,
    },
    viewReportsBtn:{
        width: '32%',
        height: 30,
        backgroundColor: 'white',
        borderRadius:15,
        borderColor: APP_GREEN_COLOR,
        borderWidth:2,
        justifyContent: 'center'
    },
    messageBtn:{
        width: '32%',
        height: 30,
        backgroundColor: APP_GREEN_COLOR,
        borderRadius:15,
        borderColor: APP_GREEN_COLOR,
        borderWidth:2,
        justifyContent: 'center'
    },
});