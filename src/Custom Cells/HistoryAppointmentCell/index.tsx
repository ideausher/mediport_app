import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { APP_GREEN_COLOR, commonShadowStyle } from '../../Constants';
import { CommonStyles } from '../../Utils/CommonStyles';
import moment from 'moment';
import { localize } from '../../Utils/LocalisedManager';

interface Props {
    viewReportsEvent: any,
    messageEvent: any,
    feedbackEvent: any
    item: any,
    index: any
}

class HistoryAppointmentCell extends Component<Props> {

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.shadowVw, commonShadowStyle]}>
                    <TouchableOpacity
                        style={styles.itemView}
                        disabled={true}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={CommonStyles.doctorImgCntnrStyle}>
                                <Image
                                    source={{ uri: this.props.item.vendor.image }}
                                    style={CommonStyles.doctorImg}
                                />
                            </View>
                            <View style={{ width: '60%', padding: 8 }}>
                                <Text style={styles.nameTxt}>{this.props.item.vendor.firstname}</Text>
                                <Text style={styles.smallTxt}>{this.props.item.service.servicecategory.cat_name}</Text>
                                <Text style={styles.smallTxt}>{this.props.item.department}</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 8, justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row',  alignItems: 'center' }}>
                                <Text style={styles.smallTxt}>{localize.patientsName}:</Text>
                                <Text style={[styles.nameTxt,{marginLeft: 0, fontSize: 12}]}>{this.props.item.booking_detail.patient_name}</Text>
                            </View>

                            <Text style={styles.nameTxt}>${this.props.item.service.price}</Text>
                        </View>
                        <Text style={[styles.nameTxt, { alignSelf: 'flex-end' }]}>{moment(this.props.item.booking_date, 'YYYY-MM-DD').format('DD/MM/YYYY')}</Text>
                        <View style={[{ flexDirection: 'row', marginTop: 8, justifyContent: 'space-between', alignSelf: 'center'}, (this.props.item.review.length > 0 ? {width : '90%'} : {width: '100%'}) ]}>
                            <TouchableOpacity
                                style={[styles.viewReportsBtn, (this.props.item.review.length > 0 ? {width : '48%'} : {})]}
                                onPress={() => this.props.viewReportsEvent(this.props.index)}>
                                <Text style={{ color: APP_GREEN_COLOR, textAlign: 'center' }}>{localize.viewReports}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[styles.messageBtn, (this.props.item.review.length > 0 ? {width : '48%'} : {})]}
                                onPress={() => this.props.messageEvent(this.props.index)}>
                                <Text style={{ color: 'white', textAlign: 'center' }}>{localize.message}</Text>
                            </TouchableOpacity>
                            {this.props.item.review.length > 0 ? null :
                            <TouchableOpacity
                                style={styles.viewReportsBtn}
                                onPress={() => this.props.feedbackEvent(this.props.index)}>
                                <Text style={{ color: APP_GREEN_COLOR, textAlign: 'center' }}>{localize.feedback}</Text>
                            </TouchableOpacity>}
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}

export default HistoryAppointmentCell;