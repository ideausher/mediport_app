//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_LIGHT_GREY_COLOR, APP_GREY_TEXT_COLOR, App_MIDIUM_GREY_COLOR, App_LIGHT_BLUE_COLOR } from '../../Constants';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    cardVw:{
        flexDirection: 'row',
        padding: 16,
        alignItems:'center',
        backgroundColor: App_LIGHT_BLUE_COLOR,
        marginBottom: 8
    },
    smallImg: {
        width: 16,
        height: 16,
        marginLeft: 8
    },
    cardText:{
        fontSize: 16,
        fontWeight: '500',
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        marginLeft: 8,
        flex: 1
    },
    visaImg:{
        width: 30,
        height: 30
    }
});