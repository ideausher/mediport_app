import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { APP_GREEN_COLOR, APP_GREY_BACK, commonShadowStyle } from '../../Constants';
import { CommonStyles } from '../../Utils/CommonStyles';
//@ts-ignore
import { connect } from 'react-redux';
import { AVATAR_URL } from '../../Utils/APIs';
import moment from 'moment';

interface Props {
    onClickEvent: any,
    item: any,
    id: any
}

class ChatsCell extends Component<Props> {

    render() {

        let user = {} as any

        if (this.props.item.sender_id == this.props.id) {
            user = this.props.item
        }
        else {
            user = this.props.item
        }

        let time = this.props.item.created_at
        let timeLeftFromNow = moment(time).fromNow()


         console.log('Booking Time >>>>', this.props.item)

        return (
            <View style={[styles.container, (this.props.item.isSelected ? { backgroundColor: '#F7FAFC' } : { backgroundColor: 'white' })]}>
                {/* <View style={[styles.shadowVw, commonShadowStyle}> */}
                <TouchableOpacity
                    style={styles.itemView}
                    onPress={this.props.onClickEvent}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={CommonStyles.doctorImgCntnrStyle}>
                            <Image
                                source={{ uri: user.image != undefined ? user.image : "" }}
                                style={CommonStyles.doctorImg}
                                resizeMode={'contain'}
                            />
                        </View>
                        <View style={{ width: '60%', padding: 8 }}>
                            <Text style={styles.nameTxt}>{user.firstname}</Text>
                            <Text style={[styles.nameTxt, { fontSize: 12,  height: 30 }]}>{this.props.item.message_content}</Text>
                        </View>
                        <Text style={[styles.nameTxt, { fontSize: 12, marginTop: 16 }]}>{timeLeftFromNow}</Text>
                    </View>

                </TouchableOpacity>
            </View>
            // </View>
        )
    }

}


const mapStateToProps = (state: any) => ({
    id: state.saveLoginInfo.id,
});

export default connect(mapStateToProps)(ChatsCell);
