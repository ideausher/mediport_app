//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREY_TEXT_COLOR,APP_GREEN_COLOR } from '../../Constants';
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        marginTop:8, 
        marginBottom: 8,
    },
    shadowVw: {
        borderRadius: 5,
    },
    itemView: {
        padding: 8,
    },
    nameTxt: {
        textAlign: 'left',
        fontSize: 14,
        color: APP_GREY_TEXT_COLOR,
        fontWeight: '300',
        marginLeft: 8,
        marginTop: 4
    },
});