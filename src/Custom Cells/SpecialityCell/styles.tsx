//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREY_TEXT_COLOR, APP_LIGHT_GREY_COLOR } from '../../Constants';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        width: '50%',
        //backgroundColor: 'green',
        height: 80,
        paddingLeft: 8, 
        paddingRight: 8,
        paddingBottom : 16,
    },
    shadowVw: {
        borderRadius: 10,
    },
    itemView: {
        height: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 10,
    },
    image: {
        width: 30,
        height: 30,
        marginLeft: 10,
    },
    itemName: {
        textAlign: 'left',
        fontSize: 14,
        color: APP_GREY_TEXT_COLOR,
        fontWeight: '300',
        marginLeft: 10, 
        marginRight: 4,
        // adjustsFontSizeToFitWidth: true,
        flexWrap: 'wrap', 
        flex: 1
    }
});