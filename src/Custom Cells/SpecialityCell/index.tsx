import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { IMAGE_URL } from '../../Utils/APIs';
//@ts-ignore
import ImageLoad from 'react-native-image-placeholder';
import { commonShadowStyle } from '../../Constants';
import General from '../../Utils/General';

interface Props {
    onClickEvent: any,
    item: any,
}

class SpecialityCell extends Component<Props> {

    render() {

        return (
            <View style={styles.container}>
                <View style={[styles.shadowVw, commonShadowStyle]}>
                    <TouchableOpacity
                        style={styles.itemView}
                        activeOpacity={1.0}
                        onPress={() => this.props.onClickEvent(this.props.item)}>
                        {this.props.item.image == '' ?
                            <Image
                                source={require('../../assets/avatar.png')}
                                style={styles.image}
                                resizeMode={'contain'} /> :
                            <ImageLoad resizeMode='contain' style={styles.image} source={{ uri: IMAGE_URL + this.props.item.image }} />
                        }
                        <Text style={styles.itemName}>{this.props.item.cat_name}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default SpecialityCell;