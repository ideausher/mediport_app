//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { Platform, Dimensions } from 'react-native'
import { APP_GREY_TEXT_COLOR, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR, APP_SHADOW_COLOR ,APP_MEDIUM_GREY_COLOR} from '../../Constants';

export const styles = ScaleSheet.create({
    container: {
        // flex: 1,
        // justifyContent: 'center',
        paddingTop: 4,
        paddingBottom: 12,
        paddingLeft: 8,
        paddingRight: 8,
        borderRadius: 15,
        //backgroundColor:'red'
    },
    shadowVw: {
        borderRadius: 15,
    },
    itemView: {
        backgroundColor: 'white',
        padding: 8,
        borderRadius: 15,
        width: '100%',
    },
    nameTxt: {
        textAlign: 'left',
        fontSize: 14,
        color: APP_GREY_TEXT_COLOR,
        fontWeight: '400',
        marginLeft: 8,
    },
    lightText: {
        textAlign: 'left',
        fontSize: 12,
        color: APP_MEDIUM_GREY_COLOR,
        fontWeight: '300',
        marginLeft: 8,
        marginTop: 8
    },
    ratingVw: {
        marginTop: 10,
        alignItems: 'flex-end',
    },
    startImg: {
        width: 20,
        height: 20,
    },
    ratingTxt: {
        textAlign: 'center',
        fontSize: 12,
        color: 'black',
        marginTop: 5,
//        backgroundColor:'green'
    },
    distanceVw: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 8,
       // backgroundColor:'red'
    },
    distanceTxt: {
        textAlign: 'center',
        fontSize: 8,
        color: 'grey',
        marginLeft: 4,
     //  backgroundColor:'green'

    },
    messageBtn: {
        width: '48%',
        height: 30,
        backgroundColor: 'white',
        borderRadius: 15,
        borderColor: APP_GREEN_COLOR,
        borderWidth: 2,
        justifyContent: 'center',
    },
    viewProfileBtn: {
        width: '48%',
        height: 30,
        backgroundColor: APP_GREEN_COLOR,
        borderRadius: 15,
        borderColor: APP_GREEN_COLOR,
        borderWidth: 2,
        justifyContent: 'center', 
        alignSelf: 'flex-end',
    },
});
