import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { APP_GREEN_COLOR, commonShadowStyle } from '../../Constants';
import { localize } from '../../Utils/LocalisedManager';
//@ts-ignore
import ImageLoad from 'react-native-image-placeholder';
import { DOCTOR_IMAGE_URL } from '../../Utils/APIs';
import { CommonStyles } from '../../Utils/CommonStyles';

interface Props {
    sendMessage: any,
    viewProfileEvent: any,
    item: any,
    index: any,
    type: any
}

class DoctorCell extends Component<Props> {
    // placeholderSource={require('../../assets/avatar.png')}
    render() {
        return (
            <View style={[styles.container]}>
                <View style={[styles.shadowVw, commonShadowStyle]}>
                    <View style={styles.itemView}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={[CommonStyles.doctorImgCntnrStyle, { alignSelf: 'center' }]}>
                                {this.props.item.profile_image == '' ?
                                    <Image
                                        source={require('../../assets/avatar.png')}
                                        style={CommonStyles.doctorImg}
                                    /> :
                                    <Image style={CommonStyles.doctorImg} source={{ uri: this.props.item.profile_image }} />
                                }
                            </View>
                            <View style={{ flex: 1, padding: 8 }}>
                                <Text style={styles.nameTxt}>{this.props.item.name}</Text>
                                <Text style={styles.lightText}>{this.props.item.cat_name}</Text>
                                <Text style={styles.lightText}>{this.props.item.full_address}</Text>
                            </View>
                            <View style={styles.ratingVw}>
                                <Image
                                    source={require('../../assets/star.png')}
                                    style={styles.startImg}
                                    resizeMode={'contain'}
                                />
                                <Text style={styles.ratingTxt}>{this.props.item.avg_rating.toFixed(1)}</Text>
                                <View style={styles.distanceVw}>
                                    <Image
                                        source={require('../../assets/loc.png')}
                                        style={{ width: 10, height: 10 }}
                                        resizeMode={'contain'}
                                    />
                                    <Text style={styles.distanceTxt}>{this.props.item.venderaddress.toFixed(1)} km</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 12, justifyContent: 'space-between', marginBottom: 8 }}>
                            <Text style={[styles.nameTxt]}> {this.props.item.price != undefined && this.props.item.price != '' ? `$${this.props.item.price}` : ''}</Text>
                            <View style={{ flexDirection: 'row', width: '80%', justifyContent: (this.props.type == 1 ? 'flex-end' : 'space-between') }}>
                                {this.props.type == 1 ? null : <TouchableOpacity
                                    style={styles.messageBtn}
                                    onPress={() => this.props.sendMessage(this.props.index)}>
                                    <Text style={{ color: APP_GREEN_COLOR, textAlign: 'center' }}>{localize.message}</Text>
                                </TouchableOpacity>}
                                <TouchableOpacity
                                    style={styles.viewProfileBtn}
                                    onPress={() => this.props.viewProfileEvent(this.props.index)}>
                                    <Text style={{ color: 'white', textAlign: 'center' }}>{localize.viewProfile}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

}

export default DoctorCell;