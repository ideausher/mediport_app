//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { Platform } from 'react-native'
import { APP_GREY_TEXT_COLOR,APP_GREEN_COLOR } from '../../Constants';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
      //  backgroundColor: 'red',
      paddingTop: 4,
      paddingBottom: 12,
      paddingLeft: 8,
      paddingRight: 8,
    },
    shadowVw: {
        borderRadius: 15,
    },
    itemView: {
        backgroundColor: 'white',
        padding: 16,
        borderRadius: 15,
        width: '100%',
    },
    nameTxt: {
        textAlign: 'left',
        fontSize: 14,
        color: APP_GREY_TEXT_COLOR,
        fontWeight: '300',
        marginLeft: 8,
        marginTop:4
    },
    cancelAppointmentBtn:{
        height: 30,
        backgroundColor: 'white',
        borderRadius:15,
        borderColor: APP_GREEN_COLOR,
        borderWidth:2,
        justifyContent: 'center'
    },
});
