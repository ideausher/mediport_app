import React, { Component } from 'react';
import { View, Text, Image, ToastAndroid, Alert } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { APP_GREEN_COLOR, commonShadowStyle, APP_HEADER_BACK_COLOR } from '../../Constants';
import moment from 'moment';
import { CommonStyles } from '../../Utils/CommonStyles';
import { localize } from '../../Utils/LocalisedManager';
import General from '../../Utils/General';
import { APP_TEXT_COLOR_WHITE } from '../../res/colors';

const oneDay = 24 * 60 * 60 * 1000;

interface Props {
    onClickEvent: any,
    messageEvent:any,
    item: any,
    index: any
}

class AppointmentCell extends Component<Props> {

    private getTimeLeft(date: any, time: any) {

       // console.log('Date Time>>>', date, time)

        let dateTimeStr = date + ' ' + time
       // console.log('dateTimeStr >>>', dateTimeStr)
        let bookingDate = moment(dateTimeStr, 'YYYY-MM-DD HH:mm:ss')
        let diff = bookingDate.fromNow()

        //console.log('Difff >>>', diff)

        let difffFormatted = diff.replace("in ", "") + ' left'
        return difffFormatted
    }

    private isTimeLeftMoreThanOneDay(date: any, time: any) {

        
        let dateTimeStr = date + ' ' + time
        let diff = moment(dateTimeStr, 'YYYY-MM-DD HH:mm:ss').diff(moment().add(24, 'hours'), 'seconds');

        console.log('DIfff >>>>', diff)

        return diff > 0


        // const oneDayNextFromNow = Date.now() + oneDay;

        // let dateTotime = (new Date(dateTimeStr)).getTime()
        // console.log('getTime >>>>', dateTotime, Date.now())
        // console.log('oneDayNextFromNow >>>>', oneDayNextFromNow, dateTotime > oneDayNextFromNow)
        // return dateTotime > oneDayNextFromNow;
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.shadowVw, commonShadowStyle]}>
                    <TouchableOpacity
                        style={styles.itemView}
                        disabled={true}>
                        <View style={{ flexDirection: 'row', marginRight:20 }}>
                            <View style={CommonStyles.doctorImgCntnrStyle}>
                                <Image
                                    source={{ uri: this.props.item.vendor.image }}
                                    style={CommonStyles.doctorImg}
                                />
                            </View>
                            <View style={{ width: '60%', padding: 8 }}>
                                <Text style={styles.nameTxt}>{this.props.item.vendor.firstname}</Text>
                                <Text style={[styles.nameTxt, { fontSize: 12 }]}>{this.props.item.service.servicecategory.cat_name}</Text>
                                <Text style={[styles.nameTxt, { fontSize: 12 }]}>{this.props.item.department}</Text>
                            </View>
                            <Text style={{color: APP_HEADER_BACK_COLOR}}>{this.getTimeLeft(this.props.item.booking_date, this.props.item.booking_start)}</Text>
                        </View>
                        {this.props.item.status == "1" ?
                        <View style={{ flexDirection: 'row', justifyContent:'space-around', alignItems:'center' }}>
                            
                            
                            {/* <TouchableOpacity
                                style={[styles.messageBtn, (this.props.item.review.length > 0 ? {width : '48%'} : {})]}
                                onPress={() => this.props.messageEvent(this.props.index)}>
                                <Text style={{ color: 'white', textAlign: 'center' }}>{localize.message}</Text>
                            </TouchableOpacity> */}

                            <TouchableOpacity
                                style={[styles.cancelAppointmentBtn,{ backgroundColor:APP_GREEN_COLOR }]}
                                onPress={() => this.props.messageEvent(this.props.index)}>
                                <Text style={{ color: APP_TEXT_COLOR_WHITE, textAlign: 'center', fontSize: 14, marginLeft: 8, marginRight: 8 }}>{localize.message}</Text>
                            </TouchableOpacity>

                            {this.isTimeLeftMoreThanOneDay(this.props.item.booking_date, this.props.item.booking_start) == true ? 
                            <TouchableOpacity
                                style={styles.cancelAppointmentBtn}
                                onPress={() => this.props.onClickEvent(this.props.index)}>
                                <Text style={{ color: APP_GREEN_COLOR, textAlign: 'center', fontSize: 14, marginLeft: 8, marginRight: 8 }}>{localize.cancelAppointment}</Text>
                            </TouchableOpacity> : null}     
                        </View> : <Text style={{ color: APP_GREEN_COLOR, textAlign: 'center', fontSize: 14, alignSelf:'flex-start'}}>Cancelled</Text>}
                        {/* <TouchableOpacity
                            style={styles.daysBtn}
                            onPress={this.props.onClickEvent}>
                            <Text style={{ color: 'white', textAlign: 'center' }}>{this.getTimeLeft(this.props.item.booking_date, this.props.item.booking_start)}</Text>
                        </TouchableOpacity> */}
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default AppointmentCell;
