//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREY_TEXT_COLOR, APP_GREEN_COLOR, APP_LIGHT_GREY_COLOR } from '../../Constants';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    itemView: {
        padding: 8,
        marginBottom: 8, 
        marginLeft: 8, 
        marginRight:8,
    },
    nameTxt: {
        textAlign: 'left',
        fontSize: 14,
        color: APP_GREY_TEXT_COLOR,
        fontWeight: '300',
        marginLeft: 8,
        marginTop: 4
    },
    viewReportBtn: {
        width: 120,
        height: 30,
        backgroundColor: 'white',
        borderRadius: 15,
        borderColor: APP_GREEN_COLOR,
        borderWidth: 2,
        justifyContent: 'center',
        alignSelf: 'flex-end'
    },
    viewReportText: {
        color: APP_GREEN_COLOR,
        textAlign: 'center',
        fontSize: 12,
        fontWeight: '600'
    }
});