import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { APP_GREEN_COLOR } from '../../Constants';
import { localize } from '../../Utils/LocalisedManager';
import { CommonStyles } from '../../Utils/CommonStyles';

interface Props {
    onClickEvent: any,
    item: any,
    index: any
}

class AppointmentCell extends Component<Props> {    
    render() {
        return (
            <View style={styles.container}>
                    <TouchableOpacity
                        style={styles.itemView}
                        disabled={true}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={CommonStyles.smallDoctorImgCntnrStyle}>
                                <Image
                                    source={{ uri: this.props.item.vendor.image }}
                                    style={CommonStyles.doctorImg}
                                   // resizeMode={'contain'}
                                />
                            </View>
                            <View style={{ width: '60%', padding: 8}}>
                                <Text style={styles.nameTxt}>{this.props.item.vendor.firstname}</Text>
                                <Text style={[styles.nameTxt, { fontSize: 12 }]}>{this.props.item.service.servicecategory.cat_name}</Text>
                                <Text style={[styles.nameTxt, { fontSize: 12 }]}>{this.props.item.department}</Text>
                            </View>
                        </View>
                        <TouchableOpacity
                                style={styles.viewReportBtn}
                                onPress={() => this.props.onClickEvent(this.props.index)}>
                                    <Text style={styles.viewReportText}>{localize.viewReports}</Text>
                                </TouchableOpacity>
                    </TouchableOpacity>
                </View>
        )
    }

}

export default AppointmentCell;