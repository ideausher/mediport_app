//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREY_TEXT_COLOR,APP_GREEN_COLOR,APP_DARK_BLUE_COLOR, APP_LIGHT_GREY_COLOR, APP_HEADER_BACK_COLOR, App_MIDIUM_GREY_COLOR } from '../../Constants';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    itemView: {
        padding: 8,
        marginBottom: 8, 
        marginLeft: 8, 
        marginRight:8, 
    },
    firstLetterText:{
        width: 50,
        height: 50,
        backgroundColor: APP_HEADER_BACK_COLOR,
        color:'white',
        borderRadius: 25,
        lineHeight: 50, 
        textAlign: 'center',
        fontSize: 20, 
        fontWeight: '600'
    },
    nameTxt: {
        textAlign: 'left',
        fontSize: 14,
        color: APP_DARK_BLUE_COLOR,
        fontWeight: '400',
        marginLeft: 8,
    },
    messageTxt: {
        textAlign: 'left',
        fontSize: 12,
        color: '#6F8BA4',
        fontWeight: '400',
        marginLeft: 8,
    },
    timeTxt:{
        textAlign: 'right',
        fontSize: 12,
        color: App_MIDIUM_GREY_COLOR,
    }
});