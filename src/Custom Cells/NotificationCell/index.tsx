import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { APP_GREEN_COLOR } from '../../Constants';
import moment from 'moment';
import { CommonStyles } from '../../Utils/CommonStyles';
import General from '../../Utils/General';

interface Props {
    item: any,
}

class NotificationCell extends Component<Props> {

    private getTime(createdAt: any) {

        console.log('CReated AT >>>', createdAt)

        // var gmtDateTime = moment.utc('2020-01-14 11:00:00', "YYYY-MM-DD HH:mm:ss")
        // console.log('gmtDateTimel >>>>>', gmtDateTime);
        // var local = gmtDateTime.local().format('YYYY-MM-DD HH:mm:ss');
        // console.log('local >>>>>', local);

        let bookingDate = moment(createdAt, 'YYYY-MM-DD HH:mm:ss')
        console.log('bookingDate >>>>>', bookingDate);
        let diff = bookingDate.fromNow()
        console.log('diff >>>>>', diff);

        return diff
    }

    render() {
        console.log('Notifications props >>>', this.props.item)

        return (
            <View style={styles.container}>
                <View style={styles.itemView}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={CommonStyles.smallDoctorImgCntnrStyle}>
                            {this.props.item.vendor.image == '' ?
                                <Text style={styles.firstLetterText}>{this.props.item.vendor.firstname.charAt(0)}</Text> : <Image
                                    source={{ uri: this.props.item.vendor.image }}
                                    style={CommonStyles.doctorImg}
                                />}
                        </View>
                        <View style={{ padding: 8, marginLeft: 16, flex: 1 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={styles.nameTxt}>{this.props.item.vendor.firstname}</Text>
                                <Text style={styles.timeTxt}>{this.getTime(this.props.item.created_at)}</Text>
                            </View>
                            <Text style={styles.messageTxt}>{this.props.item.title}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

}

export default NotificationCell;