import React, { Component } from 'react';
import { View, Text, Image, ImageBackground } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { APP_LIGHT_GREY_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREY_TEXT_COLOR, App_MIDIUM_GREY_COLOR, APP_DARK_GREY_COLOR } from '../../Constants';

interface Props {
    onClickEvent: any,
    item: any,
    index: any
}

class PDFImagesCell extends Component<Props> {
    // 18004198585
    render() {
        return (
            <View style={{ width: 55, height: 55, marginLeft: 5 }}>
                <ImageBackground style={{ flex: 1 }}
                    // source={require('../../assets/visa.png')}
                    source={{ uri: this.props.item.uri }}
                >
                    <TouchableOpacity style={{marginTop: 2, marginRight: 2, width: 20, height: 20, alignSelf: 'flex-end', backgroundColor: APP_HEADER_BACK_COLOR, borderColor: 'white', borderRadius: 10, borderWidth: 1, justifyContent: 'center', alignItems: 'center'  }}
                        onPress={() => this.props.onClickEvent(this.props.index)}>
                        <Text style={{textAlign:'center', color: 'white', fontSize: 14, fontWeight: '400'}}>X</Text>
                    </TouchableOpacity>
                </ImageBackground>
            </View>
        )
    }
}

export default PDFImagesCell;