//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREEN_COLOR, APP_GREY_TEXT_COLOR, App_MIDIUM_GREY_COLOR } from '../../Constants';
import { WHITE_COLOR } from '../../res/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        padding: 8, 
    },
    shadowVw: {
        borderRadius: 15,
    },
    itemView: {
        backgroundColor: 'white',
        padding: 8,
        borderRadius: 15,
        width: '100%',
    },
    darkText: {
        fontSize: 12,
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'left',
        marginTop: 8
    },
    lightText: {
        flex: 1,
        marginTop: 8,
        marginLeft: 8,
        fontSize: 12,
        color: App_MIDIUM_GREY_COLOR,
        textAlign: 'justify',
    },
    downloadBtn:{
        marginTop: 8,
        backgroundColor : APP_GREEN_COLOR,
        borderRadius:20,
        paddingHorizontal:5,
        paddingVertical:2
    },  
    downloadBtnText:{
        flex: 1,
        // marginTop: 8,
        marginLeft: 8,
        fontSize: 12,
        color: WHITE_COLOR,
        textAlign: 'justify',
    }
});