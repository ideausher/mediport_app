import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { styles } from './styles';
import { localize } from '../../Utils/LocalisedManager';
import moment from 'moment';
import { commonShadowStyle } from '../../Constants';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Progress from 'react-native-progress';

interface Props {
    item: any,downloadPdf:any
}

class ViewReportsCell extends Component<Props> {
    render() {
        let bookingDate = moment(this.props.item.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY')
        let bookingTime = moment(this.props.item.created_at, 'YYYY-MM-DD HH:mm:ss').format('hh:mm a')

        return (
            <View style={styles.container}>
                
                <View style={[styles.shadowVw, commonShadowStyle]}>
                    <View style={styles.itemView}>


                    
                        
                        <View style={{ flexDirection: 'row',width:'100%' , justifyContent:'space-between' }}>
                            <View style={{ flexDirection: 'row',width:'50%' }}>
                            <Text style={styles.darkText}>{localize.name}:</Text>
                            <Text style={styles.lightText}>{this.props.item.booking.booking_detail.patient_name}</Text>   
                            </View>
                            <TouchableOpacity onPress = {this.props.downloadPdf} style={[styles.downloadBtn,{ fontName: "Muli", }]}><Text style={styles.downloadBtnText}>Download PDF</Text></TouchableOpacity>
                            
                        </View>
                        {/* <TouchableOpacity onPress = {this.props.downloadPdf} style={[styles.downloadBtn,{  }]}><Text style={styles.downloadBtnText}>finger PDF</Text></TouchableOpacity> */}
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.darkText}>{localize.age}:</Text>
                            <Text style={styles.lightText}>{this.props.item.booking.booking_detail.age}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.darkText}>{localize.referenceId}:</Text>
                            <Text style={styles.lightText}>{this.props.item.booking.reference_id}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.darkText}>{localize.date}:</Text>
                            <Text style={styles.lightText}>{bookingDate}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.darkText}>{localize.time}:</Text>
                            <Text style={styles.lightText}>{bookingTime}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.darkText}>{localize.DoctorsName}:</Text>
                            <Text style={styles.lightText}>{this.props.item.booking.vendor.firstname}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.darkText}>{localize.disease}:</Text>
                            <Text style={styles.lightText}>{this.props.item.data.issue}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.darkText}>{localize.doctorFeedback}:</Text>
                            <Text style={styles.lightText}>{this.props.item.data.report}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

}

export default ViewReportsCell;