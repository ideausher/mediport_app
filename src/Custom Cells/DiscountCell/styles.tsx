//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_LIGHT_GREY_COLOR, APP_GREY_TEXT_COLOR, App_MIDIUM_GREY_COLOR } from '../../Constants';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 16
    },
    darkTxt: {
        fontSize: 14,
        fontWeight: 'bold',
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'center',
    },
    smallText: {
        fontSize: 12,
        color: APP_GREY_TEXT_COLOR,
        fontWeight: '400',
        textAlign: 'left',
        marginTop: 20,
    },
});