import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { APP_GREY_TEXT_COLOR } from '../../Constants';
import { GreenTextBtn } from '../../Custom/CustomButton';
import { TextInputWithoutImageCustomWidthHeight } from '../../Custom/CustomTextInput'
import { localize } from '../../Utils/LocalisedManager';
interface Props {
    onClickEvent: any,
    item: any,
    index: any
}

class DiscountCell extends Component<Props> {
    render() {
        return (
            <View style={styles.container}>
                <Text style={[styles.darkTxt, { textAlign: 'left' }]}>MEDIPORT</Text>
                <Text style={styles.smallText}>{this.props.item.name}</Text>
                <Text style={[styles.smallText, { fontSize: 11 }]}>{this.props.item.description}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
                    <TextInputWithoutImageCustomWidthHeight placeholder='' value={this.props.item.code} width='50%' height={40} textAlign='center' editable={false} fontWeight='bold' />
                    <GreenTextBtn title={localize.copyCode} func={() => this.props.onClickEvent(this.props.index)} textAlign='right' width={150} />
                </View>
            </View>
        )
    }
}

export default DiscountCell;