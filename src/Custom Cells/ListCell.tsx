import React from 'react';
import { StyleSheet, Text, View , TouchableOpacity} from 'react-native';
import {TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { APP_GREY_TEXT_COLOR } from '../Constants';

const ListCell = (props: any) => (

    <View style={styles.container}>
        <TouchableOpacity onPress={() => {console.log('Func called'), props.func(props.cellData)}}>
            <Text style={styles.darkGreyText}>{props.cellData}</Text>
        </TouchableOpacity>
    </View >
)

export default ListCell;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    darkGreyText: {
        fontSize: 16,
        textAlign: 'left',
        height: 24,
        paddingLeft: 16,
        color: APP_GREY_TEXT_COLOR
    },
});
