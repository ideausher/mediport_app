import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import {APP_LIGHT_GREY_COLOR, APP_HEADER_BACK_COLOR, APP_GREY_BACK, APP_GREY_TEXT_COLOR, App_MIDIUM_GREY_COLOR, APP_DARK_GREY_COLOR} from '../../Constants';

interface Props {
    onClickEvent: any,
    item: any,
    index: any
}

class AppointmentTimeCell extends Component<Props> {

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={[(this.props.item.isSelected ? styles.selectedTimeBtn : styles.timeBtn), (this.props.item.is_active == "0" ? {backgroundColor: '#D3D3D3', borderWidth:0} : null)]}
                    onPress={() => this.props.onClickEvent(this.props.index)}
                    disabled={this.props.item.is_active == "0"}>
                    <Text style={[{ textAlign: 'center', fontSize: 12}, (this.props.item.is_active == "0" ? {color: APP_DARK_GREY_COLOR} : (this.props.item.isSelected ? { color: APP_HEADER_BACK_COLOR} : { color: App_MIDIUM_GREY_COLOR}))]}>{this.props.item.start_time}</Text>
                </TouchableOpacity>
            </View>
        )
    }

}

export default AppointmentTimeCell;