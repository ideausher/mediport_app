//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { App_LIGHT_BLUE_COLOR, APP_LIGHT_GREY_COLOR, APP_HEADER_BACK_COLOR } from '../../Constants';

export const styles = ScaleSheet.create({
    container: {
        //flex: 1,
        justifyContent: 'center',
        width: '33%',
        //backgroundColor: 'red',
        height: 50,
        padding: 4
    },
    selectedTimeBtn:{
        width:'100%',
        height:40,
        backgroundColor: App_LIGHT_BLUE_COLOR,
        borderRadius:20,
        justifyContent: 'center',
    },
    timeBtn:{
        width:'100%',
        height:40,
        backgroundColor: 'white',
        borderRadius:200,
        borderColor: APP_HEADER_BACK_COLOR,
        borderWidth:1,
        justifyContent: 'center',
    },
});