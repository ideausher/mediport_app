import * as Modl from '../Modals/NotificationsModal';
import { Modal } from 'react-native';
import moment from 'moment';

export const notifiationsArr = (data: any) => {
    return Notifications(data);
}

function getLocalTime(timeUTC: any) {

    console.log('timeUTC >>>', timeUTC)

    var timeUTCMoment = moment.utc(timeUTC, "YYYY-MM-DD HH:mm:ss")
    console.log('timeUTCMoment >>>>>', timeUTCMoment);
    var timeLocal = timeUTCMoment.local().format('YYYY-MM-DD HH:mm:ss');
    console.log('timeLocal >>>>>', timeLocal);

    return timeLocal
}

async function Notifications(responeData: any) {
    return await new Promise(function (resolve, reject) {

        let notificationsArr = responeData.map((data: any, index: any) => {

            // Notification message Object
            let notMsg: Modl.NotMsg = {
                user_name: data.message.user_name,
                booking_id: data.message.booking_id,
                vender_id: data.message.vender_id,
            }

            // Notification Vendor Object
            let notVendor: Modl.NotVendor = {
                id: data.vendor.id,
                image: data.vendor.image,
                firstname: data.vendor.firstname,
            }

            // NotificatiosnArr
            let notification: Modl.NotificationsModal = {
                vender_id: data.vender_id,
                type: data.type,
                title: data.title,
                message: notMsg,
                created_at: getLocalTime(data.created_at),
                vendor: notVendor
            }
            return notification
        })

        resolve(notificationsArr)
    })
}