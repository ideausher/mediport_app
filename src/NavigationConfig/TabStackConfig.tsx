import { createStackNavigator } from 'react-navigation-stack';
import Home from '../containers/Home';
import Doctors from '../containers/Doctors';
import Appointments from '../containers/Appointments';
import Reports from '../containers/Reports'
import Chats from '../containers/Chats';
import Subscription from '../containers/Subscription';
import Settings from '../containers/Settings';
import TermsAndConditions from '../containers/TermsAndConditions';
import FAQ from '../containers/FAQ/index';
import Profile from '../containers/Profile';
import DoctorsProfile from '../containers/DoctorsProfile';
import BookAppointment from '../containers/BookAppointment';
import BookAppoitmentDate from '../containers/BookAppoitmentDate';
import PaymentMethod from '../containers/PaymentMethod';
import AddCard from '../containers/AddCard';
import BookingSuccess from '../containers/BookingSuccess';
import Verification from '../containers/Verification';
import SavedCards from '../containers/SavedCards';
import ChangePswd from '../containers/ChangePswd';
import Signout from '../containers/Signout';
import ForgotPassword from '../containers/ForgotPassword';
import UpdatePassword from '../containers/UpdatePassword'
import ViewReports from "../containers/ViewReports";
import ChatScreen from '../containers/ChatScreen';
import 'react-native-gesture-handler'
import Messages from '../containers/Messages';
import { Text } from 'react-native';
import VideoCall from '../containers/twillio_video_call'

export const HomeStack = createStackNavigator({
  Home: Home,
  VideoCall:VideoCall
})

export const DoctorsStack = createStackNavigator({
  Doctors: Doctors,
})

export const AppointmentsStack = createStackNavigator({
  Appointments: Appointments,
  ViewReports: ViewReports,
  VideoCall:VideoCall
})

export const ChatsStack = createStackNavigator({
  Chats: Chats,
  // Chats: VideoCall,
  VideoCall:VideoCall,
  // ChatScreen: ChatScreen,
  Messages:{
    screen: Messages,
   navigationOptions:() => {
    return {
      header:null,
      tabBarVisible:null,
    };
   }
  }
})

export const ReportsStackNavigator = createStackNavigator(
  {
    Reports: Reports,
    ViewReports: ViewReports,
    VideoCall:VideoCall
  }
)

export const SubscriptionStackNavigator = createStackNavigator(
  {
    Subscription: Subscription,
    VideoCall:VideoCall
  }
)

export const SettingsStackNavigator = createStackNavigator(
  {
    Settings: Settings,
    UpdatePassword: UpdatePassword,
    VideoCall:VideoCall
  }
)

export const TermsAndConditionsStackNavigator = createStackNavigator(
  {
    TermsAndConditions: TermsAndConditions,
    VideoCall:VideoCall
  }
)

export const FAQStackNavigator = createStackNavigator(
  {
    FAQ: FAQ,
    VideoCall:VideoCall
  }
)

export const ProfileStackNavigator = createStackNavigator(
  {
    Profile: Profile,
    ForgotPassword: ForgotPassword,
    Verification: Verification,
    VideoCall:VideoCall
  }
)

export const SignoutStackNavigator = createStackNavigator(
  {
    Signout: Signout,
    VideoCall:VideoCall
  },
  { initialRouteName: 'Signout', headerMode: 'none' }
)