import { createSwitchNavigator, createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer'
import 'react-native-gesture-handler'
import Welcome from '../containers/Welcome';
import Login from '../containers/Login';
import SignUp from '../containers/SignUp';
import { AppBottomTabNavigator } from '../NavigationConfig/BottomTabNavigatorConfig'
import DrawerComponent from '../Custom/DrawerComponent';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { HomeStack, ReportsStackNavigator, ChatsStack, SubscriptionStackNavigator, SettingsStackNavigator, TermsAndConditionsStackNavigator, FAQStackNavigator, ProfileStackNavigator, SignoutStackNavigator } from './TabStackConfig';
import Reports from '../containers/Reports'
import Notifications from '../containers/Notifications';
// Login Stack

import Home from '../containers/Home';
import Doctors from '../containers/Doctors';
import DoctorsProfile from '../containers/DoctorsProfile';
import BookAppointment from '../containers/BookAppointment';
import BookAppoitmentDate from '../containers/BookAppoitmentDate';
import PaymentMethod from '../containers/PaymentMethod';
import AddCard from '../containers/AddCard';
import BookingSuccess from '../containers/BookingSuccess';
import Verification from '../containers/Verification';
import SavedCards from '../containers/SavedCards';
import Feedback from '../containers/Feedback';
import ForgotPassword from '../containers/ForgotPassword';
import Discount from '../containers/Discount';
import UpdatePassword from '../containers/UpdatePassword'

import React from 'react';
import { Image, Text, View } from "react-native";
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_LIGHT_GREY_COLOR, APP_GREEN_COLOR } from "../Constants";
import { localize } from "../Utils/LocalisedManager";
import VideoCall from '../containers/twillio_video_call'

const LoginStackNavigator = createStackNavigator(
  {
    Welcome: Welcome,
    Login: Login,
    SignUp: SignUp,
    Verification: Verification,
    ForgotPassword: ForgotPassword,
    UpdatePassword: UpdatePassword
  }
)

const HomeStackNavigator = createStackNavigator(
  {
    Home: AppBottomTabNavigator,
    Doctor: Doctors,
    DoctorsProfile: DoctorsProfile,
    BookAppointment: BookAppointment,
    BookAppoitmentDate: BookAppoitmentDate,
    PaymentMethod: PaymentMethod,
    AddCard: AddCard,
    BookingSuccess: BookingSuccess,
    Verification: Verification,
    SavedCards: SavedCards,
    Feedback: Feedback,
    Discount: Discount,
    UpdatePassword: UpdatePassword,
    ForgotPassword: ForgotPassword,
    Notifications: Notifications,
    VideoCall:VideoCall
  },
  {
    initialRouteName: 'Home', defaultNavigationOptions: {
      headerStyle: { height: 0, },
      headerForceInset: { top: 'never', bottom: 'never' },
    },
  }
)

const ChatStackNavigator = createStackNavigator(
  {
    Chats: AppBottomTabNavigator
  },
  {
    initialRouteName: 'Chats', defaultNavigationOptions: {
      headerStyle: { height: 0, },
      headerForceInset: { top: 'never', bottom: 'never' },
    },
  }

)

const DrawerNavigator = createDrawerNavigator({
  Home: {
    screen: HomeStackNavigator,
    params: { image: require("../assets/home1.png"), name: localize.home },
    navigationOptions: ({ navigation }) => ({
      drawerLabel: () => (
        <Text style={[styles.revealOptionText]}>{localize.home}</Text>
      ),
      drawerIcon: () => (
        <Image
          style={styles.revealOptionImage}
          source={require("../assets/home1.png")}
        />
      )
    })
  },
  Reports: {
    screen: ReportsStackNavigator,
    params: { image: require("../assets/report.png"), name: localize.myReports },
    navigationOptions: ({ navigation }) => ({
      drawerLabel: () => (
        <Text style={styles.revealOptionText}>{localize.myReports}</Text>
      ),
      drawerIcon: () => (
        <Image
          style={styles.revealOptionImage}
          source={require("../assets/report.png")}
        />
      )
    })
  },
  Chats: {
    screen: ChatStackNavigator,
    params: { image: require("../assets/chat.png"), name: localize.chatSupport },
    navigationOptions: ({ navigation }) => ({
      drawerLabel: () => (
        <Text style={styles.revealOptionText}>{localize.chatSupport}</Text>
      ),
      drawerIcon: () => (
        <Image
          style={styles.revealOptionImage}
          source={require("../assets/chat.png")}
        />
      )
    })
  },
  // Subscription: {
  //   screen: SubscriptionStackNavigator,
  //   navigationOptions: ({ navigation }) => ({
  //     drawerLabel: () => (
  //       <Text style={styles.revealOptionText}>My Subscription</Text>
  //     ),
  //     drawerIcon: () => (
  //       <Image
  //       style={styles.revealOptionImage}
  //         source={require("../assets/subscription.png")}
  //       />
  //     )
  //   })
  // },
  Settings: {
    screen: SettingsStackNavigator,
    params: { image: require("../assets/setting.png"), name: localize.settings },
    navigationOptions: ({ navigation }) => ({
      drawerLabel: () => (
        <Text style={styles.revealOptionText}>{localize.settings}</Text>
      ),
      drawerIcon: () => (
        <Image
          style={styles.revealOptionImage}
          source={require("../assets/setting.png")}
        />
      )
    })
  },
  TermsAndConditions: {
    screen: TermsAndConditionsStackNavigator,
    params: { image: require("../assets/termcond.png"), name: localize.termsConditions },
    navigationOptions: ({ navigation }) => ({
      drawerLabel: () => (
        <Text style={styles.revealOptionText}>{localize.termsConditions}</Text>
      ),
      drawerIcon: () => (
        <Image
          style={styles.revealOptionImage}
          source={require("../assets/termcond.png")}
        />
      )
    })
  },
  FAQ: {
    screen: FAQStackNavigator,
    params: { image: require("../assets/faq.png"), name: localize.faq },
    navigationOptions: ({ navigation }) => ({
      drawerLabel: () => (
        <Text style={styles.revealOptionText}>{localize.faq}</Text>
      ),
      drawerIcon: () => (
        <Image
          style={styles.revealOptionImage}
          source={require("../assets/faq.png")}
        />
      )
    })
  },
  Profile: {
    screen: ProfileStackNavigator,
    navigationOptions: ({ navigation }) => ({
      drawerLabel: () => (
        <Text style={{ width: 0, height: 0 }}></Text>
      ),
      drawerIcon: () => (
        <Image
          style={{ width: 0, height: 0, resizeMode: 'contain' }}
          source={require("../assets/faq.png")}
        />
      )
    })
  },
  Signout: {
    screen: SignoutStackNavigator,
    params: { image: require("../assets/sign-out.png"), name: 'Sign Out' },
    navigationOptions: ({ navigation }) => ({
      drawerLabel: () => (
        <Text style={[styles.revealOptionText, { color: APP_GREEN_COLOR }]}>{localize.signOut}</Text>
      ),
      drawerIcon: () => (
        <Image
          style={styles.revealOptionImage}
          source={require("../assets/sign-out.png")}
        />
      )
    })
  }
}, {
  contentComponent: DrawerComponent,
  drawerLockMode: 'locked-closed',
})

const AppSwitcher = createSwitchNavigator({
  LoginStack: LoginStackNavigator,
  navigator: DrawerNavigator,
},
)

const AppContainer = createAppContainer(AppSwitcher);
export default AppContainer;


export const styles = ScaleSheet.create({
  revealOptionImage: {
    height: 20,
    width: 30,
    resizeMode: 'contain',
  },
  revealOptionText: {
    //marginLeft: 8,
    fontWeight: '500',
    color: 'red',
    fontSize: 16,
    marginTop: 10,
    marginBottom: 10
  },
});