import { createBottomTabNavigator } from 'react-navigation-tabs';
import { HomeStack, DoctorsStack, AppointmentsStack, ChatsStack } from './TabStackConfig';
import React from 'react';
import { Image, View, Platform } from 'react-native';
import 'react-native-gesture-handler'
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_SHADOW_COLOR } from '../Constants';
import VideoCall from '../containers/twillio_video_call'

export const AppBottomTabNavigator = createBottomTabNavigator({
  Homes: {
    screen: HomeStack,

    navigationOptions: {
      tabBarLabel: <View />,
      tabBarIcon: ({ tintColor, focused }) => (<View style={[{ borderRadius: 20, width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }, (focused ? { backgroundColor: 'rgba(66, 202, 162, 0.2)' } : null)]}>
        <Image
          source={focused ? require('../assets/home-green.png') : require('../assets/home-grey.png')}
          style={styles.tabImage}
        />
      </View>)
    },
  },
  Doctors: {
    screen: DoctorsStack,

    navigationOptions: {
      tabBarLabel: <View />,
      tabBarIcon: ({ tintColor, focused }) => (<View style={[{ borderRadius: 20, width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }, (focused ? { backgroundColor: 'rgba(66, 202, 162, 0.2)' } : null)]}>
        <Image
          source={focused ? require('../assets/doc-green.png') : require('../assets/doc-grey.png')}
          style={styles.tabImage}
        />
      </View>)
    },
  },
  Appointments: {
    screen: AppointmentsStack,
    navigationOptions: {
      tabBarLabel: <View />,
      tabBarIcon: ({ tintColor, focused }) => (<View style={[{ borderRadius: 20, width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }, (focused ? { backgroundColor: 'rgba(66, 202, 162, 0.2)' } : null)]}>
        <Image
          source={focused ? require('../assets/appoint-green.png') : require('../assets/appoint-grey.png')}
          style={styles.tabImage}
        />
      </View>
      )
    },
  },
  Chatss: {
    screen: ChatsStack,
    navigationOptions: ({ navigation }) => {
      const currentRoute = navigation.state.routes[navigation.state.index];
      const { routeName } = currentRoute;
      // console.log("TAB BAR ROUTE NAMES >>>>>>>>",routeName)
      let tabBarVisible = true;
      if (routeName === 'Messages') {
        tabBarVisible = false;
      }else{
        tabBarVisible = true;
      }
      return{
        tabBarVisible:tabBarVisible,
        tabBarLabel: <View />,
         tabBarIcon: ({ tintColor, focused }) => (<View style={[{ borderRadius: 20, width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }, (focused ? { backgroundColor: 'rgba(66, 202, 162, 0.2)' } : null)]}>
         <Image
           source={focused ? require('../assets/chat-green.png') : require('../assets/chat-grey.png')}
           style={styles.chatTabImage}
         />
       </View>)
      }
    },
    // navigationOptions: {
    //   tabBarLabel: <View />,
    //   tabBarIcon: ({ tintColor, focused }) => (<View style={[{ borderRadius: 20, width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }, (focused ? { backgroundColor: 'rgba(66, 202, 162, 0.2)' } : null)]}>
    //     <Image
    //       source={focused ? require('../assets/chat-green.png') : require('../assets/chat-grey.png')}
    //       style={styles.chatTabImage}
    //     />
    //   </View>),
      
    // },
  }
}, {
  order: ['Homes', 'Doctors', 'Appointments', 'Chatss'],
  tabBarOptions: {
    style: {
      backgroundColor: 'white',
      height: 70,
      borderWidth: 0,
      borderTopColor: "transparent",
      ...Platform.select({
        ios: {
            shadowColor: APP_SHADOW_COLOR,
            shadowOffset: { height: 0, width: 7},
            shadowOpacity: 1.0,
            shadowRadius: 7,
        },
        android: {
           elevation: 20,
        },
    }),
    }
  },
})

export const styles = ScaleSheet.create({
  tabImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain'
  },
  chatTabImage: {
    height: 24,
    width: 24,
    resizeMode: 'contain'
  },
});