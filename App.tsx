/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 *
 * @format
 */

import React, { Component, useEffect } from 'react';
import AppContainer from './src/NavigationConfig/AppContainer'
import { Provider } from 'react-redux';
import store from './src/Redux/store';
import 'react-native-gesture-handler';
import messaging from '@react-native-firebase/messaging';
import LocalDataManager from './src/Utils/LocalDataManager';
import { fcmService } from './src/Firebase/FCMService';
import { localNotificationservice } from './src/Firebase/LocalNotificationService'
import { Alert } from 'react-native';
// import crashlytics from '@react-native-firebase/crashlytics';

// async function onSignIn() {
//   await Promise.all([
//     crashlytics().setUserId("54217443543431543494"),
//     crashlytics().setAttribute('credits', String("asdnkaushd")),
//     crashlytics().setAttributes({
//       role: 'admin',
//       followers: '13',
//       email: "satinderpal.aipl@gmail.com",
//       username: "Satinder"
//     }),
//   ]);
// }

export default function App() {
  
    // const onSignIn = async() => {
    //   crashlytics().log('User signed in.');
    //   await Promise.all([
    //     crashlytics().setUserId("54217443543431543494"),
    //     crashlytics().setAttribute('credits', String("asdnkaushd")),
    //     crashlytics().setAttributes({
    //       role: 'admin',
    //       followers: '13',
    //       email: "satinderpal.aipl@gmail.com",
    //       username: "Satinder"
    //     }),
    //   ]);
    // }

    
  useEffect(() => {
    // onSignIn()
     messaging().registerDeviceForRemoteMessages();

    const token =  messaging().getToken();

    console.log(token)
    LocalDataManager.saveDataAsyncStorage('fcmToken', JSON.stringify(token));


    // const onRegister = (token:any) => {
    //   console.log("[App] On Register: ", token)
    // }

    // const onNotification = (notify:any) => {
    //   console.log("[App] On Notifications: ", notify)
    //   const options = {
    //     soundName: 'default',
    //     playSound: true
    //   }
    //   localNotificationservice.showNotification(
    //     0,
    //     notify.notification.title,
    //     notify.notification.body,
    //     notify,
    //     options
    //   )
    // }
  
    // const onOpenNotification = (notify:any) => {
    //   console.log("[App] On Open Notifications: ", notify);
  
    //   // Alert("Open Notification: ", notify);
  
      
    // }

    // fcmService.registerAppWithFCM();
    // fcmService.register(onRegister, onNotification, onOpenNotification);
    // localNotificationservice.configure(onOpenNotification);

    // return () => {
    //   console.log(' [App] UnRegister')
    //   fcmService.unRegister();
    //   localNotificationservice.unregister()
    // }

  },[]);


  

  return (
    <Provider store={store}>
      <AppContainer />
      </Provider>
    );
}

